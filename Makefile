###########################################################################
#
# Copyright (C) 2016  Jørgen Fulsebakke
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.
#
# Author: Jørgen Fulsebakke
#
###########################################################################

#### Project main directory

DIR_MAIN:=$(shell pwd)

#### Compile all
.PHONY : all test Misc CF_states Create_superposition Analyse_EP Run_MC_sphere
all: Misc CF_states Create_superposition Analyse_EP Run_MC_sphere

test: all
	@$(MAKE) -C src/Test DIR_MAIN=$(DIR_MAIN)

#### Compile individual project:

Misc:
	@$(MAKE) -C src/Misc DIR_MAIN=$(DIR_MAIN)

CF_states: Misc
	@$(MAKE) -C src/CF_states DIR_MAIN=$(DIR_MAIN)

Create_superposition: Misc
	@$(MAKE) -C src/Create_superposition DIR_MAIN=$(DIR_MAIN)

Analyse_EP: Misc
	@$(MAKE) -C src/Analyse_EP DIR_MAIN=$(DIR_MAIN)

Analyse_MC: Misc
	@$(MAKE) -C src/Analyse_MC DIR_MAIN=$(DIR_MAIN)

Run_MC_sphere: Misc
	@$(MAKE) -C src/Run_MC_sphere DIR_MAIN=$(DIR_MAIN)

#### Clean all

clean:
	@$(MAKE) -C src/Misc clean DIR_MAIN=$(DIR_MAIN)
	@$(MAKE) -C src/CF_states clean DIR_MAIN=$(DIR_MAIN)
	@$(MAKE) -C src/Create_superposition clean DIR_MAIN=$(DIR_MAIN)
	@$(MAKE) -C src/Analyse_EP clean DIR_MAIN=$(DIR_MAIN)
	@$(MAKE) -C src/Analyse_MC clean DIR_MAIN=$(DIR_MAIN)
	@$(MAKE) -C src/Run_MC_sphere clean DIR_MAIN=$(DIR_MAIN)
	@$(MAKE) -C src/Test clean DIR_MAIN=$(DIR_MAIN)
