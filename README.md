# Introduction

This project contains programs performing Monte Carlo computations for the fractional quantum Hall effect on the sphere.

In-depth documentation is available [here](http://fqhe-mc-sphere.readthedocs.org).

Following are install instructions on Ubuntu. For other Linux distributions or Mac substitute `apt-get` for the appropriate package manager; on Windows the easiest option is to use a Linux-like environment, e.g. [Cygwin](https://www.cygwin.com/).

Some features require diagonalisation of Hamiltonians and evaluation of the resulting eigenstates in real space; programs for these and other tasks (including Monte Carlo computations for the FQHE on the torus) can be found at [Hammer](http://www.thphys.nuim.ie/hammer/), whose documentation can be found [here](http://hammerqh.readthedocs.org/), and [DiagHam](http://www.phys.ens.fr/~regnault/diagham/), with documentation [here](http://nick-ux.lpa.ens.fr/diagham/wiki/index.php/Main_Page).

Docker images (executable programs not requiring installation but not always completely up to date) for the programs of both Hammer (see above) and FQHE_MC_sphere (this page) can be found [here](https://hub.docker.com/r/nmoran/hammer/).

Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

------------------------------------------------------

## Prerequisites

git (version control system)

c++ compiler that supports c++11

gsl (c++ library)

hdf5 (Fileformat library)

### To install these on a Ubuntu machine

`sudo apt-get install git`

`sudo apt-get install g++`

`sudo apt-get install libgsl0-dev`

`sudo apt-get install libhdf5-dev`

## Included external libraries etc:

[SFMT](http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/) (Random number generator)

[Eigen](http://eigen.tuxfamily.org) (Matrix library)

HDF5Wrapper (by Niall Moran) 

------------------------------------------------------

# Setup

### Download project

`git clone https://jfulse@bitbucket.org/jfulse/fqhe_mc_sphere.git`

To download to a different folder "folderpath/":

`git clone https://jfulse@bitbucket.org/jfulse/fqhe_mc_sphere.git folderpath/`

### Compilation

Go to project folder (usually "fqhe_mc_sphere/" from where project was cloned, or "folderpath/" in example above)

`cd fqhe_mc_sphere`

Compile:

`make`

to use several cores use -j #. Example with 4 cores:

`make -j 4`

When compilation is finished the program executables can be found in the folder "Programs/."

### Compilation on ICHEC

_Note: ICHEC refers to the [Irish Centre for High-End Computing](https://www.ichec.ie/). The programs can be compiled in the same way as above but they will run faster if compiled using Intel compilers as explained below. Variations of this may facilitate compilation with other Intel compilers; this has not been tested._

Point to modules:

`export MODULEPATH=${MODULEPATH}:./modules/`

Load module:

`module load mc-sphere`

Compile:

`make cmp=ichec`

to use several cores use -j #. Example with 4 cores:

`make cmp=ichec -j 4`

When compilation is finished the program executables can be found in the folder "Programs/."

### Updating programs

To get the newest version of the programs, go into the project directory and enter

`git pull origin master`

and then recompile.

### Running programs from anywhere

To add the programs to the system path, use `PATH=$PATH:[path_to_programs]`; e.g.

`PATH=$PATH:~/fqhe_mc_sphere/Programs`

To make this permanent:

`echo "PATH=$PATH:~/fqhe_mc_sphere/Programs" >> ~/.bashrc`

-----------------------------------

# Featured programs

_Note: These programs run from a terminal (Windows: one option is using MSYS2, see the bottom of this document)._

Run with '-h' to see list of input options. Example:

`cd fqhe_mc_sphere/Programs`

`./Run_MC_sphere -h`

### Run_MC_sphere

Generate Monte Carlo configurations and wavefunction values, or compute wavefunction values on preexisting configurations.

### CF_states

Generate composite fermion states, for use with Run_MC_sphere.

### Analyse_MC

Analyse Monte Carlo configurations; energy, overlap etc.

### Analyse_EP

Analyse states projected using EP or JK-EP (from Analyse_MC).

### HDF5Utility

Various hdf5 file utilities, including joining and splitting files.

### Create_superposition

Create superpositions of states (i.e. wavefunction values produced using Run_MC_sphere) in terms of hdf5 files.