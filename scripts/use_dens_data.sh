conf_file=conf_partial.dat
n=240
n_Ne=$(cat $conf_file | wc -l)

function get_field {
 echo $(echo $1 | cut -d',' -f$2)
}

function get_Nphi {
  echo $(echo "3*($1-1)+1" | bc)
}

tail -$(echo "$n_Ne-1" | bc) $conf_file | while read line; do
  Ne=$(get_field $line 1)
  N=$(get_field $line 2)
  nb=$(get_field $line 3)
  Nphi=$(get_Nphi $Ne)
  tot_N=$(echo "$N*$n" | bc)
  tot_nb=$(echo "$nb*$n" | bc)
  name=$(get_field $line 6)
  datadir=N${Ne}/data
  resultdir=N${Ne}/results
  mkdir -p $resultdir
  echo Ne $Ne Nphi $Nphi N $tot_N nb $tot_nb
  Analyse_MC -obs dens-decomp-IP-orthog-sphere -Ne $Ne -nb $tot_nb -N $tot_N -K $Nphi -Nphi $Nphi -X $(for i in `seq 0 $(echo "$n-1" | bc)`; do echo ${datadir}/dens_decomp_${i}.hdf5" "; done) -out ${resultdir}/dens_decomp_IP_os_nu_1_3_${name}_K_${Nphi}.dat
  Analyse_MC -obs density-bins -Ne $Ne -nb $tot_nb -N $tot_N -Nphi $Nphi -X $(for i in `seq 0 $(echo "$n-1" | bc)`; do echo ${datadir}/dens_bins_${i}.hdf5" "; done) -out ${resultdir}/dens_bins_nu_1_3_${name}.dat
  echo Used data for Ne $Ne
 done
