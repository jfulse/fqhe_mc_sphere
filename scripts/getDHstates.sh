#!/bin/bash

Ne=10
Nphi=16
Lz=-16
targetL=72 # set to all to use all L states
n=150 # can set to all
name=LLLCoulomb
listname=StatesUsed.dat

#############
# User input
#############

while [[ $# > 0 ]]; do
  key="$1"

  case $key in

    -h|--help|--h)
      echo "input parameters: -Ne , -Nphi , -2Lz , -2L [all] , --states|-n [all], --interaction-name|-nam , --output-file|-out"; 
      exit; shift
    ;;
    -Ne)
      Ne=$2; shift
    ;;
    -Nphi)
      Nphi=$2; shift
    ;;
    -2Lz)
      twoLz=$2; shift
    ;;
    -2L)
      if [ $2 == "all" ]; then
        targetL=all;
      else
        targetL=$(echo "$2*($2+2)/4" | bc -l);
      fi
      shift
    ;;
    --states|-n)
      n=$2; shift
    ;;
    --interaction-name|-nam)
      name=$2; shift
    ;;
    --output-file|-out)
      listname=$2; shift
    ;;
    *)
    echo "WARNING: input '$key' unknown";;

  esac
  shift
done

if [ $n == "all" ]; then
  n=100000
fi
Lz=$(echo "$Lz/1" | bc)

echo ""
echo Parameters:
echo $Ne $Nphi $twoLz $targetL $n $name $listname

rm -f $listname
prefix=Sphere_p_${Ne}_2s_${Nphi}_lz_${twoLz}_${name}
DHprefix=fermions_n_${Ne}_2s_${Nphi}_${name}_lz_${twoLz}
rm -f *.info
Efile=${prefix}_Energies.csv
sed -i 's/\r//' $Efile

infile=$(cat $Efile | wc -l)
if [ $n -gt $infile ]; then
  n=$infile
fi
found=

for i in `seq 0 $(echo "$n - 1" | bc)`; do
  equal=0
  if [ $targetL == all ]; then
    equal=1
  else
    L=$(head -n $(echo "$i+1" | bc) $Efile | tail -1 | cut -d "," -f6)
    L=`echo $L | sed -e 's/[eE]+*/\\*10\\^/'`
    if [ $(echo "$L < 0" | bc -l) -eq "1"  ]; then
      L=$(echo "-1*$L" | bc -l)
    fi
    if [ $(echo "$L-$targetL<0.00001" | bc -l) -eq "1" ]; then
      if [ $(echo "$targetL-$L<0.00001" | bc -l) -eq "1" ]; then
        equal=1
      fi
    fi
  fi

  if [ $equal -eq "1" ]; then
    echo -n "State $i: "    
    if [ ! -f ${prefix}_State_${i}.hdf5 ]; then
      echo -n "converting to hdf5, "
      ConvertBinToHDF.py ${prefix}_State_${i}.vec -r
    fi

    if [ ! -f ${DHprefix}.${i}.vec ]; then
      echo -n "converting to DH vec and "
      ConvertHDF52Diagham --state ${prefix}_State_${i}.hdf5 --output-file ${DHprefix}.${i}.vec > /dev/null 2>&1
    fi
    echo "adding to list"
    echo $(pwd)/${DHprefix}.${i}.vec >> $listname

  fi

done

echo ""
echo "Finished"
echo ""
