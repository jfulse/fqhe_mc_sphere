#!/bin/bash

if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
  
  function ce {
    file=$1
    if [ "$3" == "" ]; then
      col=$2
      row=1
    else
      row=$2
      col=$3
    fi
    head $file -n $row  | tail -1 | cut -d"," -f$col
  }

else
  
  file=$1
  if [ "$3" == "" ]; then
    col=$2
    row=1
  else
    row=$2
    col=$3
  fi
  head $file -n $row  | tail -1 | cut -d"," -f$col

fi
