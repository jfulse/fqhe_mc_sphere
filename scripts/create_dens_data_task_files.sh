conf_file=conf_partial.dat
job_template=../../fqhe_mc_sphere/scripts/job_template.pbs
n=240
n_Ne=$(cat $conf_file | wc -l)

function get_field {
 echo $(echo $1 | cut -d',' -f${2})
}

function get_Nphi {
  echo $(echo "3*($1-1)+1" | bc)
}

tail -$(echo "$n_Ne-1" | bc) $conf_file | while read line; do
  Ne=$(get_field $line 1)
  N=$(get_field $line 2)
  nb=$(get_field $line 3)
  Nphi=$(get_Nphi $Ne)
  nodes=$(get_field $line 7)
  walltime=$(get_field $line 8)
  datadir=N${Ne}/data
  mkdir -p $datadir
  echo Ne $Ne Nphi $Nphi N $N nb $nb
  task_name=task_dens_data_Ne_${Ne}.dat
  echo -n "" > $task_name
  for i in `seq 0 $(echo "$n-1" | bc)`; do echo "Analyse_MC -obs dens-decomp-IP-orthog-sphere -Ne $Ne -nb $nb -N $N -wi N${Ne}/wf/${i}.hdf5 -pi N${Ne}/pos/${i}.hdf5 -K $Nphi -Nphi $Nphi -s ${datadir}/dens_decomp_${i}.hdf5 " >> $task_name; done
  for i in `seq 0 $(echo "$n-1" | bc)`; do echo "Analyse_MC -obs density-bins -Ne $Ne -nb $nb -N $N -wi N${Ne}/wf/${i}.hdf5 -pi N${Ne}/pos/${i}.hdf5 -Nphi $Nphi -s ${datadir}/dens_bins_${i}.hdf5" >> $task_name; done
  echo Created task file $task_name
  sed "s/NODES/${nodes}/g" $job_template > tmp
  sed "s/WALLTIME/${walltime}/g" tmp > tmp2
  sed "s/NAME/nu_1_3_Lqh_MC_Ne_${Ne}/g" tmp2 > tmp3
  sed "s/TASKFILE/${task_name}/g" tmp3 > tmp4
  dir=$(echo $(pwd))
  job_name=job_dens_data_Ne_${Ne}.pbs
  sed 's|DIR|'${dir}'|g' tmp4 > $job_name
  echo Created job file $job_name
  rm tmp tmp2 tmp3 tmp4
done
