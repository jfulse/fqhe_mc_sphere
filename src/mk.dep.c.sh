#!/bin/bash
## This script lists all the dependencies that different f.90 files have
## FIXME: same moudle can be lsited twise (un-neccesary) (not a problem for make though)

set -e
set -u

###C header files
for f in *.h ; do
    #file=$f
    file=$(echo $f | sed 's|\.h||')
    ## Search through .cpp for '#incliude "header"',
    ## extract the header,
    dep_h=$(grep '^\s*\#include \"' $f | awk ' {print $2}' | sed 's|"||g' | sed 's|\r$||g' )
    dep_f=$(grep _MOD_ $f | sed 's|^\(.*\)__\(.*\)_MOD_\(.*\)$|\2|g' | tr '[:upper:]' '[:lower:]' | sed 's|$|.o|g')
    ## sort the unique ones (tr ads a new line so that sort can do it's thing)
    dep_f=$(echo $dep_f | sed 's|\.h||g' | tr ' ' '\n' | sort -u)
    dep_h=$(echo $dep_h | sed 's|\.h||g' | tr ' ' '\n' | sort -u)
    dep_cpp=$(echo $dep_h | sed 's|\.h|\.o|g' )
    ##echo dep_cpp:$dep_cpp
    ### echo out
    echo $file: $dep_h $dep_cpp $dep_f
done

###C compile
for f in *.cpp ; do
    ## Replace .cpp with .o
    file=$(echo $f | sed 's|\.cpp|.o|')
    ## Search through .cpp for '#incliude "header"',
    ## extract the header,
    dep_h=$(grep '^\s*\#include \"' $f | awk ' {print $2}' | sed 's|"||g' | sed 's|\r$||g' )
    dep_f=$(grep _MOD_ $f | sed 's|^\(.*\)__\(.*\)_MOD_\(.*\)$|\2|g' | tr '[:upper:]' '[:lower:]' | sed 's|$|.o|g')
    ## sort the unique ones (tr ads a new line so that sort can do it's thing)
    dep_f=$(echo $dep_f | sed 's|\.h||g'| tr ' ' '\n' | sort -u)
    dep_h=$(echo $dep_h | sed 's|\.h||g'| tr ' ' '\n' | sort -u)
    dep_cpp=$(echo $dep_h | sed 's|\.h|\.o|g' | sed "s|$file||g"  )
    ##echo dep_cpp:$dep_cpp
    ### echo out
    echo $file: $dep_h $dep_cpp $dep_f
done


###C-compilefiles
