/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/

#include "Analyse_EP.h"

int main(int argc, const char* argv[]) {
	
	try {
		clock_t starttime=clock();
		input *params=new input("_","");
		
		params->add_input("-Ne", "Number of electrons", "int", "6");
		params->add_input("-Nphi", "Number of magnetic flux", "int", "15");
		params->add_input("--overlap-files,-of", "Files containing eigenstate overlaps of EP states (csv format)", "VS");
		params->add_input("--bin-files,-bf", "Files containing eigenstate overlap bins (hdf5 format)", "VS");
		params->add_input("--observable,-obs", "Observable to compute", "string","overlap");
		params->add_input("obs description", "overlap, energy, eig-overlap, GS-factor, rank, superposition,;diagonal-energies, harm-mean-overlaps, Gram-Schmidt, orthog-basis","description");
		params->add_input("--out-file,-out", "Name of file in which to write results", "string","EP_results.dat");
		params->add_input("--E-file", "Eigenvalue file to compute energy using LLL eigenstate overlaps", "string");
		params->add_input("--sub-E", "Subtract from energy eigenvalues", "double");
		params->add_input("--div-E-in,-dE", "Divide energies ('--E-file') by this value", "double", "2");
		params->add_input("--E-blocks", "Files containing energy block values", "VS");
		params->add_input("--DH-energies", "Energy eigenvalues ('--E-file') given in DiagHam format", "bool");
		params->add_input("--super-coeffs,-sp", "Coefficients for superposition", "VCD");
		params->add_input("--super-c-file,-spf", "File containing coefficients for superposition", "string");
		params->add_input("--bin-out,-bout", "Name of file in which to write hdf5 bins", "string");
		params->add_input("--GS-coeffs", "Names of files in which to write orthogonal coefficiets", "VS");
		params->add_input("--GS-bins", "Names of hdf5 files in which to write orthogonal bins", "VS");
		params->add_input("--orthog-prefix,-op", "Prefix for writing orthogonal eigenstate coefficient files", "string");
		params->add_input("--store-eig-states", "Write energy eigenstate coefficients to file", "bool");
		params->add_input("--skip-eig,-se", "Energy eigenstates to skip for harmonic mean", "int");
		params->add_input("--eig-state-no", "For which # energy eigenstates to find overlap", "int");
		params->add_input("--no-csv", "Use ' ' instead of ',' as separator in output files", "bool");
		params->add_input("--verbose,-v", "Print extra information to screen", "bool");
		if(params->command_line_input(argc, argv)!=0) return 1;
		if(!params->check_integrity()) return 1;
		
		int Ne=params->getint("-Ne"), Nphi=params->getint("-Nphi"), se=0, no_eig=0;
		VS coeffs=params->getVS("-of"), bins=params->getVS("-bf"), GS_c, GS_b, Eblockfiles=VS();
		VCD super_coeffs;
		double Esub=0, dE=params->getdouble("-dE");
		bool verb=params->is_activated("-v"), write_eig=params->is_activated("--store-eig-states"), DH_energy=false;
		string obs=params->getstring("-obs"), Efile, outfile, sep=",", binout="", orthog_prefix="", super_coeffs_file="";
		if(params->is_activated("--E-file")) Efile=params->getstring("--E-file");
		if(params->is_activated("--E-blocks")) Eblockfiles=params->getVS("--E-blocks");
		if(params->is_activated("-op")) orthog_prefix=params->getstring("-op");
		if(params->is_activated("-out")) outfile=params->getstring("-out");
		if(params->is_activated("-bout")) binout=params->getstring("-bout");
		if(params->is_activated("--no-csv")) sep=" ";
		if(params->is_activated("-sp")) super_coeffs=params->getVCD("-sp");
		if(params->is_activated("-spf")) super_coeffs_file=params->getstring("-spf");
		if(params->is_activated("-se")) se=params->getint("-se");
		if(params->is_activated("--eig-state-no")) no_eig=params->getint("--eig-state-no");
		if(params->is_activated("--sub-E")) Esub=params->getdouble("--sub-E");
		if(params->is_flagged("--DH-energies")) {params->is_activated("--DH-energies"); DH_energy=true;}
		if(params->is_activated("--GS-coeffs")) GS_c=params->getVS("--GS-coeffs");
		if(params->is_activated("--GS-bins")) GS_b=params->getVS("--GS-bins");
		
		cout.precision(3);
		if(verb) display_start(params, 60, "_", argv[0]);
		delete params;
		cout.precision(10);
		
		int nstates=coeffs.size();
		if(nstates!=bins.size()) error("Unequal number of coefficient and bin files");
		
		vector<EP_state*> states;
		get_states(states, coeffs, bins);
		nstates=states.size();
		EP_observable EP_obs(states, Efile, DH_energy, Esub, Eblockfiles);
		
		//showstates(states, EP_obs);
		
		ofstream out;
		if(outfile!="") {
			out.open(outfile.c_str());
			out.precision(17);
		}
		
		if(obs=="overlap") overlap(EP_obs, nstates, verb, out, sep);
		else if(obs=="GS-factor") GS_factor(EP_obs, nstates, verb, out, sep);
		else if(obs=="energy") energy(EP_obs, nstates, verb, out, sep, Ne, Nphi, dE);
		else if(obs=="eig-overlap") eigstate_overlap(states, nstates, verb, out, sep, no_eig);
		else if(obs=="rank") rank_calc(EP_obs, nstates, verb, out, sep);
		else if(obs=="orthog-basis") orthog_basis(EP_obs, nstates, verb, out, orthog_prefix, sep);
		else if(obs=="diagonal-energies") diag_energies(EP_obs, nstates, verb, out, sep, write_eig, Ne, Nphi, dE);
		else if(obs=="harm-mean-overlaps") harm_mean_overlaps(EP_obs, nstates, verb, out, sep, se);
		else if(obs=="superposition") superposition(EP_obs, super_coeffs, verb, outfile, binout, super_coeffs_file);
		else if(obs=="Gram-Schmidt") Gram_Schmidt(EP_obs, nstates, verb, GS_c, GS_b);
		else error("Observable "+obs+" unknown");
		
		if(out.is_open()) out.close();
		cout.precision(3);
		if(verb) display_end(starttime, 60, "_");
		
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		cerr << "Run with '-h' for options" << endl; 
		return 1;
	}
	catch(...) {
		cerr << "UNKNOWN ERROR" << endl; 
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
}

void showstates(vector<EP_state*> states, EP_observable obs) {
	int n=states.size();
	for(int i=0;i<n;i++) {
		cout << "State " << i+1 << ":" << endl;
		states[i]->display_coeffs();
		//states[i]->display_coeff_bins();
	}
	cout << "Energy eigenvalues: " << endl;
	obs.display_energy_eigenvalues();
	cout << endl;
}

void clear_states(vector<EP_state*>& states) {
	for(int i=0;i<states.size();i++) delete states[i];
}

void get_states(vector<EP_state*>& states, VS coeffs, VS bins) {
	int n=coeffs.size();
	
	clear_states(states);
	states.clear();
	for(int i=0;i<n;i++) states.push_back(new EP_state(coeffs[i], bins[i]));
}

void overlap(EP_observable obs, int nstates, bool verb, ofstream& out, string sep) {
	
	if(nstates<2) error("Need at least two coeff files to compute overlap");
	vector<CD> ov;
	VD abs_ov, sqr_ov;
	obs.all_overlaps(ov, abs_ov, sqr_ov);
	cout.precision(10);
	
	if(out.is_open()) {
		out << "#NbrBins" << sep << "Re(overlap)" << sep << "Im(overlap)" << sep << "ErrReOverlap" << sep << "ErrImOverlap" << sep;
		out << "abs(overlap)" << sep << "ErrAbsOverlap" << sep << "AbsSqr(overlap)" << sep << "ErrAbsSqrOverlap" << endl;
	}
	
	int pair=0;
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			if(verb) {
				cout << "<" << i << "|" << j << "> = " << ov[2*pair] << " +/- " << ov[2*pair+1] << endl;
				cout << "|<" << i << "|" << j << ">| = " << abs_ov[2*pair] << " +/- " << abs_ov[2*pair+1] << endl;
				cout << "|<" << i << "|" << j << ">|^2 = " << sqr_ov[2*pair] << " +/- " << sqr_ov[2*pair+1] << endl;
			}
			if(out.is_open()) {
				out << obs.get_nb() << sep << real(ov[2*pair]) << sep << imag(ov[2*pair]) << sep << real(ov[2*pair+1]) << sep << imag(ov[2*pair+1]) << sep;
				out << abs_ov[2*pair] << sep << abs_ov[2*pair+1] << sep << sqr_ov[2*pair] << sep << sqr_ov[2*pair+1] << endl;
			}
			pair++;
		}
	}
}

void eigstate_overlap(vector<EP_state*> states, int nstates, bool verb, ofstream& out, string sep, int no_eig) {
	
	cout.precision(10);
	
	if(out.is_open()) {
		out << "#NbrBins" << sep << "Re(overlap)" << sep << "Im(overlap)" << sep << "ErrReOverlap" << sep << "ErrImOverlap" << sep;
		out << "abs(overlap)" << sep << "ErrAbsOverlap" << sep << "AbsSqr(overlap)" << sep << "ErrAbsSqrOverlap" << endl;
	}
	
	for(int i=0;i<nstates;i++) {
		VD ov=states[i]->get_single_overlap(no_eig);
		if(verb) {
			cout << "<e_" << no_eig << "|" << i << "> = " << CD(ov[0], ov[1]) << " +/- " << CD(ov[2], ov[3]) << endl;
			cout << "|<e_" << no_eig << "|" << i << ">| = " << ov[4] << " +/- " << ov[5] << endl;
			cout << "|<e_" << no_eig << "|" << i << ">|^2 = " << ov[6] << " +/- " << ov[7] << endl;
		}
		if(out.is_open()) {
			out << states[i]->get_nb() << sep << ov[0] << sep << ov[1] << sep << ov[2] << sep << ov[3] << sep;
			out << ov[4] << sep << ov[5] << sep << ov[6] << sep << ov[7] << endl;
		}
	}
}

void GS_factor(EP_observable obs, int nstates, bool verb, ofstream& out, string sep) {
	
	if(nstates<2) error("Need at least two coeff files to compute GS factor");
	vector<CD> GS_fac;
	obs.all_GS_factors(GS_fac);
	cout.precision(10);
	
	if(out.is_open()) {
		out << "#NbrBins = " << obs.get_nb() << "; NbrStates = " << obs.get_ne() << endl;
		out << "#Re(GS_factor)" << sep << "Im(GS_factor)" << sep << "ErrReGS_factor" << sep << "ErrImGS_factor" << endl;
	}
	
	int pair=0;
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			if(verb) {
				cout << "<" << i << "|" << j << ">*<" << j << "|" << j << "> = " << GS_fac[2*pair] << " +/- " << GS_fac[2*pair+1] << endl;
			}
			if(out.is_open()) {
				out << real(GS_fac[2*pair]) << sep << imag(GS_fac[2*pair]) << sep << real(GS_fac[2*pair+1]) << sep << imag(GS_fac[2*pair+1]) << endl;
			}
			pair++;
		}
	}
}

void energy(EP_observable obs, int nstates, bool verb, ofstream& out, string sep, int Ne, int Nphi, double dE) {
	
	VD E;
	obs.all_energies(E);
	cout.precision(10);
	double BG=-Ne/(2.0*sqrt(Nphi/2.0));
	
	if(out.is_open()) {
		out << "#Stateidx" << sep << "NbrBins" << sep << "Energy" << sep << "ErrEnergy" << sep << "TotBareEnergy" << sep << "ErrTotBareEnergy" << endl;
	}
	
	for(int i=0;i<nstates;i++) {
		if(verb) {
			cout << "Energy " << i << " = " << E[2*i]/(dE*Ne)+BG << " +/- " << E[2*i+1]/(dE*Ne) << "; Bare total E = " << E[2*i]/dE << sep;
			cout << " +/- " << E[2*i+1]/dE << endl;
		}
		if(out.is_open()) out << i << sep << obs.get_nb() << sep << E[2*i]/(dE*Ne)+BG << sep << E[2*i+1]/(dE*Ne) << sep << E[2*i]/dE << sep << E[2*i+1]/dE << endl;
	}
	
}

void rank_calc(EP_observable obs, int nstates, bool verb, ofstream& out, string sep) {
	
	if(nstates<2) error("Need at least two coeff files to compute rank");
	VD det;
	vector<VD> sing_vals;
	obs.rank(det, sing_vals);
	cout.precision(10);
	
	if(out.is_open()) {
		out << "#NbrBins = " << obs.get_nb() << "; NbrStates = " << obs.get_ne() << "; determinant = " << det[0] << ", error = " << det[1] << endl;
		out << "#SingularValue ErrSingularValue" << endl;
	}
	
	if(verb) cout << "Determinant = " << det[0] << " +/- " << det[1] << endl;
	for(int i=0;i<nstates;i++) {
		if(verb) cout << "Singular value " << i << " = " << sing_vals[0][i] << " +/- " << sing_vals[1][i] << endl;
		if(out.is_open()) out << sing_vals[0][i] << sep << sing_vals[1][i] << endl;
	}	
}

void diag_energies(EP_observable obs, int nstates, bool verb, ofstream& out, string sep, bool write_eigvals, int Ne, int Nphi, double dE) {
	
	vector<VD> energies;
	obs.diag_energies(energies, write_eigvals);
	if(verb && write_eigvals) cout << endl << "Eigenstates written to 'Eigenstate_#.dat'" << endl;
	cout.precision(10);
	double BG=-Ne/(2.0*sqrt(Nphi/2.0));
	
	if(out.is_open()) {
		out << "#Stateidx" << sep << "NbrBins" << sep << "Energy" << sep << "ErrEnergy" << sep << "TotBareEnergy" << sep << "ErrTotBareEnergy" << endl;
	}
	
	for(int i=0;i<nstates;i++) {
		if(verb) {
			cout << "Energy " << i << " = " << energies[0][i]/(dE*Ne)+BG << " +/- " << energies[1][i]/(dE*Ne) << "; Bare total E = " << energies[0][i]/dE;
			cout << " +/- " << energies[1][i]/dE << endl;
		}
		if(out.is_open()) {
			out << i << sep << obs.get_nb() << sep << energies[0][i]/(dE*Ne)+BG << sep << energies[1][i]/(dE*Ne);
			out << sep << energies[0][i]/dE << sep << energies[1][i]/dE << endl;
		}
	}	
}

void harm_mean_overlaps(EP_observable obs, int nstates, bool verb, ofstream& out, string sep, int skip_e) {
	
	vector<CD> mean;
	VD absmean, sqmean;
	obs.harmonic_mean(mean, absmean, sqmean, skip_e);
	cout.precision(10);
	
	if(out.is_open()) {
		out << "#NbrBins" << sep << "Re(overlap)" << sep << "Im(overlap)" << sep << "ErrReOverlap" << sep << "ErrImOverlap" << sep;
		out << "abs(overlap)" << sep << "ErrAbsOverlap" << sep << "AbsSqr(overlap)" << sep << "ErrAbsSqrOverlap" << endl;
	}
	
	if(verb) {
		cout << "Harmonic mean overlap " << " = " << mean[0] << " +/- " << mean[1] << endl;
		cout << "abs = " << absmean[0] << " +/- " << absmean[1] << " , abs^2  = " << sqmean[0] << " +/- " << sqmean[1] << endl;
	}
	if(out.is_open()) {
		out << obs.get_nb() << sep << real(mean[0]) << sep << imag(mean[0]) << sep << real(mean[1]) << sep << imag(mean[1]) << sep;
		out << absmean[0] << sep << absmean[1] << sep << sqmean[0] << sep << sqmean[1] << endl;
	}
}

void superposition(EP_observable obs, VCD super_coeffs, bool verb, string out, string binout, string super_coeffs_file) {
	
	VCD super_coeffs_used;
	if(super_coeffs.size()>0 && super_coeffs_file!="") warning("Both superposition coefficients and file containing such supplied; the latter is ignored");
	if(super_coeffs.size()>0) super_coeffs_used=super_coeffs;
	else {
		if(super_coeffs_file=="") error("Neither superposition coefficients nor a file containing such supplied");
		ifstream super_in(super_coeffs_file.c_str());
		if(!super_in) error("Could not open "+super_coeffs_file+" for reading");
		super_in.precision(17);
		string line;
		while(getline(super_in, line)) {
			double re, im;
			istringstream iss(line);
			vector<string> coeffs;
			copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(coeffs));
			if(coeffs.size()<1) error("Line in "+super_coeffs_file+" contains no numbers.");
			re=stod(coeffs[0]);
			if(coeffs.size()<2) {
				warning("Line in "+super_coeffs_file+" contains only one number; assuming real coefficient");
				im=0.0;
			}
			else {
				if(coeffs.size()>2) warning("Line in "+super_coeffs_file+" contains more than two numbers; the rest is ignored");
				im=stod(coeffs[1]);
			}
			super_coeffs_used.push_back(CD(re, im));
		}
		super_in.close();
	}
	
	EP_state* state=obs.superposition(super_coeffs_used);
	//state->display_coeffs();
	//state->display_coeff_bins();
	state->write_state(out, binout, verb);
	delete state;
}

void Gram_Schmidt(EP_observable obs, int nstates, bool verb, VS GS_c, VS GS_b) {
	if(GS_c.size()!=nstates) error("Have "+to_string(nstates)+" state(s) and "+to_string(GS_c.size())+" orthogonal coefficient output file(s)");
	if(GS_b.size()!=nstates) error("Have "+to_string(nstates)+" state(s) and "+to_string(GS_b.size())+" orthogonal bin output file(s)");
	vector<EP_state*> orthog_states;
	obs.Gram_Schmidt(orthog_states);
	for(int i=0;i<nstates;i++) {
		orthog_states[i]->write_state(GS_c[i], GS_b[i], verb);
		delete orthog_states[i];
	}
}

void orthog_basis(EP_observable obs, int nstates, bool verb, ofstream& out, string orthog_prefix, string sep) {
	
	if(nstates<2) error("Need at least two coeff files to compute orthogonal basis");
	VD det;
	int rank;
	vector<VCD> eigenvectors, eigenvector_errors;
	VD eigvals, eigval_errors;
	obs.orthog_basis(rank, eigvals, eigenvectors, eigval_errors, eigenvector_errors, verb);
	cout.precision(10);
	
	cout << "Rank = " << rank << endl;
	if(verb) {
		cout << endl << "Eigenvalues:" << endl;
		for(int i=0;i<eigvals.size();i++) cout << eigvals[i] << " +/- " << eigval_errors[i] << endl;
		cout << endl << "Orthogonal eigenvectors:" << endl;
		for(int i=0;i<eigenvectors.size();i++) {
			cout << "state " << i+1 << endl;
			for(int j=0;j<eigenvectors[i].size();j++) cout << eigenvectors[i][j] << " +/- " << eigenvector_errors[i][j] << endl;
			cout << endl;
		}
	}
	
	if(out.is_open()) {
		
		out << "Rank:" << endl << rank << endl;
		out << endl << "Eigenvalues:" << endl;
		for(int i=0;i<eigvals.size();i++) out << eigvals[i] << " +/- " << eigval_errors[i] << endl;
		
		out << endl << "Orthogonal eigenstates:" << endl;
		out << "Idx" << sep;
		for(int i=0;i<eigenvectors[0].size()-1;i++) {
			out << "Re(c_" << i+1 << ")" << sep << "Im(c_" << i+1 << ")" << sep;
			out << "Re(c_" << i+1 << ")_error" << sep << "Im(c_" << i+1 << ")_error" << sep;
		}
		out << "Re(c_" << eigenvectors[0].size() << ")" << sep << "Im(c_" << eigenvectors[0].size() << ")" << sep;
		out << "Re(c_" << eigenvectors[0].size() << ")_error" << sep << "Im(c_" << eigenvectors[0].size() << ")_error" << endl;
		
		for(int i=0;i<eigenvectors.size();i++) {
			out << i+1 << sep;
			for(int j=0;j<eigenvectors[i].size()-1;j++) {
				out << real(eigenvectors[i][j]) << sep << imag(eigenvectors[i][j]) << sep;
				out << real(eigenvector_errors[i][j]) << sep << imag(eigenvector_errors[i][j]) << sep;
			}
			out << real(eigenvectors[i][eigenvectors[i].size()-1]) << sep << imag(eigenvectors[i][eigenvectors[i].size()-1]) << sep;
			out << real(eigenvector_errors[i][eigenvectors[i].size()-1]) << sep << imag(eigenvector_errors[i][eigenvectors[i].size()-1]) << endl;
		}
	}
	
	if(orthog_prefix!="") {
		if(verb) cout << endl << "Orthogonal eigenstate coefficients written to:" << endl;
		for(int i=0;i<eigenvectors.size();i++) {
			string filename=orthog_prefix+"_"+to_string(i+1)+".dat";
			ofstream orthogout(filename.c_str());
			orthogout.precision(17);
			if(!orthogout) error("Could not open "+filename+" for writing");
			for(int j=0;j<eigenvectors[i].size();j++) orthogout << real(eigenvectors[i][j]) << " " << imag(eigenvectors[i][j]) << endl;
			orthogout.close();
			if(verb) cout << filename << endl;
		}
		if(verb) cout << endl;
	}
}
