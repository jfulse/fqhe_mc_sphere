/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef ANALYSE_EP_H
#define ANALYSE_EP_H

#include "EP_state.h"
#include "main_misc.h"
#include "EP_observable.h"
#include <time.h>

void showstates(vector<EP_state*>, EP_observable);
void overlap(EP_observable, int, bool, ofstream&, string);
void eigstate_overlap(vector<EP_state*>, int, bool, ofstream&, string, int);
void energy(EP_observable, int, bool, ofstream&, string, int, int, double);
void clear_states(vector<EP_state*>&);
void get_states(vector<EP_state*>&, VS, VS);
void rank_calc(EP_observable, int, bool, ofstream&, string);
void superposition(EP_observable, VCD, bool, string, string, string);
void GS_factor(EP_observable, int, bool, ofstream&, string);
void diag_energies(EP_observable, int, bool, ofstream&, string, bool, int, int, double);
void harm_mean_overlaps(EP_observable, int, bool, ofstream&, string, int);
void Gram_Schmidt(EP_observable, int, bool, VS, VS);
void orthog_basis(EP_observable, int, bool, ofstream&, string, string);

#endif
