/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/

#include "EP_observable.h"

EP_observable::EP_observable(vector<EP_state*> states_, string Efile, bool DH_vals, double Esub, VS Eblockfiles, double threshold_) : 
							states(states_), threshold(threshold_) {
	nstates=states.size();
	npairs=(int)(nstates*(nstates-1.0)/2.0);
	if(nstates<1) error("Need at least one EP state");
	ne=states[0]->get_ne();
	nb=states[0]->get_nb();
	for(int i=1;i<nstates;i++) {
		if(ne!=states[i]->get_ne()) error("Different number of eigenstates in EP states");
		if(nb!=states[i]->get_nb()) error("Different number of bins in EP states");
	}
	
	if(Efile!="") {
		E_eigvals.resize(ne);
		ifstream Ein(Efile.c_str()); 
		Ein.precision(17);
		if(!Ein) error("Cannot find energy eigenvalue file "+Efile);
		int foundvals=0;
		double eigenval;
		string eigenstring;
		if(DH_vals) {
			getline(Ein,eigenstring);
			while(Ein >> eigenstring) {
				Ein >> eigenstring;
				eigenval=stod(eigenstring)-Esub;
				getline(Ein,eigenstring);
				if(foundvals<ne) E_eigvals[foundvals]=eigenval;
				foundvals++;
			}
		}
		else {
			while(getline(Ein,eigenstring,',')) {
				eigenval=stod(eigenstring)-Esub;
				getline(Ein,eigenstring);
				if(foundvals<ne) E_eigvals[foundvals]=eigenval;
				foundvals++;
			}
		}
		if(foundvals>ne) warning("Found "+to_string(foundvals)+" energy eigenvalues but only using "+to_string(ne)+" eigenstates");
		if(foundvals<ne) error("Only found "+to_string(foundvals)+" energy eigenvalues but have "+to_string(ne)+" eigenstates");
		Ein.close();
		has_eigvals=true;
	}
	else has_eigvals=false;
	
	if(Eblockfiles.size()>0) {
		if(!has_eigvals) error("Provided energy blocks but not energy values");
		if(Eblockfiles.size()!=ne) error("Number of energy block files ("+to_string(Eblockfiles.size())+") different than number of states ("+to_string(ne)+")");
		has_Eblocks=true;
		
		for(int i=0;i<ne;i++) {
			Eblocks.push_back(VD(nb));
			ifstream in(Eblockfiles[i].c_str());
			in.precision(17);
			double E;
			int ctr=0;
			while(in >> E) {
				Eblocks[i][ctr]=E;
				ctr++;
				if(ctr>nb) {
					warning("Energy block file "+Eblockfiles[i]+" has more than "+to_string(nb)+" blocks");
					break;
				}
			}
			if(ctr<nb) error("Energy block file "+Eblockfiles[i]+"has "+to_string(ctr)+" blocks; expected "+to_string(nb));
			in.close();
		}
	}
	else has_Eblocks=false;
		
}
	
void EP_observable::all_overlaps(vector<CD>& ovs, VD& abs_ovs, VD& sqr_ovs) {warning("Using abs / norm without pairing");
	ovs.clear();
	sqr_ovs.clear();
	abs_ovs.clear();
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			ovs.push_back(overlap(i, j));
			ovs.push_back(overlap_error(i, j));
			abs_ovs.push_back(abs(overlap(i, j)));
			abs_ovs.push_back(abs_overlap_error(i, j));
			sqr_ovs.push_back(norm(overlap(i, j)));
			sqr_ovs.push_back(sqr_overlap_error(i, j));
		}
	}
}

void EP_observable::all_GS_factors(vector<CD>& GSfacs) {
	GSfacs.clear();
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			GSfacs.push_back(GS_factor(i, j));
			GSfacs.push_back(GS_factor_error(i, j));
		}
	}
}

void EP_observable::all_energies(VD& Es) {
	if(!has_eigvals) error("Energy eigenvalues not supplied");
	Es.clear();
	for(int i=0;i<nstates;i++) {
		Es.push_back(energy(i));
		Es.push_back(energy_error(i));
	}
}

void EP_observable::rank(VD& det, vector<VD>& sing_vals) {
	det.clear();
	sing_vals.clear();
	double detval;
	VD sing_vals_val;
	rank(detval, sing_vals_val);
	det.push_back(detval);
	sing_vals.push_back(sing_vals_val);
	rank_error(detval, sing_vals_val);
	det.push_back(detval);
	sing_vals.push_back(sing_vals_val);
}

void EP_observable::orthog_basis(int& rank, VD& eigvalues, vector<VCD>& eigenvectors, VD& eigvalue_errors, vector<VCD>& eigenvector_errors, bool verb) {
	
	vector<CD> overlaps;
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			overlaps.push_back(overlap(i, j));
		}
	}
	
	Eigen::MatrixXcd G_matrix=overlap_matrix(overlaps);
	VI basis_states;
	get_eigenvectors(G_matrix, eigvalues, eigenvectors, rank, basis_states);
	get_eigenvector_errors(eigvalue_errors, eigenvector_errors, rank, basis_states);
	
}

void EP_observable::diag_energies(vector<VD>& energies, bool write_eigenstates) {
	if(!has_eigvals) error("Energy eigenvalues not supplied");
	energies.clear();
	VD E, E_err;
	diag_energies(E, write_eigenstates);
	energies.push_back(E);
	diag_energies_error(E_err);
	energies.push_back(E_err);
}

void EP_observable::harmonic_mean(vector<CD>& mean, VD& absmean, VD& sqmean, int skip) {
	if(skip+nstates>ne) error("Have "+to_string(ne)+" energy eigenstates; cannot skip "+to_string(skip)+" and use "+to_string(nstates));
	mean.clear();
	absmean.clear();
	sqmean.clear();
	CD comp_mean;
	harm_mean(comp_mean, skip);
	mean.push_back(comp_mean);
	absmean.push_back(abs(comp_mean));
	sqmean.push_back(norm(comp_mean));
	double comp_abs, comp_sq;
	harm_mean_error(comp_mean, comp_abs, comp_sq, skip);
	mean.push_back(comp_mean);
	absmean.push_back(comp_abs);
	sqmean.push_back(comp_sq);	
}

void EP_observable::Gram_Schmidt(vector<EP_state*>& orthog_states) {
	orthog_states.resize(nstates);
	vector<vector<CD> > orthog_coeffs, orthog_coeffs_1sthalf, orthog_coeffs_2ndhalf;
	get_orthog_coeffs(orthog_coeffs);
	get_orthog_coeffs_1sthalf(orthog_coeffs_1sthalf);
	get_orthog_coeffs_2ndhalf(orthog_coeffs_2ndhalf);
	vector<vector<vector<CD> > > orthog_coeff_bins, orthog_coeff_bins_1sthalf, orthog_coeff_bins_2ndhalf;
	get_orthog_coeff_bins(orthog_coeff_bins);
	get_orthog_coeff_bins_1sthalf(orthog_coeff_bins_1sthalf);
	get_orthog_coeff_bins_2ndhalf(orthog_coeff_bins_2ndhalf);
	for(int i=0;i<nstates;i++) {
		vector<vector<CD> > bins(nb, vector<CD>(ne));
		vector<vector<CD> > bins_1sthalf(nb, vector<CD>(ne));
		vector<vector<CD> > bins_2ndhalf(nb, vector<CD>(ne));
		for(int b=0;b<nb;b++) {
      bins[b]=orthog_coeff_bins[b][i];
      bins_1sthalf[b]=orthog_coeff_bins_1sthalf[b][i];
      bins_2ndhalf[b]=orthog_coeff_bins_2ndhalf[b][i];
    }
		orthog_states[i]=new EP_state(orthog_coeffs[i], bins, orthog_coeffs_1sthalf[i], orthog_coeffs_2ndhalf[i], bins_1sthalf, bins_2ndhalf);
	}
  
	/*cout << "Test bin overlaps:" << endl;
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			for(int b=0;b<nb;b++) {
				cout << "<" << i << "|" << j << ">_" << b << " = " << overlap(orthog_states[i]->get_normed_bin(b), orthog_states[j]->get_normed_bin(b)) << endl;
			}
		}
	}*/
}

void EP_observable::get_orthog_coeffs(vector<vector<CD> >& orthog_coeffs) {
	orthog_coeffs.resize(nstates, vector<CD>(ne));
	for(int i=0;i<nstates;i++) next_orthog_coeff(i, orthog_coeffs, states[i]->get_coeffs());
}

void EP_observable::get_orthog_coeffs_1sthalf(vector<vector<CD> >& orthog_coeffs) {
	orthog_coeffs.resize(nstates, vector<CD>(ne));
	for(int i=0;i<nstates;i++) next_orthog_coeff(i, orthog_coeffs, states[i]->get_coeffs_1sthalf());
}

void EP_observable::get_orthog_coeffs_2ndhalf(vector<vector<CD> >& orthog_coeffs) {
	orthog_coeffs.resize(nstates, vector<CD>(ne));
	for(int i=0;i<nstates;i++) next_orthog_coeff(i, orthog_coeffs, states[i]->get_coeffs_2ndhalf());
}

void EP_observable::get_orthog_coeff_bins(vector<vector<vector<CD> > >& orthog_coeff_bins) {
	orthog_coeff_bins.resize(nb, vector<vector<CD> >(nstates, vector<CD>(ne)));
	for(int b=0;b<nb;b++) {
		for(int i=0;i<nstates;i++) next_orthog_coeff(i, orthog_coeff_bins[b], states[i]->get_bin(b));
	}
}

void EP_observable::get_orthog_coeff_bins_1sthalf(vector<vector<vector<CD> > >& orthog_coeff_bins) {
	orthog_coeff_bins.resize(nb, vector<vector<CD> >(nstates, vector<CD>(ne)));
	for(int b=0;b<nb;b++) {
		for(int i=0;i<nstates;i++) next_orthog_coeff(i, orthog_coeff_bins[b], states[i]->get_bin(b));
	}
}

void EP_observable::get_orthog_coeff_bins_2ndhalf(vector<vector<vector<CD> > >& orthog_coeff_bins) {
	orthog_coeff_bins.resize(nb, vector<vector<CD> >(nstates, vector<CD>(ne)));
	for(int b=0;b<nb;b++) {
		for(int i=0;i<nstates;i++) next_orthog_coeff(i, orthog_coeff_bins[b], states[i]->get_bin(b));
	}
}

void EP_observable::next_orthog_coeff(int st, vector<vector<CD> >& orthog_coeffs, vector<CD> state_coeffs) {
	if(st==nstates) {
		warning("Have already orthogonalised all states");
		return;
	}
	orthog_coeffs[st]=state_coeffs;
	for(int i=0;i<st;i++) {
		CD factor=GS_factor(orthog_coeffs[i], state_coeffs);
		for(int j=0;j<ne;j++) orthog_coeffs[st][j]-=factor*orthog_coeffs[i][j];
	}
}

EP_state* EP_observable::superposition(vector<CD> coeffs) {
	
	if(coeffs.size()<nstates) warning("Coefficient vector of size"+to_string(coeffs.size())+" with "+to_string(nstates)+" states; setting remaining coefficients to zero");
	if(coeffs.size()>nstates) warning("Coefficient vector of size"+to_string(coeffs.size())+" with "+to_string(nstates)+" states; ignoring remaining coefficients");
	nstates=coeffs.size();
	
	vector<CD> c(ne, 0.0);
	vector<CD> c_1sthalf(ne, 0.0);
	vector<CD> c_2ndhalf(ne, 0.0);
	vector<vector<CD> > cbins(nb, vector<CD>(ne, 0.0));
	vector<vector<CD> > cbins_1sthalf(nb, vector<CD>(ne, 0.0));
	vector<vector<CD> > cbins_2ndhalf(nb, vector<CD>(ne, 0.0));
	for(int i=0;i<nstates;i++) {
		vector<CD> addc=states[i]->get_coeffs();
		vector<CD> addc_1sthalf=states[i]->get_coeffs_1sthalf();
		vector<CD> addc_2ndhalf=states[i]->get_coeffs_2ndhalf();
		vector<vector<CD> > addbinc=states[i]->get_bins();
		vector<vector<CD> > addbinc_1sthalf=states[i]->get_bins_1sthalf();
		vector<vector<CD> > addbinc_2ndhalf=states[i]->get_bins_2ndhalf();
		for(int j=0;j<ne;j++) {
			c[j]+=coeffs[i]*addc[j];
			c_1sthalf[j]+=coeffs[i]*addc_1sthalf[j];
			c_2ndhalf[j]+=coeffs[i]*addc_2ndhalf[j];
			for(int k=0;k<nb;k++) {
				cbins[k][j]+=coeffs[i]*addbinc[k][j];
				cbins_1sthalf[k][j]+=coeffs[i]*addbinc_1sthalf[k][j];
				cbins_2ndhalf[k][j]+=coeffs[i]*addbinc_2ndhalf[k][j];
			}
		}
	}
	
	return new EP_state(c, cbins, c_1sthalf, c_2ndhalf, cbins_1sthalf, cbins_2ndhalf);
}

CD EP_observable::overlap(int i, int j) {
	vector<CD> c1=states[i]->get_normed_coeffs();
	vector<CD> c2=states[j]->get_normed_coeffs();
	return overlap(c1, c2);
}

CD EP_observable::GS_factor(int i, int j) {
	vector<CD> c1=states[i]->get_coeffs();
	double norm1=states[i]->get_norm();
	vector<CD> c2=states[j]->get_coeffs();
	return overlap(c1, c2)/(norm1*norm1);
}

CD EP_observable::GS_factor(vector<CD> c1, vector<CD> c2) {
	double norm1=0;
	for(int i=0;i<ne;i++) norm1+=norm(c1[i]);
	return overlap(c1, c2)/norm1;
}

double EP_observable::energy(int i) {
	VD c=states[i]->get_normed_coeff_norms();
	return energy(c);
}

CD EP_observable::overlap_error(int i, int j) {
	vector<vector<CD> > cbins1=states[i]->get_normed_bins();
	vector<vector<CD> > cbins2=states[j]->get_normed_bins();
	double re=0, re2=0, im=0, im2=0;
	for(int k=0;k<nb;k++) {
		CD ov=overlap(cbins1[k], cbins2[k]);
		re+=real(ov);
		re2+=real(ov)*real(ov);
		im+=imag(ov);
		im2+=imag(ov)*imag(ov);
	}
	return CD(std(re2, re, nb), std(im2, im, nb));
}

CD EP_observable::GS_factor_error(int i, int j) {
	vector<vector<CD> > cbins1=states[i]->get_bins();
	vector<vector<CD> > cbins2=states[j]->get_bins();
	double re=0, re2=0, im=0, im2=0;
	for(int k=0;k<nb;k++) {
		double norm1=states[i]->get_bin_norm(k);
		CD ov=overlap(cbins1[k], cbins2[k])/(norm1*norm1);
		re+=real(ov);
		re2+=real(ov)*real(ov);
		im+=imag(ov);
		im2+=imag(ov)*imag(ov);
	}
	return CD(std(re2, re, nb), std(im2, im, nb));
}

double EP_observable::abs_overlap_error(int i, int j) {
	vector<vector<CD> > cbins1=states[i]->get_normed_bins();
	vector<vector<CD> > cbins2=states[j]->get_normed_bins();
	double ab=0, ab2=0;
	for(int k=0;k<nb;k++) {
		CD ov=overlap(cbins1[k], cbins2[k]);
		ab+=abs(ov);
		ab2+=abs(ov)*abs(ov);
	}
	return std(ab2, ab, nb);
}

double EP_observable::sqr_overlap_error(int i, int j) {
	vector<vector<CD> > cbins1=states[i]->get_normed_bins();
	vector<vector<CD> > cbins2=states[j]->get_normed_bins();
	double sqr=0, sqr2=0;
	for(int k=0;k<nb;k++) {
		CD ov=overlap(cbins1[k], cbins2[k]);
		sqr+=norm(ov);
		sqr2+=norm(ov)*norm(ov);
	}
	return std(sqr2, sqr, nb);
}

double EP_observable::energy_error(int i) {
	vector<VD> cbins=states[i]->get_normed_bin_norms();
	double e=0, e2=0;
	if(has_Eblocks) {
		for(int k=0;k<nb;k++) {
			double E=energy_w_blocks(cbins[k], k);
			e+=E;
			e2+=E*E;
		}
	}
	else {
		for(int k=0;k<nb;k++) {
			double E=energy(cbins[k]);
			e+=E;
			e2+=E*E;
		}
	}
	return std(e2, e, nb);
}

CD EP_observable::overlap(vector<CD> c1, vector<CD> c2) {
	CD ov=0;
	for(int k=0;k<ne;k++) ov+=conj(c1[k])*c2[k];
	return ov;
}

CD EP_observable::ham_element(vector<CD> c1, vector<CD> c2) {
	CD el=0;
	for(int k=0;k<ne;k++) el+=conj(c1[k])*E_eigvals[k]*c2[k];
	return el;
}

double EP_observable::energy(VD n_c) {
	double E=0;
	for(int k=0;k<ne;k++) E+=n_c[k]*E_eigvals[k];
	return E;
}

double EP_observable::energy_w_blocks(VD n_c, int b) {
	double E=0;
	for(int k=0;k<ne;k++) E+=n_c[k]*Eblocks[k][b];
	return E;
}

void EP_observable::diag_energies(VD& energies, bool write_eigenstates) {
	vector<CD> ham_elements;
	for(int i=0;i<nstates;i++) {
		vector<CD> c1=states[i]->get_normed_coeffs();
		ham_elements.push_back(energy(i));
		for(int j=i+1;j<nstates;j++) {
			vector<CD> c2=states[j]->get_normed_coeffs();
			ham_elements.push_back(ham_element(c1, c2));
		}
	}
	
	Eigen::MatrixXcd Ham=Hamiltonian_matrix(ham_elements);
	energies=diag_energy_eigvals(Ham, write_eigenstates);
}

void EP_observable::diag_energies_error(VD& energy_errors) {
	energy_errors.resize(nstates);
	VD e(nstates, 0.0), e2(nstates, 0.0);
	for(int b=0;b<nb;b++) {
		VD energies(nstates);
		vector<CD> ham_elements;
		for(int i=0;i<nstates;i++) {
			VD c1n=states[i]->get_normed_bin_norm(b);
			vector<CD> c1=states[i]->get_normed_bin(b);
			ham_elements.push_back(energy(c1n));
			for(int j=i+1;j<nstates;j++) {
				vector<CD> c2=states[j]->get_normed_bin(b);
				ham_elements.push_back(ham_element(c1, c2));
			}
		}
		
		Eigen::MatrixXcd Ham=Hamiltonian_matrix(ham_elements);
		energies=diag_energy_eigvals(Ham, false);
		for(int i=0;i<nstates;i++) {
			e[i]+=energies[i];
			e2[i]+=energies[i]*energies[i];
		}
	}
	for(int i=0;i<nstates;i++) energy_errors[i]=std(e2[i], e[i], nb);
}

VD EP_observable::diag_energy_eigvals(Eigen::MatrixXcd Ham, bool write_eigenstates) {
	VD energies(nstates);
	if(write_eigenstates) error("Eigenstate output not set up");
	
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> eigensolver(Ham);
	if (eigensolver.info() != Eigen::Success) error("Finding eigenvalues");
	Eigen::VectorXd eigvalues=eigensolver.eigenvalues().cast<double>();
	
	for(int i=0;i<nstates;i++) energies[i]=eigvalues[i];
	return energies;
}

void EP_observable::harm_mean(CD& mean, int skip) {
	
	Eigen::MatrixXcd ov_mat;
	ov_mat.resize(nstates,nstates);
	for(int i=0;i<nstates;i++) {
		vector<CD> c=states[i]->get_normed_coeffs();
		for(int j=skip;j<nstates+skip;j++) ov_mat(i,j-skip)=c[j];
	}
	mean=pow(determinant(ov_mat), 1.0/nstates);
}

void EP_observable::harm_mean_error(CD& mean, double& absmean, double& squaremean, int skip) {
	
	double re=0, re2=0, im=0, im2=0, ab=0, ab2=0, sq=0, sq2=0;
	for(int k=0;k<nb;k++) {
		Eigen::MatrixXcd ov_mat;
		ov_mat.resize(nstates,nstates);
		for(int i=0;i<nstates;i++) {
			vector<CD> c=states[i]->get_normed_bin(k);
			for(int j=skip;j<nstates+skip;j++) ov_mat(i,j-skip)=c[j];
		}
		CD bin_mean=pow(determinant(ov_mat), 1.0/nstates);
		re+=real(bin_mean);
		re2+=real(bin_mean)*real(bin_mean);
		im+=imag(bin_mean);
		im2+=imag(bin_mean)*imag(bin_mean);
		ab+=abs(bin_mean);
		ab2+=abs(bin_mean)*abs(bin_mean);
		sq+=norm(bin_mean);
		sq2+=norm(bin_mean)*norm(bin_mean);
	}
	mean=CD(std(re2, re, nb), std(im2, im, nb));
	absmean=std(ab2, ab, nb);
	squaremean=std(sq2, sq, nb);
}

void EP_observable::rank(double& det, VD& sing_vals) {
	
	vector<CD> all_ovs;
	for(int i=0;i<nstates;i++) {
		for(int j=i+1;j<nstates;j++) {
			all_ovs.push_back(overlap(i, j));
		}
	}
	
	Eigen::MatrixXcd ov_mat=overlap_matrix(all_ovs);
	sing_vals=singular_values(ov_mat);
	CD cdet=determinant(ov_mat);
	det=real(cdet);
}

void EP_observable::rank_error(double& det, VD& sing_vals) {
	
	double d=0, d2=0;
	VD sv(ne, 0.0), sv2(ne, 0.0);
	for(int k=0;k<nb;k++) {
		vector<CD> all_ovs;
		for(int i=0;i<nstates;i++) {
			for(int j=i+1;j<nstates;j++) {
				all_ovs.push_back(overlap(states[i]->get_normed_bin(k), states[j]->get_normed_bin(k)));
			}
		}
		Eigen::MatrixXcd ov_mat=overlap_matrix(all_ovs);
		VD sing_vals_bin=singular_values(ov_mat);
		for(int i=0;i<ne;i++) {
			sv[i]+=sing_vals_bin[i];
			sv2[i]+=sing_vals_bin[i]*sing_vals_bin[i];
		}
		CD cdet=determinant(ov_mat);
		d+=real(cdet);
		d2+=real(cdet)*real(cdet);
	}
	det=std(d2, d, nb);
	sing_vals.resize(ne);
	for(int i=0;i<ne;i++) sing_vals[i]=std(sv2[i], sv[i], nb);
}
	
CD EP_observable::determinant(Eigen::MatrixXcd ov_mat) {
	return ov_mat.determinant();	
}

void EP_observable::get_eigenvectors(Eigen::MatrixXcd G_matrix, VD& eigvalues, vector<VCD>& eigenvectors, int& rank, VI& basis_states) {
	
	nstates=G_matrix.rows();
	Eigen::ComplexEigenSolver<Eigen::MatrixXcd> ES(G_matrix, true);
	Eigen::MatrixXcd V=ES.eigenvectors();
	Eigen::VectorXcd D=ES.eigenvalues();
	
	rank=0;
	basis_states.clear();
	eigvalues.resize(nstates);
	eigenvectors.clear();
	double ev_mean=0;
	for(int i=0;i<nstates;i++) ev_mean+=abs(D(i))/(double)nstates;
	
	for(int i=0;i<nstates;i++) {
		eigvalues[i]=real(D(i));
		if(abs(D(i))/ev_mean>threshold) {
			double rms=0;
			eigenvectors.push_back(VCD(nstates));
			for(int j=0;j<nstates;j++) {
				CD tmp=V(j, i);
        double Nj=states[j]->get_norm();
				eigenvectors[rank][j]=tmp/(Nj*sqrt(real(D(i))));
				rms+=norm(tmp)/(Nj*Nj*real(D(i)));
			}
			rms=sqrt(rms);
			for(int j=0;j<nstates;j++) eigenvectors[rank][j]/=rms;
			rank++;
			basis_states.push_back(i);
		}
	}
}

void EP_observable::get_eigenvector_errors(VD& eigvalue_errors, vector<VCD>& eigenvector_errors, int rank, VI basis_states) {
	
	eigvalue_errors.resize(nstates);
	eigenvector_errors.resize(rank, VCD(nstates));
	
	VD eigval(nstates, 0.0), eigval2(nstates, 0.0);
	vector<VD> re_eigvec(rank, VD(nstates, 0.0)), re_eigvec2(rank, VD(nstates, 0.0)), im_eigvec(rank, VD(nstates, 0.0)), im_eigvec2(rank, VD(nstates, 0.0));
	
	for(int k=0;k<nb;k++) {
				
		vector<CD> overlaps;
		for(int i=0;i<nstates;i++) {
			for(int j=i+1;j<nstates;j++) {
				overlaps.push_back(overlap(states[i]->get_normed_bin(k), states[j]->get_normed_bin(k)));
			}
		}
	
		Eigen::MatrixXcd G_matrix=overlap_matrix(overlaps);
		Eigen::ComplexEigenSolver<Eigen::MatrixXcd> ES(G_matrix, true);
		Eigen::MatrixXcd V=ES.eigenvectors();
		Eigen::VectorXcd D=ES.eigenvalues();
		
		int rank_ctr=0;
		VI basis_states_tmp=basis_states;
		
		for(int i=0;i<nstates;i++) {
			eigval[i]=+real(D(i));
			eigval2[i]=+real(D(i))*real(D(i));
			
			if(i==basis_states_tmp[0]) {
				
				for(int j=0;j<nstates;j++) {
					CD tmp=V(j, i)/sqrt(real(D(i)));
					double re_tmp=abs(real(tmp));
					double im_tmp=abs(imag(tmp));
					re_eigvec[rank_ctr][j]+=re_tmp;
					re_eigvec2[rank_ctr][j]+=re_tmp*re_tmp;
					im_eigvec[rank_ctr][j]+=im_tmp;
					im_eigvec2[rank_ctr][j]+=im_tmp*im_tmp;
				}
				
				basis_states_tmp.erase(basis_states_tmp.begin());
				rank_ctr++;
			}
		}
		if(rank_ctr<rank || basis_states_tmp.size()>0) error("Something went wrong with rank counting in bin "+to_string(k+1));
	}
	
	
	for(int i=0;i<nstates;i++) eigvalue_errors[i]=std(eigval2[i], eigval[i], nb);
	for(int i=0;i<rank;i++) {
		for(int j=0;j<nstates;j++) eigenvector_errors[i][j]=CD(std(re_eigvec2[i][j], re_eigvec[i][j], nb), std(im_eigvec2[i][j], im_eigvec[i][j], nb));
	}
	
}

VD EP_observable::singular_values(Eigen::MatrixXcd ov_mat) {

	Eigen::JacobiSVD<Eigen::MatrixXcd> svd(ov_mat);
	Eigen::VectorXd s_vals=svd.singularValues();
	VD values(nstates);
	for(int i=0;i<nstates;i++) values[i]=s_vals(i);
	
	return values;
}

Eigen::MatrixXcd EP_observable::overlap_matrix(vector<CD> overlaps) {
	Eigen::MatrixXcd G(nstates, nstates);
	int pair=0;
	for(int i=0;i<nstates;i++) {
		for(int j=0;j<i;j++) G(i,j)=conj(G(j,i));
		G(i,i)=1.0;
		for(int j=i+1;j<nstates;j++) {
			G(i,j)=overlaps[pair];
			pair++;
		}
	}
	return G;
}

Eigen::MatrixXcd EP_observable::Hamiltonian_matrix(vector<CD> elements) {
	Eigen::MatrixXcd Ham;
	Ham.resize(nstates, nstates);
	int pair=0;
	for(int i=0;i<nstates;i++) {
		for(int j=0;j<i;j++) Ham(i,j)=conj(Ham(j,i));
		Ham(i,i)=elements[pair];
		pair++;
		for(int j=i+1;j<nstates;j++) {
			Ham(i,j)=elements[pair];
			pair++;
		}
	}
	return Ham;
}

double EP_observable::std(double av2, double av, int n) {
	return sqrt((av2/n-av*av/(n*n))/(n-1.0));
}

int EP_observable::get_ne() {return ne;}

int EP_observable::get_nb() {return nb;}

void EP_observable::display_energy_eigenvalues() {
	if(!has_eigvals) warning("No energy eigenvalues loaded");
	else {
		for(int i=0;i<ne;i++) cout << E_eigvals[i] << endl;
	}
}
