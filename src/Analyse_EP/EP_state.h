/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef EP_STATE_H
#define EP_STATE_H

#include "input.h"
#include "misc.h"
#include "HDF5Wrapper.h"
#include <algorithm> 

class EP_state {
	
	private:
	int ne, nb;
	vector<CD> c, c_1sthalf, c_2ndhalf;
	VD abs_c, norm_c;
	vector<vector<CD> > cbins, cbin_1sthalf, cbin_2ndhalf;
	vector<VD> norm_cbins, abs_cbins;
	double normalisation;
	
	double find_norm(VD);
	void error_quantities(int, double&, double&, double&, double&);
	
	public:	
	void display_coeffs();
	void display_coeff_bins();
	vector<CD> get_normed_coeffs();
	VD get_normed_coeff_norms();
	vector<vector<CD> > get_bins();
	vector<vector<CD> > get_bins_1sthalf();
	vector<vector<CD> > get_bins_2ndhalf();
	vector<vector<CD> > get_normed_bins();
	vector<VD> get_normed_bin_norms();
	VD get_normed_bin_norm(int);
	vector<CD> get_normed_bin(int);
	vector<CD> get_bin(int);
	vector<CD> get_bin_1sthalf(int);
	vector<CD> get_bin_2ndhalf(int);
	vector<CD> get_coeffs();
	vector<CD> get_coeffs_1sthalf();
	vector<CD> get_coeffs_2ndhalf();
	double get_norm();
	double get_bin_norm(int);
	VD get_eigvals();
	int get_ne();
	int get_nb();
	void write_state(string, string, bool);
	double std(double, double, int);
	VD get_single_overlap(int);
	
	EP_state(vector<CD>, vector<vector<CD > >, vector<CD>, vector<CD>, vector<vector<CD> >, vector<vector<CD> >);
	EP_state(string, string);

};

#endif
