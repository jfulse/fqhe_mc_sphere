/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/

#include "EP_state.h"

EP_state::EP_state(vector<CD> c_,  vector<vector<CD > > cbins_, vector<CD> c_1sthalf_, vector<CD> c_2ndhalf_, 
				   vector<vector<CD> > cbin_1sthalf_, vector<vector<CD> > cbin_2ndhalf_) : 
				   c(c_), cbins(cbins_), c_1sthalf(c_1sthalf_), c_2ndhalf(c_2ndhalf_), cbin_1sthalf(cbin_1sthalf_), cbin_2ndhalf(cbin_2ndhalf_) {
	ne=c.size();
	nb=cbins.size();
	
	if(ne!=c_1sthalf.size() || ne!=c_2ndhalf.size()) {
		error("Unequal number of energy eigenstates in coeffs / coeffs 1st half / coeffs 2nd half");
	}
	if(nb!=cbin_1sthalf.size() || nb !=cbin_2ndhalf.size()) {
		error("Unequal number of bins in bins / bins 1st half / bins 2nd half");
	}
	for(int i=0;i<nb;i++) {
		if(ne!=cbins[i].size() || ne!=cbin_1sthalf[i].size() || ne!=cbin_2ndhalf[i].size()) {
			error("Unequal number of energy eigenstates in bin "+to_string(i)+"; coeffs / coeffs 1st half / coeffs 2nd half");
		}
	}
	
	abs_c.resize(ne);
	norm_c.resize(ne);
	for(int i=0;i<ne;i++) {
		abs_c[i]=sqrt(abs(real(c_1sthalf[i]*conj(c_2ndhalf[i]))));
		norm_c[i]=abs(real(c_1sthalf[i]*conj(c_2ndhalf[i])));
	}
	
	abs_cbins.resize(nb);
	norm_cbins.resize(nb);
	for(int i=0;i<nb;i++) {
		abs_cbins[i].resize(ne);
		norm_cbins[i].resize(ne);
		for(int j=0;j<ne;j++) {
			abs_cbins[i][j]=sqrt(abs(real(cbin_1sthalf[i][j]*conj(cbin_2ndhalf[i][j]))));
			norm_cbins[i][j]=abs(real(cbin_1sthalf[i][j]*conj(cbin_2ndhalf[i][j])));
		}
	}
	
	normalisation=find_norm(norm_c);
}
	
EP_state::EP_state(string cfile, string cbinfile) {
	
	ifstream c_in(cfile.c_str());
	c_in.precision(17);
	if(!c_in) error("Cannot find file "+cfile);		
	int foundcs=0;
	double coeff;
	string re, im, ab, no, dum, re_1st, im_1st, re_2nd, im_2nd;
	
	while(getline(c_in,re,',')) {
		getline(c_in,im,',');
		if(im=="") error("imag(c) missing in "+cfile);
		getline(c_in,ab,',');
		if(ab=="") error("Abs(c) missing in "+cfile);
		getline(c_in,no,',');
		if(no=="") error("Norm(c) missing in "+cfile);
		getline(c_in,re_1st,',');
		if(re_1st=="") error("Re(c_1st_half) missing in "+cfile);
		getline(c_in,im_1st,',');
		if(re_1st=="") error("Im(c_1st_half) missing in "+cfile);
		getline(c_in,re_2nd,',');
		if(re_1st=="") error("Re(c_2nd_half) missing in "+cfile);
		getline(c_in,im_2nd);
		if(re_1st=="") error("Im(c_2nd_half) missing in "+cfile);
		if(!isnumber(re)) error("In file "+cfile+": "+re+" is not a number");
		if(!isnumber(im)) error("In file "+cfile+": "+im+" is not a number");
		if(!isnumber(ab)) error("In file "+cfile+": "+ab+" is not a number");
		if(!isnumber(no)) error("In file "+cfile+": "+no+" is not a number");
		if(!isnumber(re_1st)) error("In file "+cfile+": "+re_1st+" is not a number");
		if(!isnumber(im_1st)) error("In file "+cfile+": "+im_1st+" is not a number");
		if(!isnumber(re_2nd)) error("In file "+cfile+": "+re_2nd+" is not a number");
		if(!isnumber(im_2nd)) error("In file "+cfile+": "+im_2nd+" is not a number");
		
		c.push_back(CD(stod(re), stod(im)));
		abs_c.push_back(stod(ab));
		norm_c.push_back(stod(no));
		c_1sthalf.push_back(CD(stod(re_1st), stod(im_1st)));
		c_2ndhalf.push_back(CD(stod(re_2nd), stod(im_2nd)));
	}
	ne=c.size();
	normalisation=find_norm(norm_c);
	c_in.close();
	
	HDF5Wrapper *reader=new HDF5Wrapper;
	if(reader->openFileToRead(cbinfile.c_str())!=0) error("Could not open "+cbinfile+" for reading");
	unsigned long rows, cols;
	reader->getDatasetDims("coeff_bins", rows, cols);
	if(cols%8) error("Must have an integer muliple of eight columns in "+cbinfile+" (real and imag parts, abs and abs^2, and real and imag parts of 1st and 2nd halves of coefficients)");
	if(cols!=8*ne) error("Unequal number of coefficients in "+cfile+" ("+to_string(ne)+") and "+cbinfile+" ("+to_string(cols/8)+")");
	nb=rows;
	
	HDF5Buffer *readbuffer=reader->setupRealSamplesBuffer("coeff_bins", 0, 10000);
	double *coeffs=new double[8*ne];
	
	cbins.resize(nb, vector<CD>(ne));
	abs_cbins.resize(nb, VD(ne));
	norm_cbins.resize(nb, VD(ne));
	cbin_1sthalf.resize(nb, vector<CD>(ne));
	cbin_2ndhalf.resize(nb, vector<CD>(ne));
	for(int i=0;i<nb;i++) {
		readbuffer->readFromBuffer(coeffs, 8*ne);
		for(int j=0;j<ne;j++) {
			cbins[i][j]=CD(coeffs[8*j], coeffs[8*j+1]);
			abs_cbins[i][j]=coeffs[8*j+2];
			norm_cbins[i][j]=coeffs[8*j+3];
			cbin_1sthalf[i][j]=CD(coeffs[8*j+4], coeffs[8*j+5]);
			cbin_2ndhalf[i][j]=CD(coeffs[8*j+6], coeffs[8*j+7]);
		}
	}
	
	delete [] coeffs;
	delete reader;
}

void EP_state::display_coeffs() {
	for(int i=0;i<ne;i++) cout << "c_" << i << " = " << c[i] << ", |c_" << i << "| = " << abs_c[i] <<  ", |c_" << i << "|^2 = " << norm_c[i] << endl;
}

void EP_state::display_coeff_bins() {
	for(int j=0;j<nb;j++) {
		cout << "Bin " << j << ":" << endl;
		for(int i=0;i<ne;i++) cout << "c_" << i << "=" << cbins[j][i] << ", |c_" << i << "| = " << abs_cbins[j][i] <<  ", |c_" << i << "|^2 = " 
								   << norm_cbins[j][i] << endl;
	}
}

vector<CD> EP_state::get_normed_coeffs() {
	vector<CD> normed(ne);
	transform(c.begin(), c.end(), normed.begin(), bind1st(multiplies<CD>(),1.0/normalisation));
	return normed;
}

VD EP_state::get_normed_coeff_norms() {
	VD normed(ne);
	transform(norm_c.begin(), norm_c.end(), normed.begin(), bind1st(multiplies<double>(),1.0/(normalisation*normalisation)));
	return normed;
}

double EP_state::find_norm(VD norm_coeffs) {
	double tot_norm=0;
	for(int i=0;i<ne;i++) tot_norm+=norm_coeffs[i];
	return sqrt(tot_norm);
}

vector<vector<CD> > EP_state::get_normed_bins() {
	vector<vector<CD> >  normed(nb, vector<CD>(ne));
	for(int i=0;i<nb;i++) {
		double bin_norm=find_norm(norm_cbins[i]);
		transform(cbins[i].begin(), cbins[i].end(), normed[i].begin(), bind1st(multiplies<CD>(),1.0/bin_norm));
	}
	return normed;
}

vector<VD> EP_state::get_normed_bin_norms() {
	vector<VD>  normed(nb, VD(ne));
	for(int i=0;i<nb;i++) {
		double bin_norm=find_norm(norm_cbins[i]);
		transform(norm_cbins[i].begin(), norm_cbins[i].end(), normed[i].begin(), bind1st(multiplies<double>(),1.0/(bin_norm*bin_norm)));
	}
	return normed;
}

VD EP_state::get_normed_bin_norm(int i) {
	VD normed(ne);
	double bin_norm=find_norm(norm_cbins[i]);
	transform(norm_cbins[i].begin(), norm_cbins[i].end(), normed.begin(), bind1st(multiplies<double>(),1.0/(bin_norm*bin_norm)));
	return normed;
}

vector<vector<CD> > EP_state::get_bins() {return cbins;}

vector<vector<CD> > EP_state::get_bins_1sthalf() {return cbin_1sthalf;}

vector<vector<CD> > EP_state::get_bins_2ndhalf() {return cbin_2ndhalf;}

vector<CD> EP_state::get_normed_bin(int i) {
	vector<CD> normed(ne);
	double bin_norm=find_norm(norm_cbins[i]);
	transform(cbins[i].begin(), cbins[i].end(), normed.begin(), bind1st(multiplies<CD>(),1.0/bin_norm));
	return normed;
}

vector<CD> EP_state::get_bin(int i) {return cbins[i];}

vector<CD> EP_state::get_bin_1sthalf(int i) {return cbin_1sthalf[i];}

vector<CD> EP_state::get_bin_2ndhalf(int i) {return cbin_2ndhalf[i];}

vector<CD> EP_state::get_coeffs() {return c;}

vector<CD> EP_state::get_coeffs_1sthalf() {return c_1sthalf;}

vector<CD> EP_state::get_coeffs_2ndhalf() {return c_2ndhalf;}

double EP_state::get_norm() {return normalisation;}

double EP_state::get_bin_norm(int i) {return find_norm(norm_cbins[i]);}

int EP_state::get_ne() {return ne;}

int EP_state::get_nb() {return nb;}

double EP_state::std(double av2, double av, int n) {
	return sqrt((av2/n-av*av/(n*n))/(n-1.0));
}

VD EP_state::get_single_overlap(int i) {
	if(i>ne) error("Asked for overlap "+to_string(i)+" of "+to_string(ne));
	double e_re, e_im, e_abs, e_sq;
	error_quantities(i, e_re, e_im, e_abs, e_sq);
	VD ov;
	ov.push_back(real(c[i])/normalisation);
	ov.push_back(imag(c[i])/normalisation);
	ov.push_back(e_re);
	ov.push_back(e_im);
	ov.push_back(abs(c[i])/normalisation);
	ov.push_back(e_abs);
	ov.push_back(norm(c[i])/(normalisation*normalisation));
	ov.push_back(e_sq);
	return ov;
}

void EP_state::error_quantities(int state, double& reoverlap, double& imoverlap, double& absoverlap, double& normsquareoverlap) {
	double re=0, re2=0, im=0, im2=0, ab=0, ab2=0, sq=0, sq2=0;
	for(int i=0;i<nb;i++) {
		CD ov=cbins[i][state];
		re+=real(ov);
		re2+=real(ov)*real(ov);
		im+=imag(ov);
		im2+=imag(ov)*imag(ov);
		ab+=abs(ov);
		ab2+=abs(ov)*abs(ov);
		sq+=norm(ov)/(normalisation*normalisation);
		sq2+=norm(ov)*norm(ov)/(normalisation*normalisation*normalisation*normalisation);
	}
	reoverlap=std(re2, re, nb);
	imoverlap=std(im2, im, nb);
	absoverlap=std(ab2, ab, nb);
	normsquareoverlap=std(sq2, sq, nb);
}

void EP_state::write_state(string out, string binout, bool verb) {
	
	string sep=",";
	ofstream cwriter(out.c_str());
	if(!cwriter) error("Could not open "+out+" for writing");
	cwriter.precision(17);
	for(int i=0;i<ne;i++) {
		cwriter << real(c[i]) << "," << imag(c[i]) << "," << abs_c[i] << "," << norm_c[i] << "," << real(c_1sthalf[i]) << "," << imag(c_1sthalf[i]) << ",";
		cwriter << real(c_2ndhalf[i]) << "," << imag(c_2ndhalf[i]) << endl;
	}	
	
	HDF5Wrapper *writer=new HDF5Wrapper;
	if(writer->openFileToWrite(binout.c_str())!=0) error("Could not open "+binout+" for writing");
	writer->createRealDataset("coeff_bins", nb, 8*ne);
	HDF5Buffer *writebuffer=writer->setupRealSamplesBuffer("coeff_bins", 0, 10000);
	double *block=new double[8*ne];
	
	for(int i=0;i<nb;i++) {
		for(int j=0;j<ne;j++) {
			block[8*j]=real(cbins[i][j]);
			block[8*j+1]=imag(cbins[i][j]);
			block[8*j+2]=abs_cbins[i][j];
			block[8*j+3]=norm_cbins[i][j];
			block[8*j+4]=real(cbin_1sthalf[i][j]);
			block[8*j+5]=imag(cbin_1sthalf[i][j]);
			block[8*j+6]=real(cbin_2ndhalf[i][j]);
			block[8*j+7]=imag(cbin_2ndhalf[i][j]);
		}
		writebuffer->writeToBuffer(block, 8*ne);
	}
	
	if(verb) {
		cout << "Coefficients written to " << out << endl;
		cout << "Bins written to " << binout << endl;
	}
	
	cwriter.close();
	delete [] block;
	delete writer;
}
