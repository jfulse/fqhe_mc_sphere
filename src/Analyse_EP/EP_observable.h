/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef EP_OBSERVABLE_H
#define EP_OBSERVABLE_H

#include "EP_state.h"
#include "Eigen/SVD"
#include "Eigen/Dense"

class EP_observable {
	
	private:
	double threshold;
	int nstates, ne, nb, npairs;
	vector<EP_state*> states;
	bool has_eigvals, has_Eblocks;
	VD E_eigvals;
	vector<VD> Eblocks;
	
	CD overlap(int, int);
	CD overlap_error(int, int);
	double abs_overlap_error(int, int);
	double sqr_overlap_error(int, int);
	CD overlap(vector<CD>, vector<CD>);
	CD GS_factor(int, int);
	CD GS_factor_error(int, int);
	CD GS_factor(vector<CD>, vector<CD>);
	double energy(int);
	double energy_error(int);
	double alt_energy_error(int);
	double energy(VD);
	double energy_w_blocks(VD, int);
	CD ham_element(vector<CD>, vector<CD>);
	double std(double, double, int);
	void rank(double&, VD&);
	void rank_error(double&, VD&);
	void diag_energies(VD&, bool);
	void diag_energies_error(VD&);
	//VD diag_energy_eigvals(alglib::complex_2d_array, bool);
	VD diag_energy_eigvals(Eigen::MatrixXcd, bool);
	Eigen::MatrixXcd overlap_matrix(vector<CD>);
	//alglib::complex_2d_array Hamiltonian_matrix(vector<CD>);
	Eigen::MatrixXcd Hamiltonian_matrix(vector<CD>);
	VD singular_values(Eigen::MatrixXcd);
	CD determinant(Eigen::MatrixXcd);
	//CD determinant(alglib::complex_2d_array);
	void harm_mean(CD&, int);
	void harm_mean_error(CD&, double&, double&, int);
	void get_orthog_coeffs(vector<vector<CD> >&);
	void get_orthog_coeffs_1sthalf(vector<vector<CD> >&);
	void get_orthog_coeffs_2ndhalf(vector<vector<CD> >&);
	void get_orthog_coeff_bins(vector<vector<vector<CD> > >&);
	void get_orthog_coeff_bins_1sthalf(vector<vector<vector<CD> > >&);
	void get_orthog_coeff_bins_2ndhalf(vector<vector<vector<CD> > >&);
	void next_orthog_coeff(int, vector<vector<CD> >&, vector<CD>);
	void get_eigenvectors(Eigen::MatrixXcd, VD&, vector<VCD>&, int&, VI&);
	void get_eigenvector_errors(VD&, vector<VCD>&, int, VI);
	
	public:
	
	void display_energy_eigenvalues();
	int get_ne();
	int get_nb();
	void all_overlaps(vector<CD>&, VD&, VD&);
	void all_GS_factors(vector<CD>&);
	void all_energies(VD&);
	void rank(VD&, vector<VD>&);
	void diag_energies(vector<VD>&, bool);
	void harmonic_mean(vector<CD>&, VD&, VD&, int);
	void Gram_Schmidt(vector<EP_state*>&);
	EP_state* superposition(vector<CD>);
	void orthog_basis(int&, VD&, vector<VCD>&, VD&, vector<VCD>&, bool);
	
	EP_observable(vector<EP_state*> states_, string Efile="", bool DH_vals=false, double Esub=0.0, VS Eblockfiles=VS(), double threshold_=1e-4);
	
	~EP_observable() {}
	
};

#endif
