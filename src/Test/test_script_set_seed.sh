#!/bin/bash

set -e
set -u

###MAKEDEP:../../Programs/Run_MC_sphere
###MAKEDEP:check_identical_vectors.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../Programs
dir_CFT=$(pwd)
echo $dir_CFT
MC_Sphere=$dir_CFT/Run_MC_sphere

Catalouge=$MyDir/Test_set_seed
mkdir -p $Catalouge
cd $Catalouge

N=1000
Ne=5
Kmom=0
q=3

pos_start=pos-here.hdf5

###Run the torus code one
$MC_Sphere -wf L -m $q -Ne $Ne -N $N -po $Catalouge/$pos_start -wo $Catalouge/wf.hdf5 -wg $Catalouge/weight.hdf5 --seed 5435 -th 100 --no-stab

### Run with the same seed again
$MC_Sphere -wf L -m $q -Ne $Ne -N $N -po $Catalouge/$pos_start -wo $Catalouge/wf_same.hdf5 -wg $Catalouge/weight.hdf5 --seed 5435 -th 100 --no-stab

### Run with a different seed
$MC_Sphere -wf L -m $q -Ne $Ne -N $N -po $Catalouge/$pos_start -wo $Catalouge/wf_diff.hdf5 -wg $Catalouge/weight.hdf5 --seed 2854 -th 100 --no-stab

### Run with a random seed
$MC_Sphere -wf L -m $q -Ne $Ne -N $N -po $Catalouge/$pos_start -wo $Catalouge/wf_rnd.hdf5 -wg $Catalouge/weight.hdf5 -th 100 --no-stab


####     Tests

###Test that the wave functions are the same for the same seed
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_same.hdf5 loglog same same

###Test that the wave functions are the different for different seeds
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_diff.hdf5 loglog diff diff

###Test that the wave functions are the different with random seed
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_rnd.hdf5 loglog diff diff

###Cleaning afterwards
rm -r $Catalouge
