#ifndef TEST_SUITE_MISC_H
#define TEST_SUITE_MISC_H

#include "catch.hpp"
#include "CF_Misc.h"

CD get_u(double polar,double azimutal);
CD get_v(double polar,double azimutal);
CD get_lu(double polar,double azimutal);
CD get_lv(double polar,double azimutal);

double mod2pi(double value);

void test_report_two_linear_psi(CD Psi,CD Psi2);
void test_report_two_log_psi(CD Psi,CD Psi2);
void test_report_two_psi(CD Psi,CD Psi2,int mpq,int twoq,int nu,int Ne);
void test_report_three_psi(CD Psi,CD Psi2,CD Psi3,double shift,int mpq,int twoq,int nu,int Ne);
void test_report_three_psi_log(CD Psi,CD Psi2,CD Psi3,double shift,int mpq,
			       int twoq,int nu,int Ne,double Precition=0.0000001);

CD Naive_UP_CF_orbital(int mpq,int nu,CD logu, CD logv, int twoq);
CD Naive_rev_CF_JK_orbital_p2(int mpq,int nu,
			  Eigen::MatrixXcd logesp, int i,CD logu, CD logv,int Ne,int twoq,double p);


inline double logbin(int a, int b) {return logfact(a)-logfact(b)-logfact(a-b);}


stringstream capture_matrix(Eigen::MatrixXcd Mat,int Dim);

#endif
