#! /usr/bin/env python

import sys
import argparse
import numpy as np
import h5py

def __main__():

    """
    Main driver function, if script called from the command line it will use the command line arguments as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Test if a wave function is zero')
    Parser.add_argument('wfn_in', type=str, help='input file for wfn samples psi(z|tau).')
    Args = Parser.parse_args()
    Wf_in=Args.wfn_in

    
    print("Open file ",Wf_in," for read")
    with h5py.File(Wf_in, 'r') as wf:
        wfval=np.array(wf.get('wf_values'))

    print("|wfval|:\n",np.abs(wfval))
    AverageElem=np.mean(np.abs(wfval))
    print("AverageElem:",AverageElem)
    if AverageElem < 10**(-20) :
        print "ERROR: The average wave fuction value is close to zero"
        #print("|wfval:\n",np.abs(wfval))
        print("AverageElem:",AverageElem)
        raise Exception


if __name__ == '__main__' :
    __main__()

