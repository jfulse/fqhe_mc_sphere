#include "catch.hpp"
#include "CF_det.h"
#include "sfmt.h"
#include "test_suite_Misc.h"

TEST_CASE( "Test that LogDet(LogMat) gives same result as log(Det(Mat))", "[determinant]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int MaxSize=4;
  int Tries=20;

  for(int Try=0;Try<Tries;Try++) { // "Internal" LL
    int Size=(int)(rangen.Random()*MaxSize)+1;
    INFO("Size of matrix  = "<<Size);
    Eigen::MatrixXcd Matrix,LogMatrix;
    Matrix.resize(Size,Size);
    LogMatrix.resize(Size,Size);

    //Now fill the matrix with data
    for(int i=0;i<Size;i++) { // "Row
      for(int j=0;j<Size;j++) { // "Collumn"
	CD value=(rangen.Random()-.5)+iunit*(rangen.Random()-.5);
	Matrix(i,j)=value;
	LogMatrix(i,j)=log(value);
      }
    }
    INFO("Matrix:");
    INFO(capture_matrix(Matrix,Size).str());
    INFO("LogMatrix:");
    INFO(capture_matrix(LogMatrix,Size).str());
    CD LogPsi=TakeLogDeterminant(Matrix,Size);
    CD LogPsiOfLog=TakeLogDeterminantOfLog(LogMatrix,Size);
    test_report_two_linear_psi(exp(LogPsi),exp(LogPsiOfLog));
    test_report_two_log_psi(LogPsi,LogPsiOfLog);
  }      
}

