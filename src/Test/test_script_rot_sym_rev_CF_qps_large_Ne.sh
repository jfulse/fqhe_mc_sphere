#!/bin/bash

set -e
set -u

###MAKEDEP:../../Programs/Run_MC_sphere
###MAKEDEP:check_identical_vectors.py
###MAKEDEP:RotateSpinors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../Programs
dir_CFT=$(pwd)
echo $dir_CFT
MC_Sphere=$dir_CFT/Run_MC_sphere

Catalouge=$MyDir/Test_angular_mom
mkdir -p $Catalouge
cd $Catalouge

MCSAMPLES=10
Ne=25

pos_start=pos-here.hdf5
pos_shift=pos-shift.hdf5

WFLOPTS=" -Ne $Ne -N $MCSAMPLES -th 100"
WFOPTS="-wf CF -Ne $Ne -N $MCSAMPLES --CF:qp_num 1"

qp_pos_file=$Catalouge/qp_pos_file.txt
qp_pos_file_shift=$Catalouge/qp_pos_file2.txt
qp_pos_file_shift_both=$Catalouge/qp_pos_file_both.txt
Angle=0.001
Azimuth=0.2
echo $Azimuth 0.000 >  $qp_pos_file
echo $Azimuth $Angle >  $qp_pos_file_shift
cat $qp_pos_file > $qp_pos_file_shift_both
cat $qp_pos_file_shift >> $qp_pos_file_shift_both

###Run the Laughlin code one
$MC_Sphere $WFLOPTS -po $Catalouge/$pos_start -wo $Catalouge/wf.hdf5 -wg $Catalouge/weight.hdf5 -dl 50 

#### Now rotate the electron positions
python $MyDir/RotateSpinors.py -ci $Catalouge/$pos_start -co $Catalouge/$pos_shift --angle $Angle

echo -.-.-.-.-.-.-.-.-.-..-.-.-.-.-..-
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file -X -pi $Catalouge/$pos_start -wo $Catalouge/wf_elec_qp.hdf5 -nc 1
echo -.-.-.-.-.-.-.-.-.-..-.-.-.-.-..-
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file_shift -X -pi $Catalouge/$pos_start -wo $Catalouge/wf_elec_qp_shift.hdf5 -nc 1
echo -.-.-.-.-.-.-.-.-.-..-.-.-.-.-..-
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file_shift -X -pi $Catalouge/$pos_shift -wo $Catalouge/wf_elec_shift_qp_shift.hdf5 -nc 1
echo -.-.-.-.-.-.-.-.-.-..-.-.-.-.-..-
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file_shift_both -X -pi $Catalouge/$pos_start -wo $Catalouge/wf_elec_qp_both.hdf5 -nc 2
echo -.-.-.-.-.-.-.-.-.-..-.-.-.-.-..-


###Test that the wave functions are differnt if only the qu are rotated
python $MyDir/check_identical_vectors.py $Catalouge/wf_elec_qp.hdf5 $Catalouge/wf_elec_qp_shift.hdf5 loglog diff diff

###Test that the wave functions are proprotional after rotation
python $MyDir/check_identical_vectors.py $Catalouge/wf_elec_qp.hdf5 $Catalouge/wf_elec_shift_qp_shift.hdf5 loglog same prop

python $MyDir/check_identical_vectors.py $Catalouge/wf_elec_qp.hdf5 $Catalouge/wf_elec_qp_both.hdf5 loglog same same --use-col-in-2 1

python $MyDir/check_identical_vectors.py $Catalouge/wf_elec_qp_shift.hdf5 $Catalouge/wf_elec_qp_both.hdf5 loglog same same --use-col-in-2 2

###Cleaning afterwards
rm -r $Catalouge
