#!/bin/bash

set -e
set -u

###MAKEDEP:../../Programs/Run_MC_sphere
###MAKEDEP:check_identical_vectors.py
###MAKEDEP:RotateSpinors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../Programs
dir_CFT=$(pwd)
echo $dir_CFT
MC_Sphere=$dir_CFT/Run_MC_sphere

Catalouge=$MyDir/Test_angular_mom_qh
mkdir -p $Catalouge
cd $Catalouge

MCSAMPLES=10
Ne=10

pos_start=pos-here.hdf5
pos_shift=pos-shift.hdf5

WFOPTS="-wf CF -Ne $Ne -N $MCSAMPLES --CF:qh_num 2"

AngleShift=0.1

qh_pos_file=$Catalouge/qp_pos_file.txt
qh_pos_file_shift=$Catalouge/qp_pos_file2.txt
echo 1.2 0.4  3.2 4.5 >  $qh_pos_file
echo 1.2 `echo 0.4 + $AngleShift | bc` 3.2 `echo 4.5 + $AngleShift | bc`  >  $qh_pos_file_shift

###Run the Laughlin code one
$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file -po $Catalouge/$pos_start -wo $Catalouge/wf.hdf5 -wg $Catalouge/weight.hdf5 -lp

### Run with the same positions again
$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file -X -pi $Catalouge/$pos_start -wo $Catalouge/wf_same.hdf5

###Test that the wave functions are the same when recomputing
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_same.hdf5 loglog same same


python $MyDir/RotateSpinors.py -ci $Catalouge/$pos_start -co $Catalouge/$pos_shift --angle $AngleShift

$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file -X -pi $Catalouge/$pos_shift -wo $Catalouge/wf_shift_elec.hdf5
$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file_shift -X -pi $Catalouge/$pos_shift -wo $Catalouge/wf_shift_elec_qp.hdf5


###Test that the wave functions are differnt if only the electrons are rotated
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_shift_elec.hdf5 loglog diff diff

###Test that the wave functions are proprotional after rotation
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_shift_elec_qp.hdf5 loglog same prop

###Cleaning afterwards
rm -r $Catalouge
