#include "catch.hpp"
#include "CF_Misc.h"
#include "sfmt.h"
#include "test_suite_Misc.h"


TEST_CASE( "Check CF angular momentum of UP CF orbitals large Ne,q", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);
  
  int n=1; //Only LLL
  int MaxNe=61;

  for(int Ne=10;Ne<MaxNe;Ne+=5) { // Ne
    int twoq=3*Ne;
    double p=1.0;
    CF_Misc My_CF_Misc(Ne);
    My_CF_Misc.initialize(n,twoq,p,"JK_pos");
    
    for(int nu=0;nu<n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
	//Now choose the orbital
	INFO("-------------------");
	INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);
	double polar=2*pi*rangen.Random();
	double azimuthal=pi*rangen.Random();
	
	double shift=pi*rangen.Random();
	double polar2=polar+shift;
	double azimuthal3=azimuthal+shift;
      
	CD lu=get_lu(polar,azimuthal);
	CD lv=get_lv(polar,azimuthal);
	
	CD lu2=get_lu(polar2,azimuthal);
	CD lv2=get_lv(polar2,azimuthal);
	
	CD lu3=get_lu(polar,azimuthal3);
	CD lv3=get_lv(polar,azimuthal3);
	
	INFO("Polar ,azimuthal  ="<<polar<<" , "<<azimuthal);
	INFO("Polar2,azimuthal3 ="<<polar2<<" , "<<azimuthal3);
	INFO("shift  ="<<shift);
	
	INFO("lu ,lv ="<<lu<<" , "<<lv);
	INFO("lu2,lv2="<<lu2<<" , "<<lv2);
	INFO("lu3,lv3="<<lu3<<" , "<<lv3);
	
	CD Psi=My_CF_Misc.log_UP_CF_orbital(mpq,nu,lu,lv);
	CD Psi2=My_CF_Misc.log_UP_CF_orbital(mpq,nu,lu2,lv2);
	CD Psi3=My_CF_Misc.log_UP_CF_orbital(mpq,nu,lu3,lv3);
	//cout<<"Psi:"<<Psi<<endl;
	test_report_three_psi_log(Psi,Psi2,Psi3,shift,mpq,twoq,nu,Ne,0.00001);
      }
    }      
  }
}


TEST_CASE( "Test that logarithmic UP_CF gives same as linear one", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);
  
  int Maxn=3; 
  int MaxTwoQ=20;
  double p=1.0;
  int Ne=1;
  for(int twoq=0;twoq<MaxTwoQ;twoq++) { // Ne
    CF_Misc My_CF_Misc(Ne);
    My_CF_Misc.initialize(Maxn,twoq,p,"JK_pos");
    for(int nu=0;nu<Maxn;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|

	INFO("-------------------");
	INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);
	
	{
	  //Now choose north pole
	  double polar=2*pi*rangen.Random();
	  double azimuthal=0.0;
	  CD lu=get_lu(polar,azimuthal);
	  CD lv=get_lv(polar,azimuthal);
	  CAPTURE(polar);
	  CAPTURE(azimuthal);
	  CAPTURE(lu);
	  CAPTURE(lv);
	  CD Psi=My_CF_Misc.log_UP_CF_orbital(mpq,nu,lu,lv);
	  CD Psi2=Naive_UP_CF_orbital(mpq,nu,lu,lv,twoq);
	  test_report_two_psi(exp(Psi),Psi2,mpq,twoq,nu,Ne);
	}

	{
	  //Now choose south pole
	  double polar=2*pi*rangen.Random();
	  double azimuthal=pi;
	  CAPTURE(polar);
	  CAPTURE(azimuthal);
	  CD lu=get_lu(polar,azimuthal);
	  CD lv=get_lv(polar,azimuthal);
	  CAPTURE(lu);
	  CAPTURE(lv);
	  lu=log(0.0)+iunit*lu.imag();
	  lv=iunit*iunit*lv.imag();
	  CAPTURE(lu);
	  CAPTURE(lv);
	  CD Psi=My_CF_Misc.log_UP_CF_orbital(mpq,nu,lu,lv);
	  CD Psi2=Naive_UP_CF_orbital(mpq,nu,lu,lv,twoq);
	  test_report_two_psi(exp(Psi),Psi2,mpq,twoq,nu,Ne);
	}

       	{
	  //Now choose random number
	  double polar=2*pi*rangen.Random();
	  double azimuthal=pi*rangen.Random();
	  CD lu=get_lu(polar,azimuthal);
	  CD lv=get_lv(polar,azimuthal);
	  CAPTURE(polar);
	  CAPTURE(azimuthal);
	  CAPTURE(lu);
	  CAPTURE(lv);
	  CD Logsi=My_CF_Misc.log_UP_CF_orbital(mpq,nu,lu,lv);
	  CD Psi=My_CF_Misc.UP_CF_orbital(mpq,nu,lu,lv);
	  CD PsiNaive=Naive_UP_CF_orbital(mpq,nu,lu,lv,twoq);
	  test_report_two_psi(exp(Logsi),PsiNaive,mpq,twoq,nu,Ne);
	  test_report_two_psi(Psi,PsiNaive,mpq,twoq,nu,Ne);
	}
      }
    }      
  }
}


TEST_CASE( "Test that logarithmic Rev p=2 orbitals gives same as linear ones", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);
  
  int Ne=6;
  int Maxn=2;
  int twoq=4;
  double p2=2.0;
  
  CF_Misc My_CF_Misc_JK_rev_p2(Ne);
  My_CF_Misc_JK_rev_p2.initialize(Maxn,twoq,p2,"JK_neg");
  
  //Setup the coordinates
  VD PolarVector;PolarVector.resize(Ne);
  VD AzimuthalVector;AzimuthalVector.resize(Ne);
  VCD uv;uv.resize(2*Ne);
  for(int elec=0;elec<Ne;elec++){
    PolarVector[elec]=2*pi*rangen.Random();
    AzimuthalVector[elec]=pi*rangen.Random();
    
    uv[2*elec]  =get_u(PolarVector[elec],AzimuthalVector[elec]);
    uv[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector[elec]);
  }
  
  for(int elec=0;elec<Ne;elec++){
    Eigen::MatrixXcd logesp_p2=My_CF_Misc_JK_rev_p2.getlogesp(uv);
    CD lu =log( uv[2*elec]), lv =log( uv[2*elec+1]);
    
    for(int nu=0;nu<Maxn;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
	//Now choose the orbital
	INFO("-------------------");
	INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);
	
	CD LogPsi=My_CF_Misc_JK_rev_p2.log_rev_CF_JK_orbital_p2(mpq,nu,logesp_p2,elec,lu,lv);
	CAPTURE(LogPsi);
	CD Psi=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(mpq,nu,logesp_p2,elec,lu,lv);
	CAPTURE(Psi);
	CD PsiNaive=Naive_rev_CF_JK_orbital_p2(mpq,nu,logesp_p2,elec,lu,lv,Ne,twoq,p2);
	CAPTURE(PsiNaive);
	test_report_two_psi(exp(LogPsi),PsiNaive,mpq,twoq,nu,Ne);
	test_report_two_psi(Psi,PsiNaive,mpq,twoq,nu,Ne);
      }
    }
  }      
}
