#!/bin/bash

set -e
set -u

###MAKEDEP:../../Programs/Run_MC_sphere
###MAKEDEP:check_identical_vectors.py
###MAKEDEP:RotateSpinors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../Programs
dir_CFT=$(pwd)
echo $dir_CFT
MC_Sphere=$dir_CFT/Run_MC_sphere

Catalouge=$MyDir/Test_angular_mom
mkdir -p $Catalouge
cd $Catalouge

MCSAMPLES=100
Ne=5

pos_start=pos-here.hdf5
pos_shift=pos-shift.hdf5

WFOPTS="-wf CF -Ne $Ne -N $MCSAMPLES --CF:qp_num 1"

qp_pos_file=$Catalouge/qp_pos_file.txt
qp_pos_file_shift=$Catalouge/qp_pos_file2.txt
echo 1.2 2.3 >  $qp_pos_file
echo 1.2 2.4 >  $qp_pos_file_shift

###Run the Laughlin code one
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file -po $Catalouge/$pos_start -wo $Catalouge/wf.hdf5 -wg $Catalouge/weight.hdf5 -lp

### Run with the same positions again
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file -X -pi $Catalouge/$pos_start -wo $Catalouge/wf_same.hdf5

###Test that the wave functions are the same when recomputing
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_same.hdf5 loglog same same


python $MyDir/RotateSpinors.py -ci $Catalouge/$pos_start -co $Catalouge/$pos_shift --angle 0.1

$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file -X -pi $Catalouge/$pos_shift -wo $Catalouge/wf_shift_elec.hdf5
$MC_Sphere $WFOPTS --CF:qp_pos_file $qp_pos_file_shift -X -pi $Catalouge/$pos_shift -wo $Catalouge/wf_shift_elec_qp.hdf5


###Test that the wave functions are differnt if only the electrons are rotated
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_shift_elec.hdf5 loglog diff diff

###Test that the wave functions are proprotional after rotation
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_shift_elec_qp.hdf5 loglog same prop

###Cleaning afterwards
rm -r $Catalouge
