#!/bin/bash

set -e
set -u

###MAKEDEP:../../Programs/Run_MC_sphere
###MAKEDEP:check_identical_vectors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../Programs
dir_CFT=$(pwd)
echo $dir_CFT
MC_Sphere=$dir_CFT/Run_MC_sphere

Catalouge=$MyDir/Test_multiple_configs_rev_CF
mkdir -p $Catalouge
cd $Catalouge

MCSAMPLES=100
Ne=5

posfile=pos-here.hdf5
wfnfile1=wfnfile1.hdf5
wfnfile2=wfnfile2.hdf5
wfnfile12=wfnfile12.hdf5

WFOPTSL="-wf L -Ne $Ne -N $MCSAMPLES -dl 1 -th 10"
WFOPTS="-wf CF -Ne $Ne -N $MCSAMPLES --CF:qh_num 2 -R"

qh_pos_file_1=$Catalouge/qh_pos_file_1.txt
qh_pos_file_2=$Catalouge/qh_pos_file_2.txt
qh_pos_file_12=$Catalouge/qh_pos_file_12.txt
echo 1.2 0.4  3.2 4.5 >  $qh_pos_file_1
echo 2.4 2.4  1.7 4.9 >  $qh_pos_file_2
cat $qh_pos_file_1 > $qh_pos_file_12
cat $qh_pos_file_2 >> $qh_pos_file_12

###Run the Laughlin code one
$MC_Sphere $WFOPTSL -po $Catalouge/$posfile -wo $Catalouge/wf.hdf5 -wg $Catalouge/weight.hdf5

$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file_1 -X -pi $Catalouge/$posfile -wo $Catalouge/$wfnfile1
$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file_2 -X -pi $Catalouge/$posfile -wo $Catalouge/$wfnfile2
$MC_Sphere $WFOPTS --CF:qh_pos_file $qh_pos_file_12 -X -pi $Catalouge/$posfile -wo $Catalouge/$wfnfile12 -nc 2 ###Here we say that there are two configurations



###Test that the first row of wf12 is same as wf1 and second is the same as wf2.
python $MyDir/check_identical_vectors.py $Catalouge/$wfnfile1 $Catalouge/$wfnfile2 loglog diff diff
python $MyDir/check_identical_vectors.py $Catalouge/$wfnfile1 $Catalouge/$wfnfile12 loglog same same --use-col-in-1 1  --use-col-in-2 1
python $MyDir/check_identical_vectors.py $Catalouge/$wfnfile2 $Catalouge/$wfnfile12 loglog same same --use-col-in-1 1  --use-col-in-2 2


###Cleaning afterwards
rm -r $Catalouge
