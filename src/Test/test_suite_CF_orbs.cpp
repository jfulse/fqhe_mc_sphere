#include "catch.hpp"
#include "CF_Misc.h"
#include "sfmt.h"
#include "test_suite_Misc.h"

TEST_CASE( "Check CF angular momentum of UP CF orbitals", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int Ne=6;
  int n=3;
  int twoq=10;
  double p=1.0;
  CF_Misc My_CF_Misc(Ne);
  My_CF_Misc.initialize(n,twoq,p,"JK_pos");

  
  for(int nu=0;nu<n;nu++) { // "Internal" LL
    for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
      //Now choose the orbital
      INFO("-------------------");
      INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);
      double polar=2*pi*rangen.Random();
      double azimuthal=pi*rangen.Random();

      double shift=pi*rangen.Random();
      double polar2=polar+shift;
      double azimuthal3=azimuthal+shift;
      
      CD lu=get_lu(polar,azimuthal);
      CD lv=get_lv(polar,azimuthal);
      
      CD lu2=get_lu(polar2,azimuthal);
      CD lv2=get_lv(polar2,azimuthal);
      
      CD lu3=get_lu(polar,azimuthal3);
      CD lv3=get_lv(polar,azimuthal3);
      
      INFO("Polar ,azimuthal  ="<<polar<<" , "<<azimuthal);
      INFO("Polar2,azimuthal3 ="<<polar2<<" , "<<azimuthal3);
      INFO("shift  ="<<shift);

      INFO("lu ,lv ="<<lu<<" , "<<lv);
      INFO("lu2,lv2="<<lu2<<" , "<<lv2);
      INFO("lu3,lv3="<<lu3<<" , "<<lv3);

      CD Psi=My_CF_Misc.UP_CF_orbital(mpq,nu,lu,lv);
      CD Psi2=My_CF_Misc.UP_CF_orbital(mpq,nu,lu2,lv2);
      CD Psi3=My_CF_Misc.UP_CF_orbital(mpq,nu,lu3,lv3);


      test_report_three_psi(Psi,Psi2,Psi3,shift,mpq,twoq,nu,Ne);
    }
  }      
}



TEST_CASE( "Check CF angular momentum of JK CF orbitals", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int Ne=6;
  int n=2;
  int twoq=4;
  double p1=1.0;
  double p2=2.0;
  CF_Misc My_CF_Misc_JK(Ne);
  My_CF_Misc_JK.initialize(n,twoq,p1,"JK_pos");

  //Setup the coordinates
  VD PolarVector;PolarVector.resize(Ne);
  VD AzimuthalVector;AzimuthalVector.resize(Ne);
  VD PolarVector2;PolarVector2.resize(Ne);
  VD AzimuthalVector3;AzimuthalVector3.resize(Ne);
  VCD uv;uv.resize(2*Ne);
  VCD uv2;uv2.resize(2*Ne);
  VCD uv3;uv3.resize(2*Ne);
  double shift=pi*rangen.Random();
  for(int elec=0;elec<Ne;elec++){
    PolarVector[elec]=2*pi*rangen.Random();
    PolarVector2[elec]=PolarVector[elec]+shift;
    AzimuthalVector[elec]=pi*rangen.Random();
    AzimuthalVector3[elec]=AzimuthalVector[elec]+shift;

    uv[2*elec]  =get_u(PolarVector[elec],AzimuthalVector[elec]);
    uv[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector[elec]);

    uv2[2*elec]  =get_u(PolarVector2[elec],AzimuthalVector[elec]);
    uv2[2*elec+1]=get_v(PolarVector2[elec],AzimuthalVector[elec]);

    uv3[2*elec]  =get_u(PolarVector[elec],AzimuthalVector3[elec]);
    uv3[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector3[elec]);
  }

  for(int elec=0;elec<Ne;elec++){
    Eigen::MatrixXcd P=My_CF_Misc_JK.createP(elec, uv);
    Eigen::MatrixXcd P2=My_CF_Misc_JK.createP(elec, uv2);
    Eigen::MatrixXcd P3=My_CF_Misc_JK.createP(elec, uv3);

    CD lu =log( uv[2*elec]), lv =log( uv[2*elec+1]);
    CD lu2=log(uv2[2*elec]), lv2=log(uv2[2*elec+1]);
    CD lu3=log(uv3[2*elec]), lv3=log(uv3[2*elec+1]);
    
    for(int nu=0;nu<n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
	//Now choose the orbital
	INFO("-------------------");
	INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);

	CAPTURE(shift);
	
	CD Psi=My_CF_Misc_JK.CF_orbital(mpq,nu,P,lu,lv);
	CD Psi2=My_CF_Misc_JK.CF_orbital(mpq,nu,P2,lu2,lv2);
	CD Psi3=My_CF_Misc_JK.CF_orbital(mpq,nu,P3,lu3,lv3);

	test_report_three_psi(Psi,Psi2,Psi3,shift,mpq,twoq,nu,Ne);
      }
    }
  }      
}


TEST_CASE( "Check CF angular momentum of REV CF p=1 orbitals", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int Ne=6;
  int n=2;
  int twoq=4;
  double p1=1.0;
  double p2=2.0;
  CF_Misc My_CF_Misc_JK_rev(Ne);
  My_CF_Misc_JK_rev.initialize(n,twoq,p1,"JK_neg");

  //Setup the coordinates
  VD PolarVector;PolarVector.resize(Ne);
  VD AzimuthalVector;AzimuthalVector.resize(Ne);
  VD PolarVector2;PolarVector2.resize(Ne);
  VD AzimuthalVector3;AzimuthalVector3.resize(Ne);
  VCD uv;uv.resize(2*Ne);
  VCD uv2;uv2.resize(2*Ne);
  VCD uv3;uv3.resize(2*Ne);
  double shift=pi*rangen.Random();
  for(int elec=0;elec<Ne;elec++){
    PolarVector[elec]=2*pi*rangen.Random();
    PolarVector2[elec]=PolarVector[elec]+shift;
    AzimuthalVector[elec]=pi*rangen.Random();
    AzimuthalVector3[elec]=AzimuthalVector[elec]+shift;

    uv[2*elec]  =get_u(PolarVector[elec],AzimuthalVector[elec]);
    uv[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector[elec]);

    uv2[2*elec]  =get_u(PolarVector2[elec],AzimuthalVector[elec]);
    uv2[2*elec+1]=get_v(PolarVector2[elec],AzimuthalVector[elec]);

    uv3[2*elec]  =get_u(PolarVector[elec],AzimuthalVector3[elec]);
    uv3[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector3[elec]);
  }

  for(int elec=0;elec<Ne;elec++){
    Eigen::MatrixXcd logesp=My_CF_Misc_JK_rev.getlogesp(uv);
    Eigen::MatrixXcd logesp_2=My_CF_Misc_JK_rev.getlogesp(uv2);
    Eigen::MatrixXcd logesp_3=My_CF_Misc_JK_rev.getlogesp(uv3);

    CD lu =log( uv[2*elec]), lv =log( uv[2*elec+1]);
    CD lu2=log(uv2[2*elec]), lv2=log(uv2[2*elec+1]);
    CD lu3=log(uv3[2*elec]), lv3=log(uv3[2*elec+1]);
    
    for(int nu=0;nu<n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
	//Now choose the orbital
	INFO("-------------------");
	INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);

	CAPTURE(shift);
	
	CD Psi=My_CF_Misc_JK_rev.rev_CF_JK_orbital(mpq,nu,logesp,elec,lu,lv);
	CD Psi2=My_CF_Misc_JK_rev.rev_CF_JK_orbital(mpq,nu,logesp_2,elec,lu2,lv2);
	CD Psi3=My_CF_Misc_JK_rev.rev_CF_JK_orbital(mpq,nu,logesp_3,elec,lu3,lv3);

	test_report_three_psi(Psi,Psi2,Psi3,shift,-mpq,-twoq,nu,Ne);
      }
    }
  }      
}



TEST_CASE( "Check CF angular momentum of REV CF p=2 orbitals", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int Ne=6;
  int n=2;
  int twoq=4;
  double p1=1.0;
  double p2=2.0;
  CF_Misc My_CF_Misc_JK_rev_p2(Ne);
  My_CF_Misc_JK_rev_p2.initialize(n,twoq,p2,"JK_neg");

  //Setup the coordinates
  VD PolarVector;PolarVector.resize(Ne);
  VD AzimuthalVector;AzimuthalVector.resize(Ne);
  VD PolarVector2;PolarVector2.resize(Ne);
  VD AzimuthalVector3;AzimuthalVector3.resize(Ne);
  VCD uv;uv.resize(2*Ne);
  VCD uv2;uv2.resize(2*Ne);
  VCD uv3;uv3.resize(2*Ne);
  double shift=pi*rangen.Random();
  for(int elec=0;elec<Ne;elec++){
    PolarVector[elec]=2*pi*rangen.Random();
    PolarVector2[elec]=PolarVector[elec]+shift;
    AzimuthalVector[elec]=pi*rangen.Random();
    AzimuthalVector3[elec]=AzimuthalVector[elec]+shift;

    uv[2*elec]  =get_u(PolarVector[elec],AzimuthalVector[elec]);
    uv[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector[elec]);

    uv2[2*elec]  =get_u(PolarVector2[elec],AzimuthalVector[elec]);
    uv2[2*elec+1]=get_v(PolarVector2[elec],AzimuthalVector[elec]);

    uv3[2*elec]  =get_u(PolarVector[elec],AzimuthalVector3[elec]);
    uv3[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector3[elec]);
  }

  for(int elec=0;elec<Ne;elec++){
    Eigen::MatrixXcd logesp_p2=My_CF_Misc_JK_rev_p2.getlogesp(uv);
    Eigen::MatrixXcd logesp_p2_2=My_CF_Misc_JK_rev_p2.getlogesp(uv2);
    Eigen::MatrixXcd logesp_p2_3=My_CF_Misc_JK_rev_p2.getlogesp(uv3);

    CD lu =log( uv[2*elec]), lv =log( uv[2*elec+1]);
    CD lu2=log(uv2[2*elec]), lv2=log(uv2[2*elec+1]);
    CD lu3=log(uv3[2*elec]), lv3=log(uv3[2*elec+1]);
    
    for(int nu=0;nu<n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
	//Now choose the orbital
	INFO("-------------------");
	INFO("twoq,nu, mpq:"<<twoq<<" , "<<nu<<" , "<<mpq);

	CAPTURE(shift);
	
	CD Psi=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(mpq,nu,logesp_p2,elec,lu,lv);
	CD Psi2=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(mpq,nu,logesp_p2_2,elec,lu2,lv2);
	CD Psi3=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(mpq,nu,logesp_p2_3,elec,lu3,lv3);

	test_report_three_psi(Psi,Psi2,Psi3,shift,-mpq,-twoq,nu,Ne);
      }
    }
  }      
}



TEST_CASE( "Check rot invariance of REV CF p=2 orbitals with 1 qh", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int Ne=6;
  int n=1;
  int nu=n-1;
  int Nqh=1;
  int twoq=Ne+Nqh-1;
  double p2=2.0;
  CF_Misc My_CF_Misc_JK_rev_p2(Ne);
  My_CF_Misc_JK_rev_p2.initialize(n,twoq,p2,"JK_neg");

  //Setup the coordinates
  VD PolarVector;PolarVector.resize(Ne);
  VD AzimuthalVector;AzimuthalVector.resize(Ne);
  VD PolarVector2;PolarVector2.resize(Ne);
  VD AzimuthalVector3;AzimuthalVector3.resize(Ne);
  VD QHPolarVector;QHPolarVector.resize(Nqh);
  VD QHAzimuthalVector;QHAzimuthalVector.resize(Nqh);
  VD QHPolarVector2;QHPolarVector2.resize(Nqh);
  VD QHAzimuthalVector3;QHAzimuthalVector3.resize(Nqh);
 
  VCD uv;uv.resize(2*Ne);
  VCD uv2;uv2.resize(2*Ne);
  VCD uv3;uv3.resize(2*Ne);
  VCD QHuv;QHuv.resize(2*Nqh);
  VCD QHuv2;QHuv2.resize(2*Nqh);
  VCD QHuv3;QHuv3.resize(2*Nqh);


  double shift=pi*rangen.Random();
  for(int elec=0;elec<Ne;elec++){
    PolarVector[elec]=2*pi*rangen.Random();
    PolarVector2[elec]=PolarVector[elec]+shift;
    AzimuthalVector[elec]=pi*rangen.Random();
    AzimuthalVector3[elec]=AzimuthalVector[elec]+shift;

    uv[2*elec]  =get_u(PolarVector[elec],AzimuthalVector[elec]);
    uv[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector[elec]);

    uv2[2*elec]  =get_u(PolarVector2[elec],AzimuthalVector[elec]);
    uv2[2*elec+1]=get_v(PolarVector2[elec],AzimuthalVector[elec]);

    uv3[2*elec]  =get_u(PolarVector[elec],AzimuthalVector3[elec]);
    uv3[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector3[elec]);
  }

  for(int elec=0;elec<Nqh;elec++){
    QHPolarVector[elec]=2*pi*rangen.Random();
    QHPolarVector2[elec]=QHPolarVector[elec]+shift;
    QHAzimuthalVector[elec]=pi*rangen.Random();
    QHAzimuthalVector3[elec]=QHAzimuthalVector[elec]+shift;
    QHuv[2*elec]  =get_u(QHPolarVector[elec],QHAzimuthalVector[elec]);
    QHuv[2*elec+1]=get_v(QHPolarVector[elec],QHAzimuthalVector[elec]);
    QHuv2[2*elec]  =get_u(QHPolarVector2[elec],QHAzimuthalVector[elec]);
    QHuv2[2*elec+1]=get_v(QHPolarVector2[elec],QHAzimuthalVector[elec]);
    QHuv3[2*elec]  =get_u(QHPolarVector[elec],QHAzimuthalVector3[elec]);
    QHuv3[2*elec+1]=get_v(QHPolarVector[elec],QHAzimuthalVector3[elec]);
  }

  Eigen::MatrixXcd logesp_p2=My_CF_Misc_JK_rev_p2.getlogesp(uv);
  Eigen::MatrixXcd logesp_p2_2=My_CF_Misc_JK_rev_p2.getlogesp(uv2);
  Eigen::MatrixXcd logesp_p2_3=My_CF_Misc_JK_rev_p2.getlogesp(uv3);

  //Construct slices through the CF determinant by multiplying
  //1 UP wfn with Ne wfns, skipping the inted of the 1 PU wfn...

  //Loop over possible mpqs
  for(int mpq=0;mpq<=twoq;mpq++) { // m+|q|
    CAPTURE(mpq);
    CD QHlu =log( QHuv[2*0]), QHlv =log( QHuv[2*0+1]);
    CD QHlu2=log(QHuv2[2*0]), QHlv2=log(QHuv2[2*0+1]);
    CD QHlu3=log(QHuv3[2*0]), QHlv3=log(QHuv3[2*0+1]);
    
    CD Psi=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq,nu,QHlu,QHlv));
    CD Psi2=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq,nu,QHlu2,QHlv2));
    CD Psi3=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq,nu,QHlu3,QHlv3));
    //Loop over all the electrons
    int stepup=0;
    for(int elec=0;elec<Ne;elec++){
      if(elec+stepup==mpq){stepup++;}
      int empq=elec+stepup;
      CD lu =log( uv[2*elec]), lv =log( uv[2*elec+1]);
      CD lu2=log(uv2[2*elec]), lv2=log(uv2[2*elec+1]);
      CD lu3=log(uv3[2*elec]), lv3=log(uv3[2*elec+1]);
      CAPTURE(elec);
      //Now choose the orbital
      INFO("-------------------");
      Psi*=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(empq,nu,logesp_p2,elec,lu,lv);
      Psi2*=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(empq,nu,logesp_p2_2,elec,lu2,lv2);
      Psi3*=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(empq,nu,logesp_p2_3,elec,lu3,lv3);
    }      
    test_report_three_psi(Psi,Psi2,Psi3,shift,0,0,nu,Ne);
  }
}


TEST_CASE( "Check rot invariance of REV CF p=2 orbitals with 2 qh", "[orbitals]" ) {
  std::random_device rd;
  int sd=rd();
  INFO("Seed is:"<<to_string(sd));
  CRandomSFMT rangen(0);
  rangen.RandomInit(sd);

  int Ne=6;
  int n=1;
  int nu=n-1;
  int Nqh=2;
  int twoq=Ne+Nqh-1;
  double p2=2.0;
  CF_Misc My_CF_Misc_JK_rev_p2(Ne);
  My_CF_Misc_JK_rev_p2.initialize(n,twoq,p2,"JK_neg");

  //Setup the coordinates
  VD PolarVector;PolarVector.resize(Ne);
  VD AzimuthalVector;AzimuthalVector.resize(Ne);
  VD PolarVector2;PolarVector2.resize(Ne);
  VD AzimuthalVector3;AzimuthalVector3.resize(Ne);
  VD QHPolarVector;QHPolarVector.resize(Nqh);
  VD QHAzimuthalVector;QHAzimuthalVector.resize(Nqh);
  VD QHPolarVector2;QHPolarVector2.resize(Nqh);
  VD QHAzimuthalVector3;QHAzimuthalVector3.resize(Nqh);
 
  VCD uv;uv.resize(2*Ne);
  VCD uv2;uv2.resize(2*Ne);
  VCD uv3;uv3.resize(2*Ne);
  VCD QHuv;QHuv.resize(2*Nqh);
  VCD QHuv2;QHuv2.resize(2*Nqh);
  VCD QHuv3;QHuv3.resize(2*Nqh);


  double shift=pi*rangen.Random();
  for(int elec=0;elec<Ne;elec++){
    PolarVector[elec]=2*pi*rangen.Random();
    PolarVector2[elec]=PolarVector[elec]+shift;
    AzimuthalVector[elec]=pi*rangen.Random();
    AzimuthalVector3[elec]=AzimuthalVector[elec]+shift;

    uv[2*elec]  =get_u(PolarVector[elec],AzimuthalVector[elec]);
    uv[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector[elec]);

    uv2[2*elec]  =get_u(PolarVector2[elec],AzimuthalVector[elec]);
    uv2[2*elec+1]=get_v(PolarVector2[elec],AzimuthalVector[elec]);

    uv3[2*elec]  =get_u(PolarVector[elec],AzimuthalVector3[elec]);
    uv3[2*elec+1]=get_v(PolarVector[elec],AzimuthalVector3[elec]);
  }

  for(int elec=0;elec<Nqh;elec++){
    QHPolarVector[elec]=2*pi*rangen.Random();
    QHPolarVector2[elec]=QHPolarVector[elec]+shift;
    QHAzimuthalVector[elec]=pi*rangen.Random();
    QHAzimuthalVector3[elec]=QHAzimuthalVector[elec]+shift;
    QHuv[2*elec]  =get_u(QHPolarVector[elec],QHAzimuthalVector[elec]);
    QHuv[2*elec+1]=get_v(QHPolarVector[elec],QHAzimuthalVector[elec]);
    QHuv2[2*elec]  =get_u(QHPolarVector2[elec],QHAzimuthalVector[elec]);
    QHuv2[2*elec+1]=get_v(QHPolarVector2[elec],QHAzimuthalVector[elec]);
    QHuv3[2*elec]  =get_u(QHPolarVector[elec],QHAzimuthalVector3[elec]);
    QHuv3[2*elec+1]=get_v(QHPolarVector[elec],QHAzimuthalVector3[elec]);
  }

  Eigen::MatrixXcd logesp_p2=My_CF_Misc_JK_rev_p2.getlogesp(uv);
  Eigen::MatrixXcd logesp_p2_2=My_CF_Misc_JK_rev_p2.getlogesp(uv2);
  Eigen::MatrixXcd logesp_p2_3=My_CF_Misc_JK_rev_p2.getlogesp(uv3);

  //Construct slices through the CF determinant by multiplying
  //1 UP wfn with Ne wfns, skipping the inted of the 1 PU wfn...

  //Loop over possible mpqs
  for(int mpq1=0;mpq1<=twoq;mpq1++) { // m+|q|
    for(int mpq2=(mpq1+1);mpq2<=twoq;mpq2++) { // m+|q|
      CAPTURE(mpq1);
      CAPTURE(mpq2);
      CD QH1lu =log( QHuv[2*0]), QH1lv =log( QHuv[2*0+1]);
      CD QH1lu2=log(QHuv2[2*0]), QH1lv2=log(QHuv2[2*0+1]);
      CD QH1lu3=log(QHuv3[2*0]), QH1lv3=log(QHuv3[2*0+1]);
      CD QH2lu =log( QHuv[2*1]), QH2lv =log( QHuv[2*1+1]);
      CD QH2lu2=log(QHuv2[2*1]), QH2lv2=log(QHuv2[2*1+1]);
      CD QH2lu3=log(QHuv3[2*1]), QH2lv3=log(QHuv3[2*1+1]);
      
      
      CD Psi=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq1,nu,QH1lu,QH1lv));
      CD Psi2=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq1,nu,QH1lu2,QH1lv2));
      CD Psi3=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq1,nu,QH1lu3,QH1lv3));
      Psi*=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq2,nu,QH2lu,QH2lv));
      Psi2*=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq2,nu,QH2lu2,QH2lv2));
      Psi3*=conj(My_CF_Misc_JK_rev_p2.UP_CF_orbital(mpq2,nu,QH2lu3,QH2lv3));

      //Loop over all the electrons
      int stepup=0;
      for(int elec=0;elec<Ne;elec++){
	if(elec+stepup==mpq1){stepup++;}
	if(elec+stepup==mpq2){stepup++;}
	int empq=elec+stepup;
	CD lu =log( uv[2*elec]), lv =log( uv[2*elec+1]);
	CD lu2=log(uv2[2*elec]), lv2=log(uv2[2*elec+1]);
	CD lu3=log(uv3[2*elec]), lv3=log(uv3[2*elec+1]);
	CAPTURE(elec);
	//Now choose the orbital
	Psi*=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(empq,nu,logesp_p2,elec,lu,lv);
	Psi2*=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(empq,nu,logesp_p2_2,elec,lu2,lv2);
	Psi3*=My_CF_Misc_JK_rev_p2.rev_CF_JK_orbital_p2(empq,nu,logesp_p2_3,elec,lu3,lv3);
      }      
      test_report_three_psi(Psi,Psi2,Psi3,shift,0,0,nu,Ne);
    }
  }
}
