#include "catch.hpp"
#include "test_suite_Misc.h"

CD get_u(double polar,double azimutal){
  double cp=cos(polar/2.0), sp=sin(polar/2.0);
  return cos(azimutal/2.0)*CD(cp, sp);
}

CD get_v(double polar,double azimutal){
  double cp=cos(polar/2.0), sp=sin(polar/2.0);
  return sin(azimutal/2.0)*CD(cp, -sp);
}

CD get_lu(double polar,double azimutal){return log(get_u(polar,azimutal));}
CD get_lv(double polar,double azimutal){return log(get_v(polar,azimutal));}


double mod2pi(double value){return fmod(fmod(value,twopi)+twopi*1.5,twopi)-twopi*.5;}


void test_report_two_log_psi(CD Psi1,CD Psi2)
{
  CAPTURE(Psi1);
  CAPTURE(Psi2);
  double Scale1=Psi1.real();
  double Scale2=Psi2.real();
  double Phase1=Psi1.imag();
  double Phase2=Psi2.imag();
  CAPTURE(Scale1-Scale2);
  CAPTURE(Phase1);
  CAPTURE(Phase2);
  CAPTURE(Phase1/pi);
  CAPTURE(Phase2/pi);
  double DiffPhase=Phase2-Phase1;
  CAPTURE(DiffPhase);
  
  REQUIRE(0.0 == Approx(Scale1-Scale2).margin(0.0000001));
  REQUIRE(0.0 == Approx(mod2pi(DiffPhase)).margin(0.0000001));
}


void test_report_two_linear_psi(CD Psi,CD Psi2)
{
  CAPTURE(Psi);
  CAPTURE(Psi2);
  CAPTURE(abs(Psi));
  CAPTURE(abs(Psi2));
  CAPTURE(abs(Psi)/abs(Psi2));
  double Phase =log(Psi ).imag();
  double Phase2=log(Psi2).imag();
  CAPTURE(Phase);
  CAPTURE(Phase2);
  CAPTURE(Phase/pi);
  CAPTURE(Phase2/pi);
  double DiffPhase2=Phase2-Phase;
  CAPTURE(DiffPhase2);
  
  REQUIRE(abs(Psi) == Approx(abs(Psi2)).epsilon(0.0000001));
  REQUIRE(0.0 == Approx(mod2pi(DiffPhase2)).margin(0.0000001));
}



void test_report_two_psi(CD Psi,CD Psi2,int mpq,int twoq,int nu,int Ne)
{
  CAPTURE(Ne);
  CAPTURE(nu);
  CAPTURE(mpq);
  CAPTURE(twoq);
 
  CAPTURE(Psi);
  CAPTURE(Psi2);
  CAPTURE(abs(Psi));
  CAPTURE(abs(Psi2));
  CAPTURE(abs(Psi)/abs(Psi2));
  double Phase =log(Psi ).imag();
  double Phase2=log(Psi2).imag();
  CAPTURE(Phase);
  CAPTURE(Phase2);
  CAPTURE(Phase/pi);
  CAPTURE(Phase2/pi);
  double DiffPhase2=Phase2-Phase;
  CAPTURE(DiffPhase2);
  
  REQUIRE(abs(Psi) == Approx(abs(Psi2)).epsilon(0.0000001));
  REQUIRE(0.0 == Approx(mod2pi(DiffPhase2)).margin(0.0000001));
}


void test_report_three_psi(CD Psi,CD Psi2,CD Psi3,double shift,int mpq,int twoq,int nu,int Ne)
{
  CAPTURE(Ne);
  CAPTURE(nu);
  CAPTURE(mpq);
  CAPTURE(twoq);
  double mvalue=mpq-twoq/2.0;
  
  CAPTURE(Psi);
  CAPTURE(Psi2);
  CAPTURE(Psi3);
  CAPTURE(abs(Psi));
  CAPTURE(abs(Psi2));
  CAPTURE(abs(Psi3));
  double Phase =log(Psi ).imag();
  double Phase2=log(Psi2).imag();
  double Phase3=log(Psi3).imag();
  CAPTURE(Phase);
  CAPTURE(Phase2);
  CAPTURE(Phase3);
  CAPTURE(Phase/pi);
  CAPTURE(Phase2/pi);
  CAPTURE(Phase3/pi);
  double DiffPhase2=Phase2-Phase;
  double DiffPhase3=Phase3-Phase;
  CAPTURE(DiffPhase2);
  CAPTURE(DiffPhase3);
  CAPTURE(DiffPhase2/shift);
  CAPTURE(DiffPhase3/shift);
  CAPTURE(mvalue);
  
  REQUIRE(abs(Psi) == Approx(abs(Psi2)).epsilon(0.0000001));
  REQUIRE(abs(Psi) != Approx(abs(Psi3)).epsilon(0.0000001));
  REQUIRE(0.0 == Approx(mod2pi(DiffPhase2-mvalue*shift)).margin(0.0000001));
}


void test_report_three_psi_log(CD Psi,CD Psi2,CD Psi3,double shift,int mpq,
			       int twoq,int nu,int Ne,double Precition)
{
  CAPTURE(Ne);
  CAPTURE(nu);
  CAPTURE(mpq);
  CAPTURE(twoq);
  double mvalue=mpq-twoq/2.0;
  
  CAPTURE(Psi);
  CAPTURE(Psi2);
  CAPTURE(Psi3);
  double Phase =Psi.imag();
  double Phase2=Psi2.imag();
  double Phase3=Psi3.imag();
  CAPTURE(Phase);
  CAPTURE(Phase2);
  CAPTURE(Phase3);
  CAPTURE(Phase/pi);
  CAPTURE(Phase2/pi);
  CAPTURE(Phase3/pi);
  double DiffPhase2=Phase2-Phase;
  double DiffPhase3=Phase3-Phase;
  CAPTURE(DiffPhase2);
  CAPTURE(DiffPhase3);
  CAPTURE(DiffPhase2/shift);
  CAPTURE(DiffPhase3/shift);
  CAPTURE(mvalue);
  
  REQUIRE(Psi.real() == Approx(Psi2.real()).epsilon(Precition));
  REQUIRE(Psi.real() != Approx(Psi3.real()).epsilon(Precition));
  REQUIRE(0.0 == Approx(mod2pi(DiffPhase2-mvalue*shift)).margin(Precition));
}


CD Naive_UP_CF_orbital(int mpq,int nu,CD logu, CD logv,int twoq)
//This is the implementation of the UP CF wave function as of 2018-03-29
//We use it to compute values against
{
  if(isinf(logv.real())){ //Special case when wfn is on north pole
    int twos=2*nu+twoq-mpq; //Only certain values of s give normalized wfns
    if(twos>=0 && twos<=2*nu && (twos%2 == 0) ) {
      int s=twos/2;
      CD loguvpow=(double)s*conj(logu)+(double)(s+mpq)*logu;
      double Norms=UP_CF_norm(nu,twoq,mpq)
	+logbin(nu, s)+logbin(twoq+nu, mpq+s);
      CD ssum=(1.0-2.0*((s+nu)&1))*exp(Norms+loguvpow);
      return ssum;
    } else { return 0.0;}
  } else if(isinf(logu.real())){ //Special case when wfn is on south pole
    int twos=-mpq; //Only certain values of s give normalized wfns
    if(twos>=0 && twos<=2*nu && (twos%2 == 0) ) {
      int s=twos/2;
      CD loguvpow=(double)(nu-s)*conj(logv)+(double)(nu-s+twoq-mpq)*logv;
      double Norms=UP_CF_norm(nu,twoq,mpq)
	+logbin(nu, s)+logbin(twoq+nu, mpq+s);
      CD ssum=(1.0-2.0*((s+nu)&1))*exp(Norms+loguvpow);
      return ssum;
    } else { return 0.0;}
  } else {
    CD ssum=0;
    for(int s=(mpq<0)*(-mpq);s<=nu+(mpq>twoq)*(twoq-mpq);s++) {
      CD loguvpow=(double)s*conj(logu)
	+(double)(s+mpq)*logu
	+(double)(nu-s)*conj(logv)
	+(double)(nu-s+twoq-mpq)*logv;
      double Norms=UP_CF_norm(nu,twoq,mpq)
	+logbin(nu, s)+logbin(twoq+nu, mpq+s);
      ssum+=(1.0-2.0*((s+nu)&1))*exp(Norms+loguvpow);
    }     
    return ssum;
  }
}



CD Naive_rev_CF_JK_orbital_p2(int mpq,int nu,
			  Eigen::MatrixXcd logesp, int i,CD logu, CD logv, int Ne,int twoq,double  p)
{
  int Nphi=2*p*(Ne-1.0)-twoq;
  int Nm1=Ne-1;
  int twoNm2=2*Nm1;
  CD CFmat=0; // initialize state (i, state)
  for(int s=(mpq<0)*(-mpq);s<=nu+(mpq>twoq)*(twoq-mpq);s++) {
    CD tsum=0.0; // initialize t-sum
    for(int t=0;t<Ne;t++) {
      for(int t2=0;t2<Ne;t2++) {
	int tt2=t+t2;
	int tmp=twoNm2-tt2+mpq-twoq;
	if(mpq+s<=tt2 && tt2<=twoNm2+mpq-twoq-nu+s) {
	  tsum+=(1.0-2.0*(tt2&1))*exp(rev_CF_norm(Ne,Nphi,p ,nu,twoq,mpq)
				      +logbin(nu, s)
				      +logbin(twoq+nu, mpq+s)
				      +logesp(i, t)+logesp(i, t2)
				      +logfact(twoNm2-tt2)-logfact(tmp-nu+s)
				      +logfact(tt2)-logfact(tt2-mpq-s)
				      +(double)tmp*logv
				      +(double)(tt2-mpq)*logu);
	  //cout<<"tdelta:"<<tdelta<<endl;
	}
      }// t2 end
      //cout<<"tdelta:"<<tdelta<<endl;
    } // t end
    CFmat+=(1.0-2.0*((s+nu)&1))*tsum;
  }// s end
  return CFmat;
}


stringstream capture_matrix(Eigen::MatrixXcd Mat,int Dim) {
  stringstream os;
  if(Dim==1) { os<<"("<<Mat(0,0)<<")"<<endl;}
  else {
    for(int i=0;i<Dim;i++) {
      if(i==0) { os<<"/";}
      else if(i==(Dim-1)) { os<<"\\";}
      else { os<<"|";}
      for(int j=0;j<Dim;j++) {
	os<<Mat(i,j)<<" ";
      }
      if(i==0) { os<<"\\"<<endl;}
      else if(i==(Dim-1)) { os<<"/"<<endl;}
      else { os<<"|"<<endl;}
    }
  }
  return os;
}
