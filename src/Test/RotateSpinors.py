#!/usr/bin/python
from __future__ import print_function

""" FQHTorusMoveCoordinates.py: Core that moves the torus corinates a sertain distance. """

__author__      = "Mikael Freming"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"



import numpy as np
import argparse
import h5py
import sys

def __main__():

    parser = argparse.ArgumentParser(description="Move the Coordinates a certain distance in x,y direction.")
    parser.add_argument("--coord-in","-ci", type=str, help="Input file for x,y coordinates",required=True)
    parser.add_argument("--coord-out","-co", type=str, help="Output file for x,y coordinates",required=True)
    groupx=parser.add_mutually_exclusive_group()
    groupx.add_argument("--angle","-a", type=float, help="Angle with with to rotate the coordinates in the polar direction")
    groupx.add_argument("-a-list", type=str, help="Angle with with to rotate the coordinates in the polar direction for each particle.",nargs='+')
    groupy=parser.add_mutually_exclusive_group()
    groupy.add_argument("--azimutal", type=float, help="Angle with with to rotate the coordinates in the azimuthal direction")
    groupy.add_argument("-az-list", type=str, help="Angle with with to rotate the coordinates in the azimuthal direction for each particle.",nargs='+')
   
    Args=parser.parse_args()

    CoordFile_in=Args.coord_in
    CoordFile_out=Args.coord_out

    print('Read coordinates from file:',CoordFile_in)
    with h5py.File(CoordFile_in, 'r') as coord:
        positions=np.array(coord.get('positions'))

    (MC,TwoNe)=positions.shape
    angleshift=OptToSHift(Args.angle,Args.a_list,TwoNe/2,'polar angle')
    azimuthalshif=OptToSHift(Args.azimutal,Args.az_list,TwoNe/2,'aximuthal angle')
        
    positions[:,1::2]+=angleshift
    positions[:,0::2]+=azimuthalshif

    print('Saving new coordinates to file:',CoordFile_out)
    try:
        of = h5py.File(CoordFile_out, 'w')
    except IOError as ioerror:
        print(("ERROR: Problem writing to file " + CoordFile_out  + ': ' + str(ioerror)))
        sys.exit(-1)
    of.create_dataset("positions", data=positions)
    of.close()

def OptToSHift(x,x_list,Ne,Type):
    if x==None and x_list==None:
        xshift=0.0
    elif x!=None:
        xshift=x
    elif x_list!=None:
        print("ERROR: "+Type+"_list='"+x_list+"' not zero not implemented yet")
        sys.exit(-1)
    else:
        print("ERROR: This option should not happen")
        sys.exit(-1)
    return xshift

    
if __name__ == '__main__' :
    __main__()
