#!/bin/bash

set -e
set -u

test_dir=$1 ##First input is the test dir
test_rules=$2 ##Second input is the file containing test rules

for test_script in $( ls ${test_dir}/test_script_*.sh) ; do
    ##echo $test_script
    target_test=$(echo $test_script | sed "s|${test_dir}\/\(.*\)|${test_dir}\/\.\1|" )

    
    if grep -q '###MAKEDEP:' $test_script ; then ###IF statement to get around set -e
	DEPS=$(grep '###MAKEDEP:' $test_script | sed 's|###MAKEDEP:||' )
    else
	DEPS=""
    fi
    DEPSLIST=""
    for Dep in $DEPS ; do
	##echo Dep: $Dep
	if [ -f $Dep ] ; then
	    DEPSLIST="$DEPSLIST $Dep"
	else
	    if /usr/bin/which "$Dep" > /dev/null; then
		wichfile=$(/usr/bin/which "$Dep")
		##echo wichfile: $wichfile
		DEPSLIST="$DEPSLIST $wichfile"
	    else
		echo "WARNING: Program '$Dep' does not exist in path!" >&2
		echo "       Neither can 'which' figure out where it is!" >&2
		echo "       This file is required by:" >&2
		echo "       $test_script ">&2
		echo "       Hope it will be present when the test is run!" >&2
		DEPSLIST="$DEPSLIST $Dep"
	    fi
	fi	    
    done
    
    echo $target_test:$test_script $DEPSLIST $test_rules
    echo -e "\t-@echo ---------------------"
    echo -e "\t-@echo \"Running script $test_script\""
    echo -e "\t-@echo ---------------------"
    echo -e "\t./$test_script" 
    echo -e "\t-@echo ---------------------"
    echo -e "\t-@echo \" Test suscessfull\""
    echo -e "\t-@echo ---------------------"
    echo -e "\t-@echo test_succesfull > $target_test"
    echo 

done
