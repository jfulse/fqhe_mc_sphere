#!/bin/bash

set -e
set -u

###MAKEDEP:../../Programs/Run_MC_sphere
###MAKEDEP:check_identical_vectors.py
###MAKEDEP:RotateSpinors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../Programs
dir_CFT=$(pwd)
echo $dir_CFT
MC_Sphere=$dir_CFT/Run_MC_sphere

Catalouge=$MyDir/Test_angular_mom
mkdir -p $Catalouge
cd $Catalouge

N=100
Ne=6

pos_start=pos-here.hdf5
pos_shift=pos-shift.hdf5
pos_shift2=pos-shift2.hdf5

WFOPTS="-wf CF -n 2 -Ne $Ne -N $N -UP"

###Run the Laughlin code one
$MC_Sphere $WFOPTS -po $Catalouge/$pos_start -wo $Catalouge/wf.hdf5 -wg $Catalouge/weight.hdf5 --no-stab -th 10

### Run with the same positions again
$MC_Sphere $WFOPTS -X -pi $Catalouge/$pos_start -wo $Catalouge/wf_same.hdf5

###Test that the wave functions are the same when recomputing
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_same.hdf5 loglog same same


python $MyDir/RotateSpinors.py -ci $Catalouge/$pos_start -co $Catalouge/$pos_shift --angle 0.1
python $MyDir/RotateSpinors.py -ci $Catalouge/$pos_start -co $Catalouge/$pos_shift2 --azimutal 0.1

### Run with the positions shifted
$MC_Sphere $WFOPTS -X -pi $Catalouge/$pos_shift -wo $Catalouge/wf_shift.hdf5

$MC_Sphere $WFOPTS -X -pi $Catalouge/$pos_shift2 -wo $Catalouge/wf_shift2.hdf5

###Test that the wave functions are differnt anfter azimutal shifts
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_shift2.hdf5 loglog diff diff

###Test that the wave functions are proprotional after rotation
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_shift.hdf5 loglog same prop

###Cleaning afterwards
rm -r $Catalouge
