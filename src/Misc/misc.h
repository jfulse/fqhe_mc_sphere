/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
using namespace std;

#include <cstdlib> 
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <iomanip>
#include <string>
#include <vector>
#include <iterator>
#include <sstream>
#include <complex>
#include <stdexcept>

#ifndef MISC_H
#define MISC_H

const double pi=3.1415926535897932384;
const double twopi=6.2831853071795864769;
const std::complex<double> iunit=std::complex<double>(0.0, 1.0);

typedef complex<double> CD;
typedef vector<double> VD;

inline void imod(CD& c, double m) {c=CD(real(c), fmod(imag(c), m));}
inline double logfact(int n) {
	double lf=0;
	for(int i=2;i<=n;i++) lf+=log(i);
	return lf;
}

void error(string);
void warning(string);
bool isint(string);
bool isnumber(string);
bool isbool(string);
string CDstring(CD);
string getndigits(double, int);
void findmax(VD, double&, int&);
void findmin(VD, double&, int&);
bool double_equal(double, double, double);
bool CD_equal(double, double, double);
string pad_string(string, string, int);
double fact(double);
bool CD_equal(CD, CD, double);
string n2s(long long);
string n2s(int);
int pow2(int);

#endif
