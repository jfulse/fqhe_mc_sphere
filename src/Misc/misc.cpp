/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "misc.h"

void error(string msg) {
	//cerr << endl << "ERROR: " << msg << endl << endl;
	throw runtime_error(msg.c_str());
}

void warning(string msg) {
	cerr << endl << "WARNING: " << msg << endl << endl;
}

bool isint(string s) {
	string::const_iterator it = s.begin(); bool hase=false;
	if(!isdigit(*it)) {if(*it!='-') return false;}
	it++;
	while(it!=s.end()) {
		if (!(isdigit(*it))) return false;
		it++;
	}
	return true;
}

bool isbool(string s) {
	if(s=="True") return true;
	if(s=="False") return true;
	if(s=="true") return true;
	if(s=="false") return true;
	if(s=="1") return true;
	if(s=="0") return true;
	return false;	
}

bool isnumber(string s) {
	string::const_iterator it = s.begin();
	bool hasnumbers=false, hase=false, hasdot=false;
	if(!isdigit(*it)) {
		if(*it=='.') hasdot=true;
		else if(*it!='-') return false;
	}
	else hasnumbers=true; 
	it++;
	while(it!=s.end()) {
		if ((isdigit(*it))) hasnumbers=true;
		else if(*it=='e') {
			if(hase) return false;
			hase=true; 
			if(*(it+1)=='-') {if(it+2==s.end() || !isdigit(*(it+2))) return false; it++;}
			else if(it+1==s.end() || !isdigit(*(it+1))) return false;
		}
		else if(*it=='.') {
			if(hasdot) return false;
			else hasdot=true;
		}
		else return false;
		it++;
	}
	return hasnumbers;
}

string CDstring(CD c) {
	string s="";
	double N=abs(c);
	//if(abs(real(c)/N)<1e-12 && abs(imag(c)/N)<1e-12) return "0";
	if(abs(real(c)/N)>1e-12) s+=to_string(real(c));	
	if(abs(imag(c)/N)>1e-12) {
		if(imag(c)>0) {
			if(abs(real(c)/N)>1e-12) s+="+";
		}
		else s+="-";
		s+=to_string(abs(imag(c)))+"i";
	}
	if(s.length()<1) s="0";
	return s;
}

void findmax(VD vec_in, double& max, int& idx) {
	max=abs(vec_in[0]);
	for(idx=1;idx<vec_in.size();idx++) {
		if(abs(vec_in[idx])>max) max=abs(vec_in[idx]);
	}
}

void findmin(VD vec_in, double& min, int& idx) {
	min=abs(vec_in[0]);
	for(idx=1;idx<vec_in.size();idx++) {
		if(abs(vec_in[idx])<min) min=abs(vec_in[idx]);
	}
}

bool double_equal(double d1, double d2, double prec) {
	return (abs(d1)<prec && abs(d2)<prec) || abs(d1-d2)/min(abs(d1),abs(d2))<prec;
}

bool CD_equal(CD cd1, CD cd2, double prec) {
	double r1=real(cd1), i1=imag(cd1), r2=real(cd2), i2=imag(cd2);
	bool realeq=(abs(r1)<prec && abs(r2)<prec) || abs(r1-r2)/min(abs(r1),abs(r2))<prec;
	bool imageq=(abs(i1)<prec && abs(i2)<prec) || abs(i1-i2)/min(abs(i1),abs(i2))<prec;
	return realeq && imageq;
}

string pad_string(string line, string symbol, int totlength) {
	int l=line.length();
	string padded="";
	if(l<1) {
		for(int i=0;i<totlength;i++) padded+=symbol;
		return padded;
	}
	int n=totlength-l;
	if(n<0) n=0;
	for(int i=0;i<n/2-1;i++) padded+=symbol;
	if(n>1) padded+=" ";
	padded+=line;
	if(n>0)padded+=" ";
	for(int i=0;i<n-n/2-1;i++) padded+=symbol;
	return padded;
}

double fact(double i) {
	if ((int)i<i||i<-0.0) { cerr << "ERROR: " << i << " is not a positive integer" << endl << endl; return -1.0; }
	if (i==0.0||i==-0.0) return 1.0;
	else if (i == 1.0) return i;
	return (i * fact(i - 1.0));
}


string n2s(int n) {
	stringstream ss;
	if(abs(n)<1000) ss << n;
	else if (abs(n)<1e6) {
		if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs(n)<1e9) {
		if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else {
		if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	return ss.str();
}

string n2s(long long n) {
	stringstream ss;
	if(abs((double)n)<1000) ss << n;
	else if (abs((double)n)<1e6) {
		if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e9) {
		if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e12) {
		if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e15) {
		if((abs(n/1.0e12)-abs((int)(n/1e12)))/1.0e12<1e-13) ss << n/1e9 << "T";
		else if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e18) {
		if((abs(n/1.0e15)-abs((int)(n/1e15)))/1.0e15<1e-13) ss << n/1e15 << "Q";
		else if((abs(n/1.0e12)-abs((int)(n/1e12)))/1.0e12<1e-13) ss << n/1e12 << "T";
		else if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else ss << n;
	return ss.str();
}

int pow2(int p) {
  if (p == 0) return 1;
  if (p == 1) return 2;

  int tmp = pow2(p/2);
  if (p%2) return 2 * tmp * tmp;
  else return tmp * tmp;
}
