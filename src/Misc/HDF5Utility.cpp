/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "HDF5Utility.h"

int main(int argc, const char* argv[]) {

	try {
		clock_t starttime=clock();
		input *params=new input("_","");

		params->add_input("headline 1"," --- Tasks ---; ","description");
		params->add_input("--task,-tk", "Task to perform", "int", "1");
		params->add_input("task description", "1 -> Create permutation of positions;2 -> Test equality of columns [exp(z)];3 -> Translate to ASCII;4 -> Join files;5 -> Split file;6 -> Number of rows and columns in dataset;7 -> Write contents to screen;8 -> Multiply wavefunctions;9 -> Conjugate wavefunctions;10 -> Rotate azimutal angle (phi);11 -> Linear increase of column;12 -> Number of consecutive equal rows;","description");
		params->add_input("headline 2"," ;--- Options ---; ","description");
		params->add_input("--samples,-N", "Number of MC samples (0-> all)", "ll","0");
		params->add_input("--buffer-size", "Buffer size used for reading/writing", "int", "10000");
		params->add_input("--in-file,-i", "Input file (hdf5 format)", "string");
		params->add_input("--all-ins,-ins", "Several input files (hdf5 format)", "VS");
		params->add_input("--out-file,-o", "Output file (hdf5 format)", "string");
		params->add_input("--all-outs,-outs", "Several output files (hdf5 format)", "VS");
		params->add_input("--dataset,-d", "Name of hdf5 dataset", "string","wf_values");
		params->add_input("-Ne", "Number of electrons", "int", "6");
		params->add_input("--perm-vec,-pv", "Task 1: New particle indices in terms of old", "VI");
		params->add_input("--abs-value,-A", "Task 2: Only check absolute value", "bool");
		params->add_input("--mtca-format,-M", "Task 3: Mathematica matrix", "bool");
		params->add_input("--mtca-form-pos,-Mp", "Task 3: Mathematica position matrix", "bool");
		params->add_input("--precision,-p", "Task 7: Number of digits", "int","5");
		params->add_input("--separator,-sp", "Task 7: Separator", "char"," ");
		params->add_input("--imaginary-sym,-is", "Task 7: symbol for imaginary part", "string");
		params->add_input("--line-bracket,-lb", "Task 7: begin/end line brackets", "VS");
		params->add_input("--skip-rows,-s", "Task 7: Number of rows to skip", "ll");
		params->add_input("--column-no,-c", "Task 8,11: Column number", "int");
		params->add_input("--angle,-a", "Task 10: Rotation angle", "double");
		params->add_input("--step,-st", "Task 11: Linear step", "double");
		params->add_input("--initial-conf,-ic", "Task 11: Initial configuration", "VD");
		params->add_input("--quiet,-q", "Do not print extra information to screen", "bool");
		if(params->command_line_input(argc, argv)!=0) return 1;
		if(!params->check_integrity()) return 1;

		cout.precision(3);
		int tk=params->getint("-tk");
		bool verb=true;
		if(params->is_flagged("-q")) {
			verb=false;
			params->is_activated("-q");
		}

		switch(tk) {
			case 1: {
				int Ne=params->getint("-Ne");
				VI pv=params->getVI("-pv");
				long long N=params->getll("-N");
				string in=params->getstring("-i");
				string out=params->getstring("-o");
				if(verb) display_start(params, 60, "_", argv[0]);
				Permute(in, out, Ne, N, pv, verb);

			} break;
			case 2: {
				string in=params->getstring("-i");
				long long N=params->getll("-N");
				bool A=params->is_flagged("-A");
				if(verb) display_start(params, 60, "_", argv[0]);
				test_eq(in, N, A, verb);
			} break;
			case 3: {
				string in=params->getstring("-i");
				string out=params->getstring("-o");
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				bool M=params->is_flagged("-M");
				bool Mp=params->is_flagged("-Mp");
				if(verb) display_start(params, 60, "_", argv[0]);
				translate_to_ASCII(in, out, dataset, N, M, Mp, verb);
			} break;
			case 4: {
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				int bs=params->getint("--buffer-size");
				VS in=params->getVS("-ins");
				string out=params->getstring("-o");
				if(verb) display_start(params, 60, "_", argv[0]);
				Join(in, out, dataset, N, bs, verb);

			} break;
			case 5: {
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				int bs=params->getint("--buffer-size");
				string in=params->getstring("-i");
				VS out=params->getVS("-outs");
				if(verb) display_start(params, 60, "_", argv[0]);
				Split(in, out, dataset, N, bs, verb);

			} break;
			case 6: {
				string dataset=params->getstring("-d");
				string in=params->getstring("-i");
				if(verb) display_start(params, 60, "_", argv[0]);
				Info(in, dataset, verb);
			} break;
			case 7: {
				string in=params->getstring("-i");
				int p=params->getint("-p");
				string dataset=params->getstring("-d"), sym="";
				VS bracket;
				long long N=params->getll("-N"), skip=0;
				if(params->is_activated("-s")) skip=params->getll("-s");
				if(params->is_activated("-is")) sym=params->getstring("-is");
				if(params->is_activated("-lb")) bracket=params->getVS("-lb");
				if(verb) display_start(params, 60, "_", argv[0]);
				Screen_dump(in, dataset, N, p, skip, verb, params->getchar("-sp"), sym, bracket);
			} break;
			case 8: {
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				int bs=params->getint("--buffer-size"), colno=1;
        if(params->is_activated("-c")) colno=params->getint("-c");
				VS in=params->getVS("-ins");
				string out=params->getstring("-o");
				if(verb) display_start(params, 60, "_", argv[0]);
				Multiply(in, out, dataset, N, bs, verb, colno);
			} break;
			case 9: {
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				int bs=params->getint("--buffer-size"), colno=-1;
        if(params->is_activated("-c")) colno=params->getint("-c");
				string in=params->getstring("-i");
				string out=params->getstring("-o");
				if(verb) display_start(params, 60, "_", argv[0]);
				Conjugate(in, out, dataset, N, bs, verb, colno);
			} break;
			case 10: {
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				int bs=params->getint("--buffer-size"), colno=-1;
        double angle=params->getdouble("-a");
				string in=params->getstring("-i");
				string out=params->getstring("-o");
				if(verb) display_start(params, 60, "_", argv[0]);
				Rotate_phi(in, out, dataset, N, bs, verb, angle);
			} break;
			case 11: {
				string dataset=params->getstring("-d");
				long long N=params->getll("-N");
				int bs=params->getint("--buffer-size"), colno=-1;
        double step=params->getdouble("-st");
        VD init_conf=params->getVD("-ic");
				string out=params->getstring("-o");
        int col=params->getint("-c");
				if(verb) display_start(params, 60, "_", argv[0]);
				Linear_increase(out, dataset, N, bs, verb, step, col, init_conf);
			} break;
			case 12: {
				string dataset=params->getstring("-d");
				string in=params->getstring("-i");
				if(verb) display_start(params, 60, "_", argv[0]);
				Consecutive_repeats(in, dataset, verb);
			} break;
			default: error("Task "+to_string(tk)+" unkown");
		}


		cout.precision(3);
		if(verb) display_end(starttime, 60, "_");
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
	catch(...) {
		cerr << "UNKNOWN ERROR" << endl;
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
	return 0;
}

inline bool complex_log_equal(CD c1, CD c2, double prec) {
	if(!double_equal(real(c1), real(c2), prec)) return false;
	if(double_equal(imag(c1), imag(c2), prec)) return true;
	double diff=fmod(imag(c2)-imag(c1), 2*pi);
	if(!double_equal(diff, 0.0, prec)) return false;
	return true;
}

inline bool complex_log_abs_equal(CD c1, CD c2, double prec) {
	if(!double_equal(real(c1), real(c2), prec)) return false;
	return true;
}

void test_eq(string infile, long long N, bool A, bool verb) {
	HDF5Wrapper *hdf5reader=new HDF5Wrapper, *hdf5writer=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer, *hdf5writebuffer;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer("wf_values", 0);

	if(verb) cout << "Reading positions from " << infile << endl;
	unsigned long rows, cols;
	hdf5reader->getDatasetDims("wf_values", rows, cols);
	if(cols!=4) error("Expected wavefunction file with 4 columns, found "+to_string(cols));
	if(N==0) N=(long long)rows;
	else if(rows<N) warning("Asked for "+to_string(N)+" samples; only found "+to_string(rows)+" in file");

	double *wf=new double[4];
	bool eq=true;
	cout.precision(16);
	int ndiff=0;

	for(long long i=0;i<N;i++) {
		hdf5readbuffer->readFromBuffer(wf, 4);
		CD val1=CD(wf[0], wf[1]), val2=CD(wf[2], wf[3]);
		if(!complex_log_equal(val1, val2, 1e-10)) {
			if(!A) {
				if(verb) cout << "Sample " << i << " unequal; log(val1)=" << val1 << " , log(val2)=" << val2 << endl;
				eq=false;
				ndiff++;
			}
			else if(!complex_log_abs_equal(val1, val2, 1e-10)) {
				if(verb) cout << "Sample " << i << " unequal; log(|val1|)=" << real(val1) << " , log(|val2|)=" << real(val2) << endl;
				eq=false;
				ndiff++;
			}
		}
	}

	string valtype="values";
	if(A) valtype="absolute values";
	if(eq) cout << endl << "All " << valtype << " equal" << endl;
	else cout << endl << ndiff << " " << valtype << " unequal" << endl;

	delete [] wf;
	delete hdf5reader;
}

void translate_to_ASCII(string infile, string outfile, string dataset, long long N, bool M, bool Mp, bool verb) {

	if(M && Mp) error("Translate to ASCII: Cannot write in  both 'regular' and 'position' Mathematica format");

	HDF5Wrapper *hdf5reader=new HDF5Wrapper, *hdf5writer=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer, *hdf5writebuffer;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);

	if(verb) cout << "Reading from dataset " << dataset << " in file " << infile << endl;
	unsigned long rows, cols;
	hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);
	if(cols<1) error("Need at least one column");
	if(Mp && cols%2) error("Must have an even number of columns to write to Mathematica 'position' format");
	if(N==0) N=(long long)rows;
	else if(rows<N) warning("Asked for "+to_string(N)+" samples; only found "+to_string(rows)+" in file");

	ofstream out(outfile.c_str());
	if(!out) error("Failed to open file "+outfile+" for writing");
	out.precision(17);

	double *vals=new double[cols];

	if(M) {
		out << "{";
		for(long long i=0;i<N-1;i++) {
			hdf5readbuffer->readFromBuffer(vals, cols);
			out << "{";
			for(int j=0;j<cols-1;j++) out << vals[j] << ",";
			out << vals[cols-1] << "}," << endl;
		}
		hdf5readbuffer->readFromBuffer(vals, cols);
		out << "{";
		for(int j=0;j<cols-1;j++) out << vals[j] << ",";
		out << vals[cols-1] << "}}" << endl;
	}
	else if(Mp) {
		out << "{";
		for(long long i=0;i<N-1;i++) {
			hdf5readbuffer->readFromBuffer(vals, cols);
			out << "{";
			for(int j=0;j<cols-2;j+=2) out << "{" << vals[j] << "," << vals[j+1] << "},";
			out << "{" << vals[cols-2] << "," << vals[cols-1] << "}}," << endl;
		}
		hdf5readbuffer->readFromBuffer(vals, cols);
		out << "{";
		for(int j=0;j<cols-2;j+=2) out << "{" << vals[j] << "," << vals[j+1] << "},";
		out << "{" << vals[cols-2] << "," << vals[cols-1] << "}}}" << endl;
	}
	else {
		for(long long i=0;i<N;i++) {
			hdf5readbuffer->readFromBuffer(vals, cols);
			for(int j=0;j<cols-1;j++) out << vals[j] << " ";
			out << vals[cols-1] << endl;
		}
	}

	if(verb) cout << "Data written to " << outfile << endl;

	delete [] vals;
	delete hdf5reader;
}

void Screen_dump(string infile, string dataset, long long N, int p, long long skip, bool verb, char sep, string sym, VS bracket) {

	cout.precision(p);
	HDF5Wrapper *hdf5reader=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);

	if(verb) cout << "Reading from dataset " << dataset << " in file " << infile << endl << endl;
	unsigned long rows, cols;
	hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);
	if(cols<1) error("Need at least one column");
	if(N==0) {
		if (skip==0) N=(long long)rows;
		else if(skip>rows) error("File only has "+to_string(rows)+" rows; cannot skip "+to_string(skip));
		else N=(long long)rows-skip;
	}
	if(rows<N+skip) error("Asked for "+to_string(N+skip)+" samples; only found "+to_string(rows)+" in file");

	double *vals=new double[cols];

	for(long long i=0;i<skip;i++) hdf5readbuffer->readFromBuffer(vals, cols);

	string begbracket="", endbracket="";
	if(bracket.size()>1) {
		if(bracket.size()<2) {
			warning("Supplied only one line bracket; assuming front");
			begbracket=bracket[0];
		}
		else {
			begbracket=bracket[0];
			endbracket=bracket[1];
		}
	}

	if(sym!="") {
		for(long long i=0;i<N;i++) {
			hdf5readbuffer->readFromBuffer(vals, cols);
			cout << begbracket;
			for(int j=0;j<cols-2;j+=2) {
				cout << vals[j];
				double im=vals[j+1];
				if(im>0) cout << "+" << im << sym;
				else cout << "-" << -im << sym;
				cout << sep;
			}
			cout << vals[cols-2];
			double im=vals[cols-1];
			if(im>0) cout << "+" << im << sym;
			else cout << "-" << -im << sym;
			cout << endbracket << endl;
		}
	}
	else {
		for(long long i=0;i<N;i++) {
			cout << begbracket;
			hdf5readbuffer->readFromBuffer(vals, cols);
			for(int j=0;j<cols-1;j++) cout << vals[j] << sep;
			cout << vals[cols-1] << endbracket << endl;
		}
	}

	delete [] vals;
	delete hdf5reader;
}

void Permute(string infile, string outfile, int Ne, long long N, VI pv, bool verb) {

	HDF5Wrapper *hdf5reader=new HDF5Wrapper, *hdf5writer=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer, *hdf5writebuffer;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer("positions", 0);
	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset("positions", N, 2*Ne);
	hdf5writebuffer=hdf5writer->setupRealSamplesBuffer("positions", 0, 10000);

	if(verb) cout << "Reading positions from " << infile << endl;

	unsigned long rows, cols;
	hdf5reader->getDatasetDims("positions", rows, cols);
	if((int)cols!=2*Ne) error("Expected 2*Ne = "+to_string(2*N)+" columns in "+infile+"; found "+to_string(cols));
	if(N<1) {
		N=(long long)rows;
		if(verb) cout << "Using " << N << " samples" << endl;
	}
	if(N>rows) error("Asked for = "+to_string(N)+" samples; only found "+to_string(rows)+" in "+infile);
	if(pv.size()>Ne) error("Gave permutation vector of length "+to_string(pv.size())+" but only have "+to_string(Ne)+" particles");
	if(pv.size()<Ne) {
		warning("Gave permutation vector of length "+to_string(pv.size())+" but have "+to_string(Ne)+" particles; leaving remainder in place");
		for(int i=pv.size();i<Ne;i++) pv.push_back(i);
	}
	VI found_several;
	for(int i=0;i<Ne;i++) {
		int t=pv[i], nt=1;
		for(int j=i+1;j<Ne;j++) {
			if(pv[j]==t) nt++;
		}
		if(nt>1) {
			bool told=false;
			for(int j=0;j<found_several.size();j++) {
				if(found_several[j]==t) {
					told=true;
					break;
				}
			}
			if(!told) {
				warning("Particle "+to_string(t)+" occurs "+to_string(nt)+" times in permutation");
				found_several.push_back(t);
			}
		}
	}

	double *pos=new double[2*Ne], *perm=new double[2*Ne];

	for(long long i=0;i<N;i++) {
		hdf5readbuffer->readFromBuffer(pos, 2*Ne);
		for(int j=0;j<Ne;j++) {
			perm[2*j]=pos[2*pv[j]];
			perm[2*j+1]=pos[2*pv[j]+1];
		}
		hdf5writebuffer->writeToBuffer(perm, 2*Ne);
	}
	hdf5writebuffer->flushBuffer();

	if(verb) cout << "Permuted positions written to " << outfile << endl;

	delete hdf5reader;
	delete hdf5writer;
	delete [] pos;
	delete [] perm;
}

long long countSamples(VS infiles, unsigned long& colsout, string dataset) {
	int ninfiles=infiles.size();
	vector<HDF5Wrapper*> hdf5readers(ninfiles);
	unsigned long cols0, rows0;
	long long Nsamples=0;
	for(int i=0;i<ninfiles;i++) {
		hdf5readers[i]=new HDF5Wrapper;
		if(hdf5readers[i]->openFileToRead(infiles[i].c_str())!=0) error("Failed to open "+infiles[i]+" to read");
		unsigned long cols, rows;
		hdf5readers[i]->getDatasetDims(dataset.c_str(), rows, cols);
		if(i==0) {
			cols0=cols;
			rows0=rows;
		}
		else if(cols!=cols0) error("File "+infiles[i]+" has different number of columns that preceding ones");
		Nsamples+=rows;
	}
	colsout=cols0;
	return Nsamples;
}

void Join(VS infiles, string outfile, string dataset, long long N, int bufsize, bool verb) {

	int ninfiles=infiles.size();
	unsigned long cols0;
	long long Nsamples=countSamples(infiles, cols0, dataset);
	if(Nsamples<N) error("Only "+to_string(Nsamples)+" samples in files; requested "+to_string(N));
	if(N==0) {
		N=Nsamples;
		if(verb) cout << "Writing " << N << " samples" << endl;
	}
	vector<HDF5Wrapper*> hdf5readers(ninfiles);
	vector<HDF5Buffer*> hdf5readbuffers(ninfiles);
	HDF5Wrapper* hdf5writer=new HDF5Wrapper;
	HDF5Buffer *hdf5writebuffer;
	for(int i=0;i<ninfiles;i++) {
		hdf5readers[i]=new HDF5Wrapper;
		if(hdf5readers[i]->openFileToRead(infiles[i].c_str())!=0) error("Failed to open "+infiles[i]+" to read");
		hdf5readbuffers[i]=hdf5readers[i]->setupRealSamplesBuffer(dataset.c_str(), 0);
	}

	if(verb) {
		cout << "Reading data from [";
		for(int i=0;i<ninfiles-1;i++) cout << infiles[i] << ",";
		cout << infiles[ninfiles-1] << "]" << endl;
	}

	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset(dataset.c_str(), N, cols0);
	hdf5writebuffer=hdf5writer->setupRealSamplesBuffer(dataset.c_str(), 0, bufsize);

	double *in=new double[cols0];

	long long Ntot=0;
	int file=0;
	while(Ntot<N) {
		unsigned long currentrows, dum;
		hdf5readers[file]->getDatasetDims(dataset.c_str(), currentrows, dum);
		for(int j=0;j<currentrows && Ntot<N;j++) {
			hdf5readbuffers[file]->readFromBuffer(in, cols0);
			hdf5writebuffer->writeToBuffer(in, cols0);
			Ntot++;
		}
		hdf5writebuffer->flushBuffer();
		file++;
	}

	if(verb) cout << "Joined data written to " << outfile << endl;

	for(int i=0;i<ninfiles;i++) delete hdf5readers[i];
	delete hdf5writer;
	delete [] in;
}

void Split(string infile, VS outfiles, string dataset, long long N, int bufsize, bool verb) {

	int noutfiles=outfiles.size();
	vector<HDF5Wrapper*> hdf5writers(noutfiles);
	vector<HDF5Buffer*> hdf5writebuffers(noutfiles);
	HDF5Wrapper* hdf5reader=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer;
	unsigned long cols, rows;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);
	if(rows<N) error("Only "+to_string(rows)+" samples in file; requested "+to_string(N));
	if(N==0) N=(long long)rows;
	long long Nperfile=floor((double)N/noutfiles);
	if(abs((double)N/noutfiles-Nperfile)>1e-13) error("Number of files ("+to_string(noutfiles)+") must divide number of samples ("+to_string(N)+")");

	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);
	double *in=new double[cols];
	if(verb) cout << "Reading data from " << infile << endl;


	for(int i=0;i<noutfiles;i++) {
		hdf5writers[i]=new HDF5Wrapper;
		if(hdf5writers[i]->openFileToWrite(outfiles[i].c_str())!=0) error("Could not open "+outfiles[i]+" for writing");
		hdf5writers[i]->createRealDataset(dataset.c_str(), Nperfile, cols);
		hdf5writebuffers[i]=hdf5writers[i]->setupRealSamplesBuffer(dataset.c_str(), 0, bufsize);
		for(long long j=0;j<Nperfile;j++) {
			hdf5readbuffer->readFromBuffer(in, cols);
			hdf5writebuffers[i]->writeToBuffer(in, cols);
		}
		hdf5writebuffers[i]->flushBuffer();
	}

	if(verb) {
		cout << "Data written to [";
		for(int i=0;i<noutfiles-1;i++) cout << outfiles[i] << ",";
		cout << outfiles[noutfiles-1] << "]" << endl;
	}

	for(int i=0;i<noutfiles;i++) delete hdf5writers[i];
	delete hdf5reader;
	delete [] in;
}

void Info(string infile, string dataset, bool verb) {

	HDF5Wrapper* hdf5reader=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer;
	unsigned long cols, rows;
	if(verb) cout << "Reading data from " << infile << endl;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);
	cout << "Dataset " << dataset << ": " << n2s((long long)rows) << " rows and " << n2s((long long)cols) << " columns." << endl;

	delete hdf5reader;
}

void Multiply(VS infiles, string outfile, string dataset, long long N, int bufsize, bool verb, int colno) {

	int ninfiles=infiles.size();
  if(ninfiles<2) error("Must have at least two wavefunctions to multiply");
	unsigned long rows;
  vector<unsigned long> allcols(ninfiles);
	vector<HDF5Wrapper*> hdf5readers(ninfiles);
	vector<HDF5Buffer*> hdf5readbuffers(ninfiles);

	hdf5readers[0]=new HDF5Wrapper;
	if(hdf5readers[0]->openFileToRead(infiles[0].c_str())!=0) error("Failed to open "+infiles[0]+" to read");
  hdf5readers[0]->getDatasetDims(dataset.c_str(), rows, allcols[0]);
  if(allcols[0]<2*colno) error("Only "+to_string(allcols[0])+" columns in file "+infiles[0]+"; cannot multiply complex column "+to_string(colno));
  if(rows<N) error("Only "+to_string(rows)+" samples in "+infiles[0]+"; requested "+to_string(N));
  else if(N==0) {
    N=rows;
    if(verb) cout << "Computing " << N << " samples" << endl;
  }
  hdf5readbuffers[0]=hdf5readers[0]->setupRealSamplesBuffer(dataset.c_str(), 0);

	for(int i=1;i<ninfiles;i++) {
		hdf5readers[i]=new HDF5Wrapper;
		if(hdf5readers[i]->openFileToRead(infiles[i].c_str())!=0) error("Failed to open "+infiles[i]+" to read");
		hdf5readers[i]->getDatasetDims(dataset.c_str(), rows, allcols[i]);
    if(allcols[i]<2*colno) error("Only "+to_string(allcols[i])+" columns in file"+infiles[i]+"; cannot multiply complex column "+to_string(colno));
    if(rows<N) error("Only "+to_string(rows)+" samples in "+infiles[i]+"; requested "+to_string(N));
		hdf5readbuffers[i]=hdf5readers[i]->setupRealSamplesBuffer(dataset.c_str(), 0);
  }

	HDF5Wrapper* hdf5writer=new HDF5Wrapper;
	HDF5Buffer *hdf5writebuffer;
	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset(dataset.c_str(), N, 4);
	hdf5writebuffer=hdf5writer->setupRealSamplesBuffer(dataset.c_str(), 0, bufsize);
	double *out=new double[4];
  vector<double*> ins(ninfiles);
  for(int i=0;i<ninfiles;i++) ins[i]=new double[allcols[i]];

  for(long long i=0;i<N;i++) {
    hdf5readbuffers[0]->readFromBuffer(ins[0], allcols[0]);
    CD product=CD(ins[0][2*colno], ins[0][2*colno+1]);
    out[0]=ins[0][0];
    out[1]=ins[0][1];
    for(int j=1;j<ninfiles;j++) {
      hdf5readbuffers[j]->readFromBuffer(ins[j], allcols[j]);
      product+=CD(ins[j][2*colno], ins[j][2*colno+1]);
    }
    imod(product, twopi);
    out[2]=real(product);
    out[3]=imag(product);
    hdf5writebuffer->writeToBuffer(out, 4);
  }

	if(verb) cout << "Result of multiplication written to " << outfile << endl;

	for(int i=0;i<ninfiles;i++) {
    delete hdf5readers[i];
    delete [] ins[i];
  }
	delete hdf5writer;
	delete [] out;
}

void Conjugate(string infile, string outfile, string dataset, long long N, int bufsize, bool verb, int colno) {

	unsigned long rows, cols;
	HDF5Wrapper* hdf5reader=new HDF5Wrapper;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
  hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);
  if(rows==0 || cols==0) error("No data in "+infile);
  if(colno<-1) error("Column number must be positive");
  if(colno>-1 && 2*colno>cols-1) error("Only "+to_string(cols)+" columns in "+infile+"; cannot conjugate complex column "+to_string(colno));
  if(N==0) N=(long long)rows;
  else if(rows<N) error("Requested "+to_string(N)+" samples but "+infile+" only contains "+to_string(rows));
  if(cols%2) error("File "+infile+" has an odd number of columns; cannot be complex conjugated");
	HDF5Buffer* hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);

	HDF5Wrapper* hdf5writer=new HDF5Wrapper;
	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset(dataset.c_str(), N, cols);
	HDF5Buffer* hdf5writebuffer=hdf5writer->setupRealSamplesBuffer(dataset.c_str(), 0, bufsize);
	double *inout=new double[cols];

  if(colno==-1) {
    for(long long i=0;i<N;i++) {
      hdf5readbuffer->readFromBuffer(inout, cols);
      for(int j=1;j<cols;j+=2) inout[j]=-inout[j];
      hdf5writebuffer->writeToBuffer(inout, cols);
    }
  }
  else {
    for(long long i=0;i<N;i++) {
      hdf5readbuffer->readFromBuffer(inout, cols);
      inout[2*colno+1]=-inout[2*colno+1];
      hdf5writebuffer->writeToBuffer(inout, cols);
    }
  }

	if(verb) cout << "Result of complex conjugation written to " << outfile << endl;

  delete hdf5reader;
	delete hdf5writer;
	delete [] inout;
}

void Rotate_phi(string infile, string outfile, string dataset, long long N, int bufsize, bool verb, double angle) {

  if(dataset != "positions") warning("Trying to rotate dataset "+dataset+", usually done with \'positions\'");
	unsigned long rows, cols;
	HDF5Wrapper* hdf5reader=new HDF5Wrapper;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
  hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);
  if(rows==0 || cols==0) error("No data in "+infile);
  angle=fmod(angle, twopi);
  if(N==0) N=(long long)rows;
  else if(rows<N) error("Requested "+to_string(N)+" samples but "+infile+" only contains "+to_string(rows));
  if(cols%2) error("File "+infile+" has an odd number of columns; is not represented as (theta, phi) pairs");
	HDF5Buffer* hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);

	HDF5Wrapper* hdf5writer=new HDF5Wrapper;
	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset(dataset.c_str(), N, cols);
	HDF5Buffer* hdf5writebuffer=hdf5writer->setupRealSamplesBuffer(dataset.c_str(), 0, bufsize);
	double *inout=new double[cols];

  for(long long i=0;i<N;i++) {
    hdf5readbuffer->readFromBuffer(inout, cols);
    for(int j=1;j<cols;j+=2) inout[j]+=angle;
    hdf5writebuffer->writeToBuffer(inout, cols);
  }

	if(verb) cout << "Result of azimuthal rotation written to " << outfile << endl;

  delete hdf5reader;
	delete hdf5writer;
	delete [] inout;
}

void Linear_increase(string outfile, string dataset, long long N, int bufsize, bool verb, double step, int col, VD init_conf) {

  if(N<0) error("Asked for N = "+to_string(N)+" linear increases");
  if(N==0) warning("Asked for N = "+to_string(N)+" linear increases");
  int cols=init_conf.size();
  if(col>cols) error("Asked to rotate column "+to_string(col)+" of "+to_string(cols));
  if(col<1) error("Asked to rotate column "+to_string(col)+"; must be positive (counts from 1)");
  col--;

	HDF5Wrapper* hdf5writer=new HDF5Wrapper;
	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset(dataset.c_str(), N+1, cols);
	HDF5Buffer* hdf5writebuffer=hdf5writer->setupRealSamplesBuffer(dataset.c_str(), 0, bufsize);
	double *out=new double[cols];
  for(int i=0;i<cols;i++) out[i]=init_conf[i];
  hdf5writebuffer->writeToBuffer(out, cols);

  for(long long i=0;i<N;i++) {
    out[col]+=step;
    hdf5writebuffer->writeToBuffer(out, cols);
  }

	if(verb) cout << "Result of azimuthal rotation written to " << outfile << endl;

	delete hdf5writer;
	delete [] out;
}


void Consecutive_repeats(string infile, string dataset, bool verb) {

	HDF5Wrapper *hdf5reader=new HDF5Wrapper;
	HDF5Buffer *hdf5readbuffer;
	if(hdf5reader->openFileToRead(infile.c_str())!=0) error("Failed to open "+infile+" to read");
	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);

	if(verb) cout << "Reading from dataset " << dataset << " in file " << infile << endl << endl;
	unsigned long rows, cols;
	hdf5reader->getDatasetDims(dataset.c_str(), rows, cols);

	double *vals=new double[cols];
	double *tmp=new double[cols];

	std::map<long long, int> no_equal;
	bool streak=true;
	long long start=0;
	hdf5readbuffer->readFromBuffer(tmp, cols);

	for(long long i=1;i<rows;i++) {
		hdf5readbuffer->readFromBuffer(vals, cols);
		bool equal=true;
		for(int j=0;j<cols;j++) {
			if(!double_equal(vals[j], tmp[j], 1e-8)) {
				equal=false;
				break;
			}
		}
		if(equal) {
			if(streak) {
				if(i==1) no_equal[0]=1;
				else no_equal[start]++;
			}
			else {
				streak=true;
				start=i-1;
				no_equal[start]=1;
			}
		}
		else {
			streak=false;
			for(int j=0;j<cols;j++) tmp[j]=vals[j];
		}
	}

	int n=no_equal.size();

	if(n) {
		int max=0, idx=0;
		for(auto const &k : no_equal) {
			if(k.second>max) {
				max=k.second;
				idx=k.first;
			}
		}
		cout << max+1 << " consecutive equal rows starting from row " << idx << endl;
	}
	else cout << "No consecutive equal rows" << endl;

	delete [] vals;
	delete [] tmp;
	delete hdf5reader;
}
