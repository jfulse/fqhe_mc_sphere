/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "input.h"

void input::add_input(string name, string description, string type, string def) {
	inputs.push_back(new input_object(name, description, type, def));
	no_inputs++;
}

void input::display_help() {
	string s1=pad_string("Input arguments", symbol, 100), s2=pad_string("", symbol, 100);
	cout << endl << s1 << endl << endl;
	for(int i=0;i<no_inputs;i++) {
		inputs[i]->print_descr();
	}
	cout << s2 << endl << endl;
	if(helptext!="") cout << helptext << endl << endl;
}

void input::display_values() {
	for(int i=0;i<no_inputs;i++) {
		if(inputs[i]->in_use) inputs[i]->display();
	}
}

int input::command_line_input(int argc, const char* argv[]) {
	VS cmd_line(argv, argv+argc);
	return command_line_input(argc, cmd_line);
}

int input::command_line_input(int no_cmd, VS cmd_line) {
	program_name=cmd_line[0];
	cmd_line.erase(cmd_line.begin()); 
	no_cmd--;
	
	for(int i=0;i<no_cmd;i++) {
		if(cmd_line[i]=="-h" || cmd_line[i]=="--h" || cmd_line[i]=="-help" || cmd_line[i]=="--help") {
			display_help(); 
			return 1;
		}
	}
	
	while(no_cmd>0) { //cout << "Looking for " << cmd_line[0] << endl;
		if(cmd_line[0][0]!='-') error("Command line inputs must start with '-' (i.e. -"+cmd_line[0]+")");
		bool active=false;
		//cmd_line[0].erase(0,1);
		for(int j=0;j<no_inputs;j++) {
			for(int k=0;k<inputs[j]->n_names;k++) { //cout << "Testing " << inputs[j]->names[k] << endl;
				if(cmd_line[0]==inputs[j]->names[k] && inputs[j]->type_id!=-1) {
					cmd_line.erase(cmd_line.begin()); 
					no_cmd--;
					inputs[j]->activate(no_cmd, cmd_line);
					active=true;
				}
			}
		}
		if(!active) error("Command line argument '"+cmd_line[0]+"' not found");
	}
	return 0;
}

bool input::check_integrity() {
	for(int i=0;i<no_inputs;i++) {
		string testname=inputs[i]->names[0];
		for(int j=i+1;j<no_inputs;j++) {
			for(int k=0;k<inputs[j]->n_names;k++) {	
				if(inputs[j]->names[k]==testname.c_str()) {
					error("Argument '"+testname+"' repeated");
					return false;
				}
			}
		}
	}
	checked=true;
	return true;
}

int input::hasname(string in_name) {
	for(int i=0;i<no_inputs;i++) {
		for(int j=0;j<inputs[i]->n_names;j++) {
			if(inputs[i]->names[j]==in_name) return i;
		}
	}
	return -1;
}

bool input::is_activated(string in_name) {
	int n=hasname(in_name);
	if(n<0) error("Input '"+in_name+"' nonexistant");
	return inputs[n]->activated;
}

void input::set(string in_name, string value) {
	int n=hasname(in_name);
	if(n<0) error("Input '"+in_name+"' nonexistant");
	return inputs[n]->activate(value);
}

bool input::is_flagged(string in_name) {
	int n=hasname(in_name);
	if(n<0) error("Flag '"+in_name+"' nonexistant");
	inputs[n]->in_use=true;
	return inputs[n]->is_flagged();
}
	
long long input::getll(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getll();
}

int input::getint(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getint();
}

double input::getdouble(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getdouble();
}

string input::getstring(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getstring();
}

char input::getchar(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getchar();
}

VI input::getVI(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getVI();
}

VD input::getVD(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getVD();
}

VS input::getVS(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getVS();
}

CD input::getCD(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getCD();
}

VCD input::getVCD(string in_name) {
	if(!checked) warning("Input object integrity not checked");
	int idx=hasname(in_name);
	if(idx<0) error(in_name+" could not be found");
	if(!inputs[idx]->activated) error(inputs[idx]->names[0]+" is not initialised");
	return inputs[idx]->getVCD();
}

void input_object::printword(string word, int width) {
	cout << left;
	if(word.length()+1>width) error("Word '"+word+"' has length "+to_string(word.length())+"; too long for width "+to_string(width));
	cout << setw(width) << word;
}

void input_object::print_descr() {
	VI widths {22,12,70};
	cout << left;
	auto old_prec=cout.precision(3);
	if(type_id==-1) {
		string printstring="";
		while(description.length()>0) {
			if(description[0]!=';') {
				printstring+=description[0];
				description.erase(description.begin());
			}
			else {
				if(printstring.length()<1) error("cannot have a description of < 1 symbol");
				printword(" ",widths[0]+widths[1]);
				cout << printstring << endl;
				printstring="";
				description.erase(description.begin());
			}
		}
		if(printstring.length()>0) {
			printword(" ",widths[0]+widths[1]);
			cout << printstring << endl;
		}
		return;
	}
	
	string namestring="";
	for(int i=0;i<n_names-1;i++) namestring+=names[i]+" , ";
	namestring+=names[n_names-1];
	printword(namestring, widths[0]);
	string typestring="("+type+")";
	printword(typestring, widths[1]);
	if(hasdefault) {
		string defaultstring="default = "+valuestring();
		string buffer=" ,  ";
		if(defaultstring.length()+buffer.length()+description.length()<=widths[2]) {
			printword(description+buffer+defaultstring,widths[2]);
		}
		else {
			printword(description, widths[2]);
			cout << endl;
			printword("", widths[0]);
			printword(defaultstring, widths[1]+widths[2]);
		}
	}
	else printword(description, widths[2]);
	cout << endl;
	cout.precision(old_prec);
}

void input_object::display() {
	if(type_id==-1 || !in_use || (type_id==4 && !activated)) return;
	string name=names[0];
	while(name[0]=='-') name.erase(name.begin());
	cout << name;
	if(type_id!=4) cout << " = " << valuestring();
	if(!setbyuser) cout << " (default)";
	cout << endl;
}

string input_object::valuestring() {
	switch(type_id) {
		case -1: return ""; break;
		case 0: return to_string(intvalue); break;
		case 1: return to_string(doublevalue); break;
		case 2: return stringvalue; break;
		case 3: return string(1, charvalue); break;
		case 4: {if(setbyuser) return "True"; else return "False"; break;}
		case 5: {
			stringstream ss; ss << "[";
			for(int i=0;i<VIvalue.size()-1;i++) ss << VIvalue[i] << ","; 
			ss << VIvalue[VIvalue.size()-1] << "]"; return ss.str();
		} break;
		case 6: {
			ostringstream os; os << "[";
			for(int i=0;i<VDvalue.size()-1;i++) os << VDvalue[i] << ","; 
			os << VDvalue[VDvalue.size()-1] << "]"; return os.str();
		} break;
		case 7: {
			string s="[";
			for(int i=0;i<VSvalue.size()-1;i++) s+=VSvalue[i]+","; 
			s+=VSvalue[VSvalue.size()-1]+"]"; return s;
		} break;
		case 8: return CDstring(CDvalue); break;
		case 9: {
			string s="[";
			for(int i=0;i<VCDvalue.size()-1;i++) s+=CDstring(VCDvalue[i])+","; 
			s+=CDstring(VCDvalue[VCDvalue.size()-1])+"]"; return s;
		} break;
		case 11: return to_string(llvalue); break;
		default: error("Invalid input type "+type);
	}
}

void input_object::set_name(string name_in) {
	if(name_in.length()<1) error("No name");
	stringstream ss;
	names.clear();
	while(name_in.length()>0) {
		if(name_in[0]!=',') {
			ss << name_in[0];
			name_in.erase(name_in.begin());
		}
		else {
			names.push_back(ss.str());
			ss.str("");
			name_in.erase(name_in.begin());
		}
	}
	names.push_back(ss.str());
	n_names=names.size();
}

void input_object::check_default() {
	if(type_id==4) {
		if(strcmp(def.c_str(), "")) error("Bool "+names[0]+" cannot have default value");
	}
	else if(type_id==-1) {
		if(strcmp(def.c_str(), "")) warning("Input description "+names[0]+" cannot have default value");
	}
	else if(strcmp(def.c_str(), "")) {
		hasdefault=true;
		activate(def);
	}
}

bool input_object::is_flagged() {
	if(type_id!=4) error("'"+names[0]+"' is not a boolean");
	return activated;
}
	
bool input_object::valid_type(int& type_id) {
	if(!strcmp(type.c_str(), "int")) {type_id=0; return true;}
	if(!strcmp(type.c_str(), "double")) {type_id=1; return true;}
	if(!strcmp(type.c_str(), "string")) {type_id=2; return true;}
	if(!strcmp(type.c_str(), "char")) {type_id=3; return true;}
	if(!strcmp(type.c_str(), "bool")) {type_id=4; return true;}
	if(!strcmp(type.c_str(), "VI")) {type_id=5; return true;}
	if(!strcmp(type.c_str(), "VD")) {type_id=6; return true;}
	if(!strcmp(type.c_str(), "VS")) {type_id=7; return true;}
	if(!strcmp(type.c_str(), "CD")) {type_id=8; return true;}
	if(!strcmp(type.c_str(), "VCD")) {type_id=9; return true;}
	if(!strcmp(type.c_str(), "ll")) {type_id=11; return true;}
	if(!strcmp(type.c_str(), "description")) {type_id=-1; return true;}
	return false;
}

void input_object::activate(int& no_cmd, VS& cmd_line) {
	if(no_cmd<1 && type_id!=4 && type_id!=-1) error("Input "+names[0]+" missing");
	switch(type_id) {
		case 0: {
			if(!isint(cmd_line[0])) error(names[0]+" input must be an int; '"+cmd_line[0]+"' will not work");
			intvalue=atoi(cmd_line[0].c_str()); activated=true; setbyuser=true; cmd_line.erase(cmd_line.begin()); no_cmd--;
		} break;
		case 1: {
			if(!isnumber(cmd_line[0])) error(names[0]+" input must be a double; '"+cmd_line[0]+"' will not work");
			doublevalue=atof(cmd_line[0].c_str()); activated=true; setbyuser=true; cmd_line.erase(cmd_line.begin()); no_cmd--;
		} break;
		case 2: {
			stringvalue=cmd_line[0]; activated=true; setbyuser=true; cmd_line.erase(cmd_line.begin()); no_cmd--;
		} break;
		case 3: {
			if(cmd_line[0].length()>1) error(names[0]+" input must be a char; '"+cmd_line[0]+"' will not work");
			charvalue=cmd_line[0].c_str()[0]; activated=true; setbyuser=true; cmd_line.erase(cmd_line.begin()); no_cmd--;
		} break;
		case 4: {
			activated=true; setbyuser=true;
		} break;
		case 5: {
			int j=0; VIvalue.clear();
			while(no_cmd>0 && isint(cmd_line[0])) {
				VIvalue.push_back(atoi(cmd_line[0].c_str()));
				cmd_line.erase(cmd_line.begin()); no_cmd--;
			}
			activated=true; setbyuser=true;
		} break;
		case 6: {
			int j=0; VDvalue.clear();
			while(no_cmd>0 && isnumber(cmd_line[0])) {
				VDvalue.push_back(atof(cmd_line[0].c_str()));
				cmd_line.erase(cmd_line.begin()); no_cmd--;
			}
			activated=true; setbyuser=true;
		} break;
		case 7: {
			int j=0; VSvalue.clear();
			while(no_cmd>0 && cmd_line[0][0]!='-') {
				VSvalue.push_back(cmd_line[0]);
				cmd_line.erase(cmd_line.begin()); no_cmd--;
			}
			activated=true; setbyuser=true;
		} break;
		case 8: {
			if(no_cmd<2) {
				CDvalue=CD(atof(cmd_line[0].c_str()), 0.0); cmd_line.erase(cmd_line.begin()); no_cmd--;
			}
			else if(!isnumber(cmd_line[1])) {
				CDvalue=CD(atof(cmd_line[0].c_str()), 0.0); cmd_line.erase(cmd_line.begin()); no_cmd--;
			}
			else {
				CDvalue=CD(atof(cmd_line[0].c_str()), atof(cmd_line[1].c_str()));
				cmd_line.erase(cmd_line.begin()); cmd_line.erase(cmd_line.begin()); no_cmd-=2; 
			}
			activated=true; setbyuser=true;
		} break;
		case 9: {
			if(no_cmd<2) error("Input "+names[0]+" missing; remember both real and complex parts must be given");
			int j=0; VCDvalue.clear();
			while(no_cmd>1 && isnumber(cmd_line[0])) {
				if(!isnumber(cmd_line[1])) error("Both real and complex parts must be given for '"+names[0]+"'");
				VCDvalue.push_back(CD(atof(cmd_line[0].c_str()),atof(cmd_line[1].c_str())));
				cmd_line.erase(cmd_line.begin()); cmd_line.erase(cmd_line.begin()); no_cmd-=2;
			}
			activated=true; setbyuser=true;
		} break;
		case 11: {
			if(!isint(cmd_line[0])) error(names[0]+" input must be a long long; '"+cmd_line[0]+"' will not work");
			llvalue=stoll(cmd_line[0].c_str()); activated=true; setbyuser=true; cmd_line.erase(cmd_line.begin()); no_cmd--;
		} break;
		default: error("Invalid input type "+type);
	}
}

void input_object::activate(string in_val) {
	switch(type_id) {
		case -1: break;
		case 0: {
			if(!isint(in_val)) error(names[0]+" input must be an int; '"+in_val+"' will not work");
			intvalue=atoi(in_val.c_str()); activated=true; 
		} break;
		case 1: {
			if(!isnumber(in_val)) error(names[0]+" input must be a double; '"+in_val+"' will not work");
			doublevalue=atof(in_val.c_str()); activated=true;
		} break;
		case 2: stringvalue=in_val.c_str(); activated=true; break;
		case 3: {
			if(in_val.length()>1) error(names[0]+" input must be a char; '"+in_val+"' will not work");
			charvalue=in_val.c_str()[0]; activated=true;
		} break;
		case 4: error("Bool "+names[0]+" cannot have default value"); break;
		case 5: {
			VIvalue.clear();
			istringstream buffer(in_val);
			VS split;
			copy(istream_iterator<std::string>(buffer), istream_iterator<string>(), back_inserter(split));
			for(int i=0;i<split.size();i++) {
				if(!isint(split[i])) error(names[0]+" elements must be int's; '"+split[i]+"' will not work");
				VIvalue.push_back(atoi(split[i].c_str()));
			}
			activated=true;
		} break;
		case 6: {
			VDvalue.clear();
			istringstream buffer(in_val);
			VS split;
			copy(istream_iterator<std::string>(buffer), istream_iterator<string>(), back_inserter(split));
			for(int i=0;i<split.size();i++) {
				if(!isnumber(split[i])) error(names[0]+" elements must be double's; '"+split[i]+"' will not work");
				VDvalue.push_back(atof(split[i].c_str()));
			}
			activated=true;
		} break;
		case 7: {
			VSvalue.clear();
			istringstream buffer(in_val);
			VS split;
			copy(istream_iterator<std::string>(buffer), istream_iterator<string>(), back_inserter(split));
			VSvalue.clear();
			for(int i=0;i<split.size();i++) {
				VSvalue.push_back(split[i]);
			}
			activated=true;
		} break;
		case 8: {
			istringstream buffer(in_val);
			VS split;
			copy(istream_iterator<std::string>(buffer), istream_iterator<string>(), back_inserter(split));
			if(split.size()<1) error("Input "+names[0]+" missing");
			if(split.size()==1) {
				CDvalue=CD(atof(split[0].c_str()), 0.0);
			}
			else if(split.size()==2) {
				if(!isnumber(split[0]) || !isnumber(split[1])) {
					error(names[0]+" elements must be complex<double>; ("+split[0]+","+split[1]+") will not work");
				}
				CDvalue=CD(atof(split[0].c_str()), atof(split[1].c_str()));
				activated=true;
			}
			else error("Input "+names[0]+" is complex<double>; needs one or two float numbers");
		} break;
		case 9: {
			VCDvalue.clear();
			istringstream buffer(in_val);
			VS split;
			copy(istream_iterator<std::string>(buffer), istream_iterator<string>(), back_inserter(split));
			if(split.size()%2) error("Input "+names[0]+" is vector<complex<double>>; needs an even number of elements (real and imaginary)");
			for(int i=0;i<split.size();i+=2) {
				if(!isnumber(split[i]) || !isnumber(split[i+1])) {
					error(names[0]+" elements must be complex<double>; ("+split[0]+","+split[1]+") will not work");
				}
				VCDvalue.push_back(CD(atof(split[i].c_str()), atof(split[i+1].c_str())));
			}
			activated=true;
			
		} break;
		case 11: {
			if(!isint(in_val)) error(names[0]+" input must be a long long; '"+in_val+"' will not work");
			llvalue=stoll(in_val.c_str()); activated=true; 
		} break;
		default: error("Invalid input type "+type);
	}
}

long long input_object::getll() {
	in_use=true;
	if(type_id!=11) error(names[0]+" is not a long long");
	return llvalue;
}

int input_object::getint() {
	in_use=true;
	if(type_id!=0) error(names[0]+" is not an int");
	return intvalue;
}

double input_object::getdouble() {
	in_use=true;
	if(type_id!=1) error(names[0]+" is not a double");
	return doublevalue;
}

string input_object::getstring() {
	in_use=true;
	if(type_id!=2) error(names[0]+" is not a string");
	return stringvalue;
}

char input_object::getchar() {
	in_use=true;
	if(type_id!=3) error(names[0]+" is not a float");
	return charvalue;
}

VI input_object::getVI() {
	in_use=true;
	if(type_id!=5) error(names[0]+" is not a vector<int>");
	return VIvalue;
}

VD input_object::getVD() {
	in_use=true;
	if(type_id!=6) error(names[0]+" is not a vector<double>");
	return VDvalue;
}

VS input_object::getVS() {
	in_use=true;
	if(type_id!=7) error(names[0]+" is not a vector<string>");
	return VSvalue;
}

CD input_object::getCD() {
	in_use=true;
	if(type_id!=8) error(names[0]+" is not a complex<double>");
	return CDvalue;
}

VCD input_object::getVCD() {
	in_use=true;
	if(type_id!=9) error(names[0]+" is not a vector<complex<double>>");
	return VCDvalue;
}
