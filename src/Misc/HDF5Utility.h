/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include <dirent.h>
#include <map>
#include "misc_sphere.h"
#include "main_misc.h"
#include "HDF5Wrapper.h"

#ifndef HDF5UTILITY_H
#define HDF5UTILITY_H

void Permute(string, string, int, long long, VI, bool);
void test_eq(string, long long, bool, bool);
void translate_to_ASCII(string, string, string, long long, bool, bool, bool);
long long countSamples(VS, unsigned long&, string);
void Join(VS, string, string, long long, int, bool);
void Multiply(VS, string, string, long long, int, bool, int);
void Conjugate(string, string, string, long long, int, bool, int);
void Rotate_phi(string, string, string, long long, int, bool, double);
void Split(string, VS, string, long long, int, bool);
void Info(string, string, bool);
void Consecutive_repeats(string, string, bool);
void Screen_dump(string, string, long long, int, long long, bool, char, string, VS);
void Linear_increase(string, string, long long, int, bool, double, int, VD);

#endif
