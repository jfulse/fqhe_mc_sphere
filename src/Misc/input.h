/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "misc.h"
#include <string.h>

#ifndef INPUT_H
#define INPUT_H

typedef vector<int> VI;
typedef vector<double> VD;
typedef vector<string> VS;
typedef vector<complex<double> > VCD;

class input_object {
	
	private:
	string description, type, def;
	bool hasdefault, setbyuser;
	long long llvalue;
	int intvalue;
	double doublevalue;
	string stringvalue;
	char charvalue;
	VI VIvalue;
	VS VSvalue;
	VD VDvalue;
	CD CDvalue;
	VCD VCDvalue;
	
	bool valid_type(int&);
	void check_default();
	string valuestring();
	void set_name(string);
	void printword(string, int);
	
	public:
	int n_names;
	int type_id;
	VS names;
	bool activated, in_use;
	
	bool is_flagged();
	void print_descr();
	void display();
	void activate(int&, VS&);
	void activate(string);
	long long getll();
	int getint();
	double getdouble();
	string getstring();
	char getchar();
	VI getVI();
	VS getVS();
	VD getVD();
	CD getCD();
	VCD getVCD();
	
	input_object(string name_, string description_, string type_, string def_): description(description_), type(type_), def(def_) {
		activated=false;
		setbyuser=false;
		hasdefault=false;
		in_use=false;
		set_name(name_);
		if(!valid_type(type_id)) error("Invalid input type "+type);
		check_default();
		
	}
	
	~input_object() {}
	
};

class input {
	
	private:
	vector<input_object*> inputs;
	string program_name, symbol, helptext;
	bool checked;
	
	int hasname(string);
	
	public:
	int no_inputs;
	
	void set(string, string);
	void add_input(string, string, string, string def="");
	void display_help();
	void display_values();
	int command_line_input(int, VS);
	int command_line_input(int, const char*[]);
	bool check_integrity();
	bool is_activated(string);
	bool is_flagged(string);
	long long getll(string);
	int getint(string);
	double getdouble(string);
	string getstring(string);
	char getchar(string);
	VI getVI(string);
	VS getVS(string);
	VD getVD(string);
	CD getCD(string);
	VCD getVCD(string);
	
	input(string symbol_="_", string helptext_=""): symbol(symbol_), helptext(helptext_) {
		no_inputs=0;
		checked=false;
	}
	
	~input() {}
	
};

#endif
