/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "main_misc.h"

string timestring() {
	time_t t = time(0);
    struct tm * now = localtime( & t );
	stringstream ss; 
	return nostring(now->tm_year + 1900)+" "+nostring(now->tm_mday)+"."+nostring(now->tm_mon+1)+" "+nostring(now->tm_hour)+":"
			+nostring(now->tm_min)+":"+nostring(now->tm_sec);
}

string nostring(int n) {
	stringstream ss;
	if(n<10) ss << 0 << n;
	else ss << n;
	return ss.str();
}

string clean_name(string& progname) {
	int n=progname.length()-1;
	stringstream ss;
	while(progname[n]!='/' && n>=0) ss << progname[n--];
	string cleaned="";
	for(int i=0;i<ss.str().length();i++) cleaned+=ss.str()[ss.str().length()-1-i];
	return cleaned;
}

void display_start(input*& params, int len, string sym, string progname) {
	string s1=pad_string("Program started "+timestring(), sym, len);
	if(progname!="") progname=pad_string(clean_name(progname), sym, len);
	string s2=pad_string("Parameters", sym, len);
	string s3=pad_string("", sym, len);
	if(progname!="") cout << endl << progname << endl;
	cout << endl << s1 << endl << s2 << endl << endl;
	params->display_values();
	cout << s3 << endl << endl;
}

void display_end(double starttime, int len, string sym) {
	double sec=(clock()-starttime)/CLOCKS_PER_SEC;
	string s1=pad_string("Total Runtime: "+to_string(sec)+"s = "+to_string(sec/60.0)+"min", sym, len);
	string s2=pad_string("Program finished "+timestring(), sym, len);
	cout << endl << s1 << endl << s2 << endl << endl;
}
