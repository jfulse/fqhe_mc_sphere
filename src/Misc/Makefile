###########################################################################
#
# Copyright (C) 2016  Jørgen Fulsebakke
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.
#
# Author: Jørgen Fulsebakke
#
###########################################################################

#### Directories and flags

ifndef $(DIR_MAIN)
  DIR_MAIN=../..
endif

DIR_EXE=$(DIR_MAIN)/Programs
DIR_SRC=$(DIR_MAIN)/src/Misc
DIR_SFMT_SRC=$(DIR_MAIN)/src/sfmt
DIR_BLD=$(DIR_MAIN)/build/Misc
COMP=g++
COMP=h5c++

COMPILE_FLAGS= -std=c++0x -O3 -lstdc++ -msse2
LINK_FLAGS= -O3 -lgsl -lgslcblas -lm -lhdf5_cpp -lhdf5

ifeq ($(cmp),ichec)
  COMP=icpc
  ###matches the hdf5/intel/1.10.1 module
  HDF5_DIR=/ichec/packages/hdf5/intel/1.10.1
  COMPILE_FLAGS+=-I${HDF5_DIR}/include # -diag-disable 858
  LINK_FLAGS+=-L${HDF5_DIR}/lib
endif


#### Compile all

all: setup $(DIR_BLD)/input.o $(DIR_BLD)/misc_sphere.o $(DIR_BLD)/misc.o $(DIR_BLD)/main_misc.o $(DIR_BLD)/jacobi.o $(DIR_EXE)/HDF5Utility $(DIR_BLD)/sfmt.o $(DIR_BLD)/userintf.o

setup:
	@mkdir -p $(DIR_BLD)
	@mkdir -p $(DIR_EXE)

#### Misc source

$(DIR_BLD)/input.o: $(DIR_SRC)/input.cpp $(DIR_SRC)/input.h $(DIR_SRC)/misc_sphere.h
	${COMP} -c input.cpp ${COMPILE_FLAGS} -o $@

$(DIR_BLD)/misc_sphere.o: $(DIR_SRC)/misc_sphere.cpp $(DIR_SRC)/misc_sphere.h $(DIR_SRC)/misc.h
	${COMP} -c misc_sphere.cpp ${COMPILE_FLAGS} -o $@

$(DIR_BLD)/misc.o: $(DIR_SRC)/misc.cpp $(DIR_SRC)/misc.h
	${COMP} -c misc.cpp ${COMPILE_FLAGS} -o $@

$(DIR_BLD)/main_misc.o: $(DIR_SRC)/main_misc.cpp $(DIR_SRC)/misc.h $(DIR_SRC)/input.h
	${COMP} -c main_misc.cpp ${COMPILE_FLAGS} -o $@

#### Jacobi polynomial  source

$(DIR_BLD)/jacobi.o: $(DIR_SRC)/jacobi.c $(DIR_SRC)/jacobi.h
	${COMP} -c jacobi.c ${COMPILE_FLAGS} -o $@

#### hdf5 utility source

$(DIR_EXE)/HDF5Utility: $(DIR_BLD)/HDF5Utility.o $(DIR_BLD)/input.o $(DIR_BLD)/misc.o $(DIR_BLD)/main_misc.o $(DIR_BLD)/HDF5Wrapper.o
	${COMP} ${DIR_BLD}/HDF5Utility.o $(DIR_BLD)/input.o $(DIR_BLD)/misc.o $(DIR_BLD)/main_misc.o $(DIR_BLD)/HDF5Wrapper.o ${LINK_FLAGS} -o $@

$(DIR_BLD)/HDF5Utility.o: ${DIR_SRC}/HDF5Utility.cpp  $(DIR_SRC)/HDF5Utility.h ${DIR_SRC}/input.h ${DIR_SRC}/misc.h ${DIR_SRC}/main_misc.h ${DIR_SRC}/HDF5Wrapper.h
	${COMP} -c HDF5Utility.cpp ${COMPILE_FLAGS} -o $@

#### Niall's HDF5 wrapper

$(DIR_BLD)/HDF5Wrapper.o: $(DIR_SRC)/HDF5Wrapper.C $(DIR_SRC)/HDF5Wrapper.h
	${COMP} -c HDF5Wrapper.C ${COMPILE_FLAGS} ${HDF5LINK} -o $@

#### SFMT random generator

$(DIR_BLD)/sfmt.o: $(DIR_SFMT_SRC)/sfmt.cpp $(DIR_SFMT_SRC)/randomc.h $(DIR_SFMT_SRC)/sfmt.h
	${COMP} -c $(DIR_SFMT_SRC)/sfmt.cpp ${COMPILE_FLAGS} -msse2 -o $@

$(DIR_BLD)/userintf.o: $(DIR_SFMT_SRC)/randomc.h $(DIR_SFMT_SRC)/userintf.cpp
	${COMP} -c $(DIR_SFMT_SRC)/userintf.cpp -msse2 -o $@

#### Clean

clean:
	@rm -f $(DIR_BLD)/*.o $(DIR_EXE)/HDF5Utility
	@echo "Object files and executables removed"
