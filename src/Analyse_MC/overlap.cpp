/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "overlap.h"

overlap::overlap(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) 
		: MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Wavefunction data");
	if(verb) status << "Overlap observable added" << endl;
	else cout << "Overlap observable added" << endl;
	no_vars=8;
	half_no_vars=4;
	no_res=8;
	no_var_blocks=4;
	half_no_blocks=no_blocks/2;
	if(!use_existing) {
		if(datacollection.size()<2) error("Require two sets of WF values. Did you remember to use 'and' between sets in input?");
		if(norms.size()<1) {
			normval2=real(datacollection[0]->norm_ave[1]-datacollection[0]->norm_ave[0]);
			normval3=real(datacollection[1]->norm_ave[1]-datacollection[1]->norm_ave[0]);
			normval1=normval2+normval3;
		}
		else {
			if(norms.size()<3) error("Overlap needs three norm values");
			else if(norms.size()>3) warning("Overlap needs three norm values; rest ignored");
			normval1=norms[0];
			normval2=norms[1];
			normval3=norms[2];
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
	double normtest=(-normval1)-0.5*(-2.0*normval2-2.0*normval3);
	if(abs(normtest)>1e-10) error("Input normalisation values do not cancel");
}

void overlap::evaluate_sample(vector<VCD> all_samples) {// conjugates 1st input
	CD F1=exp(conj(all_samples[0][1])+all_samples[1][1]-2.0*real(all_samples[0][0])-normval1);
	double F2=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval2));
	double F3=exp(2.0*(real(all_samples[1][1])-real(all_samples[0][0])-normval3));
	int addhalf=((no_observations+1)>use_samples/2)*half_no_vars;
	variables[0+addhalf]+=real(F1);
	variables[1+addhalf]+=imag(F1);
	variables[2+addhalf]+=F2;
	variables[3+addhalf]+=F3;
	int blockidx=floor(no_observations/(double)blocksize);
	blocks[blockidx][0]+=real(F1);
	blocks[blockidx][1]+=imag(F1);
	blocks[blockidx][2]+=F2;
	blocks[blockidx][3]+=F3;
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void overlap::compute_result() {
	
	double re=0.0, re2=0.0, im=0.0, im2=0.0, ab=0.0, ab2=0.0, sq=0.0, sq2=0.0;
	for(int i=0;i<half_no_blocks;i++) {
		CD est_1=CD(blocks[i][0], blocks[i][1])/sqrt(blocks[i][2]*blocks[i][3]);
		CD est_2=CD(blocks[i+half_no_blocks][0], blocks[i+half_no_blocks][1])/sqrt(blocks[i+half_no_blocks][2]*blocks[i+half_no_blocks][3]);
		CD est_all=CD(blocks[i][0]+blocks[i+half_no_blocks][0], blocks[i][1]+blocks[i+half_no_blocks][1])/
				   sqrt((blocks[i][2]+blocks[i+half_no_blocks][2])*(blocks[i][3]+blocks[i+half_no_blocks][3]));
		re+=real(est_all); re2+=real(est_all)*real(est_all);
		im+=imag(est_all); im2+=imag(est_all)*imag(est_all);
		double est_abs=sqrt(abs(real(est_1*conj(est_2)))), est_norm=abs(real(est_1*conj(est_2)));
		ab+=est_abs; ab2+=est_abs*est_abs;
		sq+=est_norm; sq2+=est_norm*est_norm;
	}
	CD overlap_split_1=CD(variables[0],variables[1])/sqrt(variables[2]*variables[3]);
	CD overlap_split_2=CD(variables[0+half_no_vars],variables[1+half_no_vars])/sqrt(variables[2+half_no_vars]*variables[3+half_no_vars]);
	CD overlap_all=CD(variables[0]+variables[0+half_no_vars], variables[1]+variables[1+half_no_vars])/
				   sqrt((variables[2]+variables[2+half_no_vars])*(variables[3]+variables[3+half_no_vars]));
	results[0]=real(overlap_all);
	results[1]=imag(overlap_all);
	results[2]=std(re2, re, half_no_blocks);
	results[3]=std(im2, im, half_no_blocks);
	results[4]=sqrt(abs(real(overlap_split_1*conj(overlap_split_2))));
	results[5]=std(ab2, ab, half_no_blocks);
	results[6]=abs(real(overlap_split_1*conj(overlap_split_2)));
	results[7]=std(sq2, sq, half_no_blocks);
}

void overlap::display_result() {
	cout << endl << "Overlap = " << CD(results[0], results[1]) << " +/- " << CD(results[2], results[3]) << endl;
	cout << "|Overlap| = " << results[4] << " +/- " << results[5] << endl;
	cout << "|Overlap|^2 = " << results[6] << " +/- " << results[7] << endl << endl;
}

void overlap::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	out << "Re(overlap)"+sep+"Im(overlap)"+sep+"Re(overlap)_error"+sep+"Im(overlap)_error" << sep << "Abs(overlap)" << sep << "Abs(overlap)_error" << sep;
	out << "Abs(overlap)^2" << sep << "Abs(overlap)^2_error" << endl;
	for(int i=0;i<no_res-1;i++) out << results[i] << sep;
	out << results[no_res-1] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}

void overlap::write_norms(string filename) {
	ofstream out(filename.c_str());
	out.precision(17);
	out << normval1 << " " << normval2 << " " << normval3 << endl;
	out.close();
}
