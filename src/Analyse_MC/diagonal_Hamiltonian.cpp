/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "diagonal_Hamiltonian.h"

diagonal_Hamiltonian::diagonal_Hamiltonian(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, vector<int> input_sizes, 
	int Ne_, int Nphi_, string eigenv_prefix_, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) : twoNe(2*Ne_), twoR(sqrt(2*Nphi_)),
	MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), eigenv_prefix(eigenv_prefix_) {
	
	if(input_sizes[2]>0 || input_sizes[3]>0) warning("Input data other than WF values or positions supplied; ignored for Diagonalise Hamiltonian");
	n_WF=input_sizes[0];
	n_pairs=(n_WF*(n_WF+1))/2;
	for(int i=0;i<n_WF;i++) required_collections.push_back("Wavefunction data");
	required_collections.push_back("Configuration data");
	if(verb) status << "Diagonalise Hamiltonian observable added" << endl;
	else cout << "Diagonalise Hamiltonian observable added" << endl;
	no_vars=4*n_pairs;
	no_res=1;
	energies.resize(n_WF);
	energy_errors.resize(n_WF);
	no_var_blocks=4*n_pairs;
	normvals.resize(n_WF);
	if(!use_existing) {
		if(datacollection.size()<2) error("Require at least two sets of WF values. Did you remember to use 'and' between sets in input?");
		if(norms.size()<1) {
			for(int i=0;i<n_WF;i++) normvals[i]=real(datacollection[i]->norm_ave[1]-datacollection[i]->norm_ave[0]);
		}
		else {
			if(norms.size()<n_WF) error("Diagonalise Hamiltonian needs "+n2s(n_WF)+" norm values");
			else if(norms.size()>n_WF) warning("Diagonalise Hamiltonian needs "+n2s(n_WF)+" norm values; rest ignored");
			for(int i=0;i<n_WF;i++) normvals[i]=norms[i];
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
}

void diagonal_Hamiltonian::evaluate_sample(vector<VCD> all_samples) {// conjugates 1st input
	
	int blockidx=floor(no_observations/(double)blocksize);
	
	double E=0;
	for(int i=0;i<twoNe-2;i+=2) {
		for(int j=i+2;j<twoNe;j+=2) E+=1.0/abs(all_samples[n_WF][i]*all_samples[n_WF][j+1]-all_samples[n_WF][j]*all_samples[n_WF][i+1]);
	}
	
	int fourpair=0;
	for(int i=0;i<n_WF;i++) {
		for(int j=i;j<n_WF;j++) {
			CD F1=exp(conj(all_samples[i][1])+all_samples[j][1]-2.0*real(all_samples[i][0])-normvals[i]-normvals[j]);
			double F2=exp(2.0*(real(all_samples[i][1])-real(all_samples[i][0])-normvals[i]));
			double F3=exp(2.0*(real(all_samples[j][1])-real(all_samples[j][0])-normvals[j]));
			variables[0+fourpair]+=E*real(F1);
			variables[1+fourpair]+=E*imag(F1);
			variables[2+fourpair]+=F2;
			variables[3+fourpair]+=F3;
			blocks[blockidx][0+fourpair]+=E*real(F1);
			blocks[blockidx][1+fourpair]+=E*imag(F1);
			blocks[blockidx][2+fourpair]+=F2;
			blocks[blockidx][3+fourpair]+=F3;
			fourpair+=4;
		}
	} //int I; cin >> I;
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void diagonal_Hamiltonian::compute_result() {
	
	if(verb) cout << endl << "Calculating results" << endl;
	
	Eigen::MatrixXcd Ham_matrix;
	Ham_matrix.resize(n_WF, n_WF);
	
	VD E_blocks(n_WF, 0.0), E_blocks2(n_WF, 0.0);
	for(int k=0;k<no_blocks;k++) {
		int fourpairs=0;
		for(int i=0;i<n_WF;i++) {
			CD element=CD(blocks[k][fourpairs],blocks[k][1+fourpairs])/(twoR*sqrt(blocks[k][2+fourpairs]*blocks[k][3+fourpairs]));
			Ham_matrix(i, i)=element;
			fourpairs+=4;
			for(int j=i+1;j<n_WF;j++) {
				CD element=CD(blocks[k][fourpairs],blocks[k][1+fourpairs])/(twoR*sqrt(blocks[k][2+fourpairs]*blocks[k][3+fourpairs]));
				Ham_matrix(i, j)=element;
				Ham_matrix(j, i)=conj(element);
				fourpairs+=4;
			}
		}
		
		Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> eigensolver(Ham_matrix);
		if (eigensolver.info()!=Eigen::Success) error("Finding block energy eigenvalue "+to_string(k+1));
		Eigen::VectorXd eigvalues=eigensolver.eigenvalues().cast<double>();
		
		for(int i=0;i<n_WF;i++) {
			E_blocks[i]+=eigvalues(i);
			E_blocks2[i]+=eigvalues(i)*eigvalues(i);
		}
	}
	
	int fourpairs=0;
	for(int i=0;i<n_WF;i++) {
		CD element=CD(variables[fourpairs],variables[1+fourpairs])/(twoR*sqrt(variables[2+fourpairs]*variables[3+fourpairs]));
		Ham_matrix(i, i)=element;
		fourpairs+=4;
		for(int j=i+1;j<n_WF;j++) {
			CD element=CD(variables[fourpairs],variables[1+fourpairs])/(twoR*sqrt(variables[2+fourpairs]*variables[3+fourpairs]));
			Ham_matrix(i, j)=element;
			Ham_matrix(j, i)=conj(element);
			fourpairs+=4;
		}
	}
	
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> eigensolver(Ham_matrix);
	if (eigensolver.info()!=Eigen::Success) error("Finding energy eigenvalues");
	Eigen::VectorXd eigvalues=eigensolver.eigenvalues().cast<double>();
	
	for(int i=0;i<n_WF;i++) {
		energies[i]=eigvalues(i);
		energy_errors[i]=std(E_blocks2[i], E_blocks[i], no_blocks);
	}
	
}

void diagonal_Hamiltonian::display_result() {
	
	double BG=twoNe/(2.0*twoR);
	
	cout << endl << "Energies: " << endl;
	for(int i=0;i<n_WF;i++) {
		cout << energies[i] << " +/- " << energy_errors[i] << endl;
	}
	
	cout << endl << "Energies per particle w. background: " << endl;
	for(int i=0;i<n_WF;i++) {
		cout << 2*energies[i]/twoNe-BG << " +/- " << 2*energy_errors[i]/twoNe << endl;
	}
	cout << endl;
}

void diagonal_Hamiltonian::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	double BG=twoNe/(2.0*twoR);
	
	out << "Idx" << sep << "Energy" << sep << "Energy_error" << sep << "EnergyPerParticleWBackground" << sep << "EnergyPerParticleWBackground_error"<< endl;
	for(int i=0;i<n_WF;i++) {
		out << i << sep << energies[i] << sep << energy_errors[i] << sep << 2*energies[i]/twoNe-BG << sep << 2*energy_errors[i]/twoNe << endl;
	}	
	
	out.close();
	cout << "Output written to " << filename << endl;
}

void diagonal_Hamiltonian::write_norms(string filename) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<n_WF;i++) out << normvals[i] << endl;
	out.close();
}
