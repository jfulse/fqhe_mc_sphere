/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "g_decomp_LS_disk.h"

g_decomp_LS_disk::g_decomp_LS_disk(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, 
		int Ne_, int Nphi_, int nfuncs_, int nbins_, double cutoff_, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) : 
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), 
		Ne(Ne_), Nphi(Nphi_), nfuncs(nfuncs_), nbins(nbins_), cutoff(cutoff_/2.0) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Configuration data");
	if(verb) status << "g decomposition least squares\ndisk observable added" << endl;
	else cout << "g decomposition least squares\ndisk observable added" << endl;
	
	int max_rec=ceil(((double)Nphi+2.0)/4.0);
	if(nfuncs>max_rec) {
		warning("Requested "+n2s(nfuncs)+" functions; reverting to recommended maximum ceil[(Nphi+2)/4]+1 = "+n2s(max_rec));
		nfuncs=max_rec;
	}
	
	R2=Nphi/2.0;
	logtwo=log(2.0);
	L=cutoff/(double)nbins;
	no_vars=nbins+1;
	no_res=2*nfuncs;
	no_var_blocks=nbins+1;
	get_logfacts();
}

void g_decomp_LS_disk::evaluate_sample(vector<VCD> all_samples) {
	double F=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval));
	int blockidx=floor(no_observations/(double)blocksize);
	
	for(int i=0;i<2*Ne-2;i+=2) {
		for(int j=i+2;j<2*Ne;j+=2) {
			double eta=abs(all_samples[1][i]*all_samples[1][j+1]-all_samples[1][j]*all_samples[1][i+1]);
			if(eta<cutoff) {
				int l=floor(eta/L);
				variables[l]+=F;
				blocks[blockidx][l]+=F;
			}
		}
	}
	
	variables[nbins]+=F;
	blocks[blockidx][nbins]+=F;
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void g_decomp_LS_disk::compute_result() {
	
	double dens=Ne/(2.0*pi*Nphi);
	Eigen::VectorXd g(nbins);
	VD areas(nbins), r2(nbins);
	
	for(int i=0;i<nbins;i++) {
		r2[i]=4.0*R2*L*(i+0.5)*L*(i+0.5);
		double ta=2.0*asin(2.0*L*i/2.0);
		double tb=2.0*asin(2.0*L*(i+1.0)/2.0);
		areas[i]=pi*Nphi*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
		g(i)=2.0*variables[i]/(Ne*variables[nbins]*dens*areas[i])-1.0+exp(-r2[i]/2.0);
	}
	
	Eigen::MatrixXd fit_mat=get_fit_mat(r2);
	Eigen::VectorXd coeffs=fit_mat.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(g);
	
	VD best(nfuncs, 0.0), best2(nfuncs,0.0);
	for(int i=0;i<no_blocks;i++) {
		for(int j=0;j<nbins;j++) g(j)=2.0*blocks[i][j]/(Ne*blocks[i][nbins]*dens*areas[j])-1.0+exp(-r2[j]/2.0);
		Eigen::VectorXd block_coeffs=fit_mat.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(g);
		for(int j=0;j<nfuncs;j++) {
			double est=block_coeffs[j]; //cout << setprecision(10) << i << " " << j << " " << est << endl;
			best[j]+=est; best2[j]+=est*est;
		}
	}
	
	for(int i=0;i<nfuncs;i++) {
		results[2*i]=coeffs(i);
		results[2*i+1]=std(best2[i], best[i], no_blocks);
	}
}

void g_decomp_LS_disk::display_result() {
	cout << endl;
	for(int i=0;i<nfuncs-1;i++) cout << "c_" << i+1 << " = " << results[2*i] << " +/- " << results[2*i+1] << endl;
	cout << "c_" << nfuncs << " = " << results[2*nfuncs-2] << " +/- " << results[2*nfuncs-1] << endl;
	cout << endl;
}

void g_decomp_LS_disk::write_result(string filename, string separator) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<nfuncs-1;i++) out << "c_" << i+1 << separator << "err_" << i+1 << separator;
	out << "c_" << nfuncs << separator << "err_" << nfuncs << endl;
	for(int i=0;i<nfuncs-1;i++) out << results[2*i] << separator << results[2*i+1] << separator;
	out << results[2*nfuncs-2] << separator << results[2*nfuncs-1] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}

double g_decomp_LS_disk::funcval(double r2, int n) {
	int m=2*n-1;
	return exp(logtwo-logfactlist[n-1]+m*log(r2/4.0)-r2/4.0);
}

void g_decomp_LS_disk::get_logfacts() {
	logfactlist.resize(nfuncs);
	for(int n=0;n<nfuncs;n++) logfactlist[n]=logfact(2*n+1);
}

Eigen::MatrixXd g_decomp_LS_disk::get_fit_mat(VD r2) {	
	Eigen::MatrixXd mat(nbins, nfuncs);
	for(int i=0;i<nbins;i++) {
		for(int j=0;j<nfuncs;j++) mat(i, j)=funcval(r2[i], j+1);
	}
	return mat;
}
