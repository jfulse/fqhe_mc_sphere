/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "eigenvectors.h"

eigenvectors::eigenvectors(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, vector<int> input_sizes, int Ne, 
	string eigenv_prefix_, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) : 
	MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), eigenv_prefix(eigenv_prefix_) {
	
	if(input_sizes[1]>0 || input_sizes[2]>0 || input_sizes[3]>0) error("Input data other than WF values supplied; ignored for eigenvector");
	compute_log_nu1_norm(Ne);
	n_WF=input_sizes[0];
	n_pairs=(n_WF*(n_WF+1))/2;
	for(int i=0;i<n_WF;i++) required_collections.push_back("Wavefunction data");
	if(verb) status << "Eigenvector observable added" << endl << endl << "!!! NOTE: Must be used with samples generated from probability nu=1 !!!" << endl;
	else cout << "Eigenvector observable added" << endl << "!!! NOTE: Must be used with samples generated from probability nu=1 !!!" << endl;
	no_vars=2*n_pairs;
	no_res=2*n_WF+2*n_pairs+2+n_WF+1;
	no_var_blocks=2*n_pairs;
	if(norms.size()>0) warning("Eigenvector does not use input norm; ignored");
	if(!use_existing && datacollection.size()<2) error("Require at least two sets of WF values. Did you remember to use 'and' between sets in input?");
	
}

void eigenvectors::compute_log_nu1_norm(int Ne) {
	double l4pi=log(4*pi), lfNe=logfact(Ne);
	log_nu1_norm=0;
	for(int i=1;i<=Ne;i++) log_nu1_norm+=log(i)+l4pi-lfNe+logfact(i-1)+logfact(Ne-i);
}

void eigenvectors::evaluate_sample(vector<VCD> all_samples) {// conjugates 1st input
	
	int blockidx=floor(no_observations/(double)blocksize);
	
	int twopair=0;
	for(int i=0;i<n_WF;i++) {
		for(int j=i;j<n_WF;j++) {
			CD F1=exp(conj(all_samples[i][1])+all_samples[j][1]-2.0*real(all_samples[i][0]));
			variables[twopair]+=real(F1);
			variables[1+twopair]+=imag(F1);
			blocks[blockidx][twopair]+=real(F1);
			blocks[blockidx][1+twopair]+=imag(F1);
			twopair+=2;
		}
	}
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void eigenvectors::compute_result() {
	
	if(verb) cout << endl << "Calculating results" << endl;
	
	eigvalues.resize(n_WF);
	eigvalue_errors.resize(n_WF);
	Eigen::MatrixXcd G(n_WF, n_WF);
	Eigen::MatrixXcd G_norm(n_WF, n_WF);
	Eigen::MatrixXcd Gblock(n_WF, n_WF);
	Eigen::MatrixXcd Gblock_norm(n_WF, n_WF);
	
	int twopairs=0;
	for(int i=0;i<n_WF;i++) {
		CD overlap=CD(variables[twopairs],variables[1+twopairs])*exp(log_nu1_norm);
		G(i, i)=overlap;
		twopairs+=2*(n_WF-i);
	}
	
	twopairs=0;
	for(int i=0;i<n_WF;i++) {
		twopairs+=2;
		G_norm(i, i)=1.0;
		for(int j=i+1;j<n_WF;j++) {
			CD overlap=CD(variables[twopairs],variables[1+twopairs])*exp(log_nu1_norm);
			double norm=sqrt(real(G(i, i))*real(G(j, j)));
			G(i, j)=overlap;
			G(j, i)=conj(overlap);
			G_norm(i, j)=G(i, j)/norm;
			G_norm(j, i)=G(j, i)/norm;
			twopairs+=2;
		}
	}
	
	Eigen::ComplexEigenSolver<Eigen::MatrixXcd> ES(G, true);
	Eigen::ComplexEigenSolver<Eigen::MatrixXcd> ES_n(G_norm, false);
	Eigen::MatrixXcd V=ES.eigenvectors();
	Eigen::VectorXcd D=ES.eigenvalues();
	Eigen::VectorXcd DN=ES_n.eigenvalues();
		
	//cout << "eigenvectors: " << endl << V << endl << "eigenvalues" << endl << D << endl;
	
	rank=0;
	vector<int> basis_states;
	for(int i=0;i<n_WF;i++) {
		eigvalues[i]=real(DN(i));
		if(abs(DN(i))>1e-11) {
			double rms=0;
			coefficients.push_back(VCD(n_WF));
			for(int j=0;j<n_WF;j++) {
				CD tmp=V(j, i);
				coefficients[rank][j]=tmp/sqrt(real(D(i)));
				rms+=norm(tmp)/real(D(i));
				//coefficients[rank][j]=tmp;
			}
			rms=sqrt(rms);
			for(int j=0;j<n_WF;j++) coefficients[rank][j]/=rms;
			rank++;
			basis_states.push_back(i);
		}
	}
	
	VD D_blocks(n_WF, 0.0), D_blocks2(n_WF, 0.0);
	vector<VCD> V_blocks(rank, VCD(n_WF, 0.0)), V_blocks2(rank, VCD(n_WF, 0.0));
	
	for(int k=0;k<no_blocks;k++) {
				
		int twopairs=0;
		for(int i=0;i<n_WF;i++) {
			CD overlap=CD(blocks[k][twopairs], blocks[k][1+twopairs])*exp(log_nu1_norm);
			Gblock(i, i)=overlap;
			twopairs+=2*(n_WF-i);
		}
		
		twopairs=0;
		for(int i=0;i<n_WF;i++) {
			twopairs+=2;
			Gblock_norm(i, i)=1.0;
			for(int j=i+1;j<n_WF;j++) {
				CD overlap=CD(blocks[k][twopairs],blocks[k][1+twopairs])*exp(log_nu1_norm);
				double norm=sqrt(real(Gblock(i, i))*real(Gblock(j, j)));
				Gblock(i, j)=overlap;
				Gblock(j, i)=conj(overlap);
				Gblock_norm(i, j)=Gblock(i, j)/norm;
				Gblock_norm(j, i)=Gblock(j, i)/norm;
				twopairs+=2;
			}
		}
		Eigen::ComplexEigenSolver<Eigen::MatrixXcd> ES(Gblock, true);
		Eigen::ComplexEigenSolver<Eigen::MatrixXcd> ES_n(Gblock_norm, false);
		Eigen::MatrixXcd V=ES.eigenvectors();
		Eigen::VectorXcd D=ES.eigenvalues();
		Eigen::VectorXcd DN=ES_n.eigenvalues();
		
		for(int i=0;i<n_WF;i++) {
			D_blocks[i]+=real(DN[i]);
			D_blocks2[i]+=real(DN[i])*real(DN[i]);
		}
		
		for(int i=0;i<rank;i++) {
			for(int j=0;j<n_WF;j++) {
				V_blocks[i][j]+=V(basis_states[i],j)/sqrt(real(D(j)));
				V_blocks2[i][j]+=V(basis_states[i],j)*V(basis_states[i],j)/real(D(j));
			}
		}
	}
	
	for(int i=0;i<n_WF;i++) eigvalue_errors[i]=std(D_blocks2[i], D_blocks[i], no_blocks);
	
	coefficient_errors.resize(rank, VCD(n_WF));
	for(int i=0;i<rank;i++) {
		for(int j=0;j<n_WF;j++) {
			double Re_err=std(real(V_blocks2[i][j]), real(V_blocks[i][j]), no_blocks);
			double Im_err=std(imag(V_blocks2[i][j]), imag(V_blocks[i][j]), no_blocks);
			if(std::isnan(Re_err)) Re_err=0.0;
			if(std::isnan(Im_err)) Im_err=0.0;
			coefficient_errors[i][j]=CD(Re_err, Im_err);
		}
	}
	
	if(eigenv_prefix!="") {
		if(verb) cout << endl << "Eigenvectors stored to:" << endl;
		for(int i=0;i<rank;i++) {
			string name=eigenv_prefix+"_"+to_string(i+1)+".dat";
			ofstream evout(name.c_str());
			if(!evout) error("Could not open "+name+" for writing");
			evout.precision(17);
			for(int j=0;j<n_WF;j++) evout << real(coefficients[i][j]) << " " << imag(coefficients[i][j]) << endl;
			evout.close();
			if(verb) cout << name << endl;
		}
	}
	
}

void eigenvectors::display_result() {
	
	cout.precision(8);
	cout << endl << "Eigenvalues:" << endl;
	for(int i=0;i<n_WF;i++) cout << eigvalues[i] << " +/- " << eigvalue_errors[i] << endl;
	cout << endl << "Estimated rank: " << rank << endl << endl << "Orthogonal coefficients:" << endl;
	for(int j=0;j<rank;j++) {
		cout << "v_" << j+1 << " =" << endl;
		for(int i=0;i<n_WF;i++) cout << CDstring(coefficients[j][i])+" +/- "+CDstring(coefficient_errors[j][i]) << endl;
	}
	
	cout << endl;
}

void eigenvectors::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	
	out << "Estimated rank:" << endl << rank << endl << endl << "Orthogonal coefficients:" << endl;
	for(int i=0;i<rank-1;i++) out << "Re(v_"+to_string(i+1) << ")" << sep << "Im(v_"+to_string(i+1) << ")" << sep 
								  << "Re(v_"+to_string(i+1) << ")_err" << sep << "Im(v_"+to_string(i+1) << ")_err" << sep;
	out << "Re(v_"+to_string(rank) << ")" << sep << "Im(v_"+to_string(rank) << ")" << endl;
	
	for(int i=0;i<n_WF;i++) {
		for(int j=0;j<rank-1;j++) out << real(coefficients[j][i]) << sep << imag(coefficients[j][i]) << sep 
									  << real(coefficient_errors[j][i]) << sep << imag(coefficient_errors[j][i]) << sep;
		out << real(coefficients[rank-1][i]) << sep << imag(coefficients[rank-1][i]) << sep
			<< real(coefficient_errors[rank-1][i]) << sep << imag(coefficient_errors[rank-1][i]) << endl;
	}
	
	out.close();
	cout << "Output written to " << filename << endl;
}
