/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef DENS_DECOMP_IP_ORTHOG_SPHERE_H
#define DENS_DECOMP_IP_ORTHOG_SPHERE_H

#include "MC_observable.h"
#include "jacobi.h"

class dens_decomp_IP_orthog_sphere : public MC_observable {

	private:
	int Ne, Nphi, fourS, nfuncs;
	double cutoff;
	VD nu1_int, norms;
	CD u0, v0; // Origin of density plot

	void setup_nu1_int();
	void setup_norms();
	double funcval(double, int);

	public:

	void evaluate_sample(vector<VCD>);
	void compute_result();
	void display_result();
	void write_result(string, string);
	void compute_samples_special() {error("No special computation procedure for this observable");}

	dens_decomp_IP_orthog_sphere(vector<MC_data*>, int, stringstream&, int, int, int, double, bool use_existing_=false, bool verb_=false,
										int blocksize_=-1, int no_blocks_=-1, VD norms=VD());
};

#endif
