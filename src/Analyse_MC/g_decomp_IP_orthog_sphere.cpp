/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "g_decomp_IP_orthog_sphere.h"

g_decomp_IP_orthog_sphere::g_decomp_IP_orthog_sphere(vector<MC_data*> datacollection_, int use_samples_, stringstream& status,
		int Ne_, int Nphi_, int nfuncs_, double cutoff_, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) :
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), Ne(Ne_), Nphi(Nphi_), nfuncs(nfuncs_), cutoff(cutoff_/2.0) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Configuration data");
	if(verb) status << "g decomposition inner product\northogonal sphere observable added" << endl;
	else cout << "g decomposition inner product\northogonal sphere observable added" << endl;
	if(nfuncs>Nphi) {
		warning("Requested "+n2s(nfuncs)+" functions; reverting to maximum Nphi="+n2s(Nphi));
		nfuncs=Nphi;
	}
	fourS=2*Nphi;
	no_vars=nfuncs+1;
	no_res=2*nfuncs;
	no_var_blocks=nfuncs+1;
	setup_norms();
	setup_nu1_int();
}

void g_decomp_IP_orthog_sphere::evaluate_sample(vector<VCD> all_samples) {
	double F=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval));
	int blockidx=floor(no_observations/(double)blocksize);

	for(int i=0;i<2*Ne-2;i+=2) {
		for(int j=i+2;j<2*Ne;j+=2) {
			double eta=abs(all_samples[1][i]*all_samples[1][j+1]-all_samples[1][j]*all_samples[1][i+1]);
			if(eta<cutoff) {
				double eta2=eta*eta;
				for(int k=0;k<nfuncs;k++) {
					double tmp=funcval(eta2, k+1)*F;
					variables[k]+=tmp;
					blocks[blockidx][k]+=tmp;
				}
			}
		}
	}

	variables[nfuncs]+=F;
	blocks[blockidx][nfuncs]+=F;

	verb && counter(no_observations, samplecount);
	no_observations++;
}

void g_decomp_IP_orthog_sphere::compute_result() {

	double dens=Ne/(2.0*pi*Nphi);

	VD best(nfuncs, 0.0), best2(nfuncs,0.0);
	for(int i=0;i<no_blocks;i++) {
		for(int j=0;j<nfuncs;j++) {
			double est=blocks[i][j]*2.0/(Ne*blocks[i][nfuncs]*dens)-nu1_int[j];
			best[j]+=est; best2[j]+=est*est;
		}
	}

	for(int i=0;i<nfuncs;i++) {
		results[2*i]=variables[i]*2.0/(Ne*variables[nfuncs]*dens)-nu1_int[i];
		results[2*i+1]=std(best2[i], best[i], no_blocks);
	}
}

void g_decomp_IP_orthog_sphere::display_result() {
	cout << endl;
	for(int i=0;i<nfuncs-1;i++) cout << "c_" << i+1 << " = " << results[2*i] << " +/- " << results[2*i+1] << endl;
	cout << "c_" << nfuncs << " = " << results[2*nfuncs-2] << " +/- " << results[2*nfuncs-1] << endl;
	cout << endl;
}

void g_decomp_IP_orthog_sphere::write_result(string filename, string separator) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<nfuncs-1;i++) out << "c_" << i+1 << separator << "err_" << i+1 << separator;
	out << "c_" << nfuncs << separator << "err_" << nfuncs << endl;
	for(int i=0;i<nfuncs-1;i++) out << results[2*i] << separator << results[2*i+1] << separator;
	out << results[2*nfuncs-2] << separator << results[2*nfuncs-1] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}

void g_decomp_IP_orthog_sphere::setup_nu1_int() {
	nu1_int.resize(nfuncs);
	for(int i=0;i<nfuncs;i++) {
		nu1_int[i]=(1.0-2.0*(i%2))*norms[i]*2.0*pi*(fourS/(fourS+2.0))*((2*i+3.0)*Nphi-i*(i+1.0)+1.0)/((fourS+1.0-i)*(fourS-i));
	}
}

void g_decomp_IP_orthog_sphere::setup_norms() {
	norms.resize(nfuncs);
	for(int i=0;i<nfuncs;i++) {
		norms[i]=sqrt((fourS+1.0-i)*(fourS-i)*(fourS-1.0-2*i)/(fourS*pi*(i+1.0)*(i+2.0)));
	}
}

double g_decomp_IP_orthog_sphere::funcval(double eta2, int n) {
	return norms[n-1]*eta2*pow(1-eta2,  Nphi-n)*jac_jacobi(1-2.0*eta2, n-1, 2, fourS+1-2*n);
}
