/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "MC_data.h"

MC_data::MC_data(VS inputnames_, string dataset_, bool folder_, string dataname_, int n_ave_norm, bool verb) : 
	inputnames(inputnames_), dataset(dataset_), folder(folder_), dataname(dataname_) {
		
	files;
	setup_input(files);
	
	if(nsamples<n_ave_norm) {
		warning("Number of samples in average for normalisation greater than total number of samples; using the latter");
		n_ave_norm=nsamples;
	}
	if(n_ave_norm==0) norm_ave.resize(cols/2, 0.0);
	else {
		if(verb) cout << "Finding input data normalisation" << endl;
		getnorm(n_ave_norm, files);
	}
}

MC_data::MC_data(VS inputnames_, string dataset_, bool folder_, string dataname_, int nfiles_, int samplesperfile_, int nsamples_) : 
	inputnames(inputnames_), dataset(dataset_), folder(folder_), dataname(dataname_), nfiles(nfiles_), samplesperfile(samplesperfile_), nsamples(nsamples_) {
	
}


VS MC_data::get_input_names() {return inputnames;}

VS MC_data::get_files() {return files;}

string MC_data::get_dataset() {return dataset;}

void MC_data::setup_input(VS& files) {
	
	files.clear();
	if(folder) {
		if(inputnames.size()>1) error("Can only accept single folder ('--read-folder' activated)");
		get_folder_files(files, inputnames[0], nfiles);
	}
	else {
		files=inputnames;
		nfiles=files.size();
	}
	
	for(int i=0;i<hdf5reader.size();i++) delete hdf5reader[i];
	hdf5reader.clear();
	hdf5readbuffer.clear();
	
	hdf5reader.push_back(new HDF5Wrapper);
	unsigned long check_rows, check_cols;
	if(hdf5reader[0]->openFileToRead(files[0].c_str())) error("Failed to open "+files[0]+" to read");
	hdf5readbuffer.push_back(hdf5reader[0]->setupRealSamplesBuffer(dataset.c_str(), 0));
	hdf5reader[0]->getDatasetDims(dataset.c_str(), check_rows, check_cols);
	if(check_rows<1 || check_cols<1) error("No data in file "+files[0]+", dataset "+dataset);
	samplesperfile=check_rows;
	cols=check_cols;
	
	for(int i=1;i<nfiles;i++) {
		hdf5reader.push_back(new HDF5Wrapper);
		if(hdf5reader[i]->openFileToRead(files[i].c_str())) error("Failed to open "+files[i]+" to read");
		hdf5readbuffer.push_back(hdf5reader[i]->setupRealSamplesBuffer(dataset.c_str(), 0));
		hdf5reader[i]->getDatasetDims(dataset.c_str(), check_rows, check_cols);
		if(check_rows<1 || check_cols<1) error("No data in file "+files[i]+", dataset "+dataset);
		if(check_cols!=cols) error("File "+files[i]+" has "+n2s((long long)check_cols)+" columns; preceding ones had "+n2s(cols));
		if(check_rows!=samplesperfile) error("File "+files[i]+" has "+n2s((long long)check_rows)+" samples; preceding ones had "+n2s(samplesperfile));
	}
	
	nsamples=samplesperfile*(long long)nfiles;
	data_position=0;
}

void MC_data::getnorm(int n_ave_norm, VS files) {
	int tallied=0;
	HDF5Wrapper* tempreader=new HDF5Wrapper;
	if(tempreader->openFileToRead(files[0].c_str())) error("Failed to open "+files[0]+" to read");
	HDF5Buffer* tempbuffer=tempreader->setupRealSamplesBuffer(dataset.c_str(), 0);
	unsigned long r, c;
	tempreader->getDatasetDims(dataset.c_str(), r, c);
	int no_aves=c/2;
	norm_ave.resize(no_aves);
	if(c%2) error("Normalisation assumes complex variable; need even number of columns (turn off normalisation with '--no-norm')");
	VCD aves(no_aves, 0.0);
	double *val=new double[c];
	int opened=1;
	
	tempbuffer->readFromBuffer(val, c);
	for(int j=0;j<2*no_aves;j+=2) aves[j/2]=CD(val[j], val[j+1])-log(n_ave_norm);
	tallied++;
	int boing=1;
	
	while(tallied<n_ave_norm) {
		for(int i=boing;i<(int)r && tallied<n_ave_norm;i++) {
			tempbuffer->readFromBuffer(val, c);
			for(int j=0;j<2*no_aves;j+=2) aves[j/2]+=log(1.0+exp(CD(val[j], val[j+1])-aves[j/2]));
			tallied++;
		}
		boing=0;
		if(tallied<n_ave_norm) {
			delete tempreader;
			tempreader=new HDF5Wrapper;
			if(tempreader->openFileToRead(files[opened].c_str())) error("Failed to open "+files[opened]+" to read");
			opened++;
			tempbuffer=tempreader->setupRealSamplesBuffer(dataset.c_str(), 0);
		}
	}
	delete tempreader;
	delete [] val;
	for(int i=0;i<no_aves;i++) norm_ave[i]=aves[i];
}

void MC_data::display() {
	string name=dataname, source="file(s)", nfilestring="", title=inputnames[0]; 
	if(name=="") name="Unknown data";
	if(folder) {
		source="folder";
		nfilestring=" ("+n2s(nfiles)+" files)";
	}
	else if(nfiles>1) {
		for(int i=1;i<nfiles;i++) title+=","+inputnames[i];
	}
	cout << name << " from " << source << " '" << title << "'" << nfilestring << endl;
	cout << "\t" << n2s(nsamples) << " samples of " << n2s(cols) << " data entries" << endl;
}

string MC_data::getname() {
	if(inputnames.size()<1) error("No input name");
	if(inputnames.size()==1) return inputnames[0];
	else {
		string name="[";
		for(int i=0;i<inputnames.size()-1;i++) name+=inputnames[i]+",";
		name+=inputnames[inputnames.size()-1]+"]";
		return name;
	}
}

void MC_data::get_folder_files(VS& files, string folder, int &nfiles) {
	
	VS addfiles=dirfiles(folder);
	if(addfiles.size()<1) error("No files in "+folder);
	nfiles=addfiles.size();
	orderfiles(addfiles);
	for(int j=0;j<nfiles;j++) files.push_back(folder+addfiles[j]);
}

void MC_data::orderfiles(VS& files) {
	int n=files.size();
	int order[n];
	for(int i=0;i<n;i++) {// Find current order
		string sno;
		int ctr=files[i].length()-1;
		while(ctr>=0 && files[i][ctr]!='.') ctr--;
		if(ctr<0) ctr=files[i].length()-1; //  No dot in filename
		while(ctr>=0 && !isint(string(1, files[i][ctr]))) ctr--;
		if(ctr<0) warning("No number in filename "+files[i]+"; cannot be ordered");
		while(ctr>=0 && isint(string(1, files[i][ctr]))) ctr--;
		sno=files[i][++ctr];
		while(isint(string(1, files[i][ctr]))) sno+=files[i][ctr++];
		order[i]=atoi(sno.c_str());
	}
	for(int i=0;i<n;i++) { // Order accordingly
		for(int j=i+1;j<n;j++) {
			if(order[j]<order[i]) {
				int itmp=order[i];
				order[i]=order[j];
				order[j]=itmp;
				string stmp=files[i];
				files[i]=files[j];
				files[j]=stmp;
			}
		}
	}
}

VS MC_data::dirfiles(string& path) {

	DIR* dir;
	dirent* pdir;
	VS files;
	if(path[path.length()-1]!='/') path+="/";
	dir=opendir(path.c_str());
	if(!dir) error("Folder "+path+" not found");
	while(pdir=readdir(dir)) files.push_back(pdir->d_name);
	for(int i=0;i<files.size();i++) {
		if(strcmp(files[i].c_str(),".")==0 || strcmp(files[i].c_str(),"..")==0) {files.erase(files.begin()+i); i--;}
	}
	return files;
}
