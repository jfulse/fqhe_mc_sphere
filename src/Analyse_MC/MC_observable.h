/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef MC_OBSERVABLE_H
#define MC_OBSERVABLE_H

#include "misc.h"
#include "MC_data.h"
#include "WF_data.h"
#include "pos_data.h"

class MC_observable {

	protected:
	bool verb, use_existing;
	long long use_samples, samplecount;
	int no_collections, no_blocks, no_var_blocks, no_vars, no_res, no_observations, blocksize;
	double normval;
	vector<MC_data*> datacollection;
	VS required_collections;
	VD variables, results;
	vector<VD> blocks;
	
	double std(double, double, int);
	inline bool counter(int no_observations, int samplecount) {
		if(use_samples>10 && !(no_observations%samplecount) && no_observations>0) {
			cout << n2s(no_observations) << " of " << n2s(use_samples) << " samples computed" << endl;
			return true;
		}
		return false;
	}
	void get_input_norm(VD);
	
	public:
	
	bool compute_special;
	
	void display_collection();
	void setup();
	void compute_samples();
	void write_variables(string, int);
	double* VD2da(VD);
	void read_samples(VS);
	void check_sample_file(HDF5Wrapper*, string, int);
	void display_data();
	virtual void write_norms(string);
	VCD read_n_samples(MC_data*, string, int, int index=0);
	
	virtual void evaluate_sample(vector<VCD>)=0;
	virtual void compute_result()=0;
	virtual void display_result()=0;
	virtual void write_result(string, string)=0;
	virtual void compute_samples_special()=0;

	MC_observable(vector<MC_data*>, int, VD, bool use_existing_=false, int blocksize_=-1, int no_blocks=-1, bool verb_=false);
	
	~MC_observable() {}
};


#endif
