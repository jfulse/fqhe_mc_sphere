/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "energy.h"

energy::energy(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, int Ne_, int Nphi_, string blocknamefile_, bool use_existing_, 
		bool verb_, int blocksize_, int no_blocks_, VD norms) : MC_observable(datacollection_, use_samples_, norms, use_existing_, 
		blocksize_, no_blocks_, verb_), Ne(Ne_), Nphi(Nphi_), twoNe(2*Ne_), blocknamefile(blocknamefile_) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Configuration data");
	if(verb) status << "Energy observable added" << endl;
	else cout << "Energy observable added" << endl;
	R=sqrt(Nphi/2.0);
	BG=Ne/(2.0*R);
	no_vars=2;
	no_res=4;
	no_var_blocks=2;
}

void energy::evaluate_sample(vector<VCD> all_samples) {
	double F=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval)), E=0;
	for(int i=0;i<twoNe-2;i+=2) {
		for(int j=i+2;j<twoNe;j+=2) E+=1.0/abs(all_samples[1][i]*all_samples[1][j+1]-all_samples[1][j]*all_samples[1][i+1]);
	}
	variables[0]+=E*F;
	variables[1]+=F;
	int blockidx=floor(no_observations/(double)blocksize);
	blocks[blockidx][0]+=E*F;
	blocks[blockidx][1]+=F;
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void energy::compute_result() {
	
	ofstream bout;
	if(blocknamefile!="") {
		bout.open(blocknamefile);
		bout.precision(17);
	}
	
	double best=0.0, best2=0.0;
	for(int i=0;i<no_blocks;i++) {
		double est=blocks[i][0]/blocks[i][1];
		best+=est; best2+=est*est;
		if(blocknamefile!="") bout << est/(2.0*R) << endl;
	}
	if(blocknamefile!="") {
		bout.close();
		if(verb) cout << endl << "Energy blocks written to " << blocknamefile << endl;
	}
		
	results[0]=variables[0]/(variables[1]*2.0*R);
	results[1]=std(best2, best, no_blocks)/(2.0*R);
	results[2]=results[0]/Ne-BG;
	results[3]=results[1]/Ne;
}

void energy::display_result() {
	cout << endl << "Energy = " << results[0] << " +/- " << results[1] << endl;
	cout << "Per particle with background: " << results[2] << " +/- " << results[3] << endl << endl;
}

void energy::write_result(string filename, string separator) {
	ofstream out(filename.c_str());
	out.precision(17);
	out << "Energy"+separator+"Energy_error"+separator+"Energy_per_N_with_BG"+separator+"Energy_per_N_with_BG_error" << endl;
	out << results[0] << separator<< results[1] << separator<< results[2] << separator<< results[3] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}
