/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef EIGSTATE_DATA_H
#define EIGSTATE_DATA_H

#include "MC_data.h"

class eigstate_data : public MC_data {

	private:
	int ne;
	double *eigval;

	public:
	void next_values(VCD&);
	
	eigstate_data(VS, bool, int* use_ne, int n_ave_norm=10000, bool norm=false, int ne=-1);
	
	~eigstate_data() {
		delete [] eigval;
	}

};

#endif
