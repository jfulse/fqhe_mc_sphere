/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Analyse_MC.h"

int main(int argc, const char* argv[]) {

	try {
		clock_t starttime=clock();
		input *params=new input("_","");

		params->add_input("headline 1","--- Observable options ---; ","description");
		params->add_input("--observable,-obs", "Observable to compute", "string","energy");
		params->add_input("obs description", "energy, overlap, eigenstate-overlap, pair-corr-bins,;g-decomp-IP-disk, g-decomp-IP-orthog-sphere,;g-decomp-LS-orthog-sphere, g-decomp-LS-disk,;rank, Gram-Schmidt, mean-log-WF, JK-EP-overlap,;max-orthog-basis, diag-Ham, geo-mean-eigstate-overlap;density-bins, dens-decomp-LS-orthog-sphere;dens-decomp-IP-orthog-sphere","description");
		params->add_input("-Ne", "Number of electrons", "int");
		params->add_input("-Nphi", "Number of magnetic flux", "int");
		params->add_input("--EP-data,-EP", "Filename prefix for storing EP coefficients and bins", "string");
		params->add_input("--JK-EP-data,-JKEP", "Filename prefix for storing JK-EP coefficients (and bins)", "string");
		params->add_input("--EP-format,-EPf", "Store JK-EP coefficients and bins using EP format", "bool");
		params->add_input("--no-eigstates,-ne", "Number of energy eigenstates to be used", "int","-1");
		params->add_input("--skip-eig,-se", "Energy eigenstates to skip for harmonic mean", "int","0");
		params->add_input("--no-g-bins,-gb", "Number of bins in g", "int","100");
		params->add_input("--eigenvectors,-ev", "File prefix for storing orthogonal eigenvectors", "string");
		params->add_input("--no-coeffs,-K", "Number of coefficients in g decomposition", "int","1");
		params->add_input("--GS-folders,-Gf", "Give folders for orthogonal states explicutly", "VS");
		params->add_input("--GS-coeffs,-Gc", "File to write orthogonal coefficients in terms of original functions", "string");
		params->add_input("--E-blocks", "File in which to write block energy values", "string");
		params->add_input("headline 2"," ;--- Input data options ---; ","description");
		params->add_input("--read-folder,-f", "Inputs given as folders; reads all files within", "bool");
		params->add_input("--samples,-N", "Number of MC samples", "ll", "10");
		params->add_input("--bin-size,-b", "Number of samples in bins for error estimate", "int");
		params->add_input("--no-bins,-nb", "Number error bins", "int");
		params->add_input("--wf-input,-wi", "File(s)/folder containing wf values (hdf5 format)", "VS");
		params->add_input("--eig-input,-ei", "File(s)/folder containing energy eigenstate wf values (hdf5 format)", "VS");
		params->add_input("--pos-input,-pi", "File(s)/folder containing position values (hdf5 format)", "VS");
		params->add_input("--bin-input,-bi", "File(s)/folder containing orthogonal bin values (hdf5 format)", "VS");
		params->add_input("input description","Above ('-wi','-ei','-pi','-bi'): Whitespace separates sources;for single state while ' and ' separates states","description");
		params->add_input("--wf-in-file,-wif", "File listing file(s)/folder containing wf values", "string");
		params->add_input("--eig-in-file,-eif", "File listing file(s)/folder containing energy eigenstate values", "string");
		params->add_input("--pos-in-file,-pif", "File listing file(s)/folder containing position values", "string");
		params->add_input("--bin-in-file,-bif", "File listing file(s)/folder containing orthogonal bin values", "string");
		params->add_input("input description 2","Fileformat of above ('-wif','-eif','-pif','-bif'):;Whitespace separates sources for single state while newline;separates states","description");
		params->add_input("--existing-data,-X", "File(s) with existing variables and error bins", "VS");
		params->add_input("--skip-samples,-sN", "Number of MC samples in file(s) to skip", "ll","0");
		params->add_input("headline 3"," ;--- Input normalisation options ---; ","description");
		params->add_input("--norm-ave,-na", "Number of samples used for input normalisation", "int", "10000");
		params->add_input("--write-norm,-wn", "File in which to write input norm values", "string");
		params->add_input("--input-norm,-nin", "Input normalisation values", "VD");
		params->add_input("--no-norm,-nn", "Do not normalise input files", "bool");
		params->add_input("headline 4"," ;--- Miscellaneous options ---; ","description");
		params->add_input("--out-file,-out", "Name of results file", "string","results.dat");
		params->add_input("--store-data,-s", "File for storing variables and error bins for later use", "string");
		params->add_input("--no-csv", "Use ' ' instead of ',' as separator in output files", "bool");
		params->add_input("--buffer-size", "Buffer size used for reading/writing hdf5 files", "int", "10000");
		params->add_input("--seed,-sd", "Integer added to cpu time for random seed", "int");
		params->add_input("--quiet,-q", "Do not print extra information to screen", "bool");
		if(params->command_line_input(argc, argv)!=0) return 1;
		if(!params->check_integrity()) return 1;

		stringstream status;
		int nbins=-1, bsize=-1, n_ave_norm=params->getint("-na"), use_ne;
		if(params->is_activated("-nb")) nbins=params->getint("-nb");
		if(params->is_activated("-b")) bsize=params->getint("-b");
		long long N;
		N=params->getll("-N");
		bool verb=true, folder=false, use_existing=false, EPformat=false;
		if(params->is_flagged("-q")) {
			params->is_activated("-q");
			verb=false;
		}
		if(params->is_flagged("-EPf")) {
			params->is_activated("-EPf");
			EPformat=true;
		}
		if(params->is_flagged("-f")) {
			params->is_activated("-f");
			folder=true;
		}
		if(params->is_flagged("-nn")) {
			params->is_activated("-nn");
			n_ave_norm=0;
		}
		VD in_norms;
		if(params->is_activated("-nin")) {
			n_ave_norm=0;
			in_norms=params->getVD("-nin");
		}
		string storefile="", outfile=params->getstring("-out"), separator=",", EP_data="", JK_EP_data="", obs=params->getstring("-obs"), normfile="";
		string wfinfile="", posinfile="", eiginfile="", bininfile="", Eblockfile="", GS_coeffs="", eigenv_prefix="";
		VS wfin, posin, eigin, binin, existing, GSdirs;
		VI input_sizes;
		if(params->is_activated("-ev")) eigenv_prefix=params->getstring("-ev");
		if(params->is_activated("-wn")) normfile=params->getstring("-wn");
		if(params->is_activated("-Gc")) GS_coeffs=params->getstring("-Gc");
		if(params->is_activated("--E-blocks")) Eblockfile=params->getstring("--E-blocks");
		if(params->is_activated("--GS-folders")) GSdirs=params->getVS("--GS-folders");
		if(params->is_activated("-wi")) wfin=params->getVS("-wi");
		if(params->is_activated("-pi")) posin=params->getVS("-pi");
		if(params->is_activated("-ei")) eigin=params->getVS("-ei");
		if(params->is_activated("-bi")) binin=params->getVS("-bi");
		if(params->is_activated("-wif")) wfinfile=params->getstring("-wif");
		if(params->is_activated("-pif")) posinfile=params->getstring("-pif");
		if(params->is_activated("-eif")) eiginfile=params->getstring("-eif");
		if(params->is_activated("-bif")) bininfile=params->getstring("-bif");
		if(params->is_activated("-X")) {
			if(obs=="Gram-Schmidt") error("Cannot read stored data for Gram-Schmidt procedure");
			if(params->is_activated("-wi")) warning("Using saved data; input wf data ('--wf-input') ignored");
			if(params->is_activated("-pi")) warning("Using saved data; input position data ('--pos-input') ignored");
			if(params->is_activated("-ei")) warning("Using saved data; input eigenstate data ('--eig-input') ignored");
			if(params->is_activated("-bi")) warning("Using saved data; input bin data ('--bin-input') ignored");
			if(params->is_activated("-wif")) warning("Using saved data; input wf data ('--wf-in-file') ignored");
			if(params->is_activated("-pif")) warning("Using saved data; input position data ('--pos-in-file') ignored");
			if(params->is_activated("-eif")) warning("Using saved data; input eigenstate data ('--eig-in-file') ignored");
			if(params->is_activated("-bif")) warning("Using saved data; input bin data ('--bin-in-file') ignored");
			existing=params->getVS("-X");
			if(params->is_activated("-ne")) use_ne=params->getint("-ne");
			if(use_ne<1 && (obs=="eigenstate-overlap" || obs=="geo-mean-eigstate-overlap")) {
				error("When using preexisting data ('-X') and eigenstate data, remember to give number of eigenstates ('-ne'))");
			}
			use_existing=true;
			if(n_ave_norm>0) warning("Cannot perform input normalisation when using existing data (disable using '--no-norm')");
			n_ave_norm=0;
		}
		if(params->is_activated("-s")) storefile=params->getstring("-s");
		if(params->is_activated("-EP")) EP_data=params->getstring("-EP");
		if(params->is_activated("-JKEP")) JK_EP_data=params->getstring("-JKEP");
		if(params->is_activated("--no-csv")) separator=" ";

		vector<MC_data*> datacollection;
		vector<VS> wstates, pstates, estates, bstates;
		get_states(wstates, pstates, estates, bstates, wfin, posin, eigin, binin, wfinfile, posinfile, eiginfile, bininfile, input_sizes);

		for(int i=0;i<wstates.size();i++) datacollection.push_back(new WF_data(wstates[i], folder, n_ave_norm, verb));
		for(int i=0;i<estates.size();i++) datacollection.push_back(new eigstate_data(estates[i], folder, &use_ne, 0, verb, params->getint("-ne")));
		for(int i=0;i<pstates.size();i++) datacollection.push_back(new pos_data(pstates[i], folder, params->getint("-Ne")));

		MC_observable *MCO;

		if(obs=="energy") MCO=new energy(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"), Eblockfile, use_existing, verb, bsize, nbins);
		else if(obs=="overlap") MCO=new overlap(datacollection, N, status, use_existing, verb, bsize, nbins);
		else if(obs=="eigenstate-overlap") MCO=new eigstate_overlap(datacollection, N, status, use_existing, verb, bsize, nbins, use_ne, EP_data, in_norms);
		else if(obs=="geo-mean-eigstate-overlap") MCO=new geo_mean_eigstate_overlap(datacollection, N, status, input_sizes, use_existing, verb, bsize, nbins, use_ne, in_norms);
		else if(obs=="pair-corr-bins") MCO=new pair_corr_bins(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"), 2.0,
									params->getint("-gb"), use_existing, verb, bsize, nbins);
		else if(obs=="density-bins") MCO=new density_bins(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"), 2.0,
									params->getint("-gb"), use_existing, verb, bsize, nbins);
		else if(obs=="g-decomp-IP-orthog-sphere") MCO=new g_decomp_IP_orthog_sphere(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"),
									params->getint("-K"), 2.0, use_existing, verb, bsize, nbins);
		else if(obs=="dens-decomp-IP-orthog-sphere") MCO=new dens_decomp_IP_orthog_sphere(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"),
									params->getint("-K"), 2.0, use_existing, verb, bsize, nbins);
		else if(obs=="g-decomp-LS-orthog-sphere") MCO=new g_decomp_LS_orthog_sphere(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"),
									params->getint("-K"), params->getint("-gb"), 2.0, use_existing, verb, bsize, nbins);
		else if(obs=="dens-decomp-LS-orthog-sphere") MCO=new dens_decomp_LS_orthog_sphere(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"),
									params->getint("-K"), params->getint("-gb"), 2.0, use_existing, verb, bsize, nbins);
		else if(obs=="g-decomp-IP-disk") MCO=new g_decomp_IP_disk(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"),
									params->getint("-K"), 2.0, use_existing, verb, bsize, nbins);
		else if(obs=="g-decomp-LS-disk") MCO=new g_decomp_LS_disk(datacollection, N, status, params->getint("-Ne"), params->getint("-Nphi"),
									params->getint("-K"), params->getint("-gb"), 2.0, use_existing, verb, bsize, nbins);
		else if(obs=="rank") MCO=new find_rank(datacollection, N, status, input_sizes, eigenv_prefix, use_existing, verb, bsize, nbins, in_norms);
		else if(obs=="diag-Ham") MCO=new diagonal_Hamiltonian(datacollection, N, status, input_sizes, params->getint("-Ne"), params->getint("-Nphi"),
															  eigenv_prefix, use_existing, verb, bsize, nbins, in_norms);
		else if(obs=="max-orthog-basis") MCO=new eigenvectors(datacollection, N, status, input_sizes, params->getint("-Ne"), eigenv_prefix, use_existing, verb,
															  bsize, nbins, in_norms);
		else if(obs=="Gram-Schmidt") MCO=new GramSchmidt(datacollection, N, status, input_sizes, GSdirs, GS_coeffs, use_existing, verb, bsize, nbins, in_norms);
		else if(obs=="JK-EP-overlap") MCO=new JK_EP_overlap(datacollection, N, status, input_sizes, EPformat, use_existing,
															verb, bsize, nbins, JK_EP_data, in_norms);
		else if(obs=="mean-log-WF") MCO=new mean_log_WF(datacollection, N, status, use_existing, verb, bsize, nbins);
		else error("Observable "+obs+" unknown or not set up");

		if(verb) {
			display_start(params, 60, "_", argv[0]);
			cout << endl << status.str();
		}

		MCO->setup();
		if(normfile!="") MCO->write_norms(normfile);
		if(use_existing) {
			if(folder) warning("Reading preexisting data from folder not set up");
			MCO->read_samples(existing);
		}
		else {
			if(verb) MCO->display_collection();
			if(MCO->compute_special) MCO->compute_samples_special();
			else MCO->compute_samples();
		}

		//MCO->display_data();
		MCO->compute_result();
		if(verb) MCO->display_result();
		if(storefile!="") MCO->write_variables(storefile, params->getint("--buffer-size"));
		MCO->write_result(outfile,separator);

		delete MCO;
		delete params;
		cout.precision(3);
		if(verb) display_end(starttime, 60, "_");
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
	catch(...) {
		cerr << "UNKNOWN ERROR" << endl;
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
	return 0;
}

void data_in(string infile, VS indata, vector<VS>& states, string name) {

	if(infile!="") {
		if(indata.size()>0) warning("Supplied both "+name+" data input file(s)/folder and file listing such; former is ignored");
		ifstream in(infile.c_str());
		if(!in) error("File "+infile+" not found");
		string line;
		int ctr=0;
		while(getline(in, line)) {
			states.push_back(VS());
			stringstream ss(line);
			string input;
			while(ss >> input) states[ctr].push_back(input);
			ctr++;
		}
	}
	else {
		if(indata.size()>0) {
			if(indata[0]=="and") indata.erase(indata.begin());
			if(indata.size()>0) states.resize(1);
		}
		while(indata.size()>0) {
			if (indata[0]!="and") {
				states[states.size()-1].push_back(indata[0]);
				indata.erase(indata.begin());
			}
			else {
				if(indata.size()<2) error(name+" input: Cannot have 'and' at end");
				states.push_back(VS(0));
				indata.erase(indata.begin());
			}
		}
	}
}

void get_states(vector<VS>& wstates, vector<VS>& pstates, vector<VS>& estates, vector<VS>& bstates, VS wfin, VS posin, VS eigin, VS binin, string wfinfile,
				string posinfile, string eiginfile, string bininfile, VI& input_sizes) {

	data_in(wfinfile, wfin, wstates,"WF");
	data_in(posinfile, posin, pstates,"pos");
	data_in(eiginfile, eigin, estates,"eigstate");
	data_in(bininfile, binin, bstates,"bin");

	input_sizes.resize(4);
	input_sizes[0]=wstates.size();
	input_sizes[1]=pstates.size();
	input_sizes[2]=estates.size();
	input_sizes[3]=bstates.size();
}
