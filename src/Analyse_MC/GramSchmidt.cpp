/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "GramSchmidt.h"

GramSchmidt::GramSchmidt(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, vector<int> input_sizes, VS GSdirs_, 
		string coeff_file_, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) : 
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), GSdirs(GSdirs_), coeff_file(coeff_file_) {
	
	compute_special=true;
	if(input_sizes[1]>0 || input_sizes[2]>0 || input_sizes[3]>0) error("Input data other than WF values supplied; ignored for rank");
	n_WF=input_sizes[0];
	n_pairs=(n_WF*(n_WF-1))/2;
	for(int i=0;i<n_WF;i++) required_collections.push_back("Wavefunction data");
	if(verb) status << "Gram-Schmidt observable added" << endl;
	else cout << "Gram-Schmidt observable added" << endl;
	no_vars=3*n_pairs;
	no_res=2*n_WF+2;
	no_var_blocks=3*n_pairs;
	normvals.resize(n_WF);
	if(!use_existing) {
		if(datacollection.size()<2) error("Require at least two sets of WF values. Did you remember to use 'and' between sets in input?");
		if(norms.size()<1) {
			for(int i=0;i<n_WF;i++) normvals[i]=2.0*real(datacollection[i]->norm_ave[1]-2.0*datacollection[i]->norm_ave[0]);
		}
		else {
			if(norms.size()<n_WF) error("Gram Schmidt needs "+n2s(n_WF)+" norm values");
			else if(norms.size()>n_WF) warning("Gram Schmidt needs "+n2s(n_WF)+" norm values; rest ignored");
			for(int i=0;i<n_WF;i++) normvals[i]=norms[i];
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
}

void GramSchmidt::evaluate_sample(vector<VCD> all_samples) {
	error("GramSchmidt::evaluate_sample()");
}

VS GramSchmidt::orthog_filenames() {
	VS orthognames;
	for(int i=1;i<datacollection.size();i++) {
		string tmpname="";
		VS names=datacollection[i]->get_input_names();
		bool folder=datacollection[i]->folder;
		if(folder) {
			if(names.size()>1) error("Can only have one folder");
			int n=names[0].size();
			if(names[0][n-1]=='/') names[0][n-1]='_';
			else names[0]+="_";
			tmpname=names[0];
			tmpname+="orthog/0.hdf5";
		}
		else{
			string name_alt="orthog";
			string name_alt_bw="";
			for(int j=name_alt.size()-1;j>=0;j--) name_alt_bw+=name_alt[j];
			int ctr=names[0].size()-1;
			string backwards="";
			
			while(ctr>=0 && names[0][ctr]!='/') {
				backwards+=names[0][ctr];
				ctr--;
			}
			if(ctr<0) {
				ctr=names[0].size()-1;
				backwards="";
				while(ctr>=0 && names[0][ctr]!='.') {
					backwards+=names[0][ctr];
					ctr--;
				}
				if(ctr<0) names[0]+="_"+name_alt;
				else {
					backwards+="."+name_alt_bw+"_";
					ctr--;
					while(ctr>=0) {
						backwards+=names[0][ctr];
						ctr--;
					}
				}
			}
			else {
				backwards+="/"+name_alt_bw+"_";
				ctr--;
				while(ctr>=0) {
					backwards+=names[0][ctr];
					ctr--;
				}
			}
			for(int j=backwards.size()-1;j>=0;j--) tmpname+=backwards[j];
		}		
		orthognames.push_back(tmpname);
	}
	
	for(int i=0;i<orthognames.size();i++) {
		string tmp=orthognames[i]+"test.dat";
		ofstream out(tmp.c_str());
		if(!out) error("Cannot write to folder "+orthognames[i]);
		remove(tmp.c_str());
	}
	return orthognames;
}

void GramSchmidt::setup_output(VS filenames, int no_samples, vector<HDF5Wrapper*>& hdf5writer, vector<HDF5Buffer*>& hdf5writebuffer, int buf) { 
	int n=filenames.size();
	for(int i=0;i<n;i++) {
		hdf5writer.push_back(new HDF5Wrapper);
		if(hdf5writer[i]->openFileToWrite(filenames[i].c_str())!=0) error("Failed to open "+filenames[i]+" to write");
		hdf5writer[i]->createRealDataset("wf_values", no_samples, 4);
		hdf5writebuffer.push_back(hdf5writer[i]->setupRealSamplesBuffer("wf_values", 0, buf));
	}
}
	
void GramSchmidt::compute_samples_special() {
	
	int n=datacollection.size();
	vector<VCD> coeffs_orig(n, VCD(n, 0.0));
	coeffs_orig[0][0]=1.0;
	
	VS orthognames;
	if(GSdirs.size()>0) {
		if(GSdirs.size()!=n-1) error("Supplied folder names for orthogonal states must contain n-1 = "+to_string(n-1)+" names");
		else {
			orthognames=GSdirs;
			for(int i=0;i<orthognames.size();i++) {
				if(orthognames[i][orthognames[i].size()-1]!='/') orthognames[i]+="/";
				orthognames[i]+="0.hdf5";
			}
		}		
	}
	else orthognames=orthog_filenames();
	
	vector<HDF5Wrapper*> hdf5writer;
	vector<HDF5Buffer*> hdf5writebuffer;
	setup_output(orthognames, use_samples, hdf5writer, hdf5writebuffer);
	
	vector<MC_data*> orthogonal_functions;
	orthogonal_functions.push_back(datacollection[0]);
	
	vector<VCD> all_samples(2, VCD(2));
	VS files;
	double* wfval=new double[4];
	
	for(int i=1;i<n_WF;i++) {
		VCD GS_factors(i);
		for(int j=0;j<i;j++) {
			datacollection[i]->setup_input(files);
			orthogonal_functions[j]->setup_input(files);
			CD F1_all=0.0;
			double F2_all=0.0;
			for(int k=0;k<use_samples;k++) {
				orthogonal_functions[j]->next_values(all_samples[0]);
				datacollection[i]->next_values(all_samples[1]);
				CD F1;
				double F2;
				evaluate_GS_factor(all_samples, F1, F2, j);
				F1_all+=F1;
				F2_all+=F2;
			}
			GS_factors[j]=F1_all/F2_all;
			if(verb) cout << "GS fac (" << i << "," << j << ") = " << GS_factors[j] << endl;
		}
		
		for(int j=0;j<i;j++) {
			for(int k=0;k<=j;k++) coeffs_orig[i][k]-=GS_factors[j]*coeffs_orig[j][k];
		}
		coeffs_orig[i][i]=1.0;
		
		datacollection[i]->setup_input(files);
		for(int j=0;j<i;j++) orthogonal_functions[j]->setup_input(files);
		VCD single_wf_values(2);

		if(verb) cout << "Computing and writing orthogonal state " << i << endl;
		for(int k=0;k<use_samples;k++) {
			datacollection[i]->next_values(single_wf_values);
			CD superpos_single=exp(single_wf_values[1]);
			for(int j=0;j<i;j++) {
				orthogonal_functions[j]->next_values(single_wf_values);
				superpos_single-=GS_factors[j]*exp(single_wf_values[1]);
			}
			superpos_single=log(superpos_single);
			wfval[0]=real(single_wf_values[0]);
			wfval[1]=imag(single_wf_values[0]);
			wfval[2]=real(superpos_single);
			wfval[3]=imag(superpos_single);
			hdf5writebuffer[i-1]->writeToBuffer(wfval, 4);
		}
		hdf5writebuffer[i-1]->flushBuffer();
		WF_data *orthog_data=new WF_data(VS(1, orthognames[i-1]), false, 1, use_samples, use_samples);
		orthogonal_functions.push_back(orthog_data);
		
	}
	
	if(coeff_file!="") {
		ofstream coeffout(coeff_file.c_str());
		if(!coeffout) warning("Could not open file "+coeff_file+" for writing");
		else {		
			for(int i=0;i<coeffs_orig.size();i++) {
				for(int j=0;j<coeffs_orig[i].size();j++) coeffout << real(coeffs_orig[i][j]) << " ";
				coeffout << endl;
			}
			coeffout.close();
			if(verb) cout << endl << "Orthogonal coefficients in terms of original functions written to " << coeff_file << endl;
		}
	}
	
	if(verb) cout << endl;
	cout << "Orthogonal states written to:" << endl;
	for(int i=0;i<orthognames.size();i++) cout << orthognames[i] << endl;
		
	for(int i=0;i<hdf5writer.size();i++) delete hdf5writer[i];
	delete [] wfval;
	
}

void GramSchmidt::test_data(string filename, int nsamples, int ndisp) {
	VCD single_wf_values(2);
	WF_data *tmp=new WF_data(VS(1, filename), false, 1, nsamples, use_samples);
	VS files;
	tmp->setup_input(files);
	for(int i=0;i<ndisp;i++) {
		tmp->next_values(single_wf_values);
		cout << single_wf_values[0] << " , " << single_wf_values[1] << endl;
	}
	delete tmp;
}

void GramSchmidt::evaluate_GS_factor(vector<VCD> all_samples, CD& F1, double& F2, int i) {// conjugates 1st input
	F1=exp(conj(all_samples[0][1])+all_samples[1][1]-2.0*real(all_samples[0][0])-normvals[i]);
	F2=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0]))-normvals[i]);
}

void GramSchmidt::compute_result() {}

void GramSchmidt::display_result() {}

void GramSchmidt::write_result(string filename, string sep) {}

void GramSchmidt::write_norms(string filename) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<n_WF;i++) out << normvals[i] << endl;
	out.close();
}
