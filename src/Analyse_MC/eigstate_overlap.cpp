/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/

#include "eigstate_overlap.h"

eigstate_overlap::eigstate_overlap(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, bool use_existing_, bool verb_, 
		int blocksize_, int no_blocks_, int ne_, string EP_prefix_, VD norms) : 
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), ne(ne_), EP_prefix(EP_prefix_) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Eigenstate data");
	if(verb) status << "Eigenstate overlap observable added (" << ne << " states)" << endl;
	else cout << "Eigenstate overlap observable added (" << ne << " states)" << endl;
	three_ne=3*ne;
	no_vars=2*three_ne+2;
	half_no_vars=three_ne+1;
	half_no_blocks=no_blocks/2;
	no_res=16*ne+1;
	no_var_blocks=three_ne+1;
	if(!use_existing) {
		if(norms.size()<1) {
			normval1=real(datacollection[0]->norm_ave[1]-2.0*datacollection[0]->norm_ave[0]);
			normval2=real(datacollection[0]->norm_ave[1]-datacollection[0]->norm_ave[0]);
			normval3=-real(datacollection[0]->norm_ave[0]);
		}
		else {
			if(norms.size()<3) error("Eigenstate overlap needs three norm values");
			else if(norms.size()>3) warning("Eigenstate overlap needs three norm values; rest ignored");
			normval1=norms[0];
			normval2=norms[1];
			normval3=norms[2];
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
	double normtest=(-normval1)-0.5*(-2.0*normval2-2.0*normval3);
	if(abs(normtest)>1e-10) error("Input normalisation values do not cancel");
}

void eigstate_overlap::evaluate_sample(vector<VCD> all_samples) {// conjugates 2nd input (eigenvalue)

	int blockidx=floor(no_observations/(double)blocksize);
	CD F1_fac=exp(all_samples[0][1]-2.0*real(all_samples[0][0])-normval1);
	double F3_fac=exp(-2.0*(real(all_samples[0][0])+normval3));
	double F2=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval2));
	int addhalf=((no_observations+1)>use_samples/2)*half_no_vars;
	for(int i=0;i<ne;i++) {
		CD F1=F1_fac*conj(all_samples[1][i]);
		double F3=F3_fac*norm(all_samples[1][i]);
		int three_i=3*i;
		blocks[blockidx][three_i]+=real(F1);
		blocks[blockidx][three_i+1]+=imag(F1);
		blocks[blockidx][three_i+2]+=F3;
		
		variables[three_i+addhalf]+=real(F1);
		variables[three_i+1+addhalf]+=imag(F1);
		variables[three_i+2+addhalf]+=F3;
		
	}
	variables[three_ne+addhalf]+=F2;
	blocks[blockidx][three_ne]+=F2;
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void eigstate_overlap::compute_result() {
	
	LLLContent=0.0;
	VD LLLcont_block(half_no_blocks, 0.0);
	
	ofstream EP_out, EP_norm_out;
	if(EP_prefix!="") {
		string cname=EP_prefix+"_c.dat", bname=EP_prefix+"_b.hdf5", nname=EP_prefix+"_n.dat";
		if(verb) cout << endl;
		cout << "EP coefficients and bins written to " << cname << " and " << bname << endl;
		EP_out.open(cname.c_str());
    if(!EP_out) error("Could not open "+cname+" for writing");
		EP_norm_out.open(nname.c_str());
    if(!EP_norm_out) error("Could not open "+nname+" for writing");
		EP_out.precision(17);		
		EP_norm_out.precision(17);		
		
		HDF5Wrapper* hdf5writer=new HDF5Wrapper;
		HDF5Buffer* hdf5writebuffer;
		if(hdf5writer->openFileToWrite(bname.c_str())!=0) error("Could not open "+bname+" for hdf5 writing");
		hdf5writer->createRealDataset("coeff_bins", half_no_blocks, 8*ne);
		hdf5writebuffer=hdf5writer->setupRealSamplesBuffer("coeff_bins", 0, 10000);
		double *cblocks=new double[8*ne];
		for(int i=0;i<half_no_blocks;i++) {
			for(int j=0;j<ne;j++) {
				CD est_all=CD(blocks[i][3*j]+blocks[i+half_no_blocks][3*j], blocks[i][3*j+1]+blocks[i+half_no_blocks][3*j+1])/
						   sqrt((blocks[i][three_ne]+blocks[i+half_no_blocks][three_ne])*(blocks[i+half_no_blocks][3*j+2]+blocks[i][3*j+2]));
				CD est_1=CD(blocks[i][3*j], blocks[i][3*j+1])/sqrt(blocks[i][three_ne]*blocks[i][3*j+2]);
				CD est_2=CD(blocks[i+half_no_blocks][3*j], blocks[i+half_no_blocks][3*j+1])/
						 sqrt(blocks[i+half_no_blocks][three_ne]*blocks[i+half_no_blocks][3*j+2]);
				cblocks[8*j]=real(est_all);
				cblocks[8*j+1]=imag(est_all);
				cblocks[8*j+2]=sqrt(abs(real(est_1*conj(est_2))));
				cblocks[8*j+3]=abs(real(est_1*conj(est_2)));
				cblocks[8*j+4]=real(est_1);
				cblocks[8*j+5]=imag(est_1);
				cblocks[8*j+6]=real(est_2);
				cblocks[8*j+7]=imag(est_2);
			}
			hdf5writebuffer->writeToBuffer(cblocks, 8*ne);
		}
		delete [] cblocks;
		delete hdf5writer;
	}
	
	overlap.resize(ne);
	overlap_norm.resize(ne);
	overlap_error.resize(ne);
	overlap_norm_error.resize(ne);
	abs_overlap.resize(ne);
	abs_sq_overlap.resize(ne);
	abs_overlap_norm.resize(ne);
	abs_sq_overlap_norm.resize(ne);
	abs_overlap_error.resize(ne);
	abs_sq_overlap_error.resize(ne);
	abs_overlap_norm_error.resize(ne);
	abs_sq_overlap_norm_error.resize(ne);
	
	for(int j=0;j<ne;j++) {
		double re=0.0, re2=0.0, im=0.0, im2=0.0, ab=0.0, ab2=0.0, sq=0.0, sq2=0.0;
		for(int i=0;i<half_no_blocks;i++) {
				CD est_all=CD(blocks[i][3*j]+blocks[i+half_no_blocks][3*j], blocks[i][3*j+1]+blocks[i+half_no_blocks][3*j+1])/
						   sqrt((blocks[i][three_ne]+blocks[i+half_no_blocks][three_ne])*(blocks[i+half_no_blocks][3*j+2]+blocks[i][3*j+2]));
				CD est_1=CD(blocks[i][3*j], blocks[i][3*j+1])/sqrt(blocks[i][three_ne]*blocks[i][3*j+2]);
				CD est_2=CD(blocks[i+half_no_blocks][3*j], blocks[i+half_no_blocks][3*j+1])/
						 sqrt(blocks[i+half_no_blocks][three_ne]*blocks[i+half_no_blocks][3*j+2]);
			re+=real(est_all); re2+=real(est_all)*real(est_all);
			im+=imag(est_all); im2+=imag(est_all)*imag(est_all);
			double abstmp=sqrt(abs(real(est_1*conj(est_2)))), normtmp=abs(real(est_1*conj(est_2)));
			ab+=abstmp; ab2+=abstmp*abstmp;
			sq+=normtmp; sq2+=normtmp*normtmp;
			LLLcont_block[i]+=normtmp;
		}
		
		CD eigstate_overlap_split_1=CD(variables[3*j],variables[3*j+1])/sqrt(variables[three_ne]*variables[3*j+2]);
		CD eigstate_overlap_split_2=CD(variables[3*j+half_no_vars],variables[3*j+1+half_no_vars])/
									sqrt(variables[three_ne+half_no_vars]*variables[3*j+2+half_no_vars]);
		CD eigstate_overlap_all=CD(variables[3*j]+variables[3*j+half_no_vars],variables[3*j+1]+variables[3*j+1+half_no_vars])/
						     sqrt((variables[three_ne]+variables[three_ne+half_no_vars])*(variables[3*j+2]+variables[3*j+2+half_no_vars]));
		
		overlap[j]=eigstate_overlap_all;
		overlap_error[j]=CD(std(re2, re, half_no_blocks), std(im2, im, half_no_blocks));
		abs_overlap[j]=sqrt(abs(real(eigstate_overlap_split_1*conj(eigstate_overlap_split_2))));
		abs_overlap_error[j]=std(ab2, ab, half_no_blocks);
		abs_sq_overlap[j]=abs(real(eigstate_overlap_split_1*conj(eigstate_overlap_split_2)));
		abs_sq_overlap_error[j]=std(sq2, sq, half_no_blocks);
		LLLContent+=abs_sq_overlap[j];
		
		if(EP_prefix!="") {
			EP_out << real(eigstate_overlap_all) << "," << imag(eigstate_overlap_all) << "," << abs_overlap[j] << "," << abs_sq_overlap[j] << ",";
			EP_out << real(eigstate_overlap_split_1) << "," << imag(eigstate_overlap_split_1) << ",";
			EP_out << real(eigstate_overlap_split_2) << "," << imag(eigstate_overlap_split_2) << endl;
		}
	}
	
	for(int j=0;j<ne;j++) {
		double re=0.0, re2=0.0, im=0.0, im2=0.0, ab=0.0, ab2=0.0, sq=0.0, sq2=0.0;
		for(int i=0;i<half_no_blocks;i++) {
				CD est_all=CD(blocks[i][3*j]+blocks[i+half_no_blocks][3*j], blocks[i][3*j+1]+blocks[i+half_no_blocks][3*j+1])/
						   sqrt(LLLcont_block[i]*(blocks[i][three_ne]+blocks[i+half_no_blocks][three_ne])*(blocks[i+half_no_blocks][3*j+2]+blocks[i][3*j+2]));
				CD est_1=CD(blocks[i][3*j], blocks[i][3*j+1])/sqrt(LLLcont_block[i]*blocks[i][three_ne]*blocks[i][3*j+2]);
				CD est_2=CD(blocks[i+half_no_blocks][3*j], blocks[i+half_no_blocks][3*j+1])/
						 sqrt(LLLcont_block[i]*blocks[i+half_no_blocks][three_ne]*blocks[i+half_no_blocks][3*j+2]);
			re+=real(est_all); re2+=real(est_all)*real(est_all);
			im+=imag(est_all); im2+=imag(est_all)*imag(est_all);
			double abstmp=sqrt(abs(real(est_1*conj(est_2)))), normtmp=abs(real(est_1*conj(est_2)));
			ab+=abstmp; ab2+=abstmp*abstmp;
			sq+=normtmp; sq2+=normtmp*normtmp;
		}
		
		overlap_norm[j]=overlap[j]/sqrt(LLLContent);
		overlap_norm_error[j]=CD(std(re2, re, half_no_blocks), results[16*j+11]=std(im2, im, half_no_blocks));
		abs_overlap_norm[j]=abs_overlap[j]/sqrt(LLLContent);
		abs_overlap_norm_error[j]=std(ab2, ab, half_no_blocks);
		abs_sq_overlap_norm[j]=abs_sq_overlap[j]/sqrt(LLLContent);
		abs_sq_overlap_norm_error[j]=std(ab2, ab, half_no_blocks);
    
		if(EP_prefix!="") {
      EP_norm_out << real(overlap_norm[j]) << " " << imag(overlap_norm[j]) << endl;
    }
	}
	LLLContent=sqrt(LLLContent);
	
	if(EP_prefix!="") {
    EP_out.close();
    EP_norm_out.close();
  }
}

void eigstate_overlap::display_result() {
	cout << endl;
	for(int j=0;j<ne;j++) {
		cout << "State " << j << ":" << endl;
		cout << "  Eigenstate overlap = " << overlap[j] << " +/- " << overlap_error[j] << endl;
		cout << "  |Eigenstate overlap| = " << abs_overlap[j] << " +/- " << abs_overlap_error[j] << endl;
		cout << "  |Eigenstate overlap|^2 = " << abs_sq_overlap[j] << " +/- " << abs_sq_overlap_error[j] << endl;
		cout << "  Eigenstate overlap normed = " << overlap_norm[j] << " +/- " << overlap_norm_error[j] << endl;
		cout << "  |Eigenstate overlap normed| = " << abs_overlap_norm[j] << " +/- " << abs_overlap_norm_error[j] << endl;
		cout << "  |Eigenstate overlap normed|^2 = " << abs_sq_overlap_norm[j] << " +/- " << abs_sq_overlap_norm_error[j] << endl;
	}
	cout << endl << "sqrt(LLL content) = " << LLLContent << endl << endl;
}

void eigstate_overlap::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	
	out << "StateIdx"+sep+"Re(EigenstateOverlap)"+sep+"Im(EigenstateOverlap)"+sep+"Re(EigenstateOverlap)_error"+sep+"Im(EigenstateOverlap)_error" << sep;
	out << "Abs(EigenstateOverlap)" << sep << "Abs(EigenstateOverlap)_error" << sep;
	out << "Abs(EigenstateOverlap)^2" << sep << "Abs(EigenstateOverlap)^2_error" << sep;
	out << "Re(EigenstateOverlapNormed)"+sep+"Im(EigenstateOverlapNormed)"+sep+"Re(EigenstateOverlapNormed)_error"+sep+"Im(EigenstateOverlapNormed)_error" << sep;
	out << "Abs(EigenstateOverlapNormed)" << sep << "Abs(EigenstateOverlapNormed)_error" << sep;
	out << "Abs(EigenstateOverlapNormed)^2" << sep << "Abs(EigenstateOverlapNormed)^2_error" << sep << "sqrt(LLLcontent)" << endl;
	
	for(int j=0;j<ne;j++) {
		out << j << sep << real(overlap[j]) << sep << imag(overlap[j]) << sep << real(overlap_error[j]) << sep << imag(overlap_error[j]) << sep;
		out << abs_overlap[j] << sep << abs_overlap_error[j] << sep << abs_sq_overlap[j] << sep << abs_sq_overlap_error[j] << sep;
		out << real(overlap_norm[j]) << sep << imag(overlap_norm[j]) << sep << real(overlap_norm_error[j]) << sep << imag(overlap_norm_error[j]) << sep;
		out << abs_overlap_norm[j] << sep << abs_overlap_norm_error[j] << sep << abs_sq_overlap_norm[j] << sep << abs_sq_overlap_norm_error[j] << sep;
		out << LLLContent << endl;
	}
	
	out.close();
	cout << "Output written to " << filename << endl;
}

void eigstate_overlap::write_norms(string filename) {
	ofstream out(filename.c_str());
	if(!out) error("Could not write to file "+filename);
	out.precision(17);
	out << normval1 << " " << normval2 << " " << normval3 << endl;
	out.close();
}
