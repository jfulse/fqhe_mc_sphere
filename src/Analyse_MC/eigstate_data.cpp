/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "eigstate_data.h"

eigstate_data::eigstate_data(VS inputnames_, bool folder_, int* use_ne, int n_ave_norm, bool verb, int ne_) : 
							MC_data(inputnames_, "wf_values", folder_, "Eigenstate data", n_ave_norm, verb), ne(ne_) {
	if(cols%2) error("Expect an even number of columns for eigenstate data; have "+to_string(cols));
	if(ne<1) ne=cols/2;
	else if(ne>cols/2) error("File only contains "+n2s(cols/2)+" eigenstates; requested "+n2s(ne));
	eigval=new double[cols];
	n_return_values=ne;
	*use_ne=ne;
}

void eigstate_data::next_values(VCD& vals) {
	hdf5readbuffer[data_position/samplesperfile]->readFromBuffer(eigval, cols);
	for(int i=0;i<2*n_return_values;i+=2) vals[i/2]=CD(eigval[i], eigval[i+1]);
	data_position++;
}
