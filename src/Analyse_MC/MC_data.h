/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef MC_DATA_H
#define MC_DATA_H

#include "misc.h"
#include <dirent.h>
#include "HDF5Wrapper.h"

typedef vector<CD> VCD;
typedef vector<string> VS;

class MC_data {
	
	private:
	
	int nfiles;
	vector<HDF5Wrapper*> hdf5reader, hdf5writer;
	string dataset;
	VS inputnames, files;
	
	void get_folder_files(VS&, string, int&);
	void orderfiles(VS&);
	VS dirfiles(string&);
	void getnorm(int, VS);
	
	protected:
	
	long long samplesperfile;
	long long data_position;
	vector<HDF5Buffer*> hdf5readbuffer, hdf5writebuffer;
		
	public:
	int n_return_values, cols;
	long long nsamples;
	string dataname;
	VCD norm_ave;
	bool folder;
	
	void setup_input(VS&);
	void display();
	VS get_input_names();
	VS get_files();
	string get_dataset();
	
	virtual void next_values(VCD&)=0;
	string getname();
	
	MC_data(VS, string, bool, string, int, bool verb=false);
	MC_data(VS, string, bool, string, int, int, int);
		
	~MC_data() {
		for(int i=0;i<hdf5reader.size();i++) delete hdf5reader[i];
		hdf5reader.clear();
		for(int i=0;i<hdf5writer.size();i++) delete hdf5writer[i];
		hdf5writer.clear();
	}

};

#endif
