/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "g_decomp_IP_disk.h"

g_decomp_IP_disk::g_decomp_IP_disk(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, 
		int Ne_, int Nphi_, int nfuncs_, double cutoff_, bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) : 
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), Ne(Ne_), Nphi(Nphi_), nfuncs(nfuncs_), cutoff(cutoff_) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Configuration data");
	if(verb) status << "g decomposition inner product\ndisk observable added" << endl;
	else cout << "g decomposition inner product\ndisk observable added" << endl;
	
	int max_rec=ceil(((double)Nphi+2.0)/4.0);
	if(nfuncs>max_rec) {
		warning("Requested "+n2s(nfuncs)+" functions; reverting to recommended maximum ceil[(Nphi+2)/4]+1 = "+n2s(max_rec));
		nfuncs=max_rec;
	}
	
	R2=Nphi/2.0;
	twoNe=2*Ne;
	no_vars=nfuncs+1;
	no_res=2*nfuncs;
	no_var_blocks=nfuncs+1;
	setup_nu1_int();
	setup_ov_mat();
}

void g_decomp_IP_disk::evaluate_sample(vector<VCD> all_samples) {
	double F=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval));
	int blockidx=floor(no_observations/(double)blocksize);
	
	for(int i=0;i<twoNe-2;i+=2) {
		for(int j=i+2;j<twoNe;j+=2) {
			double r=2.0*abs(all_samples[1][i]*all_samples[1][j+1]-all_samples[1][j]*all_samples[1][i+1]);
			if(r<cutoff) {
				double r2div4=R2*r*r/4.0;
				double r4div8=r2div4*r2div4;
				double funcval=2.0*r2div4*exp(-r2div4)*F;
				for(int k=0;k<nfuncs;k++) {
					variables[k]+=funcval;
					blocks[blockidx][k]+=funcval;
					funcval*=r4div8/((2.0*k+3.0)*(2.0*k+2.0));
				}
			}
		}
	}
	
	variables[nfuncs]+=F;
	blocks[blockidx][nfuncs]+=F;
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void g_decomp_IP_disk::compute_result() {
	
	double dens=Ne/(2.0*pi*Nphi);
	Eigen::VectorXd funcs(nfuncs);
	
	for(int i=0;i<nfuncs;i++) funcs[i]=variables[i]*2.0/(Ne*variables[nfuncs]*dens)-nu1_int[i];
	Eigen::VectorXd c=ov_mat.inverse()*funcs;
	
	VD b(nfuncs, 0.0), b2(nfuncs, 0.0);
	for(int j=0;j<no_blocks;j++) {
		Eigen::VectorXd functmp(nfuncs);
		for(int k=0;k<nfuncs;k++) functmp(k)=blocks[j][k]*2.0/(Ne*blocks[j][nfuncs]*dens)-nu1_int[k];
		Eigen::VectorXd ctmp=ov_mat.inverse()*functmp;
		for(int k=0;k<nfuncs;k++) {
			b[k]+=ctmp(k);
			b2[k]+=ctmp(k)*ctmp(k);
		}
	}
		
	for(int i=0;i<nfuncs;i++) {
		results[2*i]=c(i);	
		results[2*i+1]=std(b2[i], b[i], no_blocks);
	}
	
}

void g_decomp_IP_disk::display_result() {
	cout << endl;
	for(int i=0;i<nfuncs-1;i++) cout << "c_" << i+1 << " = " << results[2*i] << " +/- " << results[2*i+1] << endl;
	cout << "c_" << nfuncs << " = " << results[2*nfuncs-2] << " +/- " << results[2*nfuncs-1] << endl;
	cout << endl;
}

void g_decomp_IP_disk::write_result(string filename, string separator) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<nfuncs-1;i++) out << "c_" << i+1 << separator << "err_" << i+1 << separator;
	out << "c_" << nfuncs << separator << "err_" << nfuncs << endl;
	for(int i=0;i<nfuncs-1;i++) out << results[2*i] << separator << results[2*i+1] << separator;
	out << results[2*nfuncs-2] << separator << results[2*nfuncs-1] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}

void g_decomp_IP_disk::setup_nu1_int() {
	nu1_int.resize(nfuncs);
	for(int i=0;i<nfuncs;i++) {
		int m1=2*i+1;
		nu1_int[i]=8.0*pi*(1.0-1.0/pow(3.0,m1+1)+(1.0/pow(3.0,m1+1)*gsl_sf_gamma_inc(m1+1,3.0*R2)-gsl_sf_gamma_inc(m1+1,R2))/fact(m1));
	}
}

void g_decomp_IP_disk::setup_ov_mat() {
	ov_mat.resize(nfuncs, nfuncs);
	for(int i=0;i<nfuncs;i++) {
		int m1=2*i+1;
		for(int j=i;j<nfuncs;j++) {
			int m2=2*j+1;
			ov_mat(i,j)=8.0*pi*fact(m1+m2)/(pow(2,m1+m2)*fact(m1)*fact(m2));
		}
		for(int j=0;j<i;j++) ov_mat(i,j)=ov_mat(j,i);
	}
}
