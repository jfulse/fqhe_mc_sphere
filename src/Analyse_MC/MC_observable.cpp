/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "MC_observable.h"

MC_observable::MC_observable(vector<MC_data*> datacollection_, int use_samples_, VD norms, bool use_existing_, int blocksize_, int no_blocks_, bool verb_) : 
		datacollection(datacollection_), blocksize(blocksize_), no_blocks(no_blocks_), use_samples(use_samples_), use_existing(use_existing_), verb(verb_) {
	
	compute_special=false;
	
	if(!use_existing) {
		no_collections=datacollection.size();
		for(int i=0;i<no_collections;i++) {
			if(datacollection[i]->nsamples<use_samples) {
				error("Data collection "+n2s(i+1)+" ("+datacollection[i]->dataname+") has only "+n2s(datacollection[i]->nsamples)+
						" samples; requested "+ n2s(use_samples));
			}
		}
	}
	if(no_blocks<1) {
		int def_blocksize=use_samples/5e3+(use_samples/5e3<1 && use_samples>=10)*10+(use_samples<10)*1;
		if(blocksize==-1) blocksize=def_blocksize;
		no_blocks=floor((double)use_samples/blocksize);
		if(abs((double)use_samples/blocksize-no_blocks)>1e-13) no_blocks++;
		if(no_blocks<2) {
			warning("Requested size "+n2s(blocksize)+" error bins with "+n2s(use_samples)+" samples; changed to "+n2s(def_blocksize));
			blocksize=def_blocksize;
			no_blocks=floor((double)use_samples/blocksize);
			if(abs((double)use_samples/blocksize-no_blocks)>1e-13) no_blocks++;
		}
	}
	else {
		if(blocksize>0) warning("Both number of error bins and binsize specified; latter is overridden");
		blocksize=floor((double)use_samples/no_blocks);
	}
	if(abs((double)use_samples/no_blocks-blocksize)>1e-13) error("Number of blocks ("+n2s(no_blocks)+
			") must divide number of samples ("+n2s(use_samples)+")");
	
	no_observations=0;
	samplecount=use_samples/10;
	get_input_norm(norms);
}

void MC_observable::display_collection() {
	cout << endl << "Data collection:" << endl;
	for(int i=0;i<no_collections;i++) {
		cout << i+1 << " - ";
		datacollection[i]->display();
	}
	cout << endl;
}

void MC_observable::setup() {
	if(!use_existing) {
		int n_required=required_collections.size();
		if(no_collections!=n_required) error("Need "+n2s(n_required)+" data collections for observable; supplied "+n2s(no_collections));
		for(int i=0;i<no_collections;i++) {
			if(datacollection[i]->dataname!=required_collections[i]) {
				bool found=false;
				for(int j=i+1;j<no_collections;j++) {
					if(datacollection[j]->dataname==required_collections[i]) {
						found=true;
						MC_data* tmp=datacollection[i];
						datacollection[i]=datacollection[j];
						datacollection[j]=tmp;
					}
				}
				if(!found) error("Observable requires '"+required_collections[i]+"'; not found among input");
			}
		}
        	VCD test_probs;
		bool first=true, different=false;
	        for(int i=0;i<no_collections && !different;i++) {
        	        if(datacollection[i]->dataname=="Wavefunction data") {
                	        VCD probs=read_n_samples(datacollection[i], "wf_values", 10);
                       		 if(first) {
                                	//test_probs=probs;
                                	first=false;
	                        }
        	                else {
                	                //for(int i=0;i<min(probs.size(), test_probs.size());i++) {
                        	                //if(abs(exp(probs[i])-exp(test_probs[i]))>1e-12) {
                                	                //different=true;
                                        	        //break;
	                                        //}
        	                        //}
                	        }
                	}
       		}
		if(different) warning("Wavefunction data with different probabilities");
	}
	
	variables.resize(no_vars, 0.0);
	results.resize(no_res, 0.0);
	blocks.resize(no_blocks, VD(no_var_blocks, 0.0));
}

VCD MC_observable::read_n_samples(MC_data* data, string dataset, int n, int index) {
	
	VCD values;
	HDF5Wrapper* hdf5reader=new HDF5Wrapper;
	HDF5Buffer* hdf5readbuffer;
	string file=data->get_files()[0];
	
	if(hdf5reader->openFileToRead(file.c_str())) error("Failed to open "+file+" to read");
	hdf5readbuffer=hdf5reader->setupRealSamplesBuffer(dataset.c_str(), 0);
	unsigned long check_rows, check_cols;
	hdf5reader->getDatasetDims(dataset.c_str(), check_rows, check_cols);
	if(n>check_rows) {
		warning("Tried testing probability values of first file with "+to_string(n)+" samples; changing to "+to_string(check_rows));
		n=check_rows;
	}
	
	double *tmp=new double[check_cols];
	for(int i=0;i<n;i++) {
		hdf5readbuffer->readFromBuffer(tmp, check_cols);
		values.push_back(CD(tmp[0+2*index], tmp[1+2*index]));
	}
	
	delete hdf5reader;
	delete [] tmp;
	return values;
}

void MC_observable::compute_samples() {
	vector<VCD> all_samples(no_collections);
	all_samples.resize(no_collections);
	for(int j=0;j<no_collections;j++) all_samples[j].resize(datacollection[j]->n_return_values);
	for(long long i=0;i<use_samples;i++) {
		for(int j=0;j<no_collections;j++) datacollection[j]->next_values(all_samples[j]);
		evaluate_sample(all_samples);
	}
}

double MC_observable::std(double av2, double av, int n) {
	return sqrt((av2/n-av*av/(n*n))/(n-1.0));
}

double* MC_observable::VD2da(VD v) {
	int n=v.size();
	double *ret=new double[n];
	for(int i=0;i<n;i++) ret[i]=v[i];
	return ret;
}

void MC_observable::write_variables(string outfile, int buffersize) {
	HDF5Wrapper *hdf5writer=new HDF5Wrapper;
	HDF5Buffer *hdf5writevarbuffer, *hdf5writeblockbuffer;
	
	if(hdf5writer->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
	hdf5writer->createRealDataset("variables", 1, no_vars);
	hdf5writer->createRealDataset("blocks", no_blocks, no_var_blocks);
	hdf5writevarbuffer=hdf5writer->setupRealSamplesBuffer("variables", 0, buffersize);
	hdf5writeblockbuffer=hdf5writer->setupRealSamplesBuffer("blocks", 0, buffersize);
	
	hdf5writevarbuffer->writeToBuffer(VD2da(variables), no_vars);
	for(int i=0;i<no_blocks;i++) hdf5writeblockbuffer->writeToBuffer(VD2da(blocks[i]), no_var_blocks);
	
	delete hdf5writer;
	cout << "Data stored in " << outfile << endl;
}

void MC_observable::check_sample_file(HDF5Wrapper* hdf5reader, string file, int blocksperfile) {
	unsigned long var_rows, var_cols, block_rows, block_cols;
	hdf5reader->getDatasetDims("variables", var_rows, var_cols);
	hdf5reader->getDatasetDims("blocks", block_rows, block_cols);
	if(var_cols!=no_vars) error("Data in "+file+" corresponds to "+n2s((long long)var_cols)+" variables; observable has "+n2s(no_vars));
	if(block_cols!=no_var_blocks) error("Data in "+file+" corresponds to "+n2s((long long)block_cols)+" error bin variables; observable has "+
											n2s(no_var_blocks));
	if(var_rows!=1) error("Data in "+file+" has "+n2s((long long)var_rows)+" variable rows; expect one");											
	if(block_rows!=blocksperfile) error("Data in "+file+" corresponds to "+n2s((long long)block_rows)+" error bins; need "+n2s(blocksperfile)+" per file");
}
	
void MC_observable::display_data() {
	cout << "variables:" << endl;
	for(int i=0;i<no_vars;i++) cout << variables[i] << " ";
	cout << endl << "blocks: " << endl;
	for(int i=0;i<no_blocks;i++) {
		for(int j=0;j<no_var_blocks;j++) cout << blocks[i][j] << "  ";
		cout << endl;
	}
	cout << endl;
}

void MC_observable::read_samples(VS existing) {
	int n=existing.size();
	int blocksperfile=floor((double)no_blocks/n);
	if(abs(blocksperfile-(double)no_blocks/n)>1e-13) error("Number of files must divide total number of error bins ("+n2s(no_blocks)+")");
	
	for(int i=0;i<n;i++) {
		HDF5Wrapper* hdf5reader=new HDF5Wrapper;
		if(hdf5reader->openFileToRead(existing[i].c_str())) error("Failed to open "+existing[i]+" to read");
		check_sample_file(hdf5reader, existing[i], blocksperfile);
		HDF5Buffer* hdf5readvalbuffer=hdf5reader->setupRealSamplesBuffer("variables", 0);
		HDF5Buffer* hdf5readblockbuffer=hdf5reader->setupRealSamplesBuffer("blocks", 0);
		
		double *var_in=new double[no_vars], *block_in=new double[no_var_blocks];
		hdf5readvalbuffer->readFromBuffer(var_in, no_vars);
		for(int k=0;k<no_vars;k++) variables[k]+=var_in[k];
		
		for(int j=0;j<blocksperfile;j++) {
			hdf5readblockbuffer->readFromBuffer(block_in, no_var_blocks);
			for(int k=0;k<no_var_blocks;k++) blocks[j+i*blocksperfile][k]+=block_in[k];
		}	
	
		delete [] var_in;
		delete [] block_in;
		delete hdf5reader;
	}
	
	if(verb) cout << endl;
	cout << "Data read from " << existing[0];
	existing.erase(existing.begin());
	int ctr=0;
	while(existing.size()>1) {
		cout << ", ";
		if(!(ctr%3)) cout << endl;
		cout << existing[0];
		existing.erase(existing.begin());
		ctr++;
	}
	if(existing.size()>0) {
		cout << endl << "and " << existing[0];
	}
	if(verb) cout << endl;
}

void MC_observable::get_input_norm(VD norms) {
	if(!use_existing) {
		if(norms.size()<1) {
			if(datacollection.size()>0) normval=real(datacollection[0]->norm_ave[1]-datacollection[0]->norm_ave[0]);
			else warning("No data input; cannot find default input norm");
		}
		else normval=norms[0];
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
}

void MC_observable::write_norms(string filename) {
	ofstream out(filename.c_str());
	if(!out) error("Could not write to file "+filename);
	out.precision(17);
	out << normval << endl;
	out.close();
}
