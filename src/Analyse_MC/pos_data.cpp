/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "pos_data.h"

pos_data::pos_data(VS inputnames_, bool folder_, int Ne_) : 
					MC_data(inputnames_, "positions", folder_, "Configuration data", 0) {
	if(cols%2) error("Expect an even number of columns for configuration data; have "+to_string(cols));
	twoNe=cols;
	if(twoNe!=2*Ne_) error("File(s) corresponds to "+to_string(twoNe/2)+" electrons; requested "+to_string(Ne_));
	posval=new double[twoNe];
	n_return_values=twoNe;
}

void pos_data::next_values(VCD& vals) {
	hdf5readbuffer[data_position/samplesperfile]->readFromBuffer(posval, twoNe);
	for(int i=0;i<twoNe;i+=2) {
		double t2=posval[i]/2.0, p2=posval[i+1]/2.0;
		vals[i]=cos(t2)*CD(cos(p2), sin(p2));
		vals[i+1]=sin(t2)*CD(cos(p2), -sin(p2));
	}
	data_position++;
}
