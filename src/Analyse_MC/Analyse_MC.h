/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef ANALYSE_MC_H
#define ANALYSE_MC_H

#include "misc.h"
#include "main_misc.h"
#include "input.h"
#include "MC_observable.h"
#include "energy.h"
#include "overlap.h"
#include "eigstate_overlap.h"
#include "geo_mean_eigstate_overlap.h"
#include "pair_corr_bins.h"
#include "density_bins.h"
#include "dens_decomp_LS_orthog_sphere.h"
#include "dens_decomp_IP_orthog_sphere.h"
#include "g_decomp_IP_orthog_sphere.h"
#include "g_decomp_LS_orthog_sphere.h"
#include "g_decomp_IP_disk.h"
#include "g_decomp_LS_disk.h"
#include "find_rank.h"
#include "eigenvectors.h"
#include "GramSchmidt.h"
#include "diagonal_Hamiltonian.h"
#include "mean_log_WF.h"
#include "JK_EP_overlap.h"
#include "MC_data.h"
#include "eigstate_data.h"
#include "pos_data.h"

void get_states(vector<VS>&, vector<VS>&, vector<VS>&, vector<VS>&, VS, VS, VS, VS, string, string, string, string, VI&);
void data_in(string, VS, vector<VS>&, string);

#endif
