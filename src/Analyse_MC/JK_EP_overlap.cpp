/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "JK_EP_overlap.h"

JK_EP_overlap::JK_EP_overlap(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, vector<int> input_sizes, bool EP_format_,
					bool use_existing_, bool verb_, int blocksize_, int no_blocks_, string JK_EP_prefix_, VD norms) : 
					MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), 
					JK_EP_prefix(JK_EP_prefix_), EP_format(EP_format_) {
	if(input_sizes[1]>0 || input_sizes[2]>0 || input_sizes[3]>0) error("Input data other than WF values supplied; ignored for JK-EP overlap");
	n_WF=input_sizes[0];
	for(int i=0;i<n_WF;i++) required_collections.push_back("Wavefunction data");
	if(verb) status << "JK-EP overlap observable added" << endl;
	else cout << "JK-EP overlap observable added" << endl;
	no_vars=6*(n_WF-1)+2;
	half_no_vars=3*(n_WF-1)+1;
	no_res=16*(n_WF-1)+1;
	no_var_blocks=3*(n_WF-1)+1;
	half_no_blocks=no_blocks/2;
	normvals.resize(2*n_WF-1);
	three_nWFm1=3*(n_WF-1);
	
	if(!use_existing) {
		if(datacollection.size()<2) error("Require at least two sets of WF values. Did you remember to use 'and' between sets in input?");
		if(norms.size()<1) {
			normvals[0]=real(datacollection[0]->norm_ave[1]-datacollection[0]->norm_ave[0]); // normval2
			for(int i=0;i<n_WF-1;i++) {
				normvals[2*i+2]=real(datacollection[i+1]->norm_ave[1]-datacollection[i+1]->norm_ave[0]); // normval3
				normvals[2*i+1]=normvals[0]+normvals[2*i+2]; // normval1
			}
		}
		else {
			if(norms.size()<2*n_WF-1) error("JK-EP overlap needs "+n2s(2*n_WF-1)+" norm values");
			else if(norms.size()>2*n_WF-1) warning("JK-EP overlap needs "+n2s(2*n_WF-1)+" norm values; rest ignored");
			for(int i=0;i<2*n_WF-1;i++) normvals[i]=norms[i];
			for(int i=0;i<2*n_WF-2;i+=2) {
				double normtest=(-normvals[i+1])-0.5*(-2.0*normvals[0]-2.0*normvals[i+2]);
				if(abs(normtest)>1e-10) error("Input normalisation values "+n2s(i/2)+" do not cancel");
			}
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
}

void JK_EP_overlap::evaluate_sample(vector<VCD> all_samples) {// conjugates 1st input
	int blockidx=floor(no_observations/(double)blocksize);
	int addhalf=((no_observations+1)>use_samples/2)*half_no_vars;
	
	double F2=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normvals[0]));
	for(int i=0;i<n_WF-1;i++) {
		int threei=3*i;
		CD F1=exp(conj(all_samples[0][1])+all_samples[i+1][1]-2.0*real(all_samples[0][0])-normvals[2*i+1]);
		double F3=exp(2.0*(real(all_samples[i+1][1])-real(all_samples[i+1][0])-normvals[2*i+2]));
		
		variables[threei+addhalf]+=real(F1);
		variables[threei+1+addhalf]+=imag(F1);
		variables[threei+2+addhalf]+=F3;
		blocks[blockidx][threei]+=real(F1);
		blocks[blockidx][threei+1]+=imag(F1);
		blocks[blockidx][threei+2]+=F3;
	}
	variables[three_nWFm1+addhalf]+=F2;
	blocks[blockidx][three_nWFm1]+=F2;
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void JK_EP_overlap::compute_result() {
	
	double LLLcont=0.0;
	VD LLLcont_block(half_no_blocks, 0.0);
	
	for(int j=0;j<n_WF-1;j++) {
		int threej=3*j, sixteenj=16*j;
		double re=0.0, re2=0.0, im=0.0, im2=0.0, ab=0.0, ab2=0.0, sq=0.0, sq2=0.0;
		for(int i=0;i<half_no_blocks;i++) {
				CD est_all=CD(blocks[i][threej]+blocks[i+half_no_blocks][threej], blocks[i][threej+1]+blocks[i+half_no_blocks][threej+1])/
						   sqrt((blocks[i][three_nWFm1]+blocks[i+half_no_blocks][three_nWFm1])*(blocks[i+half_no_blocks][threej+2]+blocks[i][threej+2]));
				CD est_1=CD(blocks[i][threej], blocks[i][threej+1])/sqrt(blocks[i][three_nWFm1]*blocks[i][threej+2]);
				CD est_2=CD(blocks[i+half_no_blocks][threej], blocks[i+half_no_blocks][threej+1])/
						 sqrt(blocks[i+half_no_blocks][three_nWFm1]*blocks[i+half_no_blocks][threej+2]);
			re+=real(est_all); re2+=real(est_all)*real(est_all);
			im+=imag(est_all); im2+=imag(est_all)*imag(est_all);
			double abstmp=sqrt(abs(real(est_1*conj(est_2)))), normtmp=abs(real(est_1*conj(est_2)));
			ab+=abstmp; ab2+=abstmp*abstmp;
			sq+=normtmp; sq2+=normtmp*normtmp;
			LLLcont_block[i]+=normtmp;
		}
		
		CD eigstate_overlap_split_1=CD(variables[threej],variables[threej+1])/sqrt(variables[three_nWFm1]*variables[threej+2]);
		CD eigstate_overlap_split_2=CD(variables[threej+half_no_vars],variables[threej+1+half_no_vars])/
									sqrt(variables[three_nWFm1+half_no_vars]*variables[threej+2+half_no_vars]);
		CD eigstate_overlap_all=CD(variables[threej]+variables[threej+half_no_vars],variables[threej+1]+variables[threej+1+half_no_vars])/
						     sqrt((variables[three_nWFm1]+variables[three_nWFm1+half_no_vars])*(variables[threej+2]+variables[threej+2+half_no_vars]));
		
		results[sixteenj]=real(eigstate_overlap_all);
		results[sixteenj+1]=imag(eigstate_overlap_all);
		results[sixteenj+2]=std(re2, re, half_no_blocks);
		results[sixteenj+3]=std(im2, im, half_no_blocks);
		results[sixteenj+4]=sqrt(abs(real(eigstate_overlap_split_1*conj(eigstate_overlap_split_2))));
		results[sixteenj+5]=std(ab2, ab, half_no_blocks);
		results[sixteenj+6]=abs(real(eigstate_overlap_split_1*conj(eigstate_overlap_split_2)));
		results[sixteenj+7]=std(sq2, sq, half_no_blocks);
		LLLcont+=results[sixteenj+6];
	}
	for(int j=0;j<n_WF-1;j++) {
		int threej=3*j, sixteenj=16*j;
		double re=0.0, re2=0.0, im=0.0, im2=0.0, ab=0.0, ab2=0.0, sq=0.0, sq2=0.0;
		for(int i=0;i<half_no_blocks;i++) {
				CD est_all=CD(blocks[i][threej]+blocks[i+half_no_blocks][threej], blocks[i][threej+1]+blocks[i+half_no_blocks][threej+1])/
						   sqrt(LLLcont_block[i]*(blocks[i][three_nWFm1]+blocks[i+half_no_blocks][three_nWFm1])*
												 (blocks[i+half_no_blocks][threej+2]+blocks[i][threej+2]));
				CD est_1=CD(blocks[i][threej], blocks[i][threej+1])/sqrt(LLLcont_block[i]*blocks[i][three_nWFm1]*blocks[i][threej+2]);
				CD est_2=CD(blocks[i+half_no_blocks][threej], blocks[i+half_no_blocks][threej+1])/
						 sqrt(LLLcont_block[i]*blocks[i+half_no_blocks][three_nWFm1]*blocks[i+half_no_blocks][threej+2]);
			re+=real(est_all); re2+=real(est_all)*real(est_all);
			im+=imag(est_all); im2+=imag(est_all)*imag(est_all);
			double abstmp=sqrt(abs(real(est_1*conj(est_2)))), normtmp=abs(real(est_1*conj(est_2)));
			ab+=abstmp; ab2+=abstmp*abstmp;
			sq+=normtmp; sq2+=normtmp*normtmp;
		}
		CD eigstate_overlap_normed=CD(variables[3*j],variables[3*j+1])/sqrt(LLLcont*variables[three_nWFm1]*variables[3*j+2]);
		results[sixteenj+8]=results[16*j]/sqrt(LLLcont);
		results[sixteenj+9]=results[16*j+1]/sqrt(LLLcont);
		results[sixteenj+10]=std(re2, re, half_no_blocks);
		results[sixteenj+11]=std(im2, im, half_no_blocks);
		results[sixteenj+12]=results[sixteenj+4]/sqrt(LLLcont);
		results[sixteenj+13]=std(ab2, ab, half_no_blocks);
		results[sixteenj+14]=results[sixteenj+6]/LLLcont;
		results[sixteenj+15]=std(sq2, sq, half_no_blocks);
	}
	results[16*(n_WF-2)]=sqrt(LLLcont);
	
	if(JK_EP_prefix!="") {
		if(EP_format) write_EP_format();
		else write_superpos_format();
	}
}

void JK_EP_overlap::write_superpos_format() {
	ofstream s_out;
	string name=JK_EP_prefix+".dat";
	s_out.open(name.c_str());
	if(!s_out) error("Could not open "+name+" for writing");
	s_out.precision(17);		
	if(verb) cout << endl;
	cout << "JK-EP coefficients written to " << name << endl;
	
	for(int j=0;j<(n_WF-1);j++) {
		int sixteenj=16*j;
		s_out << results[sixteenj] << " " << results[sixteenj+1] << endl;
	}
	s_out.close();	
}

void JK_EP_overlap::write_EP_format() {
	ofstream EP_out;
	string cname=JK_EP_prefix+"_c.dat", bname=JK_EP_prefix+"_b.dat";
	EP_out.open(cname.c_str());
	if(!EP_out) error("Could not open "+cname+" for writing");
	EP_out.precision(17);		
	if(verb) cout << endl;
	cout << "JK-EP coefficients and bins written to " << cname << " and " << bname << endl;	
	
	HDF5Wrapper* hdf5writer=new HDF5Wrapper;
	HDF5Buffer* hdf5writebuffer;
	if(hdf5writer->openFileToWrite(bname.c_str())!=0) error("Could not open "+bname+" for hdf5 writing");
	hdf5writer->createRealDataset("coeff_bins", half_no_blocks, 4*(n_WF-1));
	hdf5writebuffer=hdf5writer->setupRealSamplesBuffer("coeff_bins", 0, 10000);
	double *cblocks=new double[4*(n_WF-1)];
	for(int i=0;i<half_no_blocks;i++) {
		for(int j=0;j<n_WF-1;j++) {
			int threej=3*j, fourj=4*j;
			CD est_all=CD(blocks[i][threej]+blocks[i+half_no_blocks][threej], blocks[i][threej+1]+blocks[i+half_no_blocks][threej+1])/
					   sqrt((blocks[i][three_nWFm1]+blocks[i+half_no_blocks][three_nWFm1])*(blocks[i+half_no_blocks][threej+2]+blocks[i][threej+2]));
			CD est_1=CD(blocks[i][threej], blocks[i][threej+1])/sqrt(blocks[i][three_nWFm1]*blocks[i][threej+2]);
			CD est_2=CD(blocks[i+half_no_blocks][threej], blocks[i+half_no_blocks][threej+1])/
					 sqrt(blocks[i+half_no_blocks][three_nWFm1]*blocks[i+half_no_blocks][threej+2]);
			cblocks[fourj]=real(est_all);
			cblocks[fourj+1]=imag(est_all);
			cblocks[fourj+2]=sqrt(abs(real(est_1*conj(est_2))));
			cblocks[fourj+3]=abs(real(est_1*conj(est_2)));
		}
		hdf5writebuffer->writeToBuffer(cblocks, 4*(n_WF-1));
	}
	delete [] cblocks;
	delete hdf5writer;
	for(int j=0;j<(n_WF-1);j++) {
		int sixteenj=16*j;
		EP_out << results[sixteenj] << "," << results[sixteenj+1] << "," << results[sixteenj+4] << "," << results[sixteenj+6] << endl;
	}
	EP_out.close();
	
}

void JK_EP_overlap::display_result() {
	cout << endl;
	for(int j=0;j<n_WF-1;j++) {
		cout << "State " << j << ":" << endl;
		cout << "  Eigenstate overlap = " << CD(results[16*j], results[16*j+1]) << " +/- " << CD(results[16*j+2], results[16*j+3]) << endl;
		cout << "  |Eigenstate overlap| = " << results[16*j+4] << " +/- " << results[16*j+5] << endl;
		cout << "  |Eigenstate overlap|^2 = " << results[16*j+6] << " +/- " << results[16*j+7] << endl;
		cout << "  Eigenstate overlap normed = " << CD(results[16*j+8], results[16*j+9]) << " +/- " << CD(results[16*j+10], results[16*j+11]) << endl;
		cout << "  |Eigenstate overlap normed| = " << results[16*j+12] << " +/- " << results[16*j+13] << endl;
		cout << "  |Eigenstate overlap normed|^2 = " << results[16*j+14] << " +/- " << results[16*j+15] << endl;
	}
	cout << endl << "sqrt(LLL content) = " << results[16*(n_WF-2)] << endl << endl;
}

void JK_EP_overlap::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	
	out << "StateIdx"+sep+"Re(EigenstateOverlap)"+sep+"Im(EigenstateOverlap)"+sep+"Re(EigenstateOverlap)_error"+sep+"Im(EigenstateOverlap)_error" << sep;
	out << "Abs(EigenstateOverlap)" << sep << "Abs(EigenstateOverlap)_error" << sep;
	out << "Abs(EigenstateOverlap)^2" << sep << "Abs(EigenstateOverlap)^2_error" << sep;
	out << "Re(EigenstateOverlapNormed)"+sep+"Im(EigenstateOverlapNormed)"+sep+"Re(EigenstateOverlapNormed)_error"+sep+"Im(EigenstateOverlapNormed)_error" << sep;
	out << "Abs(EigenstateOverlapNormed)" << sep << "Abs(EigenstateOverlapNormed)_error" << sep;
	out << "Abs(EigenstateOverlapNormed)^2" << sep << "Abs(EigenstateOverlapNormed)^2_error" << sep << "sqrt(LLLcontent)" << endl;
	
	for(int i=0;i<n_WF-1;i++) {
		out << i << sep;
		for(int j=0;j<16;j++) out << results[16*i+j] << sep;
		out << results[16*(n_WF-2)] << endl;
	}
	
	out.close();
	cout << "Output written to " << filename << endl;
}

void JK_EP_overlap::write_norms(string filename) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<normvals.size();i++) out << normvals[i] << endl;
	out.close();
}
