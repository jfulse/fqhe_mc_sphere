/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef DIAGONAL_HAMILTONIAN_H
#define DIAGONAL_HAMILTONIAN_H

#include "MC_observable.h"
#include "Eigen/SVD"
#include "Eigen/Dense"

class diagonal_Hamiltonian : public MC_observable {

	private:
	int n_WF, n_pairs, twoNe;
	VD normvals;
	double twoR;
	string eigenv_prefix;
	VD energies, energy_errors;
	
	public:
	
	void evaluate_sample(vector<VCD>);
	void compute_result();
	void display_result();
	void write_result(string, string);
	void write_norms(string);
	void compute_samples_special() {error("No special computation procedure for this observable");}
	
	diagonal_Hamiltonian(vector<MC_data*>, int, stringstream&, vector<int>, int, int, string eigenv_prefix_="", bool use_existing_=false, bool verb_=false, 
						 int blocksize_=-1, int no_blocks_=-1, VD norms=VD());

};

#endif
