/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "WF_data.h"

WF_data::WF_data(VS inputnames_, bool folder_, int n_ave_norm, bool verb) : 
				MC_data(inputnames_, "wf_values", folder_, "Wavefunction data", n_ave_norm, verb) {
	if(cols!=4) error("Expect 4 columns for wavefunction data; have "+to_string(cols));
	wfval=new double[4];
	n_return_values=2;
}

WF_data::WF_data(VS inputnames_, bool folder_, int nfiles_, int samplesperfile_, int nsamples_) : 
				MC_data(inputnames_, "wf_values", folder_, "Wavefunction data", nfiles_, samplesperfile_, nsamples_) {
	
	wfval=new double[4];
	n_return_values=2;
	cols=4;
	data_position=0;
}

void WF_data::next_values(VCD& vals) {
	hdf5readbuffer[data_position/samplesperfile]->readFromBuffer(wfval, 4);
	vals[0]=CD(wfval[0], wfval[1]);
	vals[1]=CD(wfval[2], wfval[3]);
	data_position++;
}
