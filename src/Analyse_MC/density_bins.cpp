/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "density_bins.h"

density_bins::density_bins(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, int Ne_, int Nphi_, double cutoff_, int nbins_,
		bool use_existing_, bool verb_, int blocksize_, int no_blocks_, VD norms) :
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), Ne(Ne_), Nphi(Nphi_), nbins(nbins_), cutoff(cutoff_/2.0) {
	required_collections.push_back("Wavefunction data");
	required_collections.push_back("Configuration data");
	if(verb) status << "Density (binned) observable added" << endl;
	else cout << "Density (binned) observable added" << endl;
	twoNe=2*Ne;
	L=cutoff/(double)nbins;
	u0=1;
	v0=0;
	no_vars=nbins+1;
	no_res=3*nbins;
	no_var_blocks=nbins+1;
}

void density_bins::evaluate_sample(vector<VCD> all_samples) {
	int blockidx=floor(no_observations/(double)blocksize);
	double F=exp(2.0*(real(all_samples[0][1])-real(all_samples[0][0])-normval));
	for(int i=0;i<twoNe;i+=2) {
		double eta=abs(all_samples[1][i]*v0-u0*all_samples[1][i+1]);
		if(eta<cutoff) {
			int l=floor(eta/L);
			variables[l]+=F;
			blocks[blockidx][l]+=F;
		}
	}

	variables[nbins]+=F;
	blocks[blockidx][nbins]+=F;

	verb && counter(no_observations, samplecount);
	no_observations++;
}

void density_bins::compute_result() {

	double dens=Ne/(2.0*pi*Nphi);
	for(int i=0;i<nbins;i++) {
		double ta=2.0*asin(2.0*L*i/2.0);
		double tb=2.0*asin(2.0*L*(i+1.0)/2.0);
		double area=pi*Nphi*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
		results[3*i]=L*(i+0.5);
		results[3*i+1]=variables[i]/(variables[nbins]*dens*area);
		double b=0.0, b2=0.0;
		for(int j=0;j<no_blocks;j++) {
			double tmp=blocks[j][i]/(blocks[j][nbins]*dens*area);
			b+=tmp;
			b2+=tmp*tmp;
		}
		double error=std(b2, b, no_blocks);
		if(std::isnan(error) || std::isinf(error)) error=0.0;
		results[3*i+2]=error;
	}

}

void density_bins::display_result() {
	cout << endl << "Density bins: " << endl;
	for(int i=0;i<nbins;i++) cout << results[3*i] << " " << results [3*i+1] << " " << results[3*i+2] << endl;
	cout << endl;
}

void density_bins::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	out << "idx" << sep << "eta" << sep << "Density" << sep << "Density_error" << endl;
	for(int i=0;i<nbins;i++) out << i << sep << results[3*i] << sep << results [3*i+1] << sep << results[3*i+2] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}
