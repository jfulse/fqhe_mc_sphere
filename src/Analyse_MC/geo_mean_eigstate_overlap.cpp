/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "geo_mean_eigstate_overlap.h"

geo_mean_eigstate_overlap::geo_mean_eigstate_overlap(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, vector<int> input_sizes, 
		bool use_existing_, bool verb_, int blocksize_, int no_blocks_, int ne_, VD norms) : 
		MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), ne(ne_) {
	
	if(input_sizes[1]>0 || input_sizes[3]>0) {
		error("Input data other than WF and Hamiltonian eigenstate values supplied; ignored for geometric mean eigenstate overlap");
	}
	n_WF=input_sizes[0];
	if(ne<n_WF) error("Need at least as many Hamiltonian eigenstates as wavefunctions");
	if(ne>n_WF) warning("Supplied more Hamiltonian eigenstates than wavefunctions");
	required_collections.push_back("Eigenstate data");
	for(int i=0;i<n_WF;i++) required_collections.push_back("Wavefunction data");
	if(verb) status << "Geometric mean eigenstate overlap observable added (" << n_WF << " states)" << endl;
	else cout << "Geometric mean eigenstate overlap observable added (" << n_WF << " states)" << endl;
	
	no_vars=8*n_WF*n_WF;
	half_no_vars=no_vars/2;
	half_no_blocks=no_blocks/2;
	no_var_blocks=4*n_WF*n_WF;
	no_res=1;
	
	if(!use_existing) {
		if(norms.size()<1) {
			normval1=real(datacollection[0]->norm_ave[1]-2.0*datacollection[0]->norm_ave[0]);
			normval2=real(datacollection[0]->norm_ave[1]-datacollection[0]->norm_ave[0]);
			normval3=-real(datacollection[0]->norm_ave[0]);
		}
		else {
			if(norms.size()<3) error("Geometric mean eigenstate overlap needs three norm values");
			else if(norms.size()>3) warning("Geometric mean eigenstate overlap needs three norm values; rest ignored");
			normval1=norms[0];
			normval2=norms[1];
			normval3=norms[2];
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
	double normtest=(-normval1)-0.5*(-2.0*normval2-2.0*normval3);
	if(abs(normtest)>1e-10) error("Input normalisation values do not cancel");
}

void geo_mean_eigstate_overlap::evaluate_sample(vector<VCD> all_samples) {// conjugates 1st input (eigenvalue)

	int blockidx=floor(no_observations/(double)blocksize);
	int addhalf=((no_observations+1)>use_samples/2)*half_no_vars;
	int four_pair=0;
	
	for(int i=0;i<n_WF;i++) {
		for(int j=0;j<n_WF;j++) {
			CD F1=exp(all_samples[i+1][1]-2.0*real(all_samples[i+1][0])-normval1)*conj(all_samples[0][j]);
			double F2=exp(2.0*(real(all_samples[i+1][1])-real(all_samples[i+1][0])-normval2));
			double F3=exp(-2.0*(real(all_samples[i+1][0])+normval3))*norm(all_samples[0][j]);
			
			variables[four_pair+addhalf]+=real(F1);
			variables[four_pair+1+addhalf]+=imag(F1);
			variables[four_pair+2+addhalf]+=F2;
			variables[four_pair+3+addhalf]+=F3;
			blocks[blockidx][four_pair]+=real(F1);
			blocks[blockidx][four_pair+1]+=imag(F1);
			blocks[blockidx][four_pair+2]+=F2;
			blocks[blockidx][four_pair+3]+=F3;
			
			four_pair+=4;
		}
	}
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void geo_mean_eigstate_overlap::compute_result() {
	
	double power=1.0/n_WF;
	Eigen::MatrixXcd ovmat(n_WF, n_WF);
	Eigen::MatrixXcd ovmat_1st_half(n_WF, n_WF);
	Eigen::MatrixXcd ovmat_2nd_half(n_WF, n_WF);
	
	int four_pair=0;
	for(int i=0;i<n_WF;i++) {
		for(int j=0;j<n_WF;j++) {
			ovmat(i, j)=CD(variables[four_pair]+variables[four_pair+half_no_vars], variables[four_pair+1]+variables[four_pair+1+half_no_vars])/
					   sqrt((variables[four_pair+2]+variables[four_pair+2+half_no_vars])*(variables[four_pair+3]+variables[four_pair+3+half_no_vars]));
			ovmat_1st_half(i, j)=CD(variables[four_pair], variables[four_pair+1])/sqrt(variables[four_pair+2]*+variables[four_pair+3]);
			ovmat_2nd_half(i, j)=CD(variables[four_pair+half_no_vars], variables[four_pair+1+half_no_vars])/
					   sqrt(variables[four_pair+2+half_no_vars]*+variables[four_pair+3+half_no_vars]);
			four_pair+=4;
		}
	}
	
	//cout << ovmat.determinant() << " " << ovmat_1st_half.determinant() << " " << ovmat_2nd_half.determinant() << endl;
	geo_mean_overlap=pow(CD(ovmat.determinant()), power);
	abs_geo_mean_overlap=sqrt(real(pow(CD(ovmat_1st_half.determinant()), power)*conj(pow(CD(ovmat_2nd_half.determinant()), power))));
	norm_geo_mean_overlap=real(pow(CD(ovmat_1st_half.determinant()), power)*conj(pow(CD(ovmat_2nd_half.determinant()), power)));
	
	double Rgmo=0, Rgmo2=0, Igmo=0, Igmo2=0, Agmo=0, Agmo2=0, Ngmo=0, Ngmo2=0;
	for(int k=0;k<half_no_blocks;k++) {
		
		four_pair=0;
		for(int i=0;i<n_WF;i++) {
			for(int j=0;j<n_WF;j++) {
				ovmat(i, j)=CD(blocks[k][four_pair]+blocks[k+half_no_blocks][four_pair], blocks[k][four_pair+1]+blocks[k+half_no_blocks][four_pair+1])/
						   sqrt((blocks[k][four_pair+2]+blocks[k+half_no_blocks][four_pair+2])*(blocks[k][four_pair+3]+blocks[k+half_no_blocks][four_pair+3]));
				ovmat_1st_half(i, j)=CD(blocks[k][four_pair], blocks[k][four_pair+1])/sqrt(blocks[k][four_pair+2]*+blocks[k][four_pair+3]);
				ovmat_2nd_half(i, j)=CD(blocks[k+half_no_blocks][four_pair], blocks[k+half_no_blocks][four_pair+1])/
						   sqrt(blocks[k+half_no_blocks][four_pair+2]*+blocks[k+half_no_blocks][four_pair+3]);
				four_pair+=4;
			}
		}
					
		CD tmp1=pow(CD(ovmat.determinant()), power);
		double tmp2=sqrt(abs(real(pow(CD(ovmat_1st_half.determinant()), power)*conj(pow(CD(ovmat_2nd_half.determinant()), power)))));
		double tmp3=real(pow(CD(ovmat_1st_half.determinant()), power)*conj(pow(CD(ovmat_2nd_half.determinant()), power)));	  
		Rgmo+=real(tmp1);
		Rgmo2+=real(tmp1)*real(tmp1);
		Igmo+=imag(tmp1);
		Igmo2+=imag(tmp1)*imag(tmp1);
		Agmo2+=tmp2*tmp2;
		Ngmo+=tmp3;
		Ngmo2+=tmp3*tmp3;
		
	}
	
	geo_mean_overlap_error=CD(std(Rgmo2, Rgmo, half_no_blocks), std(Igmo2, Igmo, half_no_blocks));
	abs_geo_mean_overlap_error=std(Agmo2, Agmo, half_no_blocks);
	norm_geo_mean_overlap_error=std(Ngmo2, Ngmo, half_no_blocks);
}

void geo_mean_eigstate_overlap::display_result() {
	cout << endl;
	cout << "geo_mean(<eig_i,WF_j>) = " << geo_mean_overlap << " +/- " << geo_mean_overlap_error << endl;
	cout << "|geo_mean(<eig_i,WF_j>)| = " << abs_geo_mean_overlap << " +/- " << abs_geo_mean_overlap_error << endl;
	cout << "|geo_mean(<eig_i,WF_j>)|^2 = " << norm_geo_mean_overlap << " +/- " << norm_geo_mean_overlap_error << endl << endl;
}

void geo_mean_eigstate_overlap::write_result(string filename, string sep) {
	
	ofstream out(filename.c_str());
	out.precision(17);
	
	out << "Re(GeoMeanEigstateOverlap)" << sep << "Im(GeoMeanEigstateOverlap)" << sep;
	out << "Re(GeoMeanEigstateOverlap)_error" << sep << "Im(GeoMeanEigstateOverlap)_error" << sep;
	out << "AbsGeoMeanEigstateOverlap" << sep << "AbsGeoMeanEigstateOverlap_error" << sep;
	out << "NormGeoMeanEigstateOverlap" << sep << "NormGeoMeanEigstateOverlap_error" << endl;
	
	out << real(geo_mean_overlap) << sep << imag(geo_mean_overlap) << sep;
	out << real(geo_mean_overlap_error) << sep << imag(geo_mean_overlap_error) << sep;
	out << abs_geo_mean_overlap << sep << abs_geo_mean_overlap_error << sep;
	out << norm_geo_mean_overlap << sep << norm_geo_mean_overlap_error << endl;
	
	out.close();
	cout << "Output written to " << filename << endl;
}

void geo_mean_eigstate_overlap::write_norms(string filename) {
	ofstream out(filename.c_str());
	if(!out) error("Could not write to file "+filename);
	out.precision(17);
	out << normval1 << " " << normval2 << " " << normval3 << endl;
	out.close();
}
