/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "mean_log_WF.h"

mean_log_WF::mean_log_WF(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, bool use_existing_, bool verb_,
			int blocksize_, int no_blocks_, VD norms) : MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_) {
	required_collections.push_back("Wavefunction data");
	if(verb) status << "Mean log WF observable added" << endl;
	else cout << "Mean log WF observable added" << endl;
	no_vars=2;
	no_res=4;
	no_var_blocks=2;
}

void mean_log_WF::evaluate_sample(vector<VCD> all_samples) {

	variables[0]+=real(all_samples[0][1]);
	variables[1]+=imag(all_samples[0][1]);
	int blockidx=floor(no_observations/(double)blocksize);
	blocks[blockidx][0]+=real(all_samples[0][1]);;
	blocks[blockidx][1]+=imag(all_samples[0][1]);;
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void mean_log_WF::compute_result() {
	
	double re=0.0, re2=0.0, im=0.0, im2=0.0;
	for(int i=0;i<no_blocks;i++) {
		CD tmp=CD(blocks[i][0], blocks[i][1])/(double)blocksize;
		re+=real(tmp); re2+=real(tmp)*real(tmp);
		im+=imag(tmp); im2+=imag(tmp)*imag(tmp);
	}
	
	CD ave=CD(variables[0], variables[1])/(double)no_observations;
	
	results[0]=real(ave);
	results[1]=imag(ave);
	results[2]=std(re2, re, no_blocks);
	results[3]=std(im2, im, no_blocks);
}

void mean_log_WF::display_result() {
	cout << endl << "Average log(WF) = " << CD(results[0], results[1]) << endl << "Standard deviation = " << CD(results[2], results[3]) << endl;
}

void mean_log_WF::write_result(string filename, string separator) {
	ofstream out(filename.c_str());
	out.precision(17);
	out << "Re(Average_log_WF)"+separator+"Im(Average_log_WF)"+separator+"Re(StandardDeviation)"+separator+"Im(StandardDeviation)" << endl;
	out << results[0] << separator << results[1] << separator<< results[2] << separator<< results[3] << endl;
	out.close();
	cout << "Output written to " << filename << endl;
}
