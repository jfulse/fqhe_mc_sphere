/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "find_rank.h"

find_rank::find_rank(vector<MC_data*> datacollection_, int use_samples_, stringstream& status, vector<int> input_sizes, string eigenv_prefix_, bool use_existing_,
	bool verb_, int blocksize_, int no_blocks_, VD norms) : 
	MC_observable(datacollection_, use_samples_, norms, use_existing_, blocksize_, no_blocks_, verb_), eigenv_prefix(eigenv_prefix_) {
	
	if(input_sizes[1]>0 || input_sizes[2]>0 || input_sizes[3]>0) error("Input data other than WF values supplied; ignored for rank");
	n_WF=input_sizes[0];
	n_pairs=(n_WF*(n_WF-1))/2;
	for(int i=0;i<n_WF;i++) required_collections.push_back("Wavefunction data");
	if(verb) status << "Rank observable added" << endl;
	else cout << "Rank observable added" << endl;
	no_vars=4*n_pairs;
	no_res=2*n_WF+2*n_pairs+2+n_WF+1;
	no_var_blocks=4*n_pairs;
	normvals.resize(n_WF);
	if(!use_existing) {
		if(datacollection.size()<2) error("Require at least two sets of WF values. Did you remember to use 'and' between sets in input?");
		if(norms.size()<1) {
			for(int i=0;i<n_WF;i++) normvals[i]=real(datacollection[i]->norm_ave[1]-datacollection[i]->norm_ave[0]);
		}
		else {
			if(norms.size()<n_WF) error("Rank needs "+n2s(n_WF)+" norm values");
			else if(norms.size()>n_WF) warning("Rank needs "+n2s(n_WF)+" norm values; rest ignored");
			for(int i=0;i<n_WF;i++) normvals[i]=norms[i];
		}
	}
	else if(norms.size()>0) warning("Using preexisting data but supplied norms; these are ignored");
}

void find_rank::evaluate_sample(vector<VCD> all_samples) {// conjugates 1st input
	
	int blockidx=floor(no_observations/(double)blocksize);
	
	int fourpair=0;
	for(int i=0;i<n_WF-1;i++) {
		for(int j=i+1;j<n_WF;j++) {
			CD F1=exp(conj(all_samples[i][1])+all_samples[j][1]-2.0*real(all_samples[i][0])-normvals[i]-normvals[j]);
			double F2=exp(2.0*(real(all_samples[i][1])-real(all_samples[i][0])-normvals[i]));
			double F3=exp(2.0*(real(all_samples[j][1])-real(all_samples[j][0])-normvals[j]));
			variables[0+fourpair]+=real(F1);
			variables[1+fourpair]+=imag(F1);
			variables[2+fourpair]+=F2;
			variables[3+fourpair]+=F3;
			blocks[blockidx][0+fourpair]+=real(F1);
			blocks[blockidx][1+fourpair]+=imag(F1);
			blocks[blockidx][2+fourpair]+=F2;
			blocks[blockidx][3+fourpair]+=F3;
			fourpair+=4;
		}
	} //int I; cin >> I;
	
	verb && counter(no_observations, samplecount);
	no_observations++;
}

void find_rank::compute_result() {
	
	if(verb) cout << endl << "Calculating results" << endl;
	
	G.resize(n_WF, n_WF);
	Eigen::MatrixXcd Gblock(n_WF, n_WF);
	vector<VD> abs_G(n_WF, VD(n_WF));
	int rank=0;
	
	double d=0.0, d2=0.0;
	VD sv_blocks(n_WF, 0.0), sv_blocks2(n_WF, 0.0);
	for(int k=0;k<no_blocks;k++) {
		int fourpairs=0;
		for(int i=0;i<n_WF;i++) {
			Gblock(i, i)=1.0;
			for(int j=i+1;j<n_WF;j++) {
				CD overlap=CD(blocks[k][0+fourpairs],blocks[k][1+fourpairs])/sqrt(blocks[k][2+fourpairs]*blocks[k][3+fourpairs]);
				Gblock(i, j)=overlap;
				Gblock(j, i)=conj(overlap);
				fourpairs+=4;
			}
		}
		Eigen::JacobiSVD<Eigen::MatrixXcd> svd(Gblock);
		Eigen::VectorXd s_vals=svd.singularValues();
		CD Gdet=Gblock.determinant();
		double det=real(Gdet);
		d+=det; d2+=det*det;
		for(int i=0;i<n_WF;i++) {
			double sv=real(s_vals(i));
			sv_blocks[i]+=sv;
			sv_blocks2[i]+=sv*sv;
		}
	}
	
	int fourpairs=0, twopairs=0;
	for(int i=0;i<n_WF;i++) {
		G(i, i)=1.0;
		for(int j=i+1;j<n_WF;j++) {
			CD overlap=CD(variables[0+fourpairs],variables[1+fourpairs])/sqrt(variables[2+fourpairs]*variables[3+fourpairs]);
			results[twopairs]=real(overlap);
			results[twopairs+1]=imag(overlap);
			G(i, j)=overlap;
			G(j, i)=conj(overlap);
			abs_G[i][j]=abs(overlap);
			fourpairs+=4;
			twopairs+=2;
		}
	}
	
	Eigen::JacobiSVD<Eigen::MatrixXcd> svd(G, Eigen::ComputeFullU);
	Eigen::VectorXd s_vals=svd.singularValues();
	CD Gdet=G.determinant();
	
	for(int i=0;i<n_WF;i++) {
		results[2*n_pairs+2*i]=real(s_vals(i));
		results[2*n_pairs+2*i+1]=std(sv_blocks2[i], sv_blocks[i], no_blocks);
		if(abs(s_vals(i)>1e-9)) rank++;
	}
	
	results[2*n_pairs+2*n_WF]=real(Gdet);
	results[2*n_pairs+2*n_WF+1]=std(d2, d, no_blocks);
	
	vector<int> opt_states(n_WF);
	for(int i=0;i<n_WF;i++) opt_states[i]=i;
	
	for(int i=0;i<n_WF-rank;i++) {
		double max_overlap=0.0;
		int max_state=0;
		for(int j=0;j<abs_G.size()-1;j++) {
			for(int k=j+1;k<abs_G.size();k++) {
				if(abs_G[j][k]>max_overlap) {
					max_overlap=abs_G[j][k];
					max_state=k;
				}
			}
		}
		abs_G.erase(abs_G.begin()+max_state);
		for(int j=max_state;j<abs_G.size();j++) abs_G[j].erase(abs_G[j].begin()+max_state);
		for(int j=0;j<max_state;j++) abs_G[j].erase(abs_G[j].begin()+max_state);
		opt_states.erase(opt_states.begin()+max_state);
	}
	
	for(int i=0;i<opt_states.size();i++) results[2*n_pairs+2*n_WF+2+i]=opt_states[i];
	for(int i=opt_states.size();i<n_WF;i++) results[2*n_pairs+2*n_WF+2+i]=-1;
	
	results[2*n_pairs+2*n_WF+2+n_WF]=rank;
	
	if(eigenv_prefix!="") {
		if(verb) cout << endl << "Singular vectors written to:" << endl;
		//for(int i=0;i<rank;i++) {
		for(int i=0;i<n_WF;i++) {
			string name=eigenv_prefix+"_"+to_string(i+1)+".dat";
			string name2=eigenv_prefix+"_II_"+to_string(i+1)+".dat";
			ofstream vout(name.c_str());
			ofstream vout2(name2.c_str());
			if(!vout) warning("Could not open "+name+" for writing");
			else {
				vout.precision(17);
				vout2.precision(17);
				for(int j=0;j<n_WF;j++) {
					CD tmp=svd.matrixU()(j,i);
					vout << real(tmp) << " " << -imag(tmp) << endl;
					tmp=svd.matrixU()(i,j);
					vout2 << real(tmp) << " " << -imag(tmp) << endl;
				}
				if(verb) cout << name << endl;
			}
			vout.close();
			vout2.close();
		}
	}
}

void find_rank::display_result() {
	cout << endl << "Overlaps: " << endl;
	int twopairs=0;
	for(int i=0;i<n_WF-1;i++) {
		for(int j=i+1;j<n_WF;j++) {
			cout << "<" << i << "|" << j << "> = " << CD(results[twopairs], results[twopairs+1]) << endl;
			twopairs+=2;
		}
	}
	
	cout << endl << "Singular values:" << endl;
	for(int j=0;j<n_WF;j++) {
		cout << "Value " << j << " = " << results[2*n_pairs+2*j] << " +/- " << results[2*n_pairs+2*j+1] << endl;
	}
	
	cout << endl << "Determinant = " << results[2*n_pairs+2*n_WF] << " +/- " << results[2*n_pairs+2*n_WF+1] << endl;
	
	cout << endl << "Estimated rank = " << results[2*n_pairs+2*n_WF+2+n_WF] << endl;
	
	cout << endl << "Suggested optimal states:" << endl;
	for(int i=0;i<n_WF;i++) {
		int idx=results[2*n_pairs+2*n_WF+2+i];
		if(idx>=0) cout << idx << ": " << datacollection[idx]->getname() << endl;
	}
	cout << endl;
}

void find_rank::write_result(string filename, string sep) {
	ofstream out(filename.c_str());
	out.precision(17);
	out << "Overlaps:" << endl << "Idx1" << sep << "Idx2" << sep << "Re(overlap)" << sep << "Im(overlap)" << endl;
	
	int twopair=0;
	for(int i=0;i<n_WF-1;i++) {
		for(int j=i+1;j<n_WF;j++) {
			out << i << sep << j << sep << results[twopair] << sep << results[twopair+1] << endl;
			twopair+=2;
		}
	}
	
	out << endl << "Singular Values:" << endl << "Idx"+sep+"SingularValue"+sep+"SingularValue_error" << endl;
	for(int i=0;i<n_WF-1;i++) out << i << sep << results[2*n_pairs+2*i] << sep << results[2*n_pairs+2*i+1] << endl;
	
	out << endl << "Determinant:" << endl << "Determinant" << sep << "Determinant_error" << endl;
	out << results[2*n_pairs+2*n_WF] << sep << results[2*n_pairs+2*n_WF+1] << endl;
	
	int rank=results[2*n_pairs+2*n_WF+2+n_WF];
	out << endl << "Estimated rank:" << endl << rank << endl;
	
	out << endl << "Suggested optimal states:" << endl;
	out << "Idx" << sep << "State" << endl;
	for(int i=0;i<n_WF;i++) {
		int idx=results[2*n_pairs+2*n_WF+2+i];
		if(idx>=0) out << idx << sep << datacollection[idx]->getname() << endl;
	}
	
	out.close();
	cout << "Output written to " << filename << endl;
}

void find_rank::write_norms(string filename) {
	ofstream out(filename.c_str());
	out.precision(17);
	for(int i=0;i<n_WF;i++) out << normvals[i] << endl;
	out.close();
}
