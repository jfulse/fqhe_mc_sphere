#!/bin/bash
###Quit if variables are unset or there is an error
set -e
set -u
###The input file is the output
DepFile1=$1
###Remove the .o
###Make .h into _h
###Make / into _
###Make each dependecy file a record of its own
###Rename : to -> for the graph
###Remove typedef from the graph as many objects depend on it
###Remove chanels from the graph as many objects depend on it
echo "digraph G"
echo "{"
sed 's|\.o||g' $DepFile1 |  sed 's|\.h|_h|g' | sed  's|/|_|g'  | awk '{for(i=2;i<=NF;i++){print $1$i}}' | sed 's|:| -> |g' | sed 's|\(.*\) -> typedef||g' | sed 's|\(.*\) -> channels||g' | sort -u

####Make dependecy on itself (to find orpaned modules)###
#sed 's|\.o||g' $DepFile | awk '{print $1$1}' | sed 's|:| -> |' | sed 's|:||g' | sort -u
echo "}"
