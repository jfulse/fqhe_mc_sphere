/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "misc_sphere.h"
#include "input.h"
#include "HDF5Wrapper.h"
#include <dirent.h>

string nostring(int n) {
	stringstream ss;
	if(n<10) ss << 0 << n;
	else ss << n;
	return ss.str();
}

string timestring() {
	time_t t = time(0);
    struct tm * now = localtime( & t );
	stringstream ss; 
	return nostring(now->tm_year + 1900)+" "+nostring(now->tm_mday)+"."+nostring(now->tm_mon+1)+" "+nostring(now->tm_hour)+":"
			+nostring(now->tm_min)+":"+nostring(now->tm_sec);
}

void display_start(input*& params, int len, string sym) {
	string s1=pad_string("Program started "+timestring(), sym, len);
	string s2=pad_string("Parameters", sym, len);
	string s3=pad_string("", sym, len);
	cout << endl << s1 << endl << s2 << endl << endl;
	params->display_values();
	cout << s3 << endl << endl;
}

void display_end(double starttime, int len, string sym) {
	double sec=(clock()-starttime)/CLOCKS_PER_SEC;
	string s1=pad_string("Total Runtime: "+to_string(sec)+"s = "+to_string(sec/60.0)+"min", sym, len);
	string s2=pad_string("Program finished "+timestring(), sym, len);
	cout << endl << s1 << endl << s2 << endl << endl;
}

string helptext() {
	string help="Example usage: Create_superposition --wf-infiles wf1.hdf5 wf2.hdf5 --coeff-file coefficients.txt --out-file superposition.hdf5 --verbose\n\n";
	help+="Combines hdf5 files 'wf1.hdf5' and 'wf2.hdf5' using coefficients in 'coefficients.txt', showing extra info.";
	return help;
}

bool open_to_read(VS files, string dataset, vector<HDF5Wrapper*>& hdf5readers, vector<HDF5Buffer*>& hdf5readbuffers, int n) {
	
	int prevreaders=hdf5readers.size();
	for(int i=0;i<n;i++) { //cout << files[i] << " " << dataset << " " << n << " " << N << endl;
		hdf5readers.push_back(new HDF5Wrapper);
		if(hdf5readers[i+prevreaders]->openFileToRead(files[i].c_str())!=0) error("Failed to open "+files[i]+" to read");
		hdf5readbuffers.push_back(hdf5readers[i+prevreaders]->setupRealSamplesBuffer(dataset.c_str(), 0));
	}
	return true;
}

VS dirfiles(string& path) {

	DIR* dir;
	dirent* pdir;
	VS files;
	if(path[path.length()-1]!='/') path+="/";
	dir=opendir(path.c_str());
	if(!dir) {cerr << "ERROR: folder " << path << " not found" << endl; return VS();}
	while(pdir=readdir(dir)) files.push_back(pdir->d_name);
	for(int i=0;i<files.size();i++) {
		if(strcmp(files[i].c_str(),".")==0 || strcmp(files[i].c_str(),"..")==0) {files.erase(files.begin()+i); i--;}
	}
	return files;
}

void orderfiles(VS& files) {
	int n=files.size();
	int order[n];
	for(int i=0;i<n;i++) {// Find current order
		string sno;
		int ctr=files[i].length()-1;
		while(ctr>=0 && files[i][ctr]!='.') ctr--;
		if(ctr<0) ctr=files[i].length()-1; //  No dot in filename
		while(ctr>=0 && !isint(string(1, files[i][ctr]))) ctr--;
		if(ctr<0) {cerr << "WARNING: no number in filename " << files[i] << "; cannot be ordered" << endl; return;}
		while(ctr>=0 && isint(string(1, files[i][ctr]))) ctr--;
		sno=files[i][++ctr];
		while(isint(string(1, files[i][ctr]))) sno+=files[i][ctr++];
		order[i]=atoi(sno.c_str());
	}
	for(int i=0;i<n;i++) { // Order accordingly
		for(int j=i+1;j<n;j++) {
			if(order[j]<order[i]) {
				int itmp=order[i];
				order[i]=order[j];
				order[j]=itmp;
				string stmp=files[i];
				files[i]=files[j];
				files[j]=stmp;
			}
		}
	}
}

void get_folder_files(vector<VS>& files, VS names, int n_states, int &nfiles) {
	
	nfiles=0;
	
	for(int i=0;i<n_states;i++) {
		string folder=names[i];
		if(folder[folder.length()-1]!='/') folder+='/';
		VS addfiles=dirfiles(folder);
		if(i==0) {
			nfiles=addfiles.size();
			for(int j=0;j<nfiles;j++) files.push_back(VS());
		}
		else if(nfiles!=addfiles.size()) error("Different number of files in "+names[i]+" and previous folder(s)");
		orderfiles(addfiles);
		for(int j=0;j<nfiles;j++) files[j].push_back(folder+addfiles[j]);
	}
}

int check_hdf5_files(long long Ne, vector<VS> files, int n_states, int nfiles, int& nsamplesinfile, long long& N, string dataset, string type) {
	unsigned long cols=0;
	for(int i=0;i<nfiles;i++) {
		vector<HDF5Wrapper*> hdf5readers;
		for(int j=0;j<n_states;j++) {
			hdf5readers.push_back(new HDF5Wrapper);
			if(hdf5readers[j]->openFileToRead(files[i][j].c_str())!=0) error("Failed to open "+files[i][j]+" to read");
			unsigned long rows, testcols;
			hdf5readers[j]->getDatasetDims(dataset.c_str(), rows, testcols);
			if(rows==0) error("No rows in file "+files[i][j]);
			if(testcols==0) error("No columns in file "+files[i][j]);
			if(nsamplesinfile==0) {
				nsamplesinfile=rows;
				N=(long long)rows*(long long)nfiles;
			}
			else if(rows<nsamplesinfile) error(to_string(rows)+" samples in "+files[i][j]+"; expected "+to_string(nsamplesinfile));
			if(cols==0) cols=testcols;
			else if(testcols!=cols) error(to_string(testcols)+" columns in file; earlier file(s) contained "+to_string(cols));
		}
	}
	if(type=="wf" && cols!=4) error(to_string(cols)+" columns in files; for wf type 4 is expected");
	if(type=="eval" && (cols%2)!=0) error(to_string(cols)+" columns in files; for eigenstate type an even number is expected");
	if(type=="pos" && cols!=2*Ne) error(to_string(cols)+" columns in files; for position type 2*Ne = "+to_string(2*Ne)+" is expected");
	return cols;
}

void clear_pointer_vector(vector<HDF5Wrapper*>& vec) {
	int n=vec.size();
	for(int i=0;i<n;i++) delete vec[i];
}

int main(int argc, const char* argv[]) {
	
	try {
		clock_t starttime=clock();
		input *params=new input("_",helptext());
		
		params->add_input("--wf-infiles,-wi", "Name of hdf5 files/folders to read WF values", "VS");
		params->add_input("--eig-infile,-ei", "Name of hdf5 file/folder to read eigenstate values", "VS");
		params->add_input("--coeff-file,-c", "Name of file containing superposition coefficients", "string");
		params->add_input("--output,-wo", "Name of result wf file/folder", "string","superposition.hdf5");
		params->add_input("--samples,-N", "Number of MC samples (0-> all)", "ll");
		params->add_input("--read-folder,-f", "Inputs/output given as folders; reads all files within", "bool");
		params->add_input("-csv", "Coefficient file is in csv format", "bool");
		params->add_input("--quiet,-q", "Do not print extra information to screen", "bool");
		if(params->command_line_input(argc, argv)!=0) return 1;
		if(!params->check_integrity()) return 1;
		
		bool verb=true, folder=params->is_activated("-f");
		if(params->is_flagged("-q")) verb=false;
		char type, sep=' ';
		VS wfiles, efiles;
		if(params->is_flagged("-csv")) sep=',';
		if(params->is_activated("-wi") && !(params->is_activated("-ei"))) {
			wfiles=params->getVS("-wi");
			type='w';
		}
		else if(params->is_activated("-ei")) {
			if(!params->is_activated("-wi")) error("Need wavefunction input in order to read probability values");
			efiles=params->getVS("-ei");
			wfiles=params->getVS("-wi");
			if(efiles.size()>1) warning("Uses only one eigenstate file/folder; surplus ignored");
			if(wfiles.size()>1) warning("Only need one wavefunction file/folder to read probabilities when making eigenstate superposition; surplus ignored");
			type='e';
		}
		else error("Neither wavefunctions nor eigenstates supplied");
		string c_file=params->getstring("-c");
		string outfile=params->getstring("-wo");
		long long N=0;
		if(params->is_activated("-N")) N=params->getll("-N");
		
		if(verb) display_start(params, 60, "_");
		delete params;
		cout.precision(3);
		
		int n_WF, nfilesinfolder, n_e, nsamplesinfile;
		vector<VS> wf_files, eig_files;
		if(type=='w') {
			n_WF=wfiles.size();
			if(folder) get_folder_files(wf_files, wfiles, n_WF, nfilesinfolder);
			else {
				wf_files.push_back(wfiles);
				nfilesinfolder=1;
			}
			nsamplesinfile=N/(long long)nfilesinfolder;
			if(abs(nsamplesinfile-(double)N/nfilesinfolder)>1e-5) error("Number of files must divide number of samples");
			check_hdf5_files(0, wf_files, n_WF, nfilesinfolder, nsamplesinfile, N, "wf_values", "wf");
		}
		else if(type=='e') {
			if(folder) {
				get_folder_files(eig_files, efiles, 1, nfilesinfolder);
				int testnfilesinfolder;
				get_folder_files(wf_files, wfiles, 1, testnfilesinfolder);
				if(testnfilesinfolder!=nfilesinfolder) error("Unequal number of files in eigenstate and probability value folders");
			}
			else {
				eig_files.push_back(VS(1, efiles[0]));
				wf_files.push_back(VS(1, wfiles[0]));
				nfilesinfolder=1;
			}
			nsamplesinfile=N/(long long)nfilesinfolder;
			if(abs(nsamplesinfile-(double)N/nfilesinfolder)>1e-5) error("Number of files must divide number of samples");
			n_e=check_hdf5_files(0, eig_files, 1, nfilesinfolder, nsamplesinfile, N, "wf_values", "eval")/2;
			check_hdf5_files(0, wf_files, 1, nfilesinfolder, nsamplesinfile, N, "wf_values", "wf");
		}
		
		int n_coeffs=0;
		ifstream coeffs_file(c_file.c_str());
		coeffs_file.precision(17);
		if(!coeffs_file) error("Cannot find coefficient file "+c_file);
		VCD coeffs;
		string line, tmp;
		bool warned_more=false;
		while(getline(coeffs_file, line)) {
			istringstream buffer(line);
			VS split;
			while(getline(buffer, tmp, sep)) split.push_back(tmp);
			int nc=split.size();
			if(nc<1) error("No coefficient in line "+to_string(n_coeffs)+" of "+c_file);
			else if(nc==1) {
				if(!isnumber(split[0])) error(split[0]+" is not a number. Do you need to use '-csv'?");
				coeffs.push_back(CD(atof(split[0].c_str()), 0.0));
			}
			else if(nc>=2) {
				if(!isnumber(split[0])) error(split[0]+" is not a number. Do you need to use '-csv'?");
				if(!isnumber(split[1])) error(split[1]+" is not a number. Do you need to use '-csv'?");
				coeffs.push_back(CD(atof(split[0].c_str()), atof(split[1].c_str())));
			}
			if(nc>2 && !warned_more) {
				warning("Line "+to_string(n_coeffs)+" of "+c_file+" has more than two numbers; above second ignored (further warnings suppressed)");
				warned_more=true;
			}
			n_coeffs++;
		}
		if(type=='w' && n_coeffs<n_WF) error(to_string(n_WF)+" wf value files but only "+to_string(n_coeffs)+" coefficients");
		else if(type=='w' && n_coeffs>n_WF) warning("Only "+to_string(n_WF)+" wf value files but "+to_string(n_coeffs)+" coefficients; surplus ignored");
		else if(type=='e' && n_coeffs<n_e/2) {
			warning(to_string(n_e)+" eigenstates but only "+to_string(n_coeffs)+" coefficients; missing set to zero");
			n_e=n_coeffs;
		}
		else if(type=='e' && n_coeffs>n_e/2) warning("Only "+to_string(n_e)+" eigenstates but "+to_string(n_coeffs)+" coefficients; surplus ignored");
		
		vector<HDF5Wrapper*> hdf5readers;
		vector<HDF5Buffer*> hdf5readbuffers;
		vector<HDF5Wrapper*> hdf5Probreaders;
		vector<HDF5Buffer*> hdf5Probreadbuffers;
		vector<HDF5Wrapper*> hdf5writers;
		vector<HDF5Buffer*> hdf5writebuffers;
		
		if(folder) {
			string base=outfile;
			if(base[base.length()-1]!='/') base+='/';
			for(int i=0;i<nfilesinfolder;i++) {
				string filename=base+to_string(i)+".hdf5";
				hdf5writers.push_back(new HDF5Wrapper);
				if(hdf5writers[i]->openFileToWrite(filename.c_str())!=0) error("Could not open "+filename+" for writing");
				hdf5writers[i]->createRealDataset("wf_values", nsamplesinfile, 4);
				hdf5writebuffers.push_back(hdf5writers[i]->setupRealSamplesBuffer("wf_values", 0, 10000));
			}
		}
		else {
			hdf5writers.push_back(new HDF5Wrapper);
			if(hdf5writers[0]->openFileToWrite(outfile.c_str())!=0) error("Could not open "+outfile+" for writing");
			hdf5writers[0]->createRealDataset("wf_values", N, 4);
			hdf5writebuffers.push_back(hdf5writers[0]->setupRealSamplesBuffer("wf_values", 0, 10000));
		}
		
		double *single_input, *single_prob=new double[4], *new_wf=new double[4];
		CD superpos;
		
		if(type=='w') {
			single_input=new double[4];
			for(int i=0;i<nfilesinfolder;i++) {
				hdf5readers.clear();
				hdf5readbuffers.clear();
				open_to_read(wf_files[i], "wf_values", hdf5readers, hdf5readbuffers, n_WF);
				for(int j=0;j<nsamplesinfile;j++) {
					superpos=0.0;
					for(int k=0;k<n_WF;k++) {
						hdf5readbuffers[k]->readFromBuffer(single_input, 4);
						superpos+=coeffs[k]*exp(CD(single_input[2],single_input[3]));
					}
					superpos=log(superpos);
					imod(superpos, twopi);
					new_wf[0]=single_input[0];
					new_wf[1]=single_input[1];
					new_wf[2]=real(superpos);
					new_wf[3]=imag(superpos);
					hdf5writebuffers[i]->writeToBuffer(new_wf, 4);
				}
			}
		}
		else if(type=='e') {
			single_input=new double[2*n_e];
			for(int i=0;i<nfilesinfolder;i++) {
				hdf5readers.clear();
				hdf5readbuffers.clear();
				hdf5Probreaders.clear();
				hdf5Probreadbuffers.clear();
				open_to_read(eig_files[i], "wf_values", hdf5readers, hdf5readbuffers, 1);
				open_to_read(wf_files[i], "wf_values", hdf5Probreaders, hdf5Probreadbuffers, 1);
				for(int j=0;j<nsamplesinfile;j++) {
					superpos=0.0;
					hdf5readbuffers[0]->readFromBuffer(single_input, 2*n_e);
					hdf5Probreadbuffers[0]->readFromBuffer(single_prob, 4);
					for(int k=0;k<2*n_e;k+=2) superpos+=coeffs[k/2]*CD(single_input[k],single_input[k+1]);
					superpos=log(superpos);
					imod(superpos, twopi);
					new_wf[0]=single_prob[0];
					new_wf[1]=single_prob[1];
					new_wf[2]=real(superpos);
					new_wf[3]=imag(superpos);
					hdf5writebuffers[i]->writeToBuffer(new_wf, 4);
				}
			}
		}
		
		if(verb) cout << "Superposition written to " << outfile << endl << endl;
		
		clear_pointer_vector(hdf5readers);
		clear_pointer_vector(hdf5writers);
		clear_pointer_vector(hdf5Probreaders);
		delete [] single_input;
		delete [] new_wf;
		if(verb) display_end(starttime, 60, "_");
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		cerr << "Run with '-h' for options" << endl; 
		return 1;
	}
	catch(...) {
		cerr << "UNKNOWN ERROR" << endl; 
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
	
}
