/**

 Copyright (C) 2016  Mikael Fremling
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Mikael Fremling

**/
#ifndef CF_QH_H
#define CF_QH_H

#include "Sphere_WF_factor.h"

class CF_qh : public Sphere_WF_factor {
 private:
  VD qh_pos;
  VCD qh_uv;
  VCD qh_loguv;
  void set_qh_pos(string,int);
  int num_qh;

 protected:
  void specific_revert(int, CD, CD) {};
  
 public:	       
  void update(VCD);
  void update(int, VCD);
  void updateSpinor(VCD);
  void initialise(VCD);
  void updateConfig(int ConfNo);
  
 CF_qh(int Ne_, VCD uv_, stringstream& status, int num_qh_, string QP_Pos_file, int NumConfigs ) : Sphere_WF_factor(Ne_, uv_) {
    num_qh = num_qh_;
    set_qh_pos(QP_Pos_file,NumConfigs);
    status << "Initialising "<<num_qh<<" Laughlin quasihole(s)" << endl;
  };
  
  ~CF_qh() {}
};

#endif
