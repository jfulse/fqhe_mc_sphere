/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Slater.h"

void Slater::update(int r, VCD uv_) {//cout << "Slater pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
	CD newu=uv_[r];
	CD newv=uv_[r+1];
	logratio=0;
	for(int i=0;i<r;i+=2) logratio+=log(uv[i]*newv-newu*uv[i+1])-log(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
	for(int i=r+2;i<2*Ne;i+=2) logratio+=log(uv[i]*newv-newu*uv[i+1])-log(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
	logratio=m*logratio+2.0*d*real(logratio);
	logfactor+=logratio;
	imod(logratio, twopi);
	imod(logfactor, twopi);
	uv[r]=newu;
	uv[r+1]=newv; //cout << "SL new uv after upd r = "; for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << endl;cout << endl;
}

void Slater::initialise(VCD uv_) {//cout << "Slater pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne-2;i+=2) {
		for(int j=i+2;j<2*Ne;j+=2) logfactor+=log(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
	}
	logfactor=m*logfactor+2.0*d*real(logfactor);
	imod(logfactor, twopi);
}

void Slater::update(VCD uv_) {//cout << "Slater pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne-2;i+=2) {
		for(int j=i+2;j<2*Ne;j+=2) logfactor+=log(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
	}
	logfactor=m*logfactor+2.0*d*real(logfactor);
	imod(logfactor, twopi);
}
