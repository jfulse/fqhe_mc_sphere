/**

 Copyright (C) 2018  Mikael Fremling
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Mikael Fremling

**/
#include "rev_CF_qp_qh_p2.h"


Eigen::MatrixXcd rev_CF_qp_qh_p2::setupCFmat(VCD uv) {
  Eigen::MatrixXcd CFmat=rev_CF_qp_qh_p2::setupCFmatLog(uv);
  for(int i=0;i<Ne+num_qp;i++) { // rows;
    for(int j=0;j<Ne+num_qp;j++) { // collumns;
      CFmat(i,j)=exp(CFmat(i,j));
    }
  }
  logfactor=TakeLogDeterminant(CFmat,Ne+num_qp);
  //cout<<"logfactor:"<<logfactor<<endl;
  return CFmat;
}

 
Eigen::MatrixXcd rev_CF_qp_qh_p2::setupCFmatLog(VCD uv) {
  Eigen::MatrixXcd logesp=My_CF_Misc.getlogesp(uv);
  Eigen::MatrixXcd CFmat;
  CFmat.resize(Ne+num_qp, Ne+num_qp);
  //cout<<"----------------------------------"<<endl;
  //cout<<"Constructing the determinant"<<endl;
  //We put the quasiparticle at aximutal=pi/2, polar=0
  //This corresonds to u=v=sin(pi/4)=1/sqrt(2)
  for(int i=0;i<Ne;i++) { // rows; particle i
    int state=0; // state dep. on nu and m
    CD logu=log(uv[2*i]), logv=log(uv[2*i+1]);
    for(int nu=0;nu<(int)n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=(int)twoq+nu;mpq++) {
	CFmat(i,state)=My_CF_Misc.log_rev_CF_JK_orbital_p2(mpq,nu,
							   logesp,i,logu,logv);
	state++;
      } // m end (w.r.t. |q|+m
    } // nu end
  } // i end
  for(int qp_no=0;qp_no<num_qp;qp_no++) { // rows; particle i
    int state=0; // state dep. on nu and m
    int i=qp_no+Ne;
    CD logu_qp=qp_loguv[qp_no*2];
    CD logv_qp=qp_loguv[qp_no*2+1];
    for(int nu=0;nu<(int)n;nu++) { // "Internal" LL
      //cout<<"qp_no:(logu,logv) "<<qp_no<<" , "<<logu_qp<<" , "<<logv_qp<<endl;
      for(int mpq=-nu;mpq<=(int)twoq+nu;mpq++) {
	CFmat(i,state)=conj(My_CF_Misc.log_UP_CF_orbital(mpq,nu,
							 logu_qp,logv_qp));
	state++;
      } // m end (w.r.t. |q|+m
    } // nu end
  }// i end

  //cout<<"CFmat:"<<endl;
  //print_matrix(CFmat,Ne+num_qp);

  return CFmat;
}

void rev_CF_qp_qh_p2::updateConfig(int ConfNo) {
  //cout<<"----------------------------------"<<endl;
  //cout<<"Update the config to no = "<<ConfNo<<endl;
  //Transfer the static variables
  for(int i=0;i<Ne;i++) { // rows; particle i
    for(int j=0;j<Ne+num_qp;j++) { // rows; particle i
      CFmatConfig(i,j)=CFmatStatic(i,j);
    }
  }
  for(int qp_no=0;qp_no<num_qp;qp_no++) { // rows; particle i
    int state=0; // state dep. on nu and m
    int i=qp_no+Ne;
    CD logu_qp=qp_loguv[2*ConfNo*num_qp+qp_no*2];
    CD logv_qp=qp_loguv[2*ConfNo*num_qp+qp_no*2+1];
    for(int nu=0;nu<(int)n;nu++) { // "Internal" LL
      //cout<<"qp_no:(logu,logv) "<<qp_no<<" , "<<logu_qp<<" , "<<logv_qp<<endl;
      for(int mpq=-nu;mpq<=(int)twoq+nu;mpq++) {
	CFmatConfig(i,state)=conj(My_CF_Misc.log_UP_CF_orbital(mpq,nu,
							       logu_qp,logv_qp));
	state++;
      } // m end (w.r.t. |q|+m
    } // nu end
  }// i end
  //cout<<"CFmatConfig:"<<endl;
  //print_matrix(CFmatConfig,Ne+num_qp);
  logfactor=TakeLogDeterminantOfLog(CFmatConfig,Ne+num_qp);
  //cout<<"logfactor:"<<logfactor<<endl;
}


void rev_CF_qp_qh_p2::updateSpinor(VCD uv_) {
  //Update the spinor;
  uv=uv_;
  CFmatConfig.resize(Ne+num_qp, Ne+num_qp);
  CFmatStatic.resize(Ne+num_qp, Ne+num_qp);
  //Compute the determinant for the electron part (in log scale)
  CFmatStatic=setupCFmatLog(uv); 
}
