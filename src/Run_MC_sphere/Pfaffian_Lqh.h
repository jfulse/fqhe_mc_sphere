/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef PFAFFIAN_LQH_H
#define PFAFFIAN_LQH_H

#include "Sphere_WF_factor.h"

class Pfaffian_Lqh : public Sphere_WF_factor {

	private:

	Eigen::MatrixXcd P;
	CD r_1, s_1, r_2, s_2;

	void specific_revert(int, CD, CD);

	public:
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	void revert(int, CD, CD);

	Pfaffian_Lqh(int Ne_, VCD uv_, VD angles, stringstream& status) : Sphere_WF_factor(Ne_, uv_) {
		P.resize(Ne, Ne);

			if(angles.size() !=4) error("Pfaffian polynomial with localised quasiholes must have exactly four angles (theta_1, phi_1, theta_2, phi_2)");
			status << "Pfaffian polynomial with localised quasiholes at\n[" << angles[0] << "," << angles[1] << "] and ["
						 << angles[2] << "," << angles[3] << "] (random phase); Ne = " << Ne_ << endl;

			r_1=cos(angles[0]/2.0)*CD(cos(angles[1]/2.0), sin(angles[1]/2.0));
			s_1=sin(angles[0]/2.0)*CD(cos(angles[1]/2.0), -sin(angles[1]/2.0));
			r_2=cos(angles[2]/2.0)*CD(cos(angles[3]/2.0), sin(angles[3]/2.0));
			s_2=sin(angles[2]/2.0)*CD(cos(angles[3]/2.0), -sin(angles[3]/2.0));
	}

	~Pfaffian_Lqh() {}
};

#endif
