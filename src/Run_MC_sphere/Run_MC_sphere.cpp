/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Run_MC_sphere.h"

int main(int argc, const char* argv[]) {

	try {
		clock_t starttime=clock();
		input *params=new input("_",helptext());

		params->add_input("headline 1","--- Wavefunction options ---; ","description");
		params->add_input("-Ne", "Number of electrons", "int", "6");
		params->add_input("--wavefunction,-wf", "Wavefunction name", "string", "L");
		params->add_input("wf description", "L -> Laughlin, CF -> Composite fermion,;BS -> Bonderson-Slingerland, MR -> Moore-Read","description");
		params->add_input("--mod-factor,-d", "Slater absolute squared exponent", "double","0.0");
		params->add_input("--L:m,-m", "Laughlin exponent", "int", "3");
		params->add_input("--CF:n,-n", "Number of CF Landau levels", "int", "2");
		params->add_input("--CF:2p", "Number of attached CF fluxes", "int", "2");
		params->add_input("--CF:twice-q,-2q", "Twice effective CF flux (lowest CF LL has 2q+1 states)", "int", "2");
		params->add_input("--CF:conf-file,-c", "CF determinant superposition config file", "string");
		params->add_input("input description","Fileformat of above ('-c'): Every line starts with CG coefficient;(superposition weight), after which alternating n and m follows;for all Ne particles. Newline separates CF determinants.","description");
		params->add_input("--CF:qh_num", "Number of CF quasi-holes", "int","0");
		params->add_input("input description qp_num", "For now, triggers a number of qhs given by ythe file CF:qh_pos_file, but adding a factor (z-eta). For now only for direct flux n=1 states","description");
		params->add_input("--CF:qp_num", "Number of CF quasi-particles", "int","0");
		params->add_input("input description 3", "For now, triggers qp:s spefied by the CF_qp_pos_file, but only for reverse flux n=1, p=2 states","description");
		params->add_input("--CF:qp_pos_file", "Position for CF quasi-particles", "string");
		params->add_input("--CF:qh_pos_file", "Position for CF quasi-holes", "string");
		params->add_input("--CF:reverse,-R", "Reverse flux CF's", "bool");
		params->add_input("--CF:unproj,-UP", "No Jain-Kamilla LLL projection", "bool");
		params->add_input("--MR:p", "Power of Slater determinant in Moore-Read wf", "int", "2");
		params->add_input("--MR:no-phase", "Do not compute Moore-Read wf phase (will be random)", "bool");
		params->add_input("--MR:conf-file,-cPf", "Pfaffian excitation config files", "VS");
		params->add_input("--MR:loc-qh,-Pfqh", "[(theta, phi)] for two Laughlin-style localised Pfaffian quasiholes", "VD");
		params->add_input("input description 2","Fileformats of above ('-cPf'): Every line starts with CG;coefficient (superposition weight), after which alternating n and;m follows for all Ne particles. Newline separates CF determinants.","description");
		params->add_input("--MR:Nphi", "Total flux of bosonic nu=1 Pfaffian (for excitations)", "int");
		params->add_input("--MR:conf-file-sup", "Superposition of Pfaffian excitations' config files", "VS");
		params->add_input("--MR:CG", "Clebsh-Gordan coeffs for superpos of Pfaffian excitations", "int");
		params->add_input("--MR:unsym,-U", "Unsymmetrised Moore-Read", "bool");
		params->add_input("--MR:d1", "Moore-Read modification factor 1", "double");
		params->add_input("--MR:d2", "Moore-Read modification factor 2", "double");
		params->add_input("--BS:p", "Power of Slater (in addition to CF contr.)", "int", "1");
		params->add_input("--Laughlin-qh,-Lqh", "Laughlin quasihole at the origin", "bool");

		params->add_input("headline 2"," ;--- Monte Carlo options ---; ","description");
		params->add_input("--Laughlin-P,-lp", "Use Laughlin as probability", "bool");
		params->add_input("--existing-pos,-X", "Use existing positions", "bool");
		params->add_input("--thermalise,-th", "Number of thermalisation iterations", "int", "100000");
		params->add_input("--delay,-dl", "Number of iterations between sampling in terms of Ne", "int", "3");
		params->add_input("--samples,-N", "Number of MC samples", "ll", "10");
		params->add_input("--step,-st", "Steplength in factors of 1/R", "double", "2.8");
		params->add_input("--wf-outfile,-wo", "Name of hdf5 file to write WF values", "string", "wf.hdf5");
		params->add_input("--wgf-outfile,-wg", "Name of hdf5 file to write weight values", "string", "wg.hdf5");
		params->add_input("--pos-outfile,-po", "Name of hdf5 file to write positions", "string", "pos.hdf5");
		params->add_input("--pos-infile,-pi", "Name of hdf5 file to read positions", "string");
		params->add_input("--seed,-sd", "Integer used as seed ( -1 gives random seed)", "int");
		params->add_input("--no-stab", "Do not stabilise step and skip Laughlin thermalisation", "bool");
		params->add_input("--skip-samples,-ss", "Number of samples to skip in existing files", "ll");
		params->add_input("--num_configs,-nc", "Number of input configurations", "int", "-1");
		params->add_input("--init-conf", "Initial electron configuration (theta_1 phi_1 theta_2 ...)", "VD");
		params->add_input("--store-therm", "File in which to write positions from thermalisation", "string");
		params->add_input("--quiet,-q", "Do not print extra information to screen", "bool");
		if(params->command_line_input(argc, argv)!=0) return 1;
		if(!params->check_integrity()) return 1;

		cout.precision(3);
		int th, dl, Nphi, P_m;
		long long N=params->getll("-N"), skip=0;
		bool use_existing=params->is_flagged("-X"), verb=true;
		if(params->is_flagged("-q")) verb=false;
		if(params->is_flagged("-lp")) P_m=params->getint("-m");
		bool nstab=params->is_flagged("--no-stab"), lp=params->is_flagged("-lp");
		if(params->is_activated("-ss")) skip=params->getll("-ss");
		string wfin="", posin="", posout="";
		string wfout=params->getstring("-wo");
		string wgtout=params->getstring("-wg");
		string name="Probability function", P_wf=params->getstring("-wf"), thermfile="";
		if(params->is_activated("--store-therm")) thermfile=params->getstring("--store-therm");
		stringstream status;


		if(verb) cout << "Initialising..." << endl;

		if(use_existing) {
		  posin=params->getstring("-pi");
		  name="Wavefunction";
		}
		else {
		  if(skip>0) warning("Can only skip samples ('-ss') when computing on existing configurations ('-X')");
		  th=params->getint("-th");
		  dl=params->getint("-dl");
		  posout=params->getstring("-po");
		}
		
		MC_sphere MC(params, Nphi, status, nstab);
		
		if(verb) {
		  display_start(params, 60, "_", argv[0]);
		  cout << "Nphi = " << Nphi << endl << endl;
		  if(status.str()!="") cout << status.str() << endl;
		}
		int Num_conf=params->getint("-nc");
		delete params;

		if(use_existing) {
		  cout<<"Compute on existing:"<<endl;
		  cout<<"Use positons: "<<posin<<endl;
		  cout<<"Write to file: "<<wfout<<endl;
		  cout<<"Number of configs: "<<Num_conf<<endl;
		  if(Num_conf>0) {
		    cout<<"Multiple configs: "<<endl;
		    MC.ComputeOnExistingManyConfigs(N, posin, wfout, skip,Num_conf);
		  }
		  else {
		    cout<<"Single configs: "<<endl;
		    MC.ComputeOnExisting(N, posin, wfout, skip);
		  }
		}
		else {
		  if(!nstab) MC.Laughlin_therm(th);
		  
		  if(lp && P_wf!="L") {
		    if(verb) {
		      if(P_m==1) cout << "Using Slater determinant nu=1 as probability" << endl << endl;
		      else cout << "Using Laughlin nu=1/" << P_m << " as probability" << endl << endl;
		    }
		    if(P_m>1) warning("Sampling with Laughlin nu=1/"+to_string(P_m)+", which contains\nno relative angular momentum component L<"+to_string(P_m)+" and may lead\nto systematic error depending on wavefunction");
		    MC.SampleLaughlin(dl, N, wfout, wgtout,posout, P_m);
		  }
		  else {
		    if(lp && P_wf=="L") warning("Option '--Laughlin-P' ignored when computing Laughlin wavefunction samples");
		    if(!nstab) MC.stabilize_step(0.05, 20, 5e3, verb);
		    MC.thermalise(th, thermfile);
		    if(!nstab) MC.stabilize_step(0.03, 20, 1e4, verb);
		    if(verb) cout << endl;
		    MC.SampleProb(dl, N, wfout, wgtout,posout);
			}
		}
		
		cout.precision(3);
		if(verb) display_end(starttime, 60, "_");
	}
	catch(const std::exception& ex) {
	  cerr << "ERROR: " << ex.what() << endl;
	  cerr << "Run with '-h' for options" << endl;
	  return 1;
	}
	catch(...) {
	  cerr << "UNKNOWN ERROR" << endl;
	  cerr << "Run with '-h' for options" << endl;
	  return 1;
	}
	
}

string helptext() {
  string help="Example usage: ./Run_MC_sphere -wf L --L:m 5 -Ne 10 --mod-factor 1.5 -N 1000 -sd 3\n\n";
  help+="Generates 1000 MC configurations of Laughlin wf at 10 particles with exponent 5 and absolute Slater\ndeterminant to power 3, showing extra info";
  help+=" and seeding random generator with seed=3";
  return help;
}
