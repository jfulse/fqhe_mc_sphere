/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef MODIFIED_PFAFFIAN_H
#define MODIFIED_PFAFFIAN_H

#include "Sphere_WF_factor.h"
#include "Slater.h"
#include "CF_excitation.h"

class Modified_Pfaffian : public Sphere_WF_factor {

	private:
	int Nhalf;
	vector<Sphere_WF_factor*> factors;
	CD newlogfactor;
	void specific_revert(int, CD, CD) {};
	double fac, d1;
	
	void symmetrise(int n=0, int k=0);
	void evaluate();

	public:
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	
	Modified_Pfaffian(int Ne_, VCD uv_, double d1_, stringstream& status) : 
			Sphere_WF_factor(Ne_, uv_), d1(d1_) {
		
		if(Ne%2) error("Pfaffian must have even number of particles (requested Ne = "+to_string(Ne)+")");
		Nhalf=Ne/2;
		fac=-Nhalf*log(2.0);
		status << "Modified Pfaffian; d_1 = " << d1 << "; Ne = " << Ne_ << endl;
		factors.push_back(new Slater(Nhalf, VCD(uv.begin(), uv.begin()+Ne), 2, d1, status));
		factors.push_back(new Slater(Nhalf, VCD(uv.begin()+Ne, uv.end()), 2, d1, status));
	}
	
	~Modified_Pfaffian() {}
};

#endif
