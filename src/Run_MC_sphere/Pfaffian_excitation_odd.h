/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef PFAFFIAN_EXCITATION_ODD_H
#define PFAFFIAN_EXCITATION_ODD_H

#include "Sphere_WF_factor.h"
#include "Slater.h"
#include "CF_excitation.h"

class Pfaffian_excitation_odd : public Sphere_WF_factor {

	private:
	int Nfirst;
	vector<Sphere_WF_factor*> factors;
	CD newlogfactor;
	void specific_revert(int, CD, CD) {};
	//VI ordertest, ordertest_init;
	double fac;
	
	void symmetrise(int n=0, int k=0);
	void evaluate();
  void order_cfiles(VS&);

	public:
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	
  // 2q = Nphi_Pf - (Ne-1)/2
	Pfaffian_excitation_odd(int Ne_, VCD uv_, VS cfiles, int Nphi_Pf, stringstream& status) : 
			Sphere_WF_factor(Ne_, uv_) {
    
    //ordertest.resize(Ne_);
    //ordertest_init.resize(Ne_);
    //for(int i=0;i<Ne_;i++) ordertest_init[i]=i;
    
		if(!(Ne%2)) error("Pfaffian excitation (odd) must have an odd number of particles (requested Ne = "+to_string(Ne)+")");
		if(cfiles.size()!=2) error("Must supply two excitation config files (using -cPf)");
		Nfirst=(Ne-1)/2;
    order_cfiles(cfiles);
		int twoq1=Nphi_Pf-Nfirst+1;
		int twoq2=Nphi_Pf-Nfirst;
		fac=-Nfirst*log(2.0);
		status << "Excited Pfaffian with an odd number of particles; Ne = " << Ne_ << endl;
		factors.push_back(new CF_excitation(Nfirst, VCD(uv.begin(), uv.begin()+Ne-1), 1, twoq1, cfiles[0], status));
		factors.push_back(new Slater(Nfirst, VCD(uv.begin(), uv.begin()+Ne-1), 1, 0.0, status));
		factors.push_back(new CF_excitation(Nfirst+1, VCD(uv.begin()+Ne-1, uv.end()), 1, twoq2, cfiles[1], status));
		factors.push_back(new Slater(Nfirst+1, VCD(uv.begin()+Ne-1, uv.end()), 1, 0.0, status));
	}
	
	~Pfaffian_excitation_odd() {}
};

#endif
