/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef CHOOSE_WAVEFUNCTION_H
#define CHOOSE_WAVEFUNCTION_H

#include "misc.h"
#include "input.h"

inline void tp2sp(int twoNe, double* tp, VCD& uv) {
	for(int i=0;i<twoNe;i+=2) {
		double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
		uv[i]=cos(tp[i]/2.0)*CD(cp, sp);
		uv[i+1]=sin(tp[i]/2.0)*CD(cp, -sp);
	}
}

int get_wf_id(input*);
int get_Nphi(int, input*);
void check_CF_GS(int, int, int);
void check_CF_super(int, int, int);

#endif
