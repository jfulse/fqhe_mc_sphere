/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef SPHERE_WF_FACTOR_H
#define SPHERE_WF_FACTOR_H

#include "misc_sphere.h"
#include <algorithm>
#include <float.h>
#include "Eigen/Dense"

class Sphere_WF_factor {

	protected:
	int Ne;
	CD logfactor, logratio;
	VCD uv;
	
	virtual void specific_revert(int, CD, CD)=0;
	
	public:
	CD returnlogratio();
	CD returnlogfactor();
	void revert(int, CD, CD);
	void set_low_prob();
	virtual void update(VCD)=0;
	virtual void update(int, VCD)=0;
	virtual void initialise(VCD)=0;
	virtual void updateConfig(int);
	virtual void updateSpinor(VCD);

	Sphere_WF_factor(int Ne_, VCD uv_): Ne(Ne_), uv(uv_) {
		logratio=0;
		uv.resize(2*Ne);
	}
	
	~Sphere_WF_factor() {}

};

#endif
