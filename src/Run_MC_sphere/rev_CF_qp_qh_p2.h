/**

 Copyright (C) 2016  Mikael Fremling
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Mikael Fremling

**/
#ifndef REV_CF_QH_QP_P2_H
#define REV_CF_QH_QP_P2_H

#include "CF_det.h"

class rev_CF_qp_qh_p2 : public CF_det {
  
 private:
  Eigen::MatrixXcd setupCFmat(VCD);
  Eigen::MatrixXcd getlogesp(VCD);
  Eigen::MatrixXcd CFmatConfig;
  Eigen::MatrixXcd CFmatStatic;

  Eigen::MatrixXcd setupCFmatLog(VCD);
  
 public:
  //special functions
  void updateConfig(int) override;
  void updateSpinor(VCD) override;
  
  //Constructor
 rev_CF_qp_qh_p2(int Ne_, VCD uv_, int n_, int num_qp_, string QP_Pos_file, stringstream& status,int NumConfigs_) : 
  CF_det(Ne_, uv_, n_, 4, num_qp_, QP_Pos_file, status, "JK_neg", NumConfigs_){ }
  
  ~rev_CF_qp_qh_p2() {}
};

#endif
