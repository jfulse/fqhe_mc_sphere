/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "UP_Pfaffian_excitation.h"

void UP_Pfaffian_excitation::initialise(VCD uv_) {
	uv=uv_;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[2]->initialise(VCD(uv.begin()+Ne, uv.end()));
	factors[3]->initialise(VCD(uv.begin()+Ne, uv.end()));
	newlogfactor=0.0;
	symmetrise();
	logfactor=log(newlogfactor)+fac;
}

void UP_Pfaffian_excitation::update(int r, VCD uv_) {
	uv=uv_;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[2]->initialise(VCD(uv.begin()+Ne, uv.end()));
	factors[3]->initialise(VCD(uv.begin()+Ne, uv.end()));
	newlogfactor=0.0;
	symmetrise();
	newlogfactor=log(newlogfactor)+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio;
}

void UP_Pfaffian_excitation::update(VCD uv_) {
	uv=uv_;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[2]->initialise(VCD(uv.begin()+Ne, uv.end()));
	factors[3]->initialise(VCD(uv.begin()+Ne, uv.end()));
	newlogfactor=0.0;
	symmetrise();
	newlogfactor=log(newlogfactor)+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio;
}

void UP_Pfaffian_excitation::symmetrise(int n, int k) {
    if(n==Nhalf) {
        evaluate();
        return; 
    }
    
    symmetrise(n+1,k);

    if (k<Nhalf) {
      swap(uv[2*n], uv[2*(Ne-k-1)]);
      swap(uv[2*n+1], uv[2*(Ne-k-1)+1]);
      factors[0]->update(2*n, VCD(uv.begin(), uv.begin()+Ne));
      factors[1]->update(2*n, VCD(uv.begin(), uv.begin()+Ne));
      factors[2]->update(2*(Ne-k-1-Nhalf), VCD(uv.begin()+Ne, uv.end()));
      factors[3]->update(2*(Ne-k-1-Nhalf), VCD(uv.begin()+Ne, uv.end()));
      symmetrise(n,k+1);
    }
}

void UP_Pfaffian_excitation::evaluate() {
	CD result=0.0;
	for(int i=0;i<4;i++) result+=factors[i]->returnlogfactor();
	newlogfactor+=exp(result);
}
