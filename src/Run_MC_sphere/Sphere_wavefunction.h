/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef SPHERE_WAVEFUNCTION_H
#define SPHERE_WAVEFUNCTION_H

#include "Choose_Wavefunction.h"
#include "misc_sphere.h"
#include "Sphere_WF_factor.h"
#include "Slater.h"
#include "Pfaffian.h"
#include "Pfaffian_wo_sign.h"
#include "SymmetrisedPfaffian.h"
#include "UnsymmetrisedPfaffian.h"
#include "Modified_Pfaffian.h"
#include "Pfaffian_excitation.h"
#include "Pfaffian_excitation_odd.h"
#include "UP_Pfaffian_excitation_odd.h"
#include "Pfaffian_superposition.h"
#include "UP_Pfaffian_excitation.h"
#include "Pfaffian_Lqh.h"
#include "CF_GS.h"
#include "CF_qh.h"
#include "UP_CF_GS.h"
#include "CF_excitation.h"
#include "UP_CF_excitation.h"
#include "rev_CF_GS.h"
#include "rev_CF_excitation.h"
#include "rev_CF_GS_p2.h"
#include "rev_CF_excitation_p2.h"
#include "rev_CF_qp_qh_p2.h"
#include "Laughlin_qh.h"

class Sphere_wavefunction {

	private:
	int id, nfactors, Nphi, &Nphi_ret;
	string name;
	vector<Sphere_WF_factor*> factors;
	stringstream E;

	void getid(string, int, int, int, double, bool);
	void initialise(VCD, bool nostab=false);
	void setup(input*, VCD, stringstream&);

	public:
	CD returnlogWF();
	CD returnlogratio();
	void updateConfig(int);
	void updateSpinor(VCD);
	void update(int, VCD);
	void update(VCD);
	void revert(int, CD, CD);

	Sphere_wavefunction(input*, VCD, int&, stringstream&, bool nostab=false);

	Sphere_wavefunction(VCD, int&, int m=3)	;

	~Sphere_wavefunction() {}

};

#endif
