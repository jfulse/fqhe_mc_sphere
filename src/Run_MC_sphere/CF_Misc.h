/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef CF_MISC_H
#define CF_MISC_H

#include "misc_sphere.h"
#include "Eigen/Dense"
//"include "CF_det.h"

class CF_Misc
{
 private:
  int Ne,n,twoq,Nphi;
  double p;
  VD logfactlistMisc;
  CD logzero;

  vector<VD> lognorm_CF;
  vector<VD> lognorm_rev_CF;
  vector<VD> lognorm_rev_CF_p2;
  vector<VD> lognorm_UP_CF;

  
  void getfacts();

  inline double logbin(int a, int b) {return logfactlistMisc[a]-logfactlistMisc[b]-logfactlistMisc[a-b];}

  void CF_getnorm();
  void rev_CF_getnorm();
  void UP_CF_getnorm();
  void rev_CF_p2_getnorm();

  
 public:
  Eigen::MatrixXcd getlogesp(VCD uv);
  Eigen::MatrixXcd createP(int, VCD);

  void initialize(int,int,double,string);

    // CF orbitals
  CD UP_CF_orbital(int mpq,int nu,CD logu, CD logv);
  
  CD CF_orbital(int mpq,int nu,Eigen::MatrixXcd P,CD logu, CD logv);
  
  CD log_UP_CF_orbital(int mpq,int nu,CD logu, CD logv);
  
  CD rev_CF_JK_orbital(int mpq,int nu,
		       Eigen::MatrixXcd logesp,
		       int i,CD logu,CD logv);
  
  CD rev_CF_JK_orbital_p2(int mpq,int nu,
			  Eigen::MatrixXcd logesp,
			  int i,CD logu, CD logv);
  
  CD log_rev_CF_JK_orbital_p2(int mpq,int nu,
			      Eigen::MatrixXcd logesp,
			      int i,CD logu, CD logv);
  
  CF_Misc(int);
  
  ~CF_Misc() {}
  
};

// Normalisations

void print_matrix(Eigen::MatrixXcd Mat,int Dim);

double rev_CF_norm(int Ne, int Nphi,int p ,int nu,int twoq,int mpq);

double CF_norm(int Ne, int Nphi,int nu,int twoq,int mpq);

double UP_CF_norm(int nu,int twoq,int mpq);

// Usefull functions
CD add_exponentially(CD x,CD y);

CD TakeLogDeterminant(Eigen::MatrixXcd,int);
CD TakeLogDeterminantOfLog( Eigen::MatrixXcd,int);

#endif
