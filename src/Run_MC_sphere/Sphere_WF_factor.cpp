/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Sphere_WF_factor.h"

CD Sphere_WF_factor::returnlogratio() { //cout << "ratio " << logratio << endl;
	return logratio;
}

CD Sphere_WF_factor::returnlogfactor() { //cout << "factor: " << logfactor << endl;
	return logfactor;
}

void Sphere_WF_factor::revert(int r, CD oldu, CD oldv) {
	logfactor-=logratio;
	uv[r]=oldu;
	uv[r+1]=oldv;
	specific_revert(r, oldu, oldv);
}

void Sphere_WF_factor::set_low_prob() {
	logfactor=-DBL_MAX;
	logratio=0.0;
}


void Sphere_WF_factor::updateConfig(int ConfNo) {
  cout<<"Update the config to no="<<ConfNo<<endl;
  error("This functionality should be implmented locally");
}

void Sphere_WF_factor::updateSpinor(VCD spinor) {
    cout<<"Update the spinor: "<<endl;
    error("This functionality should be implmented locally");
}
