/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Sphere_wavefunction.h"

Sphere_wavefunction::Sphere_wavefunction(input* params, VCD uv, int& Nphi_ret_, stringstream& status, bool nostab) : Nphi_ret(Nphi_ret_) {
  id=get_wf_id(params);
  setup(params, uv, status);
  Nphi=get_Nphi(id, params);
  initialise(uv, nostab);
  Nphi_ret=Nphi;
}

Sphere_wavefunction::Sphere_wavefunction(VCD spinor, int& Nphi_ret_, int m) : Nphi_ret(Nphi_ret_) {
  int Ne=spinor.size()/2;
  stringstream dum;
  factors.push_back(new Slater(Ne, spinor, m, 0.0, dum));
  nfactors=1;
  Nphi=m*Ne;
  initialise(spinor);
  Nphi_ret=Nphi;
}

CD Sphere_wavefunction::returnlogratio() {
  CD logratio=0;
  for(int i=0;i<nfactors;i++) logratio+=factors[i]->returnlogratio();
  return logratio;
}

CD Sphere_wavefunction::returnlogWF() {
  CD logWF=0;
  for(int i=0;i<nfactors;i++) logWF+=factors[i]->returnlogfactor();
  return logWF;
}

void Sphere_wavefunction::revert(int r, CD newu, CD newv) {
  for(int i=0;i<nfactors;i++) factors[i]->revert(r, newu, newv);
}

void Sphere_wavefunction::initialise(VCD spinor, bool nostab) {
  if(!nostab) for(int i=0;i<nfactors;i++) factors[i]->initialise(spinor);
  else for(int i=0;i<nfactors;i++) factors[i]->set_low_prob();
}

void Sphere_wavefunction::update(int r, VCD spinor) {
  for(int i=0;i<nfactors;i++) factors[i]->update(r, spinor);
}

void Sphere_wavefunction::update(VCD spinor) {
  for(int i=0;i<nfactors;i++) factors[i]->update(spinor);
}

void Sphere_wavefunction::updateConfig(int  ConfNo) {
  for(int i=0;i<nfactors;i++) factors[i]->updateConfig(ConfNo);
}

void Sphere_wavefunction::updateSpinor(VCD spinor) {
  for(int i=0;i<nfactors;i++) factors[i]->updateSpinor(spinor);
}



void Sphere_wavefunction::setup(input* params, VCD spinor, stringstream& status) {
  nfactors=0;
  int Ne=params->getint("-Ne");
  double d=params->getdouble("-d");
  bool verb=true;
  if(params->is_flagged("-q")) verb=false;
  bool hasSlater=false;
  bool L_qh=params->is_flagged("-Lqh");

  cout<<"INFO: Choosing case no: "<<id<<endl;
  switch(id) {
  case 1: factors.push_back(new Slater(Ne, spinor, params->getint("-m"), d, status)); hasSlater=true; break;
  case 2: factors.push_back(new rev_CF_GS(Ne, spinor, params->getint("-n"), status)); break;
  case 3: {
    factors.push_back(new UnsymmetrisedPfaffian(Ne, spinor, status));
    factors.push_back(new Slater(Ne, spinor, 1, d, status)); hasSlater=true;
  } break;
  case 4: {
    factors.push_back(new rev_CF_excitation(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 5: {
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status, "UP_neg"));
    
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p"), d, status)); hasSlater=true;
  } break;
  case 6: {
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
    
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p"), d, status)); hasSlater=true;
  } break;
  case 7: {
    factors.push_back(new Slater(Ne, spinor, params->getint("--MR:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
  } break;
  case 8: {
    factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new rev_CF_GS(Ne, spinor, params->getint("-n"), status));
    factors.push_back(new Pfaffian(Ne, spinor, status));
  } break;
  case 9: {
    factors.push_back(new CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p"), d, status)); hasSlater=true;
  } break;
  case 10: {
    factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")+params->getint("--CF:2p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
    factors.push_back(new CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
  } break;
  case 11: {
    factors.push_back(new rev_CF_excitation(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
    factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
    nfactors++;
  } break;
  case 12: {
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status, "UP_neg"));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p"), d, status)); hasSlater=true;
  } break;
  case 13: {
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p"), d, status)); hasSlater=true;
  } break;
  case 14: {
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status, "UP_neg"));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p")+params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
  } break;
  case 15: {
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p")+params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
  } break;
  case 16: {
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status, "UP_neg"));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p")+params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
    nfactors++;
  } break;
  case 17: {
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
    nfactors++;
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p")+params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
  } break;
  case 18: {
    if(params->getint("--MR:p")<1) error("Symmetrised Pfaffian only available for p >= 1");
    if(params->getint("--MR:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--MR:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new SymmetrisedPfaffian(Ne, spinor, status));
  } break;
  case 19: {
    factors.push_back(new CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p"), d, status)); hasSlater=true;
  } break;
  case 20: {
    factors.push_back(new CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
    factors.push_back(new Slater(Ne, spinor, params->getint("--CF:2p")+params->getint("--BS:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian(Ne, spinor, status));
  } break;
  case 21: {
    factors.push_back(new Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--MR:p")<1) error("Symmetrised Pfaffian only available for p >= 1");
    if(params->getint("--MR:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--MR:p")-1, d, status)); hasSlater=true;
    }
  } break;
  case 22: {
    factors.push_back(new Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
  } break;
  case 23: {
    factors.push_back(new Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--BS:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new rev_CF_GS(Ne, spinor, params->getint("-n"), status));
  } break;
  case 24: factors.push_back(new rev_CF_GS_p2(Ne, spinor, params->getint("-n"), status)); break;
  case 25: {
    factors.push_back(new rev_CF_excitation_p2(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 26: {
    factors.push_back(new UP_Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
  } break;
  case 27: {
    factors.push_back(new UP_Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status, "UP_neg"));
  } break;
  case 28: { error("Pfaffian superposition not configured");
      factors.push_back(new Pfaffian_superposition(Ne, spinor, params->getVD("--MR:CG"), params->getVS("--MR:conf-file-sup"), params->getint("--MR:Nphi"), status));
      int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
      if(SlaterPower>1) {
	factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
      }
      factors.push_back(new CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
      nfactors++;
  } break;
  case 29: { error("Pfaffian superposition not configured");
      factors.push_back(new Pfaffian_superposition(Ne, spinor, params->getVD("--MR:CG"), params->getVS("--MR:conf-file-sup"), params->getint("--MR:Nphi"), status));
      int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
      if(SlaterPower>1) {
	factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
      }
      factors.push_back(new rev_CF_GS(Ne, spinor, params->getint("-n"), status));
      nfactors++;
  } break;
  case 30: {
    factors.push_back(new Slater(Ne, spinor, params->getint("--MR:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian_wo_sign(Ne, spinor, status));
  } break;
  case 31: {
    factors.push_back(new Modified_Pfaffian(Ne, spinor, params->getdouble("--MR:d1"), status));
    factors.push_back(new Slater(Ne, spinor, 1, d+params->getdouble("--MR:d2"), status)); hasSlater=true;
  } break;
  case 32: {
    factors.push_back(new UP_Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status, "UP_neg"));
  } break;
  case 33: {
    factors.push_back(new UP_Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
  } break;
  case 34: {
    factors.push_back(new Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--BS:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new rev_CF_GS(Ne, spinor, params->getint("-n"), status));
  } break;
  case 35: {
    factors.push_back(new Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new CF_GS(Ne, spinor, params->getint("-n"), params->getint("--CF:2p"), status));
  } break;
  case 36: {
    factors.push_back(new Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--MR:p")<1) error("Symmetrised Pfaffian only available for p >= 1");
    if(params->getint("--MR:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--MR:p")-1, d, status)); hasSlater=true;
    }
  } break;
  case 37: {
    factors.push_back(new UP_Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status, "UP_neg"));
    
  } break;
  case 38: {
    factors.push_back(new UP_Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status, "UP_neg"));
    
  } break;
  case 39: {
    factors.push_back(new UP_Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 40: {
    factors.push_back(new UP_Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new UP_CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 41: {
    factors.push_back(new Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--BS:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new rev_CF_excitation(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 42: {
    factors.push_back(new Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--BS:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new rev_CF_excitation(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 43: {
    factors.push_back(new Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--BS:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new rev_CF_excitation_p2(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 44: {
    factors.push_back(new Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    if(params->getint("--BS:p")>1) {
      factors.push_back(new Slater(Ne, spinor, params->getint("--BS:p")-1, d, status)); hasSlater=true;
    }
    factors.push_back(new rev_CF_excitation_p2(Ne, spinor, params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 45: {
    factors.push_back(new Pfaffian_excitation(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 46: {
    factors.push_back(new Pfaffian_excitation_odd(Ne, spinor, params->getVS("-cPf"), params->getint("--MR:Nphi"), status));
    int SlaterPower=params->getint("--BS:p")+params->getint("--CF:2p");
    if(SlaterPower>1) {
      factors.push_back(new Slater(Ne, spinor, SlaterPower-1, d, status)); hasSlater=true;
    }
    factors.push_back(new CF_excitation(Ne, spinor, params->getint("--CF:2p"), params->getint("-2q"), params->getstring("-c"), status));
  } break;
  case 47: {
    factors.push_back(new Slater(Ne, spinor, params->getint("--MR:p"), d, status)); hasSlater=true;
    factors.push_back(new Pfaffian_Lqh(Ne, spinor, params->getVD("-Pfqh"), status));
    if(params->is_flagged("-X")) {
      error("Wavefuntion is not correctly reproduced with old coefficients in this particualr case. Bug the developers to fix this!");
    }
  } break;
  case 48: {
    cout<<"INFO: Choosing reverse flux p=2, n=1 with quasiparticles."<<endl;
    factors.push_back(new rev_CF_qp_qh_p2(Ne, spinor,1, params->getint("--CF:qp_num"),params->getstring("--CF:qp_pos_file"),status,params->getint("-nc")));
  } break;
  case 49: {
    if(params->is_flagged("-R")){
      cout<<"INFO: Choosing reverse flux p=2, n=1 with quasiholes."<<endl;
      factors.push_back(new rev_CF_GS_p2(Ne, spinor,1,status));

      factors.push_back(new CF_qh(Ne, spinor,status,params->getint("--CF:qh_num"),params->getstring("--CF:qh_pos_file"),params->getint("-nc")));
    }
    else {
      cout<<"INFO: Choosing direct flux p=1, n=1 with quasiholes."<<endl;
      factors.push_back(new CF_GS(Ne, spinor,1,1,status));      
      factors.push_back(new CF_qh(Ne, spinor,status,params->getint("--CF:qh_num"),params->getstring("--CF:qh_pos_file"),params->getint("-nc")));
    }
  } break;
  default: error("id "+to_string(id)+" unknown (building wavefunction)");
  }

  
  if(abs(d)>1e-13) {
    if(!hasSlater) {
      factors.push_back(new Slater(Ne, spinor, 0, d, status));
    }
  }
  
  if(L_qh) {
    factors.push_back(new Laughlin_qh(Ne, spinor, status));
  }
  
  nfactors=factors.size();
}
