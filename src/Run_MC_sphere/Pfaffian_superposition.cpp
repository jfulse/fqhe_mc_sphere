/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Pfaffian_superposition.h"

void Pfaffian_superposition::initialise(VCD uv_) {
	uv=uv_;
	logfactor=0.0;
	for(int k=0;k<n_terms;k++) {
		WF_terms[k]->initialise(uv_);
		logfactor+=log(1.0+exp(log_CG_coeffs[k]+WF_terms[k]->returnlogfactor()-logfactor));
	}
	imod(logfactor, twopi);
}

void Pfaffian_superposition::update(int r, VCD uv_) {
	uv=uv_;
	CD newlogfactor=0.0;
	for(int k=0;k<n_terms;k++) {
		WF_terms[k]->initialise(uv_);
		newlogfactor+=log(1.0+exp(log_CG_coeffs[k]+WF_terms[k]->returnlogfactor()-newlogfactor));
	}
	imod(newlogfactor, twopi);
	logratio=newlogfactor-logfactor;
	logfactor+=logratio;
}

void Pfaffian_superposition::update(VCD uv_) {
	uv=uv_;
	CD newlogfactor=0.0;
	for(int k=0;k<n_terms;k++) {
		WF_terms[k]->initialise(uv_);
		newlogfactor+=log(1.0+exp(log_CG_coeffs[k]+WF_terms[k]->returnlogfactor()-newlogfactor));
	}
	imod(newlogfactor, twopi);
	logratio=newlogfactor-logfactor;
	logfactor+=logratio;
}
