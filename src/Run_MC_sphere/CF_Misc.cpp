/**

 Copyright (C) 2018  Mikael Fremling
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to micke.fremling@gmail.com.

 Author: Mikael Fremling

**/

#include "CF_Misc.h"

CF_Misc::CF_Misc(int Ne_) :
  Ne(Ne_){
}
void CF_Misc::initialize(int n_,int twoq_,double p_,string mode){
  n=n_;
  twoq=twoq_;
  p=p_;
  logzero=log(0.0);
  int twop=(int)(2*p);

  if(mode=="JK_pos")	{
    Nphi=twop*(Ne-1)+twoq;
  }
  else if(mode=="JK_neg") {
    Nphi=twop*(Ne-1)-twoq;
  }
  else if(mode=="UP_pos") {
    Nphi=twop*(Ne-1)+twoq;
  }
  else if(mode=="UP_neg") {
    Nphi=twop*(Ne-1)-twoq;
  }
  else error("CF determinant mode "+mode+" unknown");

  
  //cout<<"Generate factors and norms"<<endl;  
  CF_getnorm();  
  rev_CF_getnorm();
  rev_CF_p2_getnorm();
  UP_CF_getnorm();
  getfacts();
}


void CF_Misc::getfacts() {
  int MaxValue=2*Ne+twoq+n;
  //cout<<"Tabulating the logratithms of the factorial"<<endl;
  logfactlistMisc.resize(MaxValue);//This should be bigger than anything needed....
  for(int i=0;i<MaxValue;i++) logfactlistMisc[i]=logfact(i);	
}



double rev_CF_norm(int Ne, int Nphi,int p ,int nu,int twoq,int mpq)
{
  //cout<<"get reverse norm for p="<<p<<" at nu="<<nu<<" and mpq="<<mpq<<endl;
  return logfact(Nphi+1)-logfact(2.0*p*(Ne-1.0)+nu+1.0)
    +0.5*(log(twoq+2.0*nu+1.0)
	  +logfact(mpq+nu)+logfact(twoq-mpq+nu)-logfact(nu)
	  -logfact(twoq+nu)-log(4.0*pi));
}


double CF_norm(int Ne, int Nphi,int nu,int twoq,int mpq)
{
  //cout<<"get norm for nu="<<nu<<" and mpq="<<mpq<<endl;
  return logfact(Nphi+1)-logfact(Nphi+nu+1.0)
    +0.5*(log(twoq+2*nu+1.0)+logfact(mpq+nu)
	  +logfact(twoq-mpq+nu)-logfact(nu)
	  -logfact(twoq+nu)-log(4.0*pi));
}


double UP_CF_norm(int nu,int twoq,int mpq)
{
  //cout<<"get UP norm for nu="<<nu<<" and mpq="<<mpq<<endl;
  return 0.5*(log(twoq+2*nu+1.0)+logfact(mpq+nu)
	      +logfact(twoq-mpq+nu)-logfact(nu)
	      -logfact(twoq+nu)-log(4.0*pi));
}

CD CF_Misc::CF_orbital(int mpq,int nu,Eigen::MatrixXcd P,
		       CD logu, CD logv) 
{
  int qmm=twoq-mpq; // q-m
  CD Orbital=(1.0-2.0*((qmm+nu)&1))*exp(lognorm_CF[nu][mpq+nu]+(double)mpq*logu+(double)qmm*logv); // initialize state (i, state)
  CD ssum=0;
  //cout<<"mpq,nu="<<mpq<<" , "<<nu<<endl;  
  for(int s=0+(mpq<0)*(-mpq);s<=nu && s<=qmm+nu;s++) {
    //cout<<"P("<<s<<","<<nu-s<<")="<<P(s, nu-s)<<endl;
    ssum+=(1.0-2.0*(s&1))*exp(logbin(nu, s)+logbin(twoq+nu,qmm+nu-s)+(double)s*logu+(double)(nu-s)*logv)*P(s, nu-s);
    //cout<<"ssum("<<s<<")="<<ssum<<endl;    
  }
  //cout<<"Pre porbital="<<Orbital<<endl;
  //cout<<"ssum="<<ssum<<endl;    
  Orbital*=ssum;
  //cout<<"Post orbital="<<Orbital<<endl;
  return Orbital;
}

CD CF_Misc::UP_CF_orbital(int mpq,int nu,CD logu, CD logv) {
  return exp(CF_Misc::log_UP_CF_orbital(mpq,nu,logu,logv));
}

  CD CF_Misc::log_UP_CF_orbital(int mpq,int nu,CD logu, CD logv)
{
  double MaxDouble=numeric_limits<double>::max()/10.0;
  if(-MaxDouble>=(double)logv.real()){ //Special case when wfn is on north pole
    int twos=2*nu+twoq-mpq; //Only certain values of s give normalized wfns
    if(twos>=0 && twos<=2*nu && (twos%2 == 0) ) {
      int s=twos/2;
      CD loguvpow=(double)s*conj(logu)+(double)(s+mpq)*logu;
      double Norms=lognorm_UP_CF[nu][mpq+nu]+logbin(nu, s)+logbin(twoq+nu, mpq+s);
      CD ssum=iunit*pi*(double)((s+nu)&1)+Norms+loguvpow;
      return ssum;
    } else { return logzero;}
  } else if(-MaxDouble>=(double)logu.real()){ //Special case when wfn is on south pole
    int twos=-mpq; //Only certain values of s give normalized wfns
    if(twos>=0 && twos<=2*nu && (twos%2 == 0) ) {
      int s=twos/2;
      CD loguvpow=(double)(nu-s)*conj(logv)+(double)(nu-s+twoq-mpq)*logv;
      double Norms=lognorm_UP_CF[nu][mpq+nu]+logbin(nu, s)+logbin(twoq+nu, mpq+s);
      CD ssum=iunit*pi*(double)((s+nu)&1)+Norms+loguvpow;
      return ssum;
    } else { return logzero;}
  } else {
    CD ssum=logzero;
    for(int s=(mpq<0)*(-mpq);s<=nu+(mpq>twoq)*(twoq-mpq);s++) {
      CD loguvpow=(double)s*conj(logu)
	+(double)(s+mpq)*logu
	+(double)(nu-s)*conj(logv)
	+(double)(nu-s+twoq-mpq)*logv;
      double Norms=lognorm_UP_CF[nu][mpq+nu]+logbin(nu, s)+logbin(twoq+nu, mpq+s);
      ssum=add_exponentially(iunit*pi*(double)((s+nu)&1)+Norms+loguvpow,ssum);
    }     
    return ssum;
  }
}


CD CF_Misc::rev_CF_JK_orbital(int mpq,int nu,
			      Eigen::MatrixXcd logesp, int i,CD logu, CD logv)
{
  int Nm1=Ne-1;
  CD CFmat=0; // initialize element (i, j)
  for(int s=0+(mpq<0)*(-mpq);s<=nu+(mpq>twoq)*(twoq-mpq);s++) {
    int om=mpq+s;
    int tmp=Nm1+mpq-twoq;
    CD tsum=0.0; // initialize t-sum
    for(int t=om;t<=Nm1+om-nu-twoq;t++) {
      tsum+=(1.0-2.0*(t&1))*exp(lognorm_rev_CF[nu][mpq+nu]
				+logesp(i, t)+logfactlistMisc[Nm1-t]
				-logfactlistMisc[tmp+s-t-nu]+logfactlistMisc[t]
				-logfactlistMisc[t-om]
				+(double)(tmp-t)*logv
				+(double)(t-mpq)*logu);
    } // t end
    CFmat+=(1.0-2.0*((s+nu)&1))*exp(logbin(nu, s)+logbin(twoq+nu, om))*tsum;
  }
  return CFmat;
}

CD CF_Misc::rev_CF_JK_orbital_p2(int mpq,int nu,
				  Eigen::MatrixXcd logesp, int i,CD logu, CD logv){
  return exp(CF_Misc::log_rev_CF_JK_orbital_p2(mpq,nu,logesp,i,logu,logv));
}


CD CF_Misc::log_rev_CF_JK_orbital_p2(int mpq,int nu,
			Eigen::MatrixXcd logesp, int i,CD logu, CD logv)
{
  int Nm1=Ne-1;
  int twoNm2=2*Nm1;
  CD CFmat=logzero; // initialize state (i, state)
  for(int s=(mpq<0)*(-mpq);s<=nu+(mpq>twoq)*(twoq-mpq);s++) {
    CD tsum=logzero; // initialize t-sum
    for(int t=0;t<Ne;t++) {
      for(int t2=0;t2<Ne;t2++) {
	int tt2=t+t2;
	int tmp=twoNm2-tt2+mpq-twoq;
	if(mpq+s<=tt2 && tt2<=twoNm2+mpq-twoq-nu+s) {
	  CD Factors=lognorm_rev_CF_p2[nu][mpq+nu]+logbin(nu, s)
	    +logbin(twoq+nu, mpq+s)
	    +logesp(i, t)+logesp(i, t2)
	    +logfactlistMisc[twoNm2-tt2]-logfactlistMisc[tmp-nu+s]
	    +logfactlistMisc[tt2]-logfactlistMisc[tt2-mpq-s]
	    +(double)tmp*logv
	    +(double)(tt2-mpq)*logu;
	  tsum=add_exponentially(iunit*pi*(double)((tt2)&1)+Factors,tsum);
	  //cout<<"tdelta:"<<tdelta<<endl;
	}
      }// t2 end
      //cout<<"tdelta:"<<tdelta<<endl;
    } // t end
    CFmat=add_exponentially(iunit*pi*(double)((s+nu)&1)+tsum,CFmat);
  }// s end
  return CFmat;
}


// Collect the normalizations

// remember indices are nu and m+q
void CF_Misc::CF_getnorm() {
  lognorm_CF.resize(n, VD(twoq+2*n-1));
  for(int nu=0;nu<n;nu++) {
    for(int mpq=-nu;mpq<=twoq+nu;mpq++) {
      lognorm_CF[nu][mpq+nu]=CF_norm(Ne,Nphi,nu,twoq,mpq);
    }
  }
}

void CF_Misc::rev_CF_getnorm() {
  lognorm_rev_CF.resize(n,VD(twoq+2*n-1));
  int p=1;
  for(int nu=0;nu<n;nu++) {
    for(int mpq=-nu;mpq<=twoq+nu;mpq++) {
      lognorm_rev_CF[nu][mpq+nu]=rev_CF_norm(Ne,Nphi,p,nu,twoq,mpq);
    }
  }
}


void CF_Misc::rev_CF_p2_getnorm() {
  //logno.resize(n, twoq+2*n-1); 
  lognorm_rev_CF_p2.resize(n,VD(twoq+2*n-1));
  int p=2;
  for(int nu=0;nu<n;nu++) {
    for(int mpq=-nu;mpq<=twoq+nu;mpq++) {
      lognorm_rev_CF_p2[nu][mpq+nu]=rev_CF_norm(Ne,Nphi,p ,nu,twoq,mpq);
    }
  }
}


void CF_Misc::UP_CF_getnorm() {
  lognorm_UP_CF.resize(n,VD(twoq+2*n-1));
  for(int nu=0;nu<n;nu++) {
    for(int mpq=-nu;mpq<=twoq+nu;mpq++) {
      lognorm_UP_CF[nu][mpq+nu]=UP_CF_norm(nu,twoq,mpq);
    }
  }
}


// Remember j -> 2j !   also n is n in nu=n/(2pn+1), so n' goes up to n-1
Eigen::MatrixXcd CF_Misc::createP(int j, VCD uv) {
  Eigen::MatrixXcd P, f;
  //NB: The factor \prod{i\neq j}(zi-zj)^p is not taken into account here,
  //Its is taken into acocund at the outer most level, were the factor \prod{i<j}(zi-zj)^2p is multiplied onfo the CF part.
  
  P.resize(n, n); 
  f.resize(n, n);
  //Set the initial matrixes to zero
  for(int i=0;i<n;i++) {
    for(int k=0;k<n;k++) {
      f(i, k)=0.0;
      P(i, k)=0.0;
    }
  }
  P(0, 0)=1.0;
  if(n==1) return P;
  //f[a,b]=sum_{k\neq j} (v_k/den)^a*(-u_k/den)^b)
  // where den=u_j*v_k-u_k*v_j 
  for(int k=0;k<Ne;k++) {
    //cout<<"j="<<j<<endl;
    if(k!=j){
      //cout<<"k="<<k<<endl;
      //cout<<"uv...="<<endl<<uv[2*j]<<endl<<uv[2*k+1]<<endl<<uv[2*k]<<endl<<uv[2*j+1]<<endl;
      CD den=uv[2*j]*uv[2*k+1]-uv[2*k]*uv[2*j+1];
      //cout<<"den="<<den<<endl;
      CD uk = -uv[2*k]/den;
      CD vk = uv[2*k+1]/den;
      
      f(1, 0)+=vk;
      f(0, 1)+=uk;
      for(int b=2;b<n;b++) {
	f(0, b)+=pow(uk, b);
	f(b, 0)+=pow(vk, b);
      }
      for(int a=1;a<ceil(n/2.0);a++) {
	f(a, a)+=pow(uk*vk, a);
      }
      for(int a=1;a<n;a++) {
	for(int b=a+1;b<n-a;b++) {
	  f(a, b)+=pow(vk, a)*pow(uk, b);
	  f(b, a)+=pow(vk, b)*pow(uk, a);
	}
      }
    }
  }
  
  P(1, 0)=p*f(1, 0);
  P(0, 1)=p*f(0, 1);
  if(n>2) {
    P(2, 0)=p*p*f(1, 0)*f(1, 0)-p*f(2, 0);
    P(0, 2)=p*p*f(0, 1)*f(0, 1)-p*f(0, 2);
    P(1, 1)=p*p*f(0, 1)*f(1, 0)-p*f(1, 1);
  }
  if(n>3) {
    P(3, 0)=p*p*p*f(1, 0)*f(1, 0)*f(1, 0)-3.0*p*p*f(1, 0)*f(2, 0)+2.0*p*f(3, 0);
    P(0, 3)=p*p*p*f(0, 1)*f(0, 1)*f(0, 1)-3.0*p*p*f(0, 1)*f(0, 2)+2.0*p*f(0, 3);
    P(2, 1)=p*p*p*f(0, 1)*f(1, 0)*f(1, 0)-2.0*p*p*f(1, 0)*f(1, 1)-p*p*f(0, 1)*f(2, 0)+2.0*p*f(2, 1);
    P(1, 2)=p*p*p*f(1, 0)*f(0, 1)*f(0, 1)-2.0*p*p*f(0, 1)*f(1, 1)-p*p*f(1, 0)*f(0, 2)+2.0*p*f(1, 2);
  }
  if(n>4) {
    P(4,0)=p*p*p*p*f(1,0)*f(1,0)*f(1,0)*f(1,0)-6.0*p*p*p*f(1,0)*f(1,0)*f(2,0)+3.0*p*p*f(2,0)*f(2,0)+8.0*p*p*f(1,0)*f(3,0)-6.0*p*f(4,0);
    P(0,4)=p*p*p*p*f(0,1)*f(0,1)*f(0,1)*f(0,1)-6.0*p*p*p*f(0,1)*f(0,1)*f(0,2)+3.0*p*p*f(0,2)*f(0,2)+8.0*p*p*f(0,1)*f(0,3)-6.0*p*f(0,4);
    P(3,1)=p*p*p*p*f(0,1)*f(1,0)*f(1,0)*f(1,0)-3.0*p*p*p*f(1,0)*f(1,0)*f(1,1)-3.0*p*p*p*f(0,1)*f(1,0)*f(2, 0)+3.0*p*p*f(1, 1)*f(2,0)
      +6.0*p*p*f(1,0)*f(2,1)+2.0*p*p*f(0,1)*f(3,0)-6.0*p*f(3,1);
    P(1,3)=p*p*p*p*f(1,0)*f(0,1)*f(0,1)*f(0,1)-3.0*p*p*p*f(0, 1)*f(0,1)*f(1,1)-3.0*p*p*p*f(1,0)*f(0,1)*f(0,2)+3.0*p*p*f(1,1)*f(0,2)
      +6.0*p*p*f(0,1)*f(1,2)+2.0*p*p*f(1,0)*f(0,3)-6.0*p*f(1,3);
    P(2,2)=p*p*p*p*f(0,1)*f(0,1)*f(1,0)*f(1,0)-p*p*p*f(0,2)*f(1,0)*f(1,0)-4.0*p*p*p*f(0,1)*f(1,0)*f(1,1)+2.0*p*p*f(1,1)*f(1,1)
      +4.0*p*p*f(1,0)*f(1,2)-p*p*p*f(0,1)*f(0,1)*f(2,0)+p*p*f(0,2)*f(2,0)+4.0*p*p*f(0,1)*f(2,1)-6.0*p*f(2,2);
  }
  if(n>5) {
    P(5,0)=p*p*p*p*p*f(1,0)*f(1,0)*f(1,0)*f(1,0)*f(1,0)-10.0*p*p*p*p*f(1,0)*f(1,0)*f(1,0)*f(2,0)+15.0*p*p*p*f(1,0)*f(2,0)*f(2,0)
      +20.0*p*p*p*f(1,0)*f(1,0)*f(3,0)-20.0*p*p*f(2,0)*f(3,0)-30.0*p*p*f(1,0)*f(4,0)+24.0*p*f(5,0);
    P(0,5)=p*p*p*p*p*f(0,1)*f(0,1)*f(0,1)*f(0,1)*f(0,1)-10.0*p*p*p*p*f(0,1)*f(0,1)*f(0,1)*f(0,2)+15.0*p*p*p*f(0,1)*f(0,2)*f(0,2)
      +20.0*p*p*p*f(0,1)*f(0,1)*f(0,3)-20.0*p*p*f(0,2)*f(0,3)-30.0*p*p*f(0,1)*f(0,4)+24.0*p*f(0,5);
    P(4,1)=p*p*p*p*p*f(0,1)*f(1,0)*f(1,0)*f(1,0)*f(1,0)-4.0*p*p*p*p*f(1,0)*f(1,0)*f(1,0)*f(1,1)-6.0*p*p*p*p*f(0,1)*f(1,0)*f(1,0)*f(2,0)
      +12.0*p*p*p*f(1,0)*f(1,1)*f(2,0)+3.0*p*p*p*f(0,1)*f(2,0)*f(2,0)+12.0*p*p*p*f(1,0)*f(1,0)*f(2,1)-12.0*p*p*f(2,0)*f(2,1)
      +8.0*p*p*p*f(0,1)*f(1,0)*f(3,0)-8.0*p*p*f(1,1)*f(3,0)-24.0*p*p*f(1,0)*f(3,1)-6.0*p*p*f(0,1)*f(4,0)+24.0*p*f(4,1);
    P(1,4)=p*p*p*p*p*f(1,0)*f(0,1)*f(0,1)*f(0,1)*f(0,1)-4.0*p*p*p*p*f(0,1)*f(0,1)*f(0,1)*f(1,1)-6.0*p*p*p*p*f(1,0)*f(0,1)*f(0,1)*f(0,2)
      +12.0*p*p*p*f(0,1)*f(1,1)*f(0,2)+3.0*p*p*p*f(1,0)*f(0,2)*f(0,2)+12.0*p*p*p*f(0,1)*f(0,1)*f(1,2)-12.0*p*p*f(0,2)*f(1,2)
      +8.0*p*p*p*f(1,0)*f(0,1)*f(0,3)-8.0*p*p*f(1,1)*f(0,3)-24.0*p*p*f(0,1)*f(1,3)-6.0*p*p*f(1,0)*f(0,4)+24.0*p*f(1,4);
    P(3,2)=p*p*p*p*p*f(0,1)*f(0,1)*f(1,0)*f(1,0)*f(1,0)-p*p*p*p*f(0,2)*f(1,0)*f(1,0)*f(1,0)-6.0*p*p*p*p*f(0,1)*f(1,0)*f(1,0)*f(1,1)
      +6.0*p*p*p*f(1,0)*f(1,1)*f(1,1)+6.0*p*p*p*f(1,0)*f(1,0)*f(1,2)-3.0*p*p*p*p*f(0,1)*f(0,1)*f(1,0)*f(2,0)+3.0*p*p*p*f(0,2)*f(1,0)*f(2,0)
      +6.0*p*p*p*f(0,1)*f(1,1)*f(2,0)-6.0*p*p*f(1,2)*f(2,0)+12.0*p*p*p*f(0,1)*f(1,0)*f(2,1)-12.0*p*p*f(1,1)*f(2,1)-18.0*p*p*f(1,0)*f(2,2)
      +2.0*p*p*p*f(0,1)*f(0,1)*f(3,0)-2.0*p*p*f(0,2)*f(3,0)-12.0*p*p*f(0,1)*f(3,1)+24.0*p*f(3,2);
    P(2,3)=p*p*p*p*p*f(1,0)*f(1,0)*f(0,1)*f(0,1)*f(0,1)-p*p*p*p*f(2,0)*f(0,1)*f(0,1)*f(0,1)-6.0*p*p*p*p*f(1,0)*f(0,1)*f(0,1)*f(1,1)
      +6.0*p*p*p*f(0,1)*f(1,1)*f(1,1)+6.0*p*p*p*f(0,1)*f(0,1)*f(2,1)-3.0*p*p*p*p*f(1,0)*f(1,0)*f(0,1)*f(0,2)+3.0*p*p*p*f(2,0)*f(0,1)*f(0,2)
      +6.0*p*p*p*f(1,0)*f(1,1)*f(0,2)-6.0*p*p*f(2,1)*f(0,2)+12.0*p*p*p*f(1,0)*f(0,1)*f(1,2)-12.0*p*p*f(1,1)*f(1,2)-18.0*p*p*f(0,1)*f(2,2)
      +2.0*p*p*p*f(1,0)*f(1,0)*f(0,3)-2.0*p*p*f(2,0)*f(0,3)-12.0*p*p*f(1,0)*f(1,3)+24.0*p*f(2,3);
  }
  if(n>6) error("Positive CF series not realized for n > 6");
  for(int k1=0;k1<n;k1++) {
    for(int k2=0;k2<n;k2++) {
      //cout<<"P["<<j<<"]("<<k1<<","<<k2<<")="<<P(k1,k2)<<endl;
      //cout<<"f["<<j<<"]("<<k1<<","<<k2<<")="<<f(k1,k2)<<endl;	    
    }
  }
  return P;
}


Eigen::MatrixXcd CF_Misc::getlogesp(VCD uv) {
  Eigen::MatrixXcd *f=new Eigen::MatrixXcd[Ne], logesp; 
  logesp.resize(Ne, Ne); 
  for(int i=0;i<Ne;i++) f[i].resize(Ne, Ne+1);
  for(int i=0;i<Ne;i++) {
    for(int t=0;t<Ne;t++) {
      if(t==0) f[i](t,0)=1;
      else f[i](t,0)=0;
      for(int n=1;n<=Ne;n++) {
	if(n-1==i) f[i](t,n)=f[i](t,n-1);
	else if(t==0) f[i](t,n)=f[i](t,n-1)*uv[2*n-2]; 
	else f[i](t,n)=f[i](t,n-1)*uv[2*n-2]+f[i](t-1,n-1)*uv[2*n-1];
      }
      logesp(i,t)=log(f[i](t,Ne));
    }
  }
  delete [] f;
  return logesp;
}


void print_matrix(Eigen::MatrixXcd Mat,int Dim) {
  if(Dim==1) { cout<<"("<<Mat(0,0)<<")"<<endl;}
  else {
    for(int i=0;i<Dim;i++) {
      if(i==0) { cout<<"/";}
      else if(i==(Dim-1)) { cout<<"\\";}
      else { cout<<"|";}
      for(int j=0;j<Dim;j++) {
	cout<<Mat(i,j)<<" ";
      }
      if(i==0) { cout<<"\\"<<endl;}
      else if(i==(Dim-1)) { cout<<"/"<<endl;}
      else { cout<<"|"<<endl;}
    }
  }
}


CD add_exponentially(CD x,CD y){

  // we add the smaller number as a pertubation to the larger one
  if(x.real()>y.real()){ return(x + log(1.0+exp(y-x)));}
  else if(x.real()<=y.real()){ return(y + log(1.0+exp(x-y)));}
  else {
    //FIXME - is this the correct way to handle is
    //(can the situation even happen?)
    cout<<"Trying to add x="<<x<<" and y="<<y<<endl;
    cout<<"x is neither larger than, smaller than or equal to y"<<endl;
    error("");
  }
}


CD TakeLogDeterminant( Eigen::MatrixXcd CFMat,int MatDim ) {
  Eigen::PartialPivLU<Eigen::MatrixXcd> LU(CFMat);
  Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
  CD LogDet=0;
  for(int i=0;i<MatDim;i++) LogDet+=log(U(i,i));
  LogDet+=log(CD(LU.permutationP().determinant()));
  imod(LogDet, twopi);
  return LogDet;
}


CD TakeLogDeterminantOfLog( Eigen::MatrixXcd CFMat,int MatDim ) {
  //Exponentiate
  double ScaleFactor=0.0;
  for(int i=0;i<MatDim;i++) {
    //Remove largest scale factor
    double maxScaleFactor = CFMat(i,0).real();
    for(int j=0;j<MatDim;j++) { //Collumn;
      if(maxScaleFactor<CFMat(i,j).real()){
	maxScaleFactor=CFMat(i,j).real();//Make this the biggest element
      }
    }
    //Loop again and remove the biggest component
    for(int j=0;j<MatDim;j++) { //Collumn;
      CFMat(i,j)=exp(CFMat(i,j)-maxScaleFactor);
    }
    ScaleFactor+=maxScaleFactor;
  }
  CD LogDet=TakeLogDeterminant(CFMat,MatDim );
  return LogDet+ScaleFactor;
}
