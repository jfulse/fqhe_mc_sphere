/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Pfaffian_wo_sign.h"

void Pfaffian_wo_sign::update(int r, VCD uv_) {
	uv[r]=uv_[r];
	uv[r+1]=uv_[r+1];
	for(int i=0;i<r;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
	for(int i=r+2;i<2*Ne;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
	Eigen::PartialPivLU<Eigen::MatrixXcd> LU(P);
	Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
	CD newlogfactor=0;
	for(int i=0;i<Ne;i++) newlogfactor+=log(U(i,i));
	imod(newlogfactor, twopi);
	newlogfactor/=2.0;
	logratio=newlogfactor-logfactor;
	logfactor=newlogfactor;
}

void Pfaffian_wo_sign::initialise(VCD uv_) {//cout << "initialisation" << endl;
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne;i+=2) {
		for(int j=0;j<i;j+=2) P(i/2,j/2)=-P(j/2,i/2);
		for(int j=i+2;j<2*Ne;j+=2) P(i/2,j/2)=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
	}
	Eigen::PartialPivLU<Eigen::MatrixXcd> LU(P);
	Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
	logfactor=0;
	for(int i=0;i<Ne;i++) logfactor+=log(U(i,i));
	imod(logfactor, twopi);
	logfactor/=2.0;
}

void Pfaffian_wo_sign::update(VCD uv_) {
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne;i+=2) {
		for(int j=0;j<i;j+=2) P(i/2,j/2)=-P(j/2,i/2);
		for(int j=i+2;j<2*Ne;j+=2) P(i/2,j/2)=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
	}
	Eigen::PartialPivLU<Eigen::MatrixXcd> LU(P);
	Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
	logfactor=0;
	for(int i=0;i<Ne;i++) logfactor+=log(U(i,i));
	imod(logfactor, twopi);
	logfactor/=2.0;
}

void Pfaffian_wo_sign::specific_revert(int r, CD oldu, CD oldv) {
	for(int i=0;i<r;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*oldv-oldu*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
	for(int i=r+2;i<2*Ne;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*oldv-oldu*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
}
