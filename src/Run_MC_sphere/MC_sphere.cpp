/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "MC_sphere.h"
#include <random>

class timer
{
  clock_t StartWallTime=clock();
  double LastWallTime = 0.0;
  int steps=1;
  int min_len=60, hr_len=3600, sec_len=1;
  
public:
  void write_progress(long long int,long long);
};

void timer::write_progress(long long int i,long long N)
{
  double TimeWallLeft,TotalTime,RedEl,RedLe,RedTot;
  long long int SamplesLeft;
  double TimeWallElapsed=(clock()-StartWallTime)/CLOCKS_PER_SEC;
  
  int steps_el=sec_len;
  int steps_le=hr_len;
  string dimention_le,dimention_el, dimention_Tot;
  
  if(TimeWallElapsed>=LastWallTime+steps)
    {
      /**Computes samples left**/
      SamplesLeft = N -i  +1;
      /**Compute time left**/
      TimeWallLeft = (double(SamplesLeft)/double(i))*TimeWallElapsed;
      TotalTime = (double(N)/double(i))*TimeWallElapsed;
      
      
      cout << n2s(i)<< " samples collected of "<<n2s(N)<<endl;
      /**cout << "Time: " << TimeWallElapsed <<"s of "<<TotalTime<<"s, "<<TimeWallLeft<<"s to go"<<endl;**/
      LastWallTime=TimeWallElapsed;
      
      
      dimention_le=" sec";
      dimention_el=" sec";
      dimention_Tot=" sec";
      steps_le=sec_len;
      steps_el=sec_len;
      RedEl=TimeWallElapsed;
      RedLe=TimeWallLeft;
      RedTot=TotalTime;      
      if(TimeWallElapsed > min_len)
	{
	  /**cout << "set min up el"<<endl;**/
          dimention_el=" min";
	  RedEl=TimeWallElapsed/min_len;
          steps_el=min_len;
	}
      if(TimeWallElapsed > hr_len)
	{
	  /**cout << "set hr up el"<< endl;**/
	  dimention_el="  hr";
	  RedEl=TimeWallElapsed/hr_len;
	  steps_el=hr_len;
	}
      
      if(TimeWallLeft > min_len)
	{
	  /**cout << "set min up le"<<endl;**/
          dimention_le=" min";
	  RedLe=TimeWallLeft/min_len;
          steps_le=min_len;
	}
      if(TimeWallLeft > hr_len)
	{
	  /**cout << "set hr up le"<< endl;**/
	  dimention_le="  hr";
	  RedLe=TimeWallLeft/hr_len;
	  steps_le=hr_len;
	}
      
      if(TotalTime > min_len)
	{
	  /**cout << "set min up le"<<endl;**/
          dimention_Tot=" min";
	  RedTot=TotalTime/min_len;
	}
      if(TotalTime > hr_len)
	{
	  /**cout << "set hr up le"<< endl;**/
	  dimention_Tot="  hr";
	  RedTot=TotalTime/hr_len;
	}
      
      cout << "Time: " << RedEl<<dimention_el<<" of "<<RedTot<<dimention_Tot<<", "<<RedLe<<dimention_le<<" to go"<<endl;
      
      steps=min(steps_el,steps_le);
      /**cout<<"Next step size:"<<steps<<endl;**/
    }
}



MC_sphere::MC_sphere(input* params, int& Nphi, stringstream& status, bool nostab) : rangen(0) {
  int sd;
  //Get initial random seed from true randomness (like /dev/urandom)
  std::random_device random_generator;
  if(params->is_activated("-sd")){
    sd=params->getint("-sd");
    if(sd==-1){sd=random_generator();}}
  else sd=random_generator();
  //Set the random seed
  cout<<"Seed is:"<<to_string(sd)<<endl;
  rangen.RandomInit(sd);
  Ne=params->getint("-Ne");
  step=params->getdouble("-st");
  verb=true;
  if(params->is_flagged("-q")) verb=false;
  tp=new double[2*Ne];
  spinor.resize(2*Ne);
  if(params->is_activated("--init-conf")) {
    VD init_conf=params->getVD("--init-conf");
    if(init_conf.size()!=2*Ne) error("Initial configuration must have 2Ne = "+to_string(2*Ne)+" values rather than "+to_string(init_conf.size()));
    place_el(init_conf);
  }
  else ran_el();
  probability=new Sphere_wavefunction(params, spinor, Nphi, status, nostab);
  R=sqrt(Nphi/2.0);
  step/=R;
}

void MC_sphere::thermalise(int th, string thermfile) {
  int accept=0;
  clock_t starttime=clock();
  
  timer my_timer;
  cout << "Thermalizing..." << endl;
  if(thermfile=="") {
    for(int i=0;i<th;i++) {
      MC_iteration(probability, accept);
      my_timer.write_progress(i,th);
    }
  }
  else {
    ofstream thermout(thermfile.c_str());
    if(!thermout) error("Could not open "+thermfile+" for writing");
    thermout.precision(17);
    for(int i=0;i<th;i++) {
      MC_iteration(probability, accept);
      for(int j=0;j<2*Ne;j+=2) thermout << tp[j] << " " << tp[j+1] << " ";
      thermout << endl;
      my_timer.write_progress(i,th);
    }
    thermout.close();
  }
  
  double sec=(clock()-starttime)/CLOCKS_PER_SEC;
  cout << "Thermalised in " << sec << "s = " << sec/60 << "min; " << 100.0*accept/th << "% accepted" << endl;
}

void MC_sphere::SampleProb(int dl, long long N, string wfout, string wgtout, string posout) {
  
  int accept=0;
  dl*=Ne;
  double *wfnvalues=new double[2];
  double *probvalues=new double[2];
  clock_t starttime=clock();
  
  //--------The wfn values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[0]->openFileToWrite(wfout.c_str())!=0) error("Could not open "+wfout+" for writing");
  hdf5readwrite[0]->createRealDataset("wf_values", N, 2);
  HDF5Buffer *write_wf_hdf5 = hdf5readwrite[0]->setupRealSamplesBuffer("wf_values", 0, 1000);
  //--------The wgt values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[1]->openFileToWrite(wgtout.c_str())!=0) error("Could not open "+wgtout+" for writing");
  hdf5readwrite[1]->createRealDataset("wf_values", N, 2);
  HDF5Buffer *write_wgt_hdf5 = hdf5readwrite[1]->setupRealSamplesBuffer("wf_values", 0, 1000);
  //--------The pos values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[2]->openFileToWrite(posout.c_str())!=0) error("Could not open "+posout+" for writing");
  hdf5readwrite[2]->createRealDataset("positions", N, 2*Ne);
  HDF5Buffer *write_pos_hdf5 = hdf5readwrite[2]->setupRealSamplesBuffer("positions", 0, 1000);
  
  timer my_timer;
  
  for(long long int i=0;i<N;i++) {
    probability->update(spinor);
    CD logvalue=probability->returnlogWF();
    probvalues[0]=real(logvalue);
    probvalues[1]=imag(logvalue);
    wfnvalues[0]=real(logvalue);
    wfnvalues[1]=imag(logvalue);
    write_wf_hdf5->writeToBuffer(wfnvalues, 2);
    write_wgt_hdf5->writeToBuffer(probvalues, 2);
    write_pos_hdf5->writeToBuffer(tp, 2*Ne);
    for(int j=0;j<dl;j++) MC_iteration(probability, accept);
    my_timer.write_progress(i,N);
  }
  cout << n2s(N) << " samples collected" << endl;
  
  double sec=(clock()-starttime)/CLOCKS_PER_SEC;
  cout << endl << "Sampling done in " << sec << "s = " << sec/60 << "min; " << 100.0*accept/(dl*N) << "% accepted" << endl;
  hdf5readwrite[0]->close();
  hdf5readwrite[1]->close();
  hdf5readwrite[2]->close();
  delete [] probvalues;
  delete [] wfnvalues;
}

void MC_sphere::SampleLaughlin(int dl, long long N, string wfout, string wgtout, string posout, int m) {
  cout<<"------ENtering Lauglin"<<endl;
  
  int accept=0;
  dl*=Ne;
  double *probvalues=new double[2];
  double *wfnvalues=new double[2];
  clock_t starttime=clock();
  
  //--------The wfn values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[0]->openFileToWrite(wfout.c_str())!=0) error("Could not open "+wfout+" for writing");
  hdf5readwrite[0]->createRealDataset("wf_values", N, 2);
  HDF5Buffer *write_wf_hdf5 = hdf5readwrite[0]->setupRealSamplesBuffer("wf_values", 0, 1000);
  //--------The wgt values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[1]->openFileToWrite(wgtout.c_str())!=0) error("Could not open "+wgtout+" for writing");
  hdf5readwrite[1]->createRealDataset("wf_values", N, 2);
  HDF5Buffer *write_wgt_hdf5 = hdf5readwrite[1]->setupRealSamplesBuffer("wf_values", 0, 1000);
  //--------The pos values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[2]->openFileToWrite(posout.c_str())!=0) error("Could not open "+posout+" for writing");
  hdf5readwrite[2]->createRealDataset("positions", N, 2*Ne);
  HDF5Buffer *write_pos_hdf5 = hdf5readwrite[2]->setupRealSamplesBuffer("positions", 0, 1000);
  
  
  Sphere_wavefunction *Laughlin_prob=new Sphere_wavefunction(spinor, Nphi, m);
  Laughlin_prob->update(spinor);
  
  timer my_timer;
  //cout<<"------Entering the loop"<<endl;
  for(long long int i=0;i<N;i++) {
    probability->update(spinor);
    CD logvalue=probability->returnlogWF();
    CD L_value=Laughlin_prob->returnlogWF();   
    probvalues[0]=real(L_value);
    probvalues[1]=imag(L_value);
    wfnvalues[0]=real(logvalue);
    wfnvalues[1]=imag(logvalue);
    write_wf_hdf5->writeToBuffer(wfnvalues, 2);
    write_wgt_hdf5->writeToBuffer(probvalues, 2);
    write_pos_hdf5->writeToBuffer(tp, 2*Ne);
    for(int j=0;j<dl;j++) MC_iteration(Laughlin_prob, accept);
    my_timer.write_progress(i,N);
  }
  cout << n2s(N) << " samples collected" << endl;
  
  double sec=(clock()-starttime)/CLOCKS_PER_SEC;
  cout << endl << "Sampling done in " << sec << "s = " << sec/60 << "min; " << 100.0*accept/(dl*N) << "% accepted" << endl;
  cout<<"Close hdf5 chanels 0:"<<endl;
  hdf5readwrite[0]->close();
  cout<<"Close hdf5 chanels 1:"<<endl;
  hdf5readwrite[1]->close();
  cout<<"Close hdf5 chanels 2:"<<endl;
  hdf5readwrite[2]->close();
  cout<<"Delete values: 0"<<endl;
  delete [] probvalues;
  cout<<"Delete values: 1"<<endl;
  delete [] wfnvalues;
  cout<<"Done:"<<endl;
}

void MC_sphere::ComputeOnExisting(long long int N, string posin, string wfout, long long skip) {
  
  double *wfvalues=new double[2];
  clock_t starttime=clock();
  
  cout<<"Open the wf file."<<endl;
  
  //--------The wfn values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[0]->openFileToWrite(wfout.c_str())!=0) error("Could not open "+wfout+" for writing");
  hdf5readwrite[0]->createRealDataset("wf_values", N, 2);
  HDF5Buffer *write_wf_hdf5 = hdf5readwrite[0]->setupRealSamplesBuffer("wf_values", 0, 1000);
  
  cout<<"Open the pos file."<<endl;
  //--------The pos values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[1]->openFileToRead(posin.c_str())!=0) error("Could not open "+posin+" for reading");
  HDF5Buffer *read_pos_hdf5 = hdf5readwrite[1]->setupRealSamplesBuffer("positions", 0, 1000);
  unsigned long rows, cols;
  hdf5readwrite[1]->getDatasetDims("positions", rows, cols);
  if(cols!=2*Ne) error("Input position file '"+posin+"' has "+to_string(cols)+" columns; expected 2*Ne = "+to_string(2*Ne));
  if(rows<N) error("Input position file '"+posin+"' only has "+to_string(rows)+" samples; asked for "+to_string(N));
  
  cout<<"Open the begin taking data."<<endl;
  
  double *positions=new double[2*Ne], *wfinvalues=new double[4];
  VCD spinor(2*Ne);
  
  for(long long int i=0;i<skip;i++) {
    read_pos_hdf5->readFromBuffer(positions, 2*Ne);
  }
  
  timer my_timer;
  
  for(long long int i=0;i<N;i++) {
    read_pos_hdf5->readFromBuffer(positions, 2*Ne);
    tp2sp(2*Ne, positions, spinor);
    probability->update(spinor);
    CD logvalue=probability->returnlogWF();
    wfvalues[0]=real(logvalue);
    wfvalues[1]=imag(logvalue);
    write_wf_hdf5->writeToBuffer(wfvalues, 2);
    my_timer.write_progress(i,N);
  }
  cout << n2s(N) << " samples collected" << endl;
  
  double sec=(clock()-starttime)/CLOCKS_PER_SEC;
  cout << "Computation done in " << sec << "s = " << sec/60 << "min" << endl;
  hdf5readwrite[0]->close();
  hdf5readwrite[1]->close();
  delete [] positions;
  delete [] wfvalues;
  
}


void MC_sphere::ComputeOnExistingManyConfigs(long long int N, string posin, string wfout, long long skip, int NumConfigs) {
  
  double *wfvalues=new double[2*NumConfigs];
  clock_t starttime=clock();
  
  cout<<"Open the wf file."<<endl;
  
  //--------The wfn values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[0]->openFileToWrite(wfout.c_str())!=0) error("Could not open "+wfout+" for writing");
  hdf5readwrite[0]->createRealDataset("wf_values", N, 2*NumConfigs);
  HDF5Buffer *write_wf_hdf5 = hdf5readwrite[0]->setupRealSamplesBuffer("wf_values", 0, 1000);
  
  cout<<"Open the pos file."<<endl;
  //--------The pos values--------
  hdf5readwrite.push_back(new HDF5Wrapper);
  if(hdf5readwrite[1]->openFileToRead(posin.c_str())!=0) error("Could not open "+posin+" for reading");
  HDF5Buffer *read_pos_hdf5 = hdf5readwrite[1]->setupRealSamplesBuffer("positions", 0, 1000);
  unsigned long rows, cols;
  hdf5readwrite[1]->getDatasetDims("positions", rows, cols);
  if(cols!=2*Ne) error("Input position file '"+posin+"' has "+to_string(cols)+" columns; expected 2*Ne = "+to_string(2*Ne));
  if(rows<N) error("Input position file '"+posin+"' only has "+to_string(rows)+" samples; asked for "+to_string(N));
  
  cout<<"Open the positions file to begin taking data."<<endl;
  
  double *positions=new double[2*Ne], *wfinvalues=new double[4];
  VCD spinor(2*Ne);
  
  for(long long int i=0;i<skip;i++) {
    read_pos_hdf5->readFromBuffer(positions, 2*Ne);
  }
  
  timer my_timer;
  
  for(long long int i=0;i<N;i++) {
    read_pos_hdf5->readFromBuffer(positions, 2*Ne);
    tp2sp(2*Ne, positions, spinor);
    probability->updateSpinor(spinor);
    for (int ConfNo=0;ConfNo<NumConfigs;ConfNo++) {
      probability->updateConfig(ConfNo); // Loope over the configs one by one
      CD logvalue=probability->returnlogWF();
      wfvalues[2*ConfNo]=real(logvalue);
      wfvalues[2*ConfNo+1]=imag(logvalue);
      //cout << "Logvalue" << logvalue << endl;
    }
    write_wf_hdf5->writeToBuffer(wfvalues, 2*NumConfigs);
    my_timer.write_progress(i,N);
  }
  cout << n2s(N) << " samples collected" << endl;
  
  double sec=(clock()-starttime)/CLOCKS_PER_SEC;
  cout << "Computation done in " << sec << "s = " << sec/60 << "min" << endl;
  hdf5readwrite[0]->close();
  hdf5readwrite[1]->close();
  delete [] positions;
  delete [] wfvalues;
  
}



void MC_sphere::stabilize_step(double prec, int max_it, int test_it, bool verb) {
  if(step>R) {warning("MC step is > R; setting step = R"); step=R;}
  int it=0, accept;
  double test_step=2.0*R/20.0, max_step=R/10.0;
  timer my_timer;
  while(it<max_it) {
    cout << "Stabilizing..." << endl;
    it++;
    accept=0;
    for(int i=0;i<test_it;i++){
      MC_iteration(probability, accept);
      my_timer.write_progress(i,test_it);
    }
    double acceptrate=accept/(double)test_it;
    test_step=2.0*max_step*abs(0.5-acceptrate);
    if(acceptrate<0.5-prec) {
      if(test_step>step) step/=2.0;
      else step-=test_step;
    }
    else if(acceptrate>0.5+prec) step+=test_step;
    else break;
    
  }
  if(it==max_it) {
    warning("Not stabilized; acceptance ratio at "+to_string(100.0*accept/test_it)+"%"); 
    //cout << "Last ratio: " << probability->returnlogratio() << " last factor: " << probability->returnlogWF() << endl;
    //int I; cin >> I;
  }
  else {
    if(verb) cout << "Stabilised at " << 100.0*accept/test_it << "%" << endl;
  }
}

void MC_sphere::MC_iteration(Sphere_wavefunction*& WF, int& accept) {
  
  int r=2*floor(Ne*rangen.Random());
  double tnew, pnew;
  CD unew, vnew;
  newtp(tp[r], step*(rangen.Random()-0.5), tp[r+1], step*(2.0*rangen.Random()-1.0), tnew, pnew, unew, vnew);
  CD uold=spinor[r], vold=spinor[r+1];
  
  spinor[r]=unew;
  spinor[r+1]=vnew;
  //cout << "moved " << r << "; uv = ";  for(int i=0;i<2*Ne;i+=2) cout << spinor[i] << " " << spinor[i+1] << endl;cout << endl;
  WF->update(r, spinor);
  CD logratio=WF->returnlogratio();
  
  double exprat=exp(2.0*real(logratio))*sin(tnew)/sin(tp[r]);
  if(exprat>=1.0 || exprat>rangen.Random()) {
    tp[r]=tnew; tp[r+1]=pnew;
    accept++; //cout << "Y" << endl;
  }
  else {
    WF->revert(r, uold, vold);
    spinor[r]=uold;
    spinor[r+1]=vold; //cout << "N" << endl;
  }
  
  return;
}

void MC_sphere::Laughlin_therm(int it) {
  int accept=0, Nphi;
  Sphere_wavefunction *Laughlin_prob=new Sphere_wavefunction(spinor, Nphi);
  
  timer my_timer;
  cout << "Thermalizing using Laughlin ..."<< endl;
  for(int i=0;i<it;i++) {
    MC_iteration(Laughlin_prob, accept);
    my_timer.write_progress(i,it);
  }
}

void MC_sphere::ran_el() {
  for(int i=0;i<2*Ne;i+=2) {
    tp[i]=acos(2.0*rangen.Random()-1.0);
    tp[i+1]=2.0*pi*rangen.Random();
    double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
    spinor[i]=cos(tp[i]/2.0)*CD(cp, sp);
    spinor[i+1]=sin(tp[i]/2.0)*CD(cp, -sp);
  }
}

void MC_sphere::place_el(VD init_conf) {
  for(int i=0;i<2*Ne;i+=2) {
    tp[i]=init_conf[i];
    tp[i+1]=init_conf[i+1];
    double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
    spinor[i]=cos(tp[i]/2.0)*CD(cp, sp);
    spinor[i+1]=sin(tp[i]/2.0)*CD(cp, -sp);
  }
}

void clear_pointer_vector(vector<HDF5Wrapper*>& vec) {
  int n=vec.size();
  for(int i=0;i<n;i++) delete vec[i];
}
