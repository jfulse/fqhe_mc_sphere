/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Laughlin_qh.h"

void Laughlin_qh::update(int r, VCD uv_) {
	CD newu=uv_[r];
	CD newv=uv_[r+1];
	logratio=log(newu*v0-u0*newv)-log(uv[r]*v0-u0*uv[r+1]);
	logfactor+=logratio;
	imod(logratio, twopi);
	imod(logfactor, twopi);
	uv[r]=newu;
	uv[r+1]=newv;
}

void Laughlin_qh::initialise(VCD uv_) {
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne;i+=2) logfactor+=log(uv[i]*v0-u0*uv[i+1]);
	imod(logfactor, twopi);
}

void Laughlin_qh::update(VCD uv_) {
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne;i+=2) logfactor+=log(uv[i]*v0-u0*uv[i+1]);
	imod(logfactor, twopi);
}
