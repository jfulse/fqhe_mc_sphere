/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_GS.h"

Eigen::MatrixXcd CF_GS::setupCFmat(VCD uv) {
  Eigen::MatrixXcd P, CFmat; 
  CFmat.resize(Ne, Ne);
  for(int i=0;i<Ne;i++) { // rows; particle i
    P=My_CF_Misc.createP(i, uv);		
    int state=0; // state dep. on nu and m
    CD logu=log(uv[2*i]), logv=log(uv[2*i+1]);
    for(int nu=0;nu<n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+q
	CFmat(i,state)=My_CF_Misc.CF_orbital(mpq,nu,P,logu,logv);	  
	  state++;
      } // m end (w.r.t. q+m)
    }// nu end
  } // i end
  return CFmat;
}

void CF_GS::updateSpinor(VCD uv_) {
  uv=uv_; //Update the spinor;
  CF_GS::update(uv); //Update the CF-determinant
}

void CF_GS::updateConfig(int ConfNo) {}
//Nothing needs to be done, logfactor updated through updateSpinor(VCD)
