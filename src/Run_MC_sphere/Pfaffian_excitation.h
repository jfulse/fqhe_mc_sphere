/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef PFAFFIAN_EXCITATION_H
#define PFAFFIAN_EXCITATION_H

#include "Sphere_WF_factor.h"
#include "Slater.h"
#include "CF_excitation.h"

class Pfaffian_excitation : public Sphere_WF_factor {

	private:
	int Nhalf;
	vector<Sphere_WF_factor*> factors;
	CD newlogfactor;
	void specific_revert(int, CD, CD) {};
	VI ordertest;
	double fac;
	
	void symmetrise(int n=0, int k=0);
	void evaluate();

	public:
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	
	Pfaffian_excitation(int Ne_, VCD uv_, VS cfiles, int Nphi_Pf, stringstream& status) : 
			Sphere_WF_factor(Ne_, uv_) {
		
		if(Ne%2) error("Pfaffian excitation must have even number of particles (requested Ne = "+to_string(Ne)+")");
		if(cfiles.size()!=2) error("Must supply two excitation config files (using -cPf)");
		Nhalf=Ne/2;
		int twoq=Nphi_Pf-(Nhalf-1);
		fac=-Nhalf*log(2.0);
		status << "Excited Pfaffian; Ne = " << Ne_ << endl;
		factors.push_back(new CF_excitation(Nhalf, VCD(uv.begin(), uv.begin()+Ne), 1, twoq, cfiles[0], status));
		factors.push_back(new Slater(Nhalf, VCD(uv.begin(), uv.begin()+Ne), 1, 0.0, status));
		factors.push_back(new CF_excitation(Nhalf, VCD(uv.begin()+Ne, uv.end()), 1, twoq, cfiles[1], status));
		factors.push_back(new Slater(Nhalf, VCD(uv.begin()+Ne, uv.end()), 1, 0.0, status));
	}
	
	~Pfaffian_excitation() {}
};

#endif
