/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "Pfaffian.h"

void Pfaffian::update(int r, VCD uv_) {
	uv[r]=uv_[r];
	uv[r+1]=uv_[r+1];
	for(int i=0;i<r;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
	for(int i=r+2;i<2*Ne;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
	CD newfac=logPfaffian(P); //cout << "new: " << newfac << " old: " << logfactor << endl; int I; cin >> I;//cout << P << endl;
	imod(newfac, twopi);
	logratio=newfac-logfactor;
	logfactor+=logratio;
}

void Pfaffian::initialise(VCD uv_) {//cout << "initialisation" << endl;
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne;i+=2) {
		for(int j=0;j<i;j+=2) P(i/2,j/2)=-P(j/2,i/2);
		for(int j=i+2;j<2*Ne;j+=2) P(i/2,j/2)=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
	}
	logfactor=logPfaffian(P);
	imod(logfactor, twopi);
}

void Pfaffian::update(VCD uv_) {
	uv=uv_;
	logfactor=0;
	for(int i=0;i<2*Ne;i+=2) {
		for(int j=0;j<i;j+=2) P(i/2,j/2)=-P(j/2,i/2);
		for(int j=i+2;j<2*Ne;j+=2) P(i/2,j/2)=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
	}
	logfactor=logPfaffian(P);
	imod(logfactor, twopi);
}

void Pfaffian::specific_revert(int r, CD oldu, CD oldv) {
	for(int i=0;i<r;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*oldv-oldu*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
	for(int i=r+2;i<2*Ne;i+=2) {
		P(i/2,r/2)=1.0/(uv[i]*oldv-oldu*uv[i+1]);
		P(r/2,i/2)=-P(i/2,r/2);
	}
}

CD Pfaffian::logPfaffian(Eigen::MatrixXcd A) {
	int n=A.rows();
	VCD factors;
	for(int i=0;i<n-2;i++) iteration(A, factors);
	factors.push_back(A(0,1));
	CD P=0.0;
	for(int i=0;i<n;i+=2) P+=log(factors[i]);
	return P;
}

void Pfaffian::iteration(Eigen::MatrixXcd& A, VCD& factors) {
	
	Eigen::VectorXcd x=A.row(0).tail(A.cols()-1);
	CD a=x.norm()*exp(CD(0.0, arg(x[0])));
	x[0]+=a;
	double sign=-1.0;
	if(abs(x[0]+a)<1e-10) {
		x[0]=A(0,1)-a;
		sign=1.0;
	}
	factors.push_back(a*sign);
	Eigen::MatrixXcd P=herm_outer(x);
	P*=-2.0/x.squaredNorm();
	addIdentity(P);	
	A=multiply_t_inner(A, P);
	A=multiply_res_ss(P, A);
}

void Pfaffian::addIdentity(Eigen::MatrixXcd& m) {
	for(int i=0;i<m.rows();i++) m(i,i)+=1.0;
}

Eigen::MatrixXcd Pfaffian::herm_outer(Eigen::VectorXcd v) {
	int n=v.size();
	Eigen::MatrixXcd outer(n, n);
	for(int i=0;i<n;i++) {
		outer(i,i)=norm(v[i]);
		for(int j=i+1;j<n;j++) outer(i,j)=v[i]*conj(v[j]);
		for(int j=0;j<i;j++) outer(i,j)=conj(outer(j,i));
	}
	return outer;
}

Eigen::MatrixXcd Pfaffian::multiply_t_inner(Eigen::MatrixXcd m1, Eigen::MatrixXcd m2) {
	int n=m2.rows();
	Eigen::MatrixXcd res=Eigen::MatrixXcd::Zero(n, n);
	for(int i=0;i<n;i++) {
		for(int j=0;j<n;j++) {
			for(int k=0;k<n;k++) res(i,j)+=m1(i+1,k+1)*m2(j,k);
		}
	}			
	return res;
}

Eigen::MatrixXcd Pfaffian::multiply_res_ss(Eigen::MatrixXcd m1, Eigen::MatrixXcd m2) {
	int n=m1.rows();
	Eigen::MatrixXcd res=Eigen::MatrixXcd::Zero(n, n);
	for(int i=0;i<n;i++) {
		for(int j=i+1;j<n;j++) {
			for(int k=0;k<n;k++) res(i,j)+=m1(i,k)*m2(k,j);
		}
		for(int j=0;j<i;j++) res(i,j)=-res(j,i);
	}			
	return res;
}
