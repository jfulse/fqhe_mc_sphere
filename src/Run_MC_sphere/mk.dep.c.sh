#!/bin/bash
## This script lists all the dependencies that different f.90 files have
## FIXME: same moudle can be lsited twise (un-neccesary) (not a problem for make though)

set -e
set -u


###C header files
for f in *.h ; do
    #file=$f
    file=$(echo $f | sed 's|\.h|\.o|')
    ## Search through .cpp for '#incliude "header"',
    ## extract the header,
    dep_h=$(grep '^\s*\#include \"' $f | awk ' {print $2}' | sed 's|"||g' | sed 's|\r$||g' )
    ## sort the unique ones (tr ads a new line so that sort can do it's thing)
    dep_o=$(echo $dep_h | sed 's|Eigen/Dense||g' | sed 's|main_misc.h||g' | sed 's|misc_sphere.h||g' | sed 's|sfmt.h||g' | sed 's|misc.h||g' | sed 's|input.h||g' | sed 's|HDF5Wrapper.h||g' | tr ' ' '\n' | sed 's|\(.*\)\.h|$(DIR_BLD)/\1\.o|g' | sort -u)
    ##echo dep_cpp:$dep_cpp
    ### echo out
    echo '$(DIR_BLD)/'$file: $dep_o
done
