/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef UP_CF_EXCITATION_H
#define UP_CF_EXCITATION_H

#include "CF_superposition.h"

class UP_CF_excitation : public CF_superposition {
  
 private:
  
  Eigen::MatrixXcd setupCFmat(VCD, int*);

 public:
  
 UP_CF_excitation(int Ne_, VCD uv_, int twop_, int twoq_, string cfile, stringstream& status, string mode="UP_pos") : 
  CF_superposition(Ne_, uv_, twop_, twoq_, cfile, status, mode) {
    
  }
  
  ~UP_CF_excitation() {}
};

#endif
