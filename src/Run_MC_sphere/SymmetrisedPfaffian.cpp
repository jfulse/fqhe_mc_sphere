/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "SymmetrisedPfaffian.h"

//void SymmetrisedPfaffian::initialise(ACA uv_) {
void SymmetrisedPfaffian::initialise(VCD uv_) {
	uv=uv_;
	//cout.precision(10); cout << "init uv = {"; for(int i=0;i<Ne;i++) cout << real(uv[2*i]) << "+" << imag(uv[2*i]) << "*I," << real(uv[2*i+1]) << "+" << imag(uv[2*i+1]) << "*I,"; cout << "}" << endl;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin()+Ne, uv.end()));
	newlogfactor=0.0;
	symmetrise();
	logfactor=log(newlogfactor)+fac; //int I; cin >> I;
	//cout.precision(10); cout << "res " << logfactor << endl;
	//cout << "result: " << logfactor << endl; //int I; cin >> I;
	//cout << "init return uv = "; for(int i=0;i<Ne;i++) cout << "(" << uv[2*i] << "," << uv[2*i+1] << "),"; cout << endl; int I; cin >> I;
}

//void SymmetrisedPfaffian::update(int r, ACA uv_) {
void SymmetrisedPfaffian::update(int r, VCD uv_) {
	//uv[r]=uv_[r];
	//uv[r+1]=uv_[r+1];
	uv=uv_;
	//cout << "upd uv = "; for(int i=0;i<Ne;i++) cout << real(uv[2*i]) << "+" << imag(uv[2*i]) << "*I," << real(uv[2*i+1]) << "+" << imag(uv[2*i+1]) << "*I,"; cout << "}" << endl;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin()+Ne, uv.end()));
	newlogfactor=0.0;
	symmetrise();
	newlogfactor=log(newlogfactor)+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio; //int I; cin >> I;
	//cout << "res " << logfactor << endl;
	//cout << "result: " << logfactor << endl; //int I; cin >> I;
	//cout << "upd return uv = "; for(int i=0;i<Ne;i++) cout << "(" << uv[2*i] << "," << uv[2*i+1] << "),"; cout << endl; int I; cin >> I;
}

//void SymmetrisedPfaffian::update(ACA uv_) {
void SymmetrisedPfaffian::update(VCD uv_) {
	//cout << "initial conf:" << endl;
	//for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << "  ";
	//cout << endl;
	uv=uv_;
	//cout << "upd uv = "; for(int i=0;i<Ne;i++) cout << real(uv[2*i]) << "+" << imag(uv[2*i]) << "*I," << real(uv[2*i+1]) << "+" << imag(uv[2*i+1]) << "*I,"; cout << "}" << endl;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin()+Ne, uv.end()));
	newlogfactor=0.0;
	symmetrise();
	newlogfactor=log(newlogfactor)+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio; //int I; cin >> I;
	//cout << "res " << logfactor << endl;
}

// Generate all combinations subject to having already filled the first n
// of the first group and having already filled the last k of the second.
void SymmetrisedPfaffian::symmetrise(int n, int k) {
    if(n==Nhalf) {
        // Once the first group is full, the rest must be the second group
        evaluate();
        return; 
    }
    // Since the first group isn't full, recursively get all combinations
    // That make the current order[n] part of the first group
    symmetrise(n+1,k);

    if (k<Nhalf) {
      // Next try all combinations that make the current order[n] part of
      // the second group
      //std::swap(order[n], order[N-k-1]);
      swap(uv[2*n], uv[2*(Ne-k-1)]); //cout << "swapping " << 2*n << " and " << 2*(Ne-k-1) << endl;
      swap(uv[2*n+1], uv[2*(Ne-k-1)+1]); //cout << "swapping " << 2*n+1 << " and " << 2*(Ne-k-1)+1 << endl;
      //swap(ordertest[2*n], ordertest[2*(Ne-k-1)]);
      //swap(ordertest[2*n+1], ordertest[2*(Ne-k-1)+1]);
      //cout << "new order: "; for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << ","; cout << endl;
      factors[0]->update(2*n, VCD(uv.begin(), uv.begin()+Ne));
      factors[1]->update(2*(Ne-k-1-Nhalf), VCD(uv.begin()+Ne, uv.end()));
      symmetrise(n,k+1);
      // Since no one cares about the sequence of the items not yet chosen
      // there is no benefit to swapping back.
    }
}

//void SymmetrisedPfaffian::flip(int n) {
//	next_permutation(order[n].begin(), order[n].end());
//	for(int i=0;i<ngroups;i++) {
//		AC tmpu=uv[2*n];
//		AC tmpv=uv[2*n+1];
//		uv[2*n]=uv[2*(n+npergroup*order[n][i])];
//		uv[2*n+1]=uv[2*(n+npergroup*order[n][i])+1];
//		uv[2*(n+npergroup*order[n][i])]=tmpu;
//		uv[2*(n+npergroup*order[n][i])+1]=tmpv;
//		factors[i]->update(2*n, uv);
//	}
//}

void SymmetrisedPfaffian::evaluate() {
	CD result=factors[0]->returnlogfactor();
	//cout << "evaluate with" << endl;
	//for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << "  ";
	//cout << endl;
	result+=factors[1]->returnlogfactor();
	newlogfactor+=exp(result); //int I; cin >> I;
	//cout << "eval order "; for(int i=0;i<2*Ne;i++) cout << ordertest[i] << " "; cout << endl;
	//cout << factors[0]->returnlogfactor() << " " << factors[1]->returnlogfactor() << "  " << exp(result) << endl;
}
