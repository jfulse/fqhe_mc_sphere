/**

 Copyright (C) 2016  Jørgen Fulsebakke

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef LAUGHLIN_QH_H
#define LAUGHLIN_QH_H

#include "Sphere_WF_factor.h"

class Laughlin_qh : public Sphere_WF_factor {

	private:
	double m, d;
	void specific_revert(int, CD, CD) {};
	CD u0, v0; // Position of quasihole

	public:
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);

	Laughlin_qh(int Ne_, VCD uv_, stringstream& status) :
			Sphere_WF_factor(Ne_, uv_) {
		u0=1;
		v0=0;
		status << "Laughlin quasihole at position [u,v] = [" << u0 << "," << v0 << "]" << endl;
	}

	~Laughlin_qh() {}
};

#endif
