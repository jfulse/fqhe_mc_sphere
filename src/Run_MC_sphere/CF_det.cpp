/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_det.h"

CF_det::CF_det(int Ne_, VCD uv_, int n_, int twop_, int num_qp_, string QP_Pos_file, stringstream& status, string mode,int NumConfigs) : 
  Sphere_WF_factor(Ne_, uv_), n(n_), My_CF_Misc(Ne_), num_qp(num_qp_) {

  string fluxstring, namestring;
  do_conj=false;

  if(num_qp!=0 && n>1){
    error("Localized quasi-particles in LLs>1 is not implemented");
      }
  if( num_qp!=0){
    status<<"Making room for "<<num_qp<<" quasi-particle excitation(s) in the GS"<<endl;
    set_qp_pos(QP_Pos_file,NumConfigs);
  }
  if(mode=="JK_pos")	{
    twoq=Ne/n-n-num_qp;
    Nphi=twop_*(Ne-1.0)+twoq;
    fluxstring=to_string(twoq);
    namestring="CF";
  }
  else if(mode=="JK_neg") {
    twoq=Ne/n-n+num_qp;
    Nphi=twop_*(Ne-1.0)-twoq;
    fluxstring=to_string(-twoq);
    namestring="Reverse CF";
    status << "(Implicit Slater determinant to power m=" << twop_ << "; Ne = " << Ne_ << ")" << endl;
  }
  else if(mode=="UP_pos") {
    twoq=Ne/n-n-num_qp;
    Nphi=twop_*(Ne-1.0)+twoq;
    fluxstring=to_string(twoq);
    namestring="Unprojected CF";
  }
  else if(mode=="UP_neg") {
    twoq=Ne/n-n+num_qp;
    Nphi=twop_*(Ne-1.0)-twoq;
    fluxstring=to_string(-twoq);
    namestring="Unprojected reverse CF";
    do_conj=true;
  }

  p=twop_/2.0;
  
  if(Nphi<0) error("Nphi = "+to_string(Nphi)+" < 0; parameters must be wrong");
  status << namestring << " GS at effective flux 2q=" << fluxstring << ",\nwith 2p=" << 2*p << " flux attached; Ne = " << Ne_ << endl;

  My_CF_Misc.initialize(n,twoq,p,mode);
}

void CF_det::initialise(VCD uv_) {
  uv=uv_;
  Eigen::MatrixXcd CFmat=setupCFmat(uv);
  Eigen::PartialPivLU<Eigen::MatrixXcd> LU(CFmat);
  Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
  logfactor=0;
  for(int i=0;i<Ne;i++) logfactor+=log(U(i,i));
  logfactor+=log(CD(LU.permutationP().determinant()));
  imod(logfactor, twopi);
  
  if(do_conj) logfactor=conj(logfactor);
}

void CF_det::update(int r, VCD uv_) {
  uv[r]=uv_[r];
  uv[r+1]=uv_[r+1];
  Eigen::MatrixXcd CFmat=setupCFmat(uv);
  CD newlogfactor=TakeLogDeterminant(CFmat,Ne+num_qp);
  
  if(do_conj) newlogfactor=conj(newlogfactor);
  
  logratio=newlogfactor-logfactor;
  logfactor=newlogfactor;
  
}

void CF_det::update(VCD uv_) {
  uv=uv_;
  Eigen::MatrixXcd CFmat=setupCFmat(uv);
  logfactor=TakeLogDeterminant(CFmat,Ne+num_qp);
  if(do_conj) logfactor=conj(logfactor);
}



void CF_det::set_qp_pos(string filename, int NumConfigs)
{
  ifstream input(filename.c_str());
  input.precision(16);
  if(!input) {
    error("Config file '"+filename+"' not found!");
  }

  if(NumConfigs<0){NumConfigs=1;}
  cout<<"Reading from file "<<filename<<endl;
  
  qp_pos.resize(0);
  qp_uv.resize(0);
  qp_loguv.resize(0);
  string lineData;
  
  int counter=0;
  while(getline(input, lineData))
    {
      double d;
      std::stringstream lineStream(lineData);
      
      while (lineStream >> d) {
	//cout<<"counter:"<<counter<<endl;
	//cout<<"d: "<<d<<endl;
	qp_pos.push_back(d);
	//cout<<"check_number: "<<qp_pos[counter]<<endl;
	counter++;
      }
    }
  if(counter > (2*num_qp*NumConfigs)){
    error("Read in more positions ("+to_string(counter)+") than expected ("+to_string(2*num_qp*NumConfigs)+") for the qp-positions!");}
  else if (counter < (2*num_qp)){
    error("Read in fewer positions ("+to_string(counter)+") than expected ("+to_string(2*num_qp*NumConfigs)+") for the qp-positions. Aborting!");}

  for(int i=0;i<num_qp*NumConfigs;i++) { // the index of the qp:s
    double azimutal=qp_pos[2*i];
    double polar=qp_pos[2*i+1];
    cout<<"(azimut,polar) "<<azimutal<<" , "<<polar<<endl;

    double cp=cos(polar/2.0), sp=sin(polar/2.0);
    qp_uv.push_back(cos(azimutal/2.0)*CD(cp, sp));
    qp_uv.push_back(sin(azimutal/2.0)*CD(cp, -sp));
    cout<<"(u,v) "<<qp_uv[2*i]<<" , "<<qp_uv[2*i+1]<<endl;
    qp_loguv.push_back(log(qp_uv[2*i]));
    qp_loguv.push_back(log(qp_uv[2*i+1]));
    cout<<"(logu,logv) "<<qp_loguv[2*i]<<" , "<<qp_loguv[2*i+1]<<endl;

  }
}
