/**

 Copyright (C) 2018  Mikael Fremling
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Mikael Fremling

**/
#include "CF_qh.h"

void CF_qh::update(VCD uv_) {
  uv=uv_;
  //cout<<"------------UPDATE----------------------"<<endl;
  //Transfer the static variables
  logfactor=0.0;
  for(int qh_no=0;qh_no<num_qh;qh_no++) { // qh_s
    CD u_qh=qh_uv[qh_no*2];
    CD v_qh=qh_uv[qh_no*2+1];
    for(int r=0;r<2*Ne;r+=2) { // electrons
      logfactor+=log(uv[r]*v_qh-u_qh*uv[r+1]);
    }
  }
  //cout<<"logfactor:"<<logfactor<<endl;
}

void CF_qh::initialise(VCD uv_) {
  uv=uv_;
  CF_qh::update(uv);
}


void CF_qh::update(int r, VCD uv_) {
  CD newu=uv_[r];
  CD newv=uv_[r+1];
  logratio=0.0;
  for(int qh_no=0;qh_no<num_qh;qh_no++) { // qh_s
    CD u_qh=qh_uv[qh_no*2];
      CD v_qh=qh_uv[qh_no*2+1];
      logratio+=log(newu*v_qh-u_qh*newv)-log(uv[r]*v_qh-u_qh*uv[r+1]);
  }
  logfactor+=logratio;
  imod(logratio, twopi);
  imod(logfactor, twopi);
  uv[r]=newu;
  uv[r+1]=newv;
}


void CF_qh::updateConfig(int ConfNo) {
  //cout<<"----------------------------------"<<endl;
  //cout<<"Update the config to no = "<<ConfNo<<endl;
  //Transfer the static variables
  logfactor=0.0;
  for(int qh_no=0;qh_no<num_qh;qh_no++) { // qh_s
    CD u_qh=qh_uv[2*ConfNo*num_qh+qh_no*2];
    CD v_qh=qh_uv[2*ConfNo*num_qh+qh_no*2+1];
    for(int r=0;r<2*Ne;r+=2) { // electrons
      logfactor+=log(uv[r]*v_qh-u_qh*uv[r+1]);
    }
  }
  //cout<<"logfactor:"<<logfactor<<endl;
}


void CF_qh::updateSpinor(VCD uv_) {
  uv=uv_; //Update the spinor;
}


void CF_qh::set_qh_pos(string filename, int NumConfigs)
{
  ifstream input(filename.c_str());
  input.precision(16);
  if(!input) {
    error("Config file '"+filename+"' not found!");
  }

  if(NumConfigs<0){NumConfigs=1;}
  cout<<"Reading from file "<<filename<<endl;
  
  qh_pos.resize(0);
  qh_uv.resize(0);
  qh_loguv.resize(0);
  string lineData;
  
  int counter=0;
  while(getline(input, lineData))
    {
      double d;
      std::stringstream lineStream(lineData);
      
      while (lineStream >> d) {
	//cout<<"counter:"<<counter<<endl;
	//cout<<"d: "<<d<<endl;
	qh_pos.push_back(d);
	//cout<<"check_number: "<<qh_pos[counter]<<endl;
	counter++;
      }
    }
  if(counter > (2*num_qh*NumConfigs)){
    error("Read in more positions ("+to_string(counter)+") than expected ("+to_string(2*num_qh*NumConfigs)+") for the qh-positions!");}
  else if (counter < (2*num_qh)){
    error("Read in fewer positions ("+to_string(counter)+") than expected ("+to_string(2*num_qh*NumConfigs)+") for the qh-positions. Aborting!");}

  for(int i=0;i<num_qh*NumConfigs;i++) { // the index of the qh:s
    double azimutal=qh_pos[2*i];
    double polar=qh_pos[2*i+1];
    cout<<"(azimut,polar) "<<azimutal<<" , "<<polar<<endl;

    double cp=cos(polar/2.0), sp=sin(polar/2.0);
    qh_uv.push_back(cos(azimutal/2.0)*CD(cp, sp));
    qh_uv.push_back(sin(azimutal/2.0)*CD(cp, -sp));
    cout<<"(u,v) "<<qh_uv[2*i]<<" , "<<qh_uv[2*i+1]<<endl;
    qh_loguv.push_back(log(qh_uv[2*i]));
    qh_loguv.push_back(log(qh_uv[2*i+1]));
    cout<<"(logu,logv) "<<qh_loguv[2*i]<<" , "<<qh_loguv[2*i+1]<<endl;

  }
}
