/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef CF_SUPERPOSITION_H
#define CF_SUPERPOSITION_H

#include "Sphere_WF_factor.h"
#include "CF_Misc.h"

class CF_superposition : public Sphere_WF_factor {

	private:
	int mode_idx;
	
	void set_mode_idx(string);
	int getmaxn(int);
	void setup(string);
	void getGS();
	void getmaxn();
	void specific_revert(int, CD, CD) {};
	void display_determinant(int, string, string buf="");
	bool occupied(int, int, int);
	Eigen::MatrixXcd* createP(VCD, int);

	protected:
	int Nphi, twoq, K, **nu_mpq, maxn;
	double p;
	VCD logCG;
	virtual Eigen::MatrixXcd setupCFmat(VCD, int*) {error("CF_superposition: wrong setupCFmat(VCD,int*)");}
	virtual Eigen::MatrixXcd setupCFmat(VCD, int*, Eigen::MatrixXcd*) {error("CF_superposition: wrong setupCFmat(VCD, int*,Eigen::MatrixXcd*)");}
	virtual Eigen::MatrixXcd setupCFmat(VCD, Eigen::MatrixXcd, int*) {error("CF_superposition: wrong setupCFmat(VCD, Eigen::MatrixXcd, int*)");}

	public:
	CF_Misc My_CF_Misc;
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	
	CF_superposition(int, VCD, int, int, string, stringstream&, string mode="JK_pos");
	
	~CF_superposition() {
		for(int i=0;i<K;i++) delete [] nu_mpq[i];
		delete [] nu_mpq;
	}
};

#endif
