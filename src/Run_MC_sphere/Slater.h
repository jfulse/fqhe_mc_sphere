/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef SLATER_H
#define SLATER_H

#include "Sphere_WF_factor.h"

class Slater : public Sphere_WF_factor {

	private:
	double m, d;
	void specific_revert(int, CD, CD) {};

	public:	
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	
	Slater(int Ne_, VCD uv_, int m_, double d_, stringstream& status) : 
			Sphere_WF_factor(Ne_, uv_), m(m_), d(d_) {
		if(abs(m)>0) status << "Slater determinant to power m=" << m << "; Ne = " << Ne_ << endl;
		if(abs(d)>1e-12) status << "Absolute Slater determinant to power 2d=" << 2*d << endl;
	}
	
	~Slater() {}
};

#endif
