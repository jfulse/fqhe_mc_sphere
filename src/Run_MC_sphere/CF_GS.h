/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef CF_GS_H
#define CF_GS_H

#include "CF_det.h"

class CF_GS : public CF_det {
  
 private:
  Eigen::MatrixXcd setupCFmat(VCD);
  
 public:

  void updateSpinor(VCD uv_);
  void updateConfig(int ConfNo);
  
 CF_GS(int Ne_, VCD uv_, int n_, int twop_, stringstream& status) : 
  CF_det(Ne_, uv_, n_, twop_, 0, "",status) {}
  
  ~CF_GS() {}
};

#endif
