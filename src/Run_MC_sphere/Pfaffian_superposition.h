/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef PFAFFIAN_SUPERPOSITION_H
#define PFAFFIAN_SUPERPOSITION_H

#include "Sphere_WF_factor.h"
#include "Pfaffian_excitation.h"

class Pfaffian_superposition : public Sphere_WF_factor {

	private:
	int n_terms;
	vector<Sphere_WF_factor*> WF_terms;
	void specific_revert(int, CD, CD) {};
	VCD log_CG_coeffs;

	public:
	void update(VCD);
	void update(int, VCD);
	void initialise(VCD);
	
	Pfaffian_superposition(int Ne_, VCD uv_, VD CG_coeffs, VS cfiles, int Nphi_Pf, stringstream& status) : 
			Sphere_WF_factor(Ne_, uv_) {
		
		if(Ne%2) error("Pfaffian excitations must have even number of particles (requested Ne = "+to_string(Ne)+")");
		if(cfiles.size()%2) error("Pfaffian excitations must have even number of configuration files; two per state");
		n_terms=cfiles.size()/2;
		if(n_terms!=CG_coeffs.size()) error("Must supply the same number of CF coefficients and configuration files");
		status << "Superposition of excited Pfaffians" << endl;
		
		log_CG_coeffs.resize(n_terms);
		for(int k=0;k<n_terms;k++) {
			VS tmp(2);
			tmp[0]=cfiles[2*k];
			tmp[1]=cfiles[2*k+1];
			status << "Pfaffian excitation CG coefficient " << CG_coeffs[k] << endl;
			WF_terms.push_back(new Pfaffian_excitation(Ne, uv_, tmp, Nphi_Pf, status));
			log_CG_coeffs[k]=log(CG_coeffs[k]);
		}
	}
	
	~Pfaffian_superposition() {
		for(int k=0;k<n_terms;k++) delete WF_terms[k];
	}
};

#endif
