/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "UP_CF_GS.h"

Eigen::MatrixXcd UP_CF_GS::setupCFmat(VCD uv) {
  Eigen::MatrixXcd CFmat; 
  CFmat.resize(Ne, Ne);
  for(int i=0;i<2*Ne;i+=2) { // rows; particle i
    int state=0; // state dep. on nu and m
    CD lu=log(uv[i]), lv=log(uv[i+1]);
    for(int nu=0;nu<n;nu++) { // "Internal" LL
      for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
	CFmat(i/2,state)=My_CF_Misc.UP_CF_orbital(mpq,nu,lu,lv);
	state++;
      } // m end (w.r.t. |q|+m
    } // nu end
  } // i end
  return CFmat;
}

