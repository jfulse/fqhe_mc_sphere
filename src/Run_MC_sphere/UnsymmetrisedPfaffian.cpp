/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "UnsymmetrisedPfaffian.h"

void UnsymmetrisedPfaffian::initialise(VCD uv_) {
	uv=uv_; //cout << "init uv = "; for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << endl;cout << endl;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin()+Ne, uv.end()));
	logfactor=factors[0]->returnlogfactor()+factors[1]->returnlogfactor()+fac;
	//cout << "init res: " << logfactor << endl;
}

void UnsymmetrisedPfaffian::update(int r, VCD uv_) {
	uv[r]=uv_[r];
	uv[r+1]=uv_[r+1]; //cout << "upd r uv = "; for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << endl;cout << endl;
	if(r<Ne) factors[0]->update(r, VCD(uv.begin(), uv.begin()+Ne));
	else factors[1]->update(r-Ne, VCD(uv.begin()+Ne, uv.end()));
	CD newlogfactor=factors[0]->returnlogfactor()+factors[1]->returnlogfactor()+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio;
	//cout << "upd r res: " << logfactor << endl;
	//cout << setprecision(6) << "expected res: " << log(pow((uv[0]*uv[3]-uv[1]*uv[2])*(uv[4]*uv[7]-uv[5]*uv[6]), 2)) << endl;
}

void UnsymmetrisedPfaffian::update(VCD uv_) {
	uv=uv_; //cout << "upd uv = "; for(int i=0;i<2*Ne;i+=2) cout << uv[i] << " " << uv[i+1] << endl;cout << endl;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne));
	factors[1]->initialise(VCD(uv.begin()+Ne, uv.end()));
	CD newlogfactor=factors[0]->returnlogfactor()+factors[1]->returnlogfactor()+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio;
	//cout << "upd res: " << logfactor << endl;
}

void UnsymmetrisedPfaffian::specific_revert(int r, CD oldu, CD oldv) {
	factors[r>=Ne]->revert(r-(r>=Ne)*Ne, oldu, oldv);
	//cout << "r = " << r << "; spec revert " << (r>=Ne) << " at " << r-(r>=Ne)*Ne << endl;
}
