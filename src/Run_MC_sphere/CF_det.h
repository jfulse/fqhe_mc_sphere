/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef CF_DET_H
#define CF_DET_H

#include "Sphere_WF_factor.h"
#include "CF_Misc.h"

class CF_det : public Sphere_WF_factor {
  
 private:
  bool do_conj;
    
 protected:
  int Nphi, n, twoq,num_qp;
  double p;
  VD logfactlist;
  vector<VD> lognorm;
  VD qp_pos;
  VCD qp_uv;
  VCD qp_loguv;

  void specific_revert(int, CD, CD) {};
  void getfacts();
  void set_qp_pos(string,int);
  inline double logbin(int a, int b) {return logfactlist[a]-logfactlist[b]-logfactlist[a-b];}
  
  virtual Eigen::MatrixXcd setupCFmat(VCD)=0;
  virtual void getnorm() {error("CF_det: getnorm() not defined");}

 public:

  CF_Misc My_CF_Misc;
  
  void update(VCD);
  void update(int, VCD);
  void initialise(VCD);
  
  CF_det(int, VCD, int, int, int, string, stringstream&, string mode="JK_pos",int NumConfigs=1);
  
  ~CF_det() {}
};

#endif
