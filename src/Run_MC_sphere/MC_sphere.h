/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef MC_SPHERE_H
#define MC_SPHERE_H

#include "misc_sphere.h"
#include "sfmt.h"
#include "Sphere_wavefunction.h"
#include "HDF5Wrapper.h"

class MC_sphere {

	private:
	string wf;
	int Ne, Nphi;
	double *tp, step, R;
	bool verb;
	VCD spinor;
	CRandomSFMT rangen;
	Sphere_wavefunction *probability;
	vector <HDF5Wrapper*> hdf5readwrite;
	
	inline void newtp(double theta, double addt, double phi, double addp, double& tnew, double& pnew, CD& unew, CD& vnew) {
		tnew=theta+addt;
		if(tnew<0) {tnew=-tnew; phi=fmod(phi+pi,twopi); }
		else if(tnew>pi) {tnew=twopi-tnew; phi=fmod(phi+pi,twopi); }
		pnew=fmod(phi+addp, twopi);
		if(pnew<0) pnew+=twopi;
		double cp=cos(pnew/2.0), sp=sin(pnew/2.0);
		unew=cos(tnew/2.0)*CD(cp, sp);
		vnew=sin(tnew/2.0)*CD(cp, -sp);
		return;
	}

	inline void clear_pointer_vector(vector<HDF5Wrapper*>& vec) {
		int n=vec.size();
		for(int i=0;i<n;i++) delete vec[i];
	}

	void ran_el();
	void place_el(VD);
	void MC_iteration(Sphere_wavefunction*&, int&);
	
	public:
	void Laughlin_therm(int);
	void stabilize_step(double, int, int, bool verb=false);
	void thermalise(int, string thermfile="");
	void SampleProb(int, long long, string, string, string);
	void SampleLaughlin(int, long long, string, string, string, int);
	void ComputeOnExisting(long long, string, string, long long skip=0);
	void ComputeOnExistingManyConfigs(long long, string, string, long long skip=0, int NumConfigs=1);
	
	MC_sphere(input*, int&, stringstream&, bool nostab=false);

	~MC_sphere() {
		clear_pointer_vector(hdf5readwrite);
		delete [] tp;
	}

};

#endif
