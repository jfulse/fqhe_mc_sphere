###########################################################################
#
# Copyright (C) 2016  Jørgen Fulsebakke
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.
#
# Author: Jørgen Fulsebakke
#
###########################################################################

#### Directories and flags

ifndef $(DIR_MAIN)
  DIR_MAIN=../..
endif

DIR_EXE=$(DIR_MAIN)/Programs
DIR_SRC=$(DIR_MAIN)/src/Run_MC_sphere
DIR_MISC_SRC=$(DIR_MAIN)/src/Misc
DIR_SFMT_SRC=$(DIR_MAIN)/src/sfmt
DIR_MISC_BLD=$(DIR_MAIN)/build/Misc
DIR_BLD=$(DIR_MAIN)/build/Run_MC_sphere
EIGEN_DIR=$(DIR_SRC)/../eigen/eigen-eigen-b30b87236a1b/
COMP=g++
COMP=h5c++

COMPILE_FLAGS= -std=c++0x -O3 -lstdc++ -msse2 -I${DIR_MISC_SRC} -I${DIR_SFMT_SRC} -I$(EIGEN_DIR)
LINK_FLAGS= -O3 -lgsl -lgslcblas -lm -lhdf5_cpp -lhdf5



ifeq ($(cmp),ichec)
  COMP=icpc
  ###matches the hdf5/intel/1.10.1 module
  HDF5_DIR=/ichec/packages/hdf5/intel/1.10.1
  COMPILE_FLAGS+=-I${HDF5_DIR}/include # -diag-disable 858
  LINK_FLAGS+=-L${HDF5_DIR}/lib
endif

#### Objects

MAIN_CPP_OBJECTS:=$(wildcard *.cpp)
MAIN_OBJECTS:=$(patsubst %.cpp,%.o,$(MAIN_CPP_OBJECTS))

#### Compile all

all: setup $(DIR_EXE)/Run_MC_sphere .Cdependencies

setup:
	@mkdir -p $(DIR_BLD)
	@mkdir -p $(DIR_EXE)

##### Run MC sphere source

include .Cdependencies

.Cdependencies:mk.dep.c.sh
	./mk.dep.c.sh > .Cdependencies



$(DIR_EXE)/Run_MC_sphere: $(DIR_MISC_BLD)/sfmt.o $(DIR_MISC_BLD)/userintf.o $(DIR_MISC_BLD)/input.o $(DIR_MISC_BLD)/main_misc.o  $(DIR_MISC_BLD)/misc_sphere.o $(DIR_MISC_BLD)/misc.o $(DIR_MISC_BLD)/HDF5Wrapper.o $(addprefix $(DIR_BLD)/, $(MAIN_OBJECTS))
	${COMP} $(DIR_MISC_BLD)/sfmt.o $(DIR_MISC_BLD)/HDF5Wrapper.o $(DIR_MISC_BLD)/input.o $(DIR_MISC_BLD)/main_misc.o $(DIR_MISC_BLD)/misc_sphere.o $(DIR_MISC_BLD)/userintf.o $(DIR_MISC_BLD)/misc.o $(addprefix $(DIR_BLD)/, $(MAIN_OBJECTS)) ${LINK_FLAGS} -o $@


#### Compile wavefunction objects
$(DIR_BLD)/%.o: %.cpp %.h $(DIR_SFMT_SRC)/randomc.h $(DIR_SFMT_SRC)/sfmt.h $(DIR_MISC_SRC)/misc.h $(DIR_MISC_SRC)/misc_sphere.h $(DIR_MISC_SRC)/HDF5Wrapper.h $(DIR_MISC_SRC)/input.h $(DIR_MISC_SRC)/main_misc.h
	${COMP} -c $< ${COMPILE_FLAGS} -o $@ 

#### Clean

clean:
	@rm -f $(DIR_BLD)/*.o $(Run_MC_sphere)/Analyse_MC
	@echo "Object files and executables removed"
