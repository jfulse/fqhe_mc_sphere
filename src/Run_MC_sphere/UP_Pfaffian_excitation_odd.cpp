/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "UP_Pfaffian_excitation_odd.h"

void UP_Pfaffian_excitation_odd::initialise(VCD uv_) { //cout << "UP_Pfaffian_excitation_odd pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
	uv=uv_;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne-1));
	factors[1]->initialise(VCD(uv.begin(), uv.begin()+Ne-1));
	factors[2]->initialise(VCD(uv.begin()+Ne-1, uv.end()));
	factors[3]->initialise(VCD(uv.begin()+Ne-1, uv.end()));
	newlogfactor=0.0; //cout << "run" << endl; ordertest=ordertest_init;
	symmetrise();
	logfactor=log(newlogfactor)+fac; //int I; cin >> I;
}

void UP_Pfaffian_excitation_odd::update(int r, VCD uv_) { //cout << "UP_Pfaffian_excitation_odd pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
	uv=uv_;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne-1));
	factors[1]->initialise(VCD(uv.begin(), uv.begin()+Ne-1));
	factors[2]->initialise(VCD(uv.begin()+Ne-1, uv.end()));
	factors[3]->initialise(VCD(uv.begin()+Ne-1, uv.end()));
	newlogfactor=0.0; //cout << "run" << endl; ordertest=ordertest_init;
	symmetrise();
	newlogfactor=log(newlogfactor)+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio; //int I; cin >> I;
}

void UP_Pfaffian_excitation_odd::update(VCD uv_) { //cout << "UP_Pfaffian_excitation_odd pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
	uv=uv_;
	factors[0]->initialise(VCD(uv.begin(), uv.begin()+Ne-1));
	factors[1]->initialise(VCD(uv.begin(), uv.begin()+Ne-1));
	factors[2]->initialise(VCD(uv.begin()+Ne-1, uv.end()));
	factors[3]->initialise(VCD(uv.begin()+Ne-1, uv.end()));
	newlogfactor=0.0; //cout << "run" << endl; ordertest=ordertest_init;
	symmetrise();
	newlogfactor=log(newlogfactor)+fac;
	logratio=newlogfactor-logfactor;
	logfactor+=logratio; //int I; cin >> I;
}

void UP_Pfaffian_excitation_odd::symmetrise(int n, int k) {
    if(n==Nfirst) {
        evaluate();
        return; 
    }
    
    symmetrise(n+1,k);

    if (k<=Nfirst) {
      //swap(ordertest[n], ordertest[Ne-k-1]);
      swap(uv[2*n], uv[2*(Ne-k-1)]);
      swap(uv[2*n+1], uv[2*(Ne-k-1)+1]);
      factors[0]->update(2*n, VCD(uv.begin(), uv.begin()+Ne-1));
      factors[1]->update(2*n, VCD(uv.begin(), uv.begin()+Ne-1));
      factors[2]->update(2*(Ne-k-1-Nfirst), VCD(uv.begin()+Ne-1, uv.end()));
      factors[3]->update(2*(Ne-k-1-Nfirst), VCD(uv.begin()+Ne-1, uv.end()));
      symmetrise(n,k+1);
    }
}

void UP_Pfaffian_excitation_odd::evaluate() {
  //cout << "order: "; for(int i=0;i<ordertest.size();i++) cout << ordertest[i] << " "; cout << endl;
	CD result=0.0;
	for(int i=0;i<4;i++) result+=factors[i]->returnlogfactor();
	newlogfactor+=exp(result);
}

void UP_Pfaffian_excitation_odd::order_cfiles(VS cfiles) {
  ifstream in1(cfiles[0]);
  if(!in1) error("Could not open "+cfiles[0]+" for reading");
  ifstream in2(cfiles[1]);
  if(!in2) error("Could not open "+cfiles[1]+" for reading");
  string line1, line2;
  getline(in1, line1);
  getline(in2, line2);
  istringstream iss1(line1), iss2(line2);
  int ctr1=0, ctr2=0;
  while(getline(iss1, line1, ' ')) {
    if(line1!=" " && line1 !="" && line1!="  " && line1 !="   " && line1!="    " && line1 !="     ") ctr1++;
  }
  while(getline(iss2, line2, ' ')) {
    if(line2!=" " && line2 !="" && line2!="  " && line2 !="   " && line2!="    " && line2 !="     ") ctr2++;
  }
  if(!(ctr1%2) || !(ctr2%2)) error("One of the input configuration files has an even number of entries on line 1");
  int Ne1=(ctr1-1)/2, Ne2=(ctr2-1)/2;
  
  if(Ne1!=Nfirst) {
    if(Ne2!=Nfirst) error("Neither of the configuration files correspond to "+to_string(Nfirst)+" particles");
    if(Ne1!=Nfirst+1) error("The first configuration file does not correspond to "+to_string(Nfirst+1)+" particles");
    else {
      string dum=cfiles[0];
      cfiles[0]=cfiles[1];
      cfiles[1]=dum;
    }
  }
  else if(Ne2!=Nfirst+1) error("Neither of the configuration files correspond to "+to_string(Nfirst+1)+" particles");
  
  in1.close();
  in2.close();
}
