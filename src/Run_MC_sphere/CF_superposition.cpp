/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_superposition.h"

CF_superposition::CF_superposition(int Ne_, VCD uv_, int twop_, int twoq_, string cfile, stringstream& status, string mode) : 
  Sphere_WF_factor(Ne_, uv_), twoq(twoq_), My_CF_Misc(Ne_)
{
  setup(cfile);
  set_mode_idx(mode);
  string fluxstring, namestring;
  if(mode_idx==0)	{
    Nphi=twop_*(Ne-1.0)+twoq;
    fluxstring=to_string(twoq);
    namestring="CF";
  }
  else if(mode_idx==1) {
    Nphi=twop_*(Ne-1.0)-twoq;
    fluxstring=to_string(-twoq);
    namestring="Reverse CF";
    cout << "Slater determinant to power m=" << twop_ << "; Ne = " << Ne << endl;
  }
  else if(mode_idx==2) {
    Nphi=twop_*(Ne-1.0)+twoq;
    fluxstring=to_string(twoq);
    namestring="Unprojected CF";
  }
  else if(mode_idx==3) {
    Nphi=twop_*(Ne-1.0)-twoq;
    fluxstring=to_string(-twoq);
    namestring="Unprojected reverse CF";
  }
  
  p=twop_/2.0;
  if(Nphi<0) error("Nphi = "+to_string(Nphi)+" < 0; parameters must be wrong");
  cout << namestring << " excitation at effective flux 2q=" << fluxstring << "; Ne = " << Ne_ << ",\nwith 2p=" << 2*p << " flux attached" << endl;
  cout << "Composition of CF determinants and their CG coefficients:" << endl;
  
  string sep=" ";
  if((maxn==1 && Ne>30) || (maxn>1 && Ne/(maxn-1)>30)) sep="";
  for(int j=0;j<K;j++) {
    cout << "  Determinant " << j+1 << " of " << K << ": CG = " << CDstring(exp(logCG[j])) << endl;
    display_determinant(j, sep, "  ");
  }
  My_CF_Misc.initialize(maxn,twoq,p,mode);  
}

void CF_superposition::initialise(VCD uv_) { //cout << "CF_superposition pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
  cout << "-------------Initialize the MC----------------"<< endl;
  
  uv=uv_;
  Eigen::MatrixXcd* P;
  Eigen::MatrixXcd logesp;
  Eigen::MatrixXcd CFmat0;
  
  if(mode_idx==0) {
    P=CF_superposition::createP(uv, maxn);
    CFmat0=setupCFmat(uv, nu_mpq[0], P);
  }
  else if(mode_idx==1) {
    logesp=My_CF_Misc.getlogesp(uv);
    CFmat0=setupCFmat(uv, logesp, nu_mpq[0]);
  }
  else CFmat0=setupCFmat(uv, nu_mpq[0]);
  
  Eigen::PartialPivLU<Eigen::MatrixXcd> LU0(CFmat0);
  Eigen::MatrixXcd U0=LU0.matrixLU().triangularView<Eigen::Upper>();
  CD testlogdet0=0;
  for(int i=0;i<Ne;i++) testlogdet0+=log(U0(i,i));
  testlogdet0+=log(CD(LU0.permutationP().determinant()));
  CD logCGsum=testlogdet0+logCG[0];
  for(int i=1;i<K;i++) {
    Eigen::MatrixXcd CFmat;
    if(mode_idx==0) CFmat=setupCFmat(uv, nu_mpq[i], P);
    else if(mode_idx==1) CFmat=setupCFmat(uv, logesp, nu_mpq[i]);
    else CFmat=setupCFmat(uv, nu_mpq[i]);
    Eigen::PartialPivLU<Eigen::MatrixXcd> LU(CFmat);
    Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
    CD testlogdet=0;
    for(int j=0;j<Ne;j++) testlogdet+=log(U(j,j));
    testlogdet+=log(CD(LU.permutationP().determinant()));
    logCGsum+=log(1.0+exp(testlogdet+logCG[i]-logCGsum));
  }
  
  logfactor=logCGsum;
  imod(logfactor, twopi);
  
  if(mode_idx==3) logfactor=conj(logfactor);	
  if(mode_idx==0) delete [] P;
}

void CF_superposition::update(int r, VCD uv_) { //cout << "CF_superposition pos "; for(int i=0;i<uv_.size();i++) cout << uv_[i] << " "; cout << endl;
  cout << "-------------Update the one MC coord----------------"<< endl;

  
  uv[r]=uv_[r];
  uv[r+1]=uv_[r+1];
  Eigen::MatrixXcd* P;
  Eigen::MatrixXcd logesp;
  Eigen::MatrixXcd CFmat0;
  
  if(mode_idx==0) {
    P=CF_superposition::createP(uv, maxn);
    CFmat0=setupCFmat(uv, nu_mpq[0], P);
  }
  else if(mode_idx==1) {
    logesp=My_CF_Misc.getlogesp(uv);
    CFmat0=setupCFmat(uv, logesp, nu_mpq[0]);
  }
  else CFmat0=setupCFmat(uv, nu_mpq[0]);
  
  Eigen::PartialPivLU<Eigen::MatrixXcd> LU0(CFmat0);
  Eigen::MatrixXcd U0=LU0.matrixLU().triangularView<Eigen::Upper>();
  CD testlogdet0=0;
  for(int i=0;i<Ne;i++) testlogdet0+=log(U0(i,i));
  testlogdet0+=log(CD(LU0.permutationP().determinant()));
  CD logCGsum=testlogdet0+logCG[0];
  for(int i=1;i<K;i++) {
    Eigen::MatrixXcd CFmat;
    if(mode_idx==0) CFmat=setupCFmat(uv,nu_mpq[i], P);
    else if(mode_idx==1) CFmat=setupCFmat(uv, logesp, nu_mpq[i]);
    else CFmat=setupCFmat(uv, nu_mpq[i]);
    Eigen::PartialPivLU<Eigen::MatrixXcd> LU(CFmat);
    Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
    CD testlogdet=0;
    for(int j=0;j<Ne;j++) testlogdet+=log(U(j,j));
    testlogdet+=log(CD(LU.permutationP().determinant()));
    logCGsum+=log(1.0+exp(testlogdet+logCG[i]-logCGsum));
  }
  
  CD newlogfactor=logCGsum;
  imod(newlogfactor, twopi);
  if(mode_idx==3) logfactor=conj(logfactor);
  
  logratio=newlogfactor-logfactor;
  logfactor=newlogfactor;
  
  imod(logfactor, twopi);
  
  if(mode_idx==0) delete [] P;
}

void CF_superposition::update(VCD uv_) {
  //cout<<endl;
  //cout << "CF_superposition pos "<<endl;
  //cout.precision(17);
  //for(int i=0;i<uv_.size();i++) cout << uv_[i] <<endl;
  //cout << "-------------Update the MC----------------"<< endl;
  uv=uv_;
  Eigen::MatrixXcd* P;
  Eigen::MatrixXcd logesp;
  Eigen::MatrixXcd CFmat0;
  if(mode_idx==0) {
    P=CF_superposition::createP(uv, maxn);
    CFmat0=setupCFmat(uv, nu_mpq[0], P);
  }
  else if(mode_idx==1) {
    logesp=My_CF_Misc.getlogesp(uv);
    CFmat0=setupCFmat(uv, logesp, nu_mpq[0]);
  }
  else {
    //cout << "-------------Setup CF mat----------------"<< endl;
    CFmat0=setupCFmat(uv, nu_mpq[0]);
    //cout << "-------------Done----------------"<< endl;
  }
  
  Eigen::PartialPivLU<Eigen::MatrixXcd> LU0(CFmat0);
  Eigen::MatrixXcd U0=LU0.matrixLU().triangularView<Eigen::Upper>();
  CD testlogdet0=0;
  for(int i=0;i<Ne;i++) testlogdet0+=log(U0(i,i));
  testlogdet0+=log(CD(LU0.permutationP().determinant()));
  CD logCGsum=testlogdet0+logCG[0];
  //cout<<"logCGsum:"<<logCGsum<<endl;
  for(int i=1;i<K;i++) {
    Eigen::MatrixXcd CFmat;
    if(mode_idx==0) CFmat=setupCFmat(uv, nu_mpq[i], P);
    else if(mode_idx==1) CFmat=setupCFmat(uv, logesp, nu_mpq[i]);
    else CFmat=setupCFmat(uv, nu_mpq[i]);
    Eigen::PartialPivLU<Eigen::MatrixXcd> LU(CFmat);
    Eigen::MatrixXcd U=LU.matrixLU().triangularView<Eigen::Upper>();
    CD testlogdet=0;
    for(int j=0;j<Ne;j++) testlogdet+=log(U(j,j));
    testlogdet+=log(CD(LU.permutationP().determinant()));
    logCGsum+=log(1.0+exp(testlogdet+logCG[i]-logCGsum));
  }
  
  logfactor=logCGsum;
  imod(logfactor, twopi);
  //cout<<"logfactor:"<<logfactor<<endl;
  if(mode_idx==3) logfactor=conj(logfactor);	
  if(mode_idx==0) delete [] P;
}

void CF_superposition::setup(string cfile) {
  ifstream in(cfile.c_str()); in.precision(16);
  if(!in) {
    warning("Config file '"+cfile+"' not found; using elementary state (single determinant of lowest energy and total L)");
    getGS();
    return;
  }
  K=0; string dum;
  while(getline(in, dum)) {
    K++;
    istringstream iss(dum);
    int ctr=0;
    while(getline(iss, dum, ' ')) {
      if(dum!=" " && dum !="" && dum!="  " && dum !="   " && dum!="    " && dum !="     ") ctr++;
    }
    if(!(ctr%2)) error("Line "+to_string(K)+" in "+cfile+" has an even number of entries");
    if(Ne==0) Ne=(ctr-1)/2;
    else if(Ne!=(ctr-1)/2) error("Line "+to_string(K)+" in "+cfile+" does not correspond to "+to_string(Ne)+" particles");
  }
  in.clear();
  in.seekg(0, ios::beg);
  nu_mpq=new int*[K];
  for(int i=0;i<K;i++) {
    int lowest=0;
    nu_mpq[i]=new int[2*Ne];
    in >> dum;
    if(!isnumber(dum)) error("CG coefficient in config file");
    logCG.push_back(log(CD(atof(dum.c_str()))));
    for(int j=0;j<2*Ne;j+=2) {
      if(!in) error("Number of entries in config file");
      in >> dum;
      if(!isnumber(dum)) error("CF LL index in config file not number");
      if(!isint(dum.c_str()) || atof(dum.c_str())<0) error("CF LL index in config file not positive integer");
      nu_mpq[i][j]=atoi(dum.c_str());
      if(nu_mpq[i][j]==0) lowest++;
      if(j>0 && nu_mpq[i][j]<nu_mpq[i][j-2]) warning("CF determinant not canonically ordered");
      if(!in) error("Number of entries in config file");
      in >> dum;
      if(!isnumber(dum)) error("m index in config file not number");
      nu_mpq[i][j+1]=int(atof(dum.c_str())+twoq/2.0);
    }
    if(lowest>twoq+1) warning("More than "+to_string(twoq+1)+" CF's in lowest Landau level");
  }
  if(in >> dum) warning("Number of entries in config file "+cfile+" does not correspond to Ne = "+to_string(Ne));
  
  getmaxn();
}

void CF_superposition::getmaxn() {
  maxn=nu_mpq[0][0];
  for(int i=0;i<K;i++) {
    for(int j=0;j<2*Ne;j+=2) {
      if(nu_mpq[i][j]>maxn) maxn=nu_mpq[i][j];
    }
  }
  maxn++;
}

int CF_superposition::getmaxn(int k) {
  int maxn_k=nu_mpq[k][0];
  for(int j=0;j<2*Ne;j+=2) {
    if(nu_mpq[k][j]>maxn_k) maxn_k=nu_mpq[k][j];
  }
  return maxn_k;
}

void CF_superposition::getGS() {
  K=1; logCG.push_back(0.0); nu_mpq=new int*[1]; nu_mpq[0]=new int[2*Ne];
  int Ne_placed=0, nu=0;
  while(Ne_placed<Ne) {
    int mpq=-nu;
    while(Ne_placed<Ne && mpq<=twoq+nu) {
      nu_mpq[0][Ne_placed*2]=nu;
      nu_mpq[0][Ne_placed*2+1]=mpq;
      Ne_placed++;
      mpq++;
    }
    nu++;
  }
  getmaxn();
}

bool CF_superposition::occupied(int k, int nu, int mpq) {
  for(int i=0;i<2*Ne;i+=2) {
    if(nu_mpq[k][i]==nu && nu_mpq[k][i+1]==mpq) return true;
  }
  return false;
}

void CF_superposition::display_determinant(int k, string sep, string buf) {
  string empt="_", occ="x";
  int max_LL=getmaxn(k);
  for(int nu=max_LL;nu>=0;nu--) {
    cout << buf;
    for(int i=0;i<max_LL-nu;i++) cout << " " << sep;
    for(int mpq=-nu;mpq<=nu+twoq;mpq++) {
      if(occupied(k, nu, mpq)) cout << occ << sep;
      else cout << empt << sep;
    }
    cout << endl;
  }	
}

void CF_superposition::set_mode_idx(string mode) {
  if(mode=="JK_pos") mode_idx=0;
  else if(mode=="JK_neg") mode_idx=1;
  else if(mode=="UP_pos") mode_idx=2;
  else if(mode=="UP_neg") mode_idx=3;
  else error("CF determinant mode "+mode+" unknown");
}

Eigen::MatrixXcd* CF_superposition::createP(VCD uv, int ntemp) {
  Eigen::MatrixXcd* P=new Eigen::MatrixXcd[Ne];
  for(int j=0;j<Ne;j++) {
    P[j]=My_CF_Misc.createP(j,uv);
  }
  return P;
}
