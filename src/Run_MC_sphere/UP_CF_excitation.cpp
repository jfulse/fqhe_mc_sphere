/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "UP_CF_excitation.h"

Eigen::MatrixXcd UP_CF_excitation::setupCFmat(VCD uv, int* nu_mpq) {
  Eigen::MatrixXcd CFmat; 
  CFmat.resize(Ne, Ne);
  for(int i=0;i<Ne;i++) { // rows; particle i
    CD lu=log(uv[2*i]), lv=log(uv[2*i+1]);
    for(int j=0;j<Ne;j++) { // columns; state j
      int nu=nu_mpq[2*j];
      int mpq=nu_mpq[2*j+1];
      CFmat(i,j)=My_CF_Misc.UP_CF_orbital(mpq,nu,lu,lv);
    } // j end
  } // i end
  return CFmat;
}
//cout<<" inner loop"<<endl;
