/**
   
   Copyright (C) 2016  Jørgen Fulsebakke
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   
   Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.
   
   Author: Jørgen Fulsebakke
   
**/
#include "Choose_Wavefunction.h"

void check_CF_GS(int Ne, int n, int twop) {
  if(n<=0) error("Number of occupied CF LL's n = "+to_string(n)+"; must be positive. Use flag '-R' to get reverse flux wavefunction");
  if (n==1) {
    if(twop<0) error("Number of attached fluxes 2p = "+to_string(twop)+"; must be non-negative");}
  else {
    if(twop<1) error("Number of attached fluxes 2p = "+to_string(twop)+"; must be positive - or state will project to zero");
  }
  if(abs(Ne/n-(double)Ne/n)>1e-10 || Ne/n-n<0) error("Cannot have Ne = "+to_string(Ne)+" electrons and n = "+to_string(n)+" filled CF LL's");
}

void check_CF_super(int Ne, int twoq, int twop) {
  if(twoq<0) error("Effective flux 2q = "+to_string(twoq)+"; must be positive. Use flag '-R' to get reverse flux wavefunction");
  if(twop<0) error("Number of attached fluxes 2p = "+to_string(twop)+"; must be positive");
}

int get_wf_id(input* params) {
  int id;
  string wfname=params->getstring("-wf");
  if(wfname=="L" || wfname=="LAWF" || wfname=="Laughlin" || wfname=="l" || wfname=="laughlin" || wfname=="LAUGHLIN") {
    id=1;
    int m=params->getint("-m");
    if(m<0) error("Slater determinant power set to m = "+to_string(m)+"; must be positive");
  }
  else if(wfname=="CF" || wfname=="CFWF" || wfname=="Jain" || wfname=="cf" || wfname=="Jain_wf" || wfname=="CompositeFermions") {
    int twop=params->getint("--CF:2p");
    int Ne=params->getint("-Ne");

    if(params->getint("--CF:qp_num")>0){
      id=48;}
    else if(params->getint("--CF:qh_num")>0){
      id=49;}
    else if(!params->is_activated("-c")) {
      int n=params->getint("-n");
      check_CF_GS(Ne, n, twop);
      
      if(params->is_flagged("-UP")) {
	if(params->is_flagged("-R")) {
	  params->is_activated("-R");
	  id=12;
	}
	else{
	  id=13;
	}
      }
      else {
	if(params->is_flagged("-R")) {
	  params->is_activated("-R");
	  if(twop!=2 && twop!=4) error("Reverse CF ground state not set up for 2p = "+to_string(twop));
	  if(twop==2) id=2;
	  if(twop==4) id=24;
	}
	else {
	  id=9;
	}
      }
    }
    else {
      int twoq=params->getint("-2q");
      check_CF_super(Ne, twoq, twop);
      
      if(params->is_flagged("-UP")) {
	if(params->is_flagged("-R")) {
	  params->is_activated("-R");
	  id=5;
	}
	else {
	  id=6;
	}
      }
      else {
	if(params->is_flagged("-R")) {
	  params->is_activated("-R");
	  if(twop!=2 && twop!=4) error("Reverse flux composite fermion not set up for 2p = "+to_string(twop));
	  if(twop==2) id=4;
	  if(twop==4) id=25;
	}
	else {
	  if(twop<1) error("Composite fermion not defined for 2p = "+to_string(twop)+"; Please use -UP flag do define it before projection");
	  id=19;
	}
      }
    }
  }
  else if(wfname=="MR" || wfname=="mr" || wfname=="MooreRead" || wfname=="Moore_Read" || wfname=="mooreread" || wfname=="moore_read") {
    int MRp=params->getint("--MR:p");
    int Ne=params->getint("-Ne");
    if(MRp<0) error("Slater determinant power set to MRp = "+to_string(MRp)+"; must be positive");
    
    if(params->is_activated("-Pfqh")) {
      if(!params->is_flagged("--MR:no-phase")) warning("MR Pfaffian with localised quasiholes is only implemented with\nrandom phase (use --MR:no-phase to choose this explicitly)");
      if(params->is_flagged("-U")) error("Unprojected MR Pfaffian with localised quasiholes not set up");
      if(params->is_activated("-cPf")) error("Unprojected MR Pfaffian with both localised and momentum quasiholes not set up");
      id=47;
    }
    else if(params->is_flagged("-U")) {
      if(params->is_activated("--MR:d1") || params->is_activated("--MR:d2")) error("Unsymmetrised modified MR not set up");
      id=3;
    }
    else {
      if(params->is_activated("-cPf")) {
	if(params->is_activated("--MR:d1") || params->is_activated("--MR:d2")) error("Excited modified MR not set up");
	if(params->is_flagged("--MR:no-phase")) error("MR Pfaffian excitations with random phase not set up");
	if(params->is_flagged("-UP")) error("Unprojected MR Pfaffian excitations not set up");
	if(!(Ne%2)) id=21;
	else id=36;
      }
      else if(params->is_flagged("--MR:no-phase")) {
	if(params->is_activated("--MR:d1") || params->is_activated("--MR:d2")) error("Modified MR with random phase not set up");
	id=30;
      }
      else if(params->is_activated("--MR:d1") || params->is_activated("--MR:d2")) {
	if(!params->is_activated("--MR:d1")) params->set("--MR:d1","0");
	if(!params->is_activated("--MR:d2")) params->set("--MR:d2","0");
	id=31;
      }
      else id=7;
    }
  }
  else if(wfname=="BS" || wfname=="bs" || wfname=="BondersonSlingerland" || wfname=="BSWF" || wfname=="BS_wf" || wfname=="Bonderson_Slingerland") {
    int twop=params->getint("--CF:2p");
    int Ne=params->getint("-Ne");
    int BSp=params->getint("--BS:p");
    if(BSp<0) error("Slater determinant power set to BSp = "+to_string(BSp)+"; must be positive");
    if(params->is_flagged("--MR:no-phase")) error("BS state with random phase in Pfaffian not set up");
    
    if(!params->is_activated("-c")) {
      int n=params->getint("-n");
      check_CF_GS(Ne, n, twop);
      
      if(params->is_activated("--MR:conf-file-sup")) {
	if(!params->is_activated("--MR:Nphi")) {
	  int Ne=params->getint("-Ne");
	  params->set("--MR:Nphi",to_string(Ne-2));
	}
	if(params->is_flagged("-UP")) error("Unprojected superposition of Pfaffian excitations not set up");
	if(params->is_flagged("-R")) {
	  if(twop!=2) error("Bonderson-Slingerland only set up for CF flux attachment 2p=2");
	  params->is_activated("-R");
	  id=29;
	}
	else {
	  id=28;
	}
	
      }
      else if(params->is_activated("-cPf")) {
	if(!params->is_activated("--MR:Nphi")) {
	  int Ne=params->getint("-Ne");
	  params->set("--MR:Nphi",to_string(Ne-2));
	}
	if(params->is_flagged("-UP")) {
	  if(params->is_flagged("-R")) {
	    params->is_activated("-R");
	    if(!(Ne%2)) id=27;
	    else id=32;
	  }
	  else {
	    if(!(Ne%2)) id=26;
	    else id=33;
	  }
	}
	else {
	  if(params->is_flagged("-R")) {
	    if(twop!=2) error("Bonderson-Slingerland only set up for CF flux attachment 2p=2");
	    params->is_activated("-R");
	    if(!(Ne%2)) id=23;
	    else id=34;
	  }
	  else {
	    if(!(Ne%2)) id=22;
	    else id=35;
	  }
	}
      }
      else {
	if(params->is_flagged("-UP")) {
	  if(params->is_flagged("-R")) {
	    params->is_activated("-R");
	    id=14;
	  }
	  else  {
	    id=15;
	  }
	}
	else {
	  if(params->is_flagged("-R")) {
	    if(twop!=2) error("Bonderson-Slingerland only set up for CF flux attachment 2p=2");
	    params->is_activated("-R");
	    id=8;
	  }
	  else  {
	    id=10;
	  }
	}
      }
    }
    else {
      int twoq=params->getint("-2q");
      check_CF_super(Ne, twoq, twop);
      // if(params->is_activated("-cPf")) error("Bonderson-Slingerland with excitations in both CF and Pfaffian sector not set up");
      
      if(params->is_activated("-cPf")) {
	if(!params->is_activated("--MR:Nphi")) {
	  int Ne=params->getint("-Ne");
	  params->set("--MR:Nphi",to_string(Ne-2));
	}
	if(params->is_flagged("-UP")) {
	  if(params->is_flagged("-R")) {
	    params->is_activated("-R");
	    if(!(Ne%2)) id=37;
	    else id=38;
	  }
	  else {
	    if(!(Ne%2)) id=39;
	    else id=40;
	  }
	}
	else {
	  if(params->is_flagged("-R")) {
	    params->is_activated("-R");
	    if(twop==2) {
              if(!(Ne%2)) id=41;
              else id=42;
            }
            else if(twop==4) {
              if(!(Ne%2)) id=43;
              else id=44;
            }
            else error("Reverse Bonderson-Slingerland excitation only set up for CF flux attachment 2p=2 and 4");
	  }
	  else {
	    if(!(Ne%2)) id=45;
	    else id=46;
	  }
	}
      }
      else {
        if(params->is_flagged("-UP")) {
          if(params->is_flagged("-R")) {
            params->is_activated("-R");
            id=16;
          }
          else {
            id=17;
          }
        }
        else {
          if(params->is_flagged("-R")) {
            if(twop!=2) error("Bonderson-Slingerland only set up for CF flux attachment 2p=2");
            params->is_activated("-R");
            id=11;
          }
          else id=20;
        }
      }
    }
  }
  else if(wfname=="test" || wfname=="Test" || wfname=="TEST") id=18;
  else error(wfname+"' not recognised (possibly not configured yet)");
  
  return id;
}

int get_Nphi(int id, input* params) {
  int Nphi, Ne=params->getint("-Ne");
  switch(id) {
  case 1: {int m=params->getint("-m"); Nphi=m*(Ne-1); break;}
  case 12:
  case 24:
  case 2: {int twop=params->getint("--CF:2p"), n=params->getint("-n"); Nphi=twop*(Ne-1)+n-Ne/n; break;}
  case 3:
  case 18:
  case 30:
  case 31:
  case 7: {int MRp=params->getint("--MR:p"); Nphi=MRp*(Ne-1)-1; break;}
  case 4:
  case 25:
  case 5: {int twop=params->getint("--CF:2p"), twoq=params->getint("-2q"); Nphi=twop*(Ne-1)-twoq; break;}
  case 19:
  case 6: {int twop=params->getint("--CF:2p"), twoq=params->getint("-2q"); Nphi=twop*(Ne-1)+twoq; break;}
  case 14:
  case 8: {int BSp=params->getint("--BS:p"), n=params->getint("-n"), twop=params->getint("--CF:2p"); Nphi=(twop+BSp)*(Ne-1)+n-Ne/n-1; break;}
  case 13:
  case 9: {int twop=params->getint("--CF:2p"), n=params->getint("-n"); Nphi=twop*(Ne-1)-n+Ne/n; break;}
  case 15:
  case 10: {int BSp=params->getint("--BS:p"), n=params->getint("-n"), twop=params->getint("--CF:2p"); Nphi=(twop+BSp)*(Ne-1)-n+Ne/n-1; break;}
  case 16:
  case 11: {int twop=params->getint("--CF:2p"), twoq=params->getint("-2q"), BSp=params->getint("--BS:p"); Nphi=(twop+BSp)*(Ne-1)-twoq-1; break;}
  case 20:
  case 17: {int twop=params->getint("--CF:2p"), twoq=params->getint("-2q"), BSp=params->getint("--BS:p"); Nphi=(twop+BSp)*(Ne-1)+twoq-1; break;}
  case 21:
  case 36: {int Nphi_Pf=params->getint("--MR:Nphi"); int MRp=params->getint("--MR:p"); Nphi=(MRp-1)*(Ne-1)+Nphi_Pf; break;}
  case 26:
  case 33:
  case 35:
  case 28:
  case 22: {
    int Nphi_Pf=params->getint("--MR:Nphi"); int n=params->getint("-n"), twop=params->getint("--CF:2p"), BSp=params->getint("--BS:p");
    Nphi=(twop+BSp-1)*(Ne-1)+Nphi_Pf+Ne/n-n; break;
  }
  case 27:
  case 29:
  case 32:
  case 34:
  case 23: {
    int Nphi_Pf=params->getint("--MR:Nphi"); int n=params->getint("-n"), twop=params->getint("--CF:2p"), BSp=params->getint("--BS:p");
    Nphi=(twop+BSp-1)*(Ne-1)+Nphi_Pf-Ne/n+n; break;
  }
  case 37:
  case 38:
  case 41:
  case 42:
  case 43:
  case 44: {
    int Nphi_Pf=params->getint("--MR:Nphi"), twoq=params->getint("-2q"), twop=params->getint("--CF:2p"), BSp=params->getint("--BS:p");
    Nphi=(twop+BSp-1)*(Ne-1)+Nphi_Pf-twoq; break;
  }
  case 39:
  case 40:
  case 45:
  case 46: {
    int Nphi_Pf=params->getint("--MR:Nphi"), twoq=params->getint("-2q"), twop=params->getint("--CF:2p"), BSp=params->getint("--BS:p");
    Nphi=(twop+BSp-1)*(Ne-1)+Nphi_Pf+twoq; break;
  }
  case 47: {int MRp=params->getint("--MR:p"); Nphi=MRp*(Ne-1); break;}
  case 48: {Nphi=3*(Ne-1)-params->getint("--CF:qp_num"); break;}
  case 49: {Nphi=3*(Ne-1)-params->getint("--CF:qh_num"); break;}
  default: error("id "+to_string(id)+" unknown (finding Nphi)");
  }
  if(Nphi<=0) error("Parameters gave Nphi = "+to_string(Nphi)+"; must be positive");
  
  if(params->is_flagged("-Lqh")) Nphi++;
  return Nphi;
}
