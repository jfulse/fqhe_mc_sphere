/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#ifndef CF_CONFIGURATION_H
#define CF_CONFIGURATION_H

#include "misc.h"

static constexpr unsigned int bitPositionTable[32]={0,1,28,2,29,14,24,3,30,22,20,15,25,17,4,8,31,27,13,23,21,19,16,7,26,12,18,6,11,5,10,9};

static const unsigned char BitSetTable256[256]={
	#define B2(n) n, n+1, n+1, n+2
	#define B4(n) B2(n), B2(n+1), B2(n+1), B2(n+2)
	#define B6(n) B4(n), B4(n+1), B4(n+1), B4(n+2)
	B6(0), B6(1), B6(1), B6(2)
};

struct CF_LL {
	
	private:
	unsigned int n, conf_min, conf_max, conf;
	int twoLz, twol;
	vector<int> powers;
	
	inline unsigned int first_occupied() {return bitPositionTable[((uint32_t)((conf & -conf)*0x077CB531U)) >> 27];}
	inline unsigned int first_occupied(unsigned int c) {return bitPositionTable[((uint32_t)((c & -c)*0x077CB531U)) >> 27];}
	unsigned int count_particles();
	unsigned int count_particles(int);
	void find_conf_min() {conf_min=pow(2, n)-1;}
	void find_conf_max() {conf_max=pow(2, N)-pow(2, n);}
	void compute_2Lz();
	
	public:
	
	unsigned int N;
	
	CF_LL(int);
	
	void display(string buf="");
	void display(unsigned int);
	bool next_conf();
	bool next_conf_and_update_2Lz(int&);
	int return_2Lz() {return twoLz;}
	unsigned int return_conf() {return conf;}
	void add_particle() {n++;}
	void initialise();
	
};

class CF_configuration {
	
	private:
	
	vector<CF_LL> all_LL;
	vector<unsigned int> combinations;
	int n_particles, n_checked;
	
	int fint_2Lz_tot();
	void display_all_CF_LL();
	void display_CF_LL(int, string buf="");
	int find_2Lz_tot();
	void recursive(int&, int&, int&, int);
	void store();
	void show_result();
	void display(unsigned int, unsigned int);
	vector<vector<int> > return_result();
	int next_occupied(unsigned int, int, unsigned int);
	
	public:

	CF_configuration(vector<int>, vector<int>);
	vector<vector<int> > find_Lz_combinations(int);

};

#endif
