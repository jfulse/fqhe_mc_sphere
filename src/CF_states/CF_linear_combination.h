/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_determinant.h"
#include "input.h"
#include "main_misc.h"
#include "misc.h"
#include <algorithm>
#ifndef CF_LINEAR_COMBINATION_H
#define CF_LINEAR_COMBINATION_H

typedef std::vector<double> VD;
typedef std::vector<string> VS;
typedef std::vector<VD> VVD;

vector<VD> read_CF_state(string, int&);
int get_Ne(string, int);
void merge_states(VVD&, VVD, double);

#endif
