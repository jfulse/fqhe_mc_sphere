/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_determinant.h"

CF_determinant::CF_determinant(int n_qp_, int n_qh_, int n_qe_, bool verb_): n_qe(n_qe_), n_qh(n_qh_), verb(verb_) {
	GS=false;
	if(n_qp==0) {
		GS=true;
		Lz=0;
	}
	else {
		l=new int[n_qp];
		m=new int[n_qp];
	}
}

CF_determinant::CF_determinant(int n_qp_, int n_qh_, int n_qe_, int *l_, int *m_, bool odd_, bool verb_): 
			n_qp(n_qp_), n_qe(n_qe_), n_qh(n_qh_), odd(odd_), verb(verb_) {
	GS=false;
	if(n_qp==0) {
		GS=true;
		Lz=0;
	}
	else {
		l=new int[n_qp]; 
		m=new int[n_qp];
		Lz=0;
		for(int i=0;i<n_qp;i++) {
			l[i]=l_[i]; 
			m[i]=m_[i];
			Lz+=m[i];
		}
	}
}

CF_determinant::CF_determinant(VD conf, int twoq, bool verb_): verb(verb_) {error("This det. constr. not set up");
	GS=false; int den=1;
	int n=conf.size();
	if(n&1) error("Creating determinant; configuration vector must have an even number of elements");
	VI qhl, qhm, qel, qem, qeltmp, qemtmp;
	int LL=0, LLqhs;
	while(n>0) {
		int twol=twoq+2*LL;
		LLqhs=0;
		qeltmp.clear(); qemtmp.clear();
		for(int twom=-twol;twom<=twol;twom+=2) { //cout << "Looking for " << twol/2.0 << " , " << twom/2.0 << endl;
			bool exists=false;
			for(int j=0;j<n;j+=2) { //cout << "testing " << conf[j] << " , " << conf[j+1] << endl;
				if(conf[j]==LL && double_equal(twom/2.0, conf[j+1], 1e-8)) { //cout << "found it: " << conf[j] << " " << conf[j+1] << endl;
					exists=true;
					qeltmp.push_back(twol/(1+(den==1)));
					qemtmp.push_back(twom/(1+(den==1)));
					conf.erase(conf.begin()+j);
					conf.erase(conf.begin()+j);
					n-=2;
					break;
				}
			}
			if(!exists) { //cout << "Placing qh at " << twol/(1+(den==1)) << " , " << twom/(1+(den==1)) << endl;
				qhl.push_back(twol/(1+(den==1)));
				qhm.push_back(-twom/(1+(den==1)));
				LLqhs++;
			}
		}
		LL++;
	}
	if(LLqhs==0 && qeltmp.size()==(twoq+1+2*(LL-1))) {
		GS=true;
		Lz=0;
		n_qh=0;
		n_qe=0;
		n_qp=0;
	}
	else {
	//cout << LLqhs << " " << (twoq+1+2*(LL-1))/2 << endl;
		if(LLqhs>(twoq+1+2*(LL-1))/2) {
			//if(verb) cout << "Removing " << LLqhs << " qh's, adding " << qeltmp.size() << " qe's" << endl;
			for(int i=0;i<LLqhs;i++) {
				qhl.pop_back();
				qhm.pop_back();
			}
			qel=qeltmp;
			qem=qemtmp;
		}
		
		Lz=0;
		n_qh=qhl.size();
		n_qe=qel.size();
		n_qp=n_qh+n_qe;
		l=new int[n_qp]; 
		m=new int[n_qp];
		for (int i=0;i<qhl.size();i++) {
			l[i]=qhl[i];
			m[i]=qhm[i];
			Lz+=m[i];
		}
		for (int i=0;i<qel.size();i++) {
			l[i+qhl.size()]=qel[i];
			m[i+qhl.size()]=qem[i];
			Lz+=m[i+qhl.size()];
		}
		if(verb) {cout << "qp's = "; for(int i=0;i<n_qp;i++) cout << l[i] << " " << m[i] << "   "; cout << endl;}
	}
}

bool CF_determinant::raisedexists(int lcheck, int mcheck) {
	for(int i=0;i<n_qp;i++) {
		if(l[i]==lcheck && m[i]==mcheck+2) return true;
	}
	return false;
}

bool CF_determinant::loweredexists(int lcheck, int mcheck) {
	for(int i=0;i<n_qp;i++) {
		if(l[i]==lcheck && m[i]==mcheck-2) return true;
	}
	return false;
}

int CF_determinant::Lplus_coeff_sq(int l, int m) {
	return (l-m)*(l+m+2);
}

int CF_determinant::Lminus_coeff_sq(int l, int m) {
	return (l+m)*(l-m+2);	
	
}

int CF_determinant::Lminus_coeff_sq_st(int twol, int twom) {
	return (twol+twom)*(twol-twom+2);	
	
}

vector<CF_determinant*> CF_determinant::raise(VI& coeffs) {
	if(GS) error("determinant::raise not configured for GS");
	vector<CF_determinant*> raised;
	coeffs.clear();
	for(int i=0;i<n_qp;i++) {
		if(m[i]<l[i] && !raisedexists(l[i], m[i])) {
			//coeffs.push_back((l[i]-m[i])*(l[i]+m[i]+den)/((double)den*den));
			coeffs.push_back(Lplus_coeff_sq(l[i], m[i]));
			int *newl=new int[n_qp];
			int *newm=new int[n_qp];
			for(int j=0;j<n_qp;j++) {
				newl[j]=l[j];
				newm[j]=m[j];
			}
			newm[i]+=2;
			raised.push_back(new CF_determinant(n_qp, n_qh, n_qe, newl, newm, odd));
			delete [] newl;
			delete [] newm;
		}
	}
	return raised;
}

vector<CF_determinant*> CF_determinant::lower(VI& coeffs) {
	if(GS) error("CF_determinant::lower not configured for GS");
	vector<CF_determinant*> lowered;
	coeffs.clear();
	for(int i=0;i<n_qp;i++) {
		if(m[i]>-l[i] && !loweredexists(l[i], m[i])) {
			coeffs.push_back(Lminus_coeff_sq(l[i], m[i]));
			int *newl=new int[n_qp];
			int *newm=new int[n_qp];
			for(int j=0;j<n_qp;j++) {
				newl[j]=l[j];
				newm[j]=m[j];
			}
			newm[i]-=2;
			lowered.push_back(new CF_determinant(n_qp, n_qh, n_qe, newl, newm, odd));
			delete [] newl;
			delete [] newm;
		}
	}
	return lowered;
}

void CF_determinant::display() {
	if(GS) cout << "| GS >" << endl;
	else {
		cout << "| ";
		if(!odd) {
			for(int i=0;i<n_qp-1;i++) cout << "" << l[i]/2 << "," << m[i]/2 << " ; ";
			cout << "" << l[n_qp-1]/2 << "," << m[n_qp-1]/2 << " >" << endl;
		}
		else {
			for(int i=0;i<n_qp-1;i++) cout << "" << l[i] << "/2," << m[i] << "/2 ; ";
			cout << "" << l[n_qp-1] << "/2," << m[n_qp-1] << "/2 >" << endl;
		}
	}
	return;
}

void CF_determinant::print(ofstream& out) {error("Not updated"); int den=2;
	if(GS) out << "| GS >" << endl;
	else {
		out << "| ";
		if(den==1) {
			for(int i=0;i<n_qp-1;i++) out << "" << l[i] << "," << m[i] << " ; ";
			out << "" << l[n_qp-1] << "," << m[n_qp-1] << " >" << endl;
		}
		else if(den==2) {
			for(int i=0;i<n_qp-1;i++) out << "" << l[i] << "/2," << m[i] << "/2 ; ";
			out << "" << l[n_qp-1] << "/2," << m[n_qp-1] << "/2 >" << endl;
		}
		else {cerr << "ERROR: den must be 1 or 2" << endl << endl; throw exception();}
	}
	return;
}

void CF_determinant::print_machine(ofstream& out) {
	if(GS) error("CF_determinant::print_machine not configured for GS");
	for(int i=0;i<n_qp;i++) out << l[i] << " " << m[i] << " ";
	return;
}

void CF_determinant::print_CF_levels(ofstream& out, int twoq, int Ne) {
	int lstate=twoq, mstate=-twoq, qh_placed=0, e_placed=0;
	//cout << endl; display(); cout << endl;
	out << " ";
	
	while(qh_placed<n_qh) {
		bool is_qh=false;
		for(int i=0;i<n_qh;i++) {
			if(mstate==-m[i] && lstate==l[i]) {
				qh_placed++; 
				//cout << "qh " << qh_placed << " of " << n_qh << " placed at 2l=" << lstate << ", 2m=" << -mstate << endl;
				is_qh=true;
				break;
			}
		}
		if(!is_qh) {
			out << (lstate-twoq)/2.0 << " " << mstate/2.0 << " ";
			//cout << "print: " << (lstate-twoq)/2.0 << " " << mstate/2.0 << " " << endl;
			e_placed++;
		}
		//cout << lstate << " " << mstate << "; " << e_placed << " of " << Ne << " electrons placed" << endl;
		mstate+=2;
		if(mstate>lstate) {
			lstate+=2;
			mstate=-lstate;
		}
	}
	while(e_placed<Ne) {
		out << (lstate-twoq)/2.0 << " " << mstate/2.0 << " ";
		//cout << "print: " << (lstate-twoq)/2.0 << " " << mstate/2.0 << " " << endl;
		e_placed++;
		//cout << lstate << " " << mstate << "; " << e_placed << " of " << Ne << " electrons placed" << endl;
		mstate+=2;
		if(mstate>lstate) {
			lstate+=2;
			mstate=-lstate;
		}
	}
	//cout << "l m " << lstate << " " << mstate << endl;
	lstate-=2;
	
	if(e_placed!=Ne) error("Mismatch between number of electrons and quasiparticles");
	for(int i=0;i<n_qe;i++) {
		if(l[i+n_qh]<=lstate) error("Attempting to place quasielectron in same CF LL as electrons");
		out << (l[i+n_qh]-twoq)/2.0 << " " << m[i+n_qh]/2.0 << " ";
		//cout << "print: " << (l[i+n_qh]-twoq)/2.0 << " " << m[i+n_qh]/2.0 << " " << endl;
		//cout << "qe " << i+1 << " of " << n_qe << " placed at l=" << l[i+n_qh] << ", m=" << m[i+n_qh] << endl;
	}
	out << endl;
}
