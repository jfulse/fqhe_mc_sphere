/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_configuration.h"

CF_LL::CF_LL(int N_): N(N_), n(1), twol(N_-1) {
	powers.resize(N);
	for(int i=0;i<N;i++) powers[i]=pow2(i);
}

void CF_LL::initialise() {
	find_conf_min();
	find_conf_max();
	conf=conf_min;
	compute_2Lz();	
}

unsigned int CF_LL::count_particles() {
	return BitSetTable256[conf & 0xff]+BitSetTable256[(conf>>8) & 0xff]+
		   BitSetTable256[(conf>>16) & 0xff]+BitSetTable256[conf>>24];
}

unsigned int CF_LL::count_particles(int c) {
	return BitSetTable256[c & 0xff]+BitSetTable256[(c>>8) & 0xff]+
		   BitSetTable256[(c>>16) & 0xff]+BitSetTable256[c>>24];
}

void CF_LL::compute_2Lz() {
	twoLz=0;
	for(int i=N-1;i>-1;i--) {
		if(conf&powers[i]) twoLz+=twol-2*i;
	}
}

void CF_LL::display(string buf) {
	cout << buf;
	for(int i=N-1;i>-1;i--) cout << (bool)(conf&powers[i]);
}

void CF_LL::display(unsigned int c) {
	for(int i=N-1;i>-1;i--) cout << (bool)(c&powers[i]);
}

bool CF_LL::next_conf() {
	int conf_test=conf+1;
	while(conf_test<=conf_max) {
		if(count_particles(conf_test)==n) {
			conf=conf_test;
			return true;
		}
		conf_test++;
	}
	return false;
}

bool CF_LL::next_conf_and_update_2Lz(int& twoLz_tot) {
	unsigned int conf_test=conf+1;
	while(conf_test<=conf_max) {
		if(count_particles(conf_test)==n) {
			twoLz_tot-=twoLz;
			unsigned int change=conf^conf_test;
			unsigned int added=change&conf_test;
			unsigned int removed=change&conf;
			while(added>0) {
				unsigned int idx=first_occupied(added);
				twoLz-=2*idx;
				added-=powers[idx];
			}
			while(removed>0) {
				unsigned int idx=first_occupied(removed);
				twoLz+=2*idx;
				removed-=powers[idx];
			}
			conf=conf_test;
			twoLz_tot+=twoLz;
			return true;
		}
		conf_test++;
	}
	
	twoLz_tot-=twoLz;
	conf=conf_min;
	compute_2Lz();
	twoLz_tot+=twoLz;
	return false;
}
	
CF_configuration::CF_configuration(vector<int> qh_levels, vector<int> qe_levels) {
	if(sizeof(int)!=4) error("int not represented using 4 bytes; bit calculations will be wrong (use recursive method)");
	n_particles=qh_levels.size()+qe_levels.size();
	
	if(qh_levels.size()>0) {
		all_LL.push_back(CF_LL(qh_levels[0]+1));
		qh_levels.erase(qh_levels.begin());
	}
	while(qh_levels.size()>0) {
		if(qh_levels[0]+1==all_LL[all_LL.size()-1].N) all_LL[all_LL.size()-1].add_particle();
		else all_LL.push_back(CF_LL(qh_levels[0]+1));
		qh_levels.erase(qh_levels.begin());
	}
	
	if(qe_levels.size()>0) {
		all_LL.push_back(CF_LL(qe_levels[0]+1));
		qe_levels.erase(qe_levels.begin());
	}
	while(qe_levels.size()>0) {
		if(qe_levels[0]+1==all_LL[all_LL.size()-1].N) all_LL[all_LL.size()-1].add_particle();
		else all_LL.push_back(CF_LL(qe_levels[0]+1));
		qe_levels.erase(qe_levels.begin());
	}
	
	for(int i=0;i<all_LL.size();i++) {
		all_LL[i].initialise();
	}
	
	n_checked=0;
}

void CF_configuration::display_all_CF_LL() {
	string buf="";
	for(int i=all_LL.size()-1;i>-1;i--) {
		display_CF_LL(i, buf);
		cout << buf << " 2Lz = " << all_LL[i].return_2Lz() << endl;
		if(i>0) {
			for(int j=0;j<(all_LL[i].N-all_LL[i-1].N)/2;j++) buf+=" ";
		}
	}
}

void CF_configuration::display_CF_LL(int lvl, string buf) {
	if(lvl<0 || lvl>all_LL.size()-1) error("CF LL "+to_string(lvl)+" does not exist");
	all_LL[lvl].display(buf);
}

int CF_configuration::find_2Lz_tot() {
	int twoLz_tot=0;
	for(int i=0;i<all_LL.size();i++) twoLz_tot+=all_LL[i].return_2Lz();
	return twoLz_tot;
}

vector<vector<int> > CF_configuration::find_Lz_combinations(int twoLz_target) {
	int twoLz_tot=find_2Lz_tot();
	int level=0, prev_level=-1;
	
	recursive(level, prev_level, twoLz_tot, twoLz_target);
	cout << n_checked << " combinations tested" << endl;
	return return_result();
}

int CF_configuration::next_occupied(unsigned int c, int last_occupied, unsigned int N) {
	for(int i=last_occupied+1;i<N;i++) {
		if(c&pow2(i)) return i;
	}
	return -1;
}

vector<vector<int> > CF_configuration::return_result() {
	vector<vector<int> > result(combinations.size()/all_LL.size(), vector<int>(n_particles));
	for(int i=0;i<combinations.size()/all_LL.size();i++) {
		unsigned int found_particles=0, LL=0;
		int occupied=-1;
		while(found_particles<n_particles) {
			occupied=next_occupied(combinations[i*all_LL.size()+LL], (unsigned int)occupied, all_LL[LL].N);
			if(occupied>-1) {
				result[i][found_particles]=all_LL[LL].N-1-2*occupied;
				found_particles++;
			}
			else LL++;
		}
	}
	return result;
}

void CF_configuration::store() {
	for(int i=0;i<all_LL.size();i++) combinations.push_back(all_LL[i].return_conf());
}

void CF_configuration::recursive(int& level, int& prev_level, int& twoLz_tot, int twoLz_target) {
	//cout << endl;
	if(level<0) return;
	else if(level==all_LL.size()-1) {
		if(twoLz_tot==twoLz_target) {
			store();
			//display_all_CF_LL();
			//int I; cin >> I;
		}
		n_checked++;
		while(all_LL[level].next_conf_and_update_2Lz(twoLz_tot)) {
			if(twoLz_tot==twoLz_target) {
				store();
				//display_all_CF_LL();
				//int I; cin >> I;
			}
			n_checked++;
		}
		prev_level=level;
		level--;
		CF_configuration::recursive(level, prev_level, twoLz_tot, twoLz_target);
	}
	else {
		if(prev_level<level) {
			prev_level=level;
			level++;
		}
		else {
			if(all_LL[level].next_conf_and_update_2Lz(twoLz_tot)) {
				prev_level=level;
				level++;
			}
			else {
				prev_level=level;
				level--;
			}
		}
		CF_configuration::recursive(level, prev_level, twoLz_tot, twoLz_target);
	}
}
