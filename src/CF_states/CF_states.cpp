/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_states.h"

string helptext();

int main(int argc, const char* argv[]) {
	
	try {
		clock_t starttime=clock();
		input *params=new input("_",helptext());
		
		params->add_input("-n", "Number of filled CF states in ground state", "int", "4");
		params->add_input("--twice-flux,-2q", "2 x effective CF flux q (lowest CF LL has 2q+1 states)", "int","0");
		params->add_input("--twice-L,-2L", "2 x target L value", "int","4");
		params->add_input("--twice-Lz,-2Lz", "2 x target Lz value", "int","4");
		params->add_input("--qh-LLs,-qh", "The CF Landau levels of quasiholes (from 0)", "VI");
		params->add_input("--qe-LLs,-qe", "The CF Landau levels of quasielectrons (from 0)", "VI");
		params->add_input("--out-prefix,-op", "Prefix to use for output filenames", "string","State");
		params->add_input("--Lz-prefix,-Lzp", "File prefixes to write Lz-determinants (MC format)", "string");
		params->add_input("--det-file-out,-do", "File in which to save Lz-determinants (CF format)", "string");
		params->add_input("--det-file-in,-di", "File from which to read Lz-determinants (CF format)", "string");
		params->add_input("--kernel-method", "Kernel finding method (SparseQR, QR, SVD, LU)", "string","QR");
		params->add_input("--test-kernel", "Double check L_+ acting on kernel", "bool");
		params->add_input("--Lz-only", "Stop after finding Lz-states", "bool");
		params->add_input("--verbose,-v", "Extra verbose", "bool");
		params->add_input("--quiet,-q", "Do not output extra info", "bool");
		
		if(params->command_line_input(argc, argv)!=0) return 0;
		if(!params->check_integrity()) return 0;
		
		int twoLz=params->getint("-2Lz"), twoL=params->getint("-2L");
		int twoq=params->getint("-2q"), maxtwoLz=twoL, currenttwoLz=twoL;
		string prefix=params->getstring("--out-prefix"), Lstr=getLstr(twoL), Lzstr=getLstr(twoLz), method=params->getstring("--kernel-method");
		bool verb=true, odd=false, ex_v=false, check_kernel=false, Lzonly=false;
		if(params->is_flagged("-q")) verb=false;
		if(params->is_flagged("-v")) ex_v=true;
		if(params->is_flagged("--test-kernel")) check_kernel=true;
		if(params->is_flagged("--Lz-only")) Lzonly=true;
		if(!verb && ex_v) {
			warning("Both extra verbose ('-v') and quiet ('-q') activated; ignoring the latter");
			verb=true;
		}
		
		if(twoLz>twoL || twoLz < -twoL || twoL%2!=abs(twoLz)%2) error("No determinant with L = "+Lstr+" and Lz = "+Lzstr);
		
		VI qe_levels, qh_levels;
		if(params->is_activated(("-qh"))) qh_levels=params->getVI("-qh");
		if(params->is_activated(("-qe"))) qe_levels=params->getVI("-qe");
		transform(qh_levels.begin(), qh_levels.end(), qh_levels.begin(), bind1st(std::multiplies<int>(), 2));
		transform(qh_levels.begin(), qh_levels.end(), qh_levels.begin(), bind1st(std::plus<int>(), twoq));
		transform(qe_levels.begin(), qe_levels.end(), qe_levels.begin(), bind1st(std::multiplies<int>(), 2));
		transform(qe_levels.begin(), qe_levels.end(), qe_levels.begin(), bind1st(std::plus<int>(), twoq));
		sort(qh_levels.begin(), qh_levels.end());
		sort(qe_levels.begin(), qe_levels.end());
		int n_qh=qh_levels.size();
		int n_qe=qe_levels.size();
		if(n_qh<1 && n_qe<1) error("Need at least one qh or qe");
		int Ne=params->getint("-n")*(params->getint("-2q")+params->getint("-n"))-n_qh;
		
		string detin="", detout="", Lz_prefix="";
		if(params->is_activated("-di")) detin=params->getstring("-di");
		if(params->is_activated("-do")) detout=params->getstring("-do");
		if(params->is_activated("-Lzp")) Lz_prefix=params->getstring("-Lzp");
		if(verb) display_start(params, 60, "_", argv[0]);
		delete params;
		cout.precision(6);
		
		vector<vector<CF_determinant*> > final_result_dets;
		vector<VD> final_result_coeffs;
		
		VVI combinations;
		vector<CF_determinant*> dets;
		int n_dets;
		
		if(Lzonly) {
			if(detin!="") {
				dets=read_det_from_file(detin, n_qh, n_qe, verb, odd);
				if(verb) cout << endl << "Determinants read from " << detin << endl;
			}
			else {
				check_levels(n_qe, n_qh, qe_levels, qh_levels);
				combinations=create_Lz_combinations(n_qe, n_qh, qe_levels, qh_levels, twoLz, odd, twoq);
				if(combinations.size()<1) error("No valid Lz combinations found");
				dets=create_determinants(combinations, n_qh, n_qe, qe_levels, qh_levels, verb, odd);
				
			}
			
			n_dets=dets.size();
			if(n_dets<1) error("No determinants created");
			if(detout!="") {
				ofstream outdets(detout);
				outdets << n_qh+n_qe << endl;
				for(int i=0;i<n_dets;i++) {dets[i]->print_machine(outdets); outdets << endl;}
				outdets.close();
				if(verb) cout << "Determinants written to " << detout << endl;
			}
			if(Lz_prefix!="") {
				for(int i=0;i<n_dets;i++) {
					string filename=Lz_prefix+"_"+to_string(i+1)+".dat";
					ofstream Lz_out(filename.c_str());
					if(!Lz_out) error("Could not open "+filename+" for writing");
					Lz_out << "1";
					dets[i]->print_CF_levels(Lz_out, twoq, Ne);
					Lz_out.close();
				}
				if(verb) cout << "Individual Lz-states written to " << Lz_prefix << "_[k].dat" << endl;
			}
			
		}
		else {
				if(detin!="") {
				dets=read_det_from_file(detin, n_qh, n_qe, verb, odd);
				if(verb) cout << endl << "Determinants read from " << detin << endl;
			}
			else {
				check_levels(n_qe, n_qh, qe_levels, qh_levels);
				combinations=create_Lz_combinations(n_qe, n_qh, qe_levels, qh_levels, maxtwoLz, odd, twoq);
				if(combinations.size()<1) error("No valid Lz combinations found");
				dets=create_determinants(combinations, n_qh, n_qe, qe_levels, qh_levels, verb, odd);
				
			}
			
			n_dets=dets.size();
			if(n_dets<1) error("No determinants created");
			if(detout!="") {
				ofstream outdets(detout);
				outdets << n_qh+n_qe << endl;
				for(int i=0;i<n_dets;i++) {dets[i]->print_machine(outdets); outdets << endl;}
				outdets.close();
				if(verb) cout << "Determinants written to " << detout << endl;
			}
			if(Lz_prefix!="") {
				for(int i=0;i<n_dets;i++) {
					string filename=Lz_prefix+"_"+to_string(i+1)+".dat";
					ofstream Lz_out(filename.c_str());
					if(!Lz_out) error("COuld not open "+filename+" for writing");
					Lz_out << "1";
					dets[i]->print_CF_levels(Lz_out, twoq, Ne);
					Lz_out.close();
				}
				if(verb) cout << "Individual Lz-states written to " << Lz_prefix << "_[k].dat" << endl;
			}
		
			if(ex_v) {
				cout << endl;
				for(int i=0;i<n_dets;i++) dets[i]->display(); cout << endl;
			}
			
			vector<VD> result_coeffs;
			vector<CF_determinant*> raised;
			VI coeffs;
			VI labels;
			
			//if(dets.size()>1) {
				if(verb) cout << "Applying raising operator" << endl;
				for(int i=0;i<n_dets;i++) {
					VI coeffs_tmp;
					vector<CF_determinant*> raised_tmp=dets[i]->raise(coeffs_tmp);
					labels.insert(labels.end(), raised_tmp.size(), i);
					coeffs.insert(coeffs.end(), coeffs_tmp.begin(), coeffs_tmp.end());
					raised.insert(raised.end(), raised_tmp.begin(), raised_tmp.end());
				}
			//}
			
			if(raised.size()<1) {
				if(dets.size()>1) error("Could not raise any determinants but have more than one");
				result_coeffs.push_back(VD(1, 1.0));
			}
			else {
				cout << raised.size() << " raised determinants" << endl;
				
				vector<VD> kernel;
				if(method=="SparseQR") {error("Sparse QR not yet implemented");
					vector<Eigen::Triplet<double> > coeffmatrix=sum_determinants_triplets(n_dets, raised, labels, coeffs);
				}
				else {
					VVI coeffmatrix=sum_determinants(n_dets, raised, labels, coeffs);
					kernel=get_nullspace(coeffmatrix, method, verb);
				}
				
				if(check_kernel) {
					cout << "Computing L+ on kernel..." << endl << endl;
					compute_Lplus(dets, kernel);
				}
				
				for(int i=0;i<kernel.size();i++) {
					for(int j=0;j<kernel[i].size();j++) {
						if(insignificant(kernel[i], j, 1e-12)) kernel[i][j]=0.0;
					}
				}
				for(int i=0;i<kernel.size();i++) {
					bool allzero=true;
					for(int j=0;j<kernel[i].size();j++) {
						if(abs(kernel[i][j])>1e-14) allzero=false;
					}
					if(allzero) {
						warning("Kernel vector "+to_string(i+1)+" zero");
						kernel.erase(kernel.begin()+i);
					}
				}
				
				if(kernel.size()<1) error("Kernel empty; no states found");
				else {
					if(ex_v) cout << "Kernel:" << endl; 
					for(int i=0;i<kernel.size();i++) {
						result_coeffs.push_back(VD(kernel[i].size()));
						if(ex_v) cout << "vector " << i+1 << " = [";
						for(int j=0;j<kernel[i].size()-1;j++) {
							if(ex_v) cout << kernel[i][j] << ","; 
							result_coeffs[i][j]=kernel[i][j];
						}
						if(ex_v) cout << kernel[i][kernel[i].size()-1] << "]" << endl;
						result_coeffs[i][kernel[i].size()-1]=kernel[i][kernel[i].size()-1];
					}
				}
				if(ex_v) cout << endl;
			}
			
			if(currenttwoLz>twoLz) {
			
				if(ex_v) {
					cout << "Basis for space of determinants | " << Lstr << "," << Lstr << " >:" << endl << endl;
					for(int i=0;i<result_coeffs.size();i++) {
						cout << "State " << i+1 << ":" << endl;
						for(int j=0;j<result_coeffs[i].size();j++) {
							if((abs(result_coeffs[i][j])>1e-14)) {
								if(abs(result_coeffs[i][j]-1.0)>1e-13) cout << result_coeffs[i][j] << " * ";
								dets[j]->display();
							}
						}
					}
					cout << endl;
				}
				
				if(verb) cout << "Applying lowering operator" << endl;
				for(int k=0;k<result_coeffs.size();k++) {
					
					if(ex_v) cout << "State " << k+1 << endl;
					int currenttwoLztmp=currenttwoLz;
					vector<CF_determinant*> currentdets=dets;
					int currentdets_n=n_dets;
					VD currentcoeffs=result_coeffs[k];
					for(int i=0;i<currentdets_n;i++) {
						if((abs(currentcoeffs[i])<1e-14)) {
							currentcoeffs.erase(currentcoeffs.begin()+i);
							currentdets.erase(currentdets.begin()+i);
							currentdets_n--;
							i--;
						}
					}
					
					while(currenttwoLztmp>twoLz) {
						int totalcoeff=CF_determinant::Lminus_coeff_sq_st(twoL, currenttwoLztmp);
						if(ex_v) {
							cout << "Lowering to Lz = " << getcoeffdiv2str(currenttwoLztmp-2) << endl;
							//cout << "Coefficient on total determinant: sqrt(" << getcoeffdiv4str(totalcoeff) << ")" << endl;
						}
						VI lowering_coeffs;
						VI lowering_labels;
						vector<CF_determinant*> lowered;
						
						for(int i=0;i<currentdets_n;i++) {
							VI coeffs_tmp;
							vector<CF_determinant*> lowered_tmp=currentdets[i]->lower(coeffs_tmp);
							lowering_labels.insert(lowering_labels.end(), lowered_tmp.size(), i);
							lowering_coeffs.insert(lowering_coeffs.end(), coeffs_tmp.begin(), coeffs_tmp.end());
							lowered.insert(lowered.end(), lowered_tmp.begin(), lowered_tmp.end());
						}
						if(lowered.size()<1) error("Could not apply L_- to any of the determinants");
						currenttwoLztmp-=2;
						
						VD current_result_coeffs=concatenate_determinants(lowered, lowering_labels, lowering_coeffs, currentcoeffs);
						
						for(int i=0;i<lowered.size();i++) current_result_coeffs[i]/=sqrt(totalcoeff/4.0);
						
						currentcoeffs=current_result_coeffs;
						currentdets=lowered;
						currentdets_n=currentdets.size();
						
						for(int i=0;i<currentdets_n;i++) {	
							if(insignificant(currentcoeffs, i,1e-10)) {
								currentcoeffs.erase(currentcoeffs.begin()+i);
								currentdets.erase(currentdets.begin()+i);
								currentdets_n--;
								i--;
							}
						}
					}
					
					final_result_dets.push_back(currentdets);
					final_result_coeffs.push_back(currentcoeffs);
				}
				if(ex_v) cout << endl;
			}
			else {
				for(int i=0;i<result_coeffs.size();i++) {
					final_result_dets.push_back(dets);
					final_result_coeffs.push_back(result_coeffs[i]);
				}
			}
			
			VI n_final_dets(final_result_dets.size());
			for(int i=0;i<final_result_dets.size();i++) n_final_dets[i]=final_result_dets[i].size();
			
			for(int i=0;i<final_result_dets.size();i++) {
				for(int j=0;j<n_final_dets[i];j++) {
					if(insignificant(final_result_coeffs[i], j,1e-10)) {
						final_result_coeffs[i].erase(final_result_coeffs[i].begin()+j);
						final_result_dets[i].erase(final_result_dets[i].begin()+j);
						n_final_dets[i]--;
						j--;
					}
				}
			}
			check_linear_independence(final_result_dets, final_result_coeffs, ex_v);
			for(int i=0;i<final_result_coeffs.size();i++) {
				if(final_result_coeffs[0].size()==1) final_result_coeffs[i][0]=1.0;
			}
			
			cout << "Final result: " << final_result_dets.size() << " state(s)" << endl;
			if(ex_v) {
				cout << endl << "Basis for space of determinants | " << Lstr << "," << Lzstr << " >:" << endl << endl;
				for(int i=0;i<final_result_dets.size();i++) {
					cout << "State " << i+1 << ":" << endl;
					for(int j=0;j<n_final_dets[i];j++) {
						if(abs(final_result_coeffs[i][j]-1.0)>1e-13) cout << final_result_coeffs[i][j] << " * ";
						final_result_dets[i][j]->display();
					}
					cout << endl;
				}
			}
			
			if(verb) cout << "States written to: " << endl;
			for(int i=0;i<final_result_coeffs.size();i++) {
				string fname=prefix;
				if(final_result_coeffs.size()>1) fname+="_"+to_string(i+1);
				fname+=".dat";
				ofstream stateout(fname.c_str());
				stateout.precision(17);
				for(int j=0;j<n_final_dets[i];j++) {
					stateout << final_result_coeffs[i][j] << " ";
					final_result_dets[i][j]->print_CF_levels(stateout, twoq, Ne);
				}
				stateout.close();
				if(verb) {
					cout << fname;
					if(i<final_result_coeffs.size()-1) cout << ", ";
					if((i%4)==3) cout << endl;
				}
			}
			if(verb) cout << endl;
		}
		
		cout.precision(3);
		if(verb) display_end(starttime, 60, "_");
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		cerr << "Run with '-h' for options" << endl; 
		return 1;
	}
	catch(...) {
		cerr << "UNKNOWN ERROR" << endl; 
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
}

void compute_Lplus(vector<CF_determinant*> dets, vector<VD> states) {
	
	int n=states.size(), n_dets=dets.size();
	
	VD Enorms(n, 0.0);
	for(int i=0;i<n;i++) {
		if(states[i].size()!=n_dets) error("Computing L+: state "+to_string(i)+" has different number of CF determinants");
		VD raised_coeffs;
		vector<CF_determinant*> raised;
		for(int j=0;j<n_dets;j++) {
			VI coeffs_tmp;
			vector<CF_determinant*> raised_tmp=dets[j]->raise(coeffs_tmp);
			for(int k=0;k<raised_tmp.size();k++) {
				bool found=false;
				for(int l=0;l<raised.size();l++) {
					if(eq_det(raised_tmp[k], raised[l])) {
						raised_coeffs[l]+=states[i][j]*sqrt(coeffs_tmp[k]/4.0);
						found=true;
						break;
					}
				}
				if(!found) {
					raised.push_back(raised_tmp[k]);
					raised_coeffs.push_back(states[i][j]*sqrt(coeffs_tmp[k]/4.0));
				}
			}
		}
		cout << "L_+|ker_" << i << "> = [";
		for(int j=0;j<raised_coeffs.size()-1;j++) {
			cout << raised_coeffs[j] << ",";
			Enorms[i]+=raised_coeffs[j]*raised_coeffs[j];
		}
		cout << raised_coeffs[raised_coeffs.size()-1]; 
		Enorms[i]+=raised_coeffs[raised_coeffs.size()-1]*raised_coeffs[raised_coeffs.size()-1];
		cout << "]" << endl;
	}
	
	cout << endl << "Euclidian norms of L_+|ker_k>:" << endl;
	for(int i=0;i<n;i++) cout << i << ": " << sqrt(Enorms[i]) << endl;
	cout << endl;
}

void check_linear_independence(vector<vector<CF_determinant*> >& dets, vector<VD>& coeffs, bool ex_v) {
	int n=dets.size();
	if(n!=coeffs.size()) error("Checking linear independence: unequal number of determinants and coefficients");
	if(n==1) return;
	for(int i=0;i<n-1;i++) {
		for(int j=i+1;j<n;j++) {
			bool found_prop_factor=false, equal=true;
			int no_equal=0;
			double prop_factor=0.0;
			int nd=dets[i].size();
			if(nd!=dets[j].size()) equal=false;
			else {
				for(int k=0;k<nd;k++) {
					for(int l=0;l<nd;l++) {
						if(equal && eq_det(dets[i][k], dets[j][l])) {
							no_equal++;
							if(found_prop_factor) {
								if(abs(coeffs[j][l]/coeffs[i][k]-prop_factor)>1e-13) equal=false;
							}
							else {
								prop_factor=coeffs[j][l]/coeffs[i][k];
								found_prop_factor=true;
							}
						}
					}
				}
			}
			if(equal && nd!=no_equal) equal=false;
			
			if(equal) {
				if(ex_v) cout << "States " << i+1 << " and " << j+1 << " linearly dependent; latter is removed" << endl;
				dets.erase(dets.begin()+j);
				coeffs.erase(coeffs.begin()+j);
				n--;
				j--;
				
			}
		}
	}
}

string getcoeffdiv4str(int coeff) {
	stringstream ss;
	if(coeff%2) ss << coeff << "/4";
	else {
		if(coeff/2%1) ss << coeff/2 << "/2";
		else ss << coeff/4;
	}
	return ss.str();
}

string getcoeffdiv2str(int coeff) {
	stringstream ss;
	if(coeff%2) ss << coeff << "/2";
	else ss << coeff/2;
	return ss.str();
}

string getLstr(int twoL) {
	stringstream ss;
	if(twoL%2) ss << twoL << "/2";
	else ss << twoL/2;
	return ss.str();
}

string helptext() {
	return "";
}

vector<CF_determinant*> create_determinants(VVI combinations, int n_qh, int n_qe, VI qe_levels, VI qh_levels, bool verb, bool odd) {
	int n=combinations.size();
	vector<CF_determinant*> dets;
	for(int i=0;i<n;i++) {
		int *l=new int[n_qh+n_qe], *m=new int[n_qh+n_qe];
		for(int j=0;j<n_qh;j++) {
			l[j]=qh_levels[j];
			m[j]=combinations[i][j];
		}
		for(int j=0;j<n_qe;j++) {
			l[j+n_qh]=qe_levels[j];
			m[j+n_qh]=combinations[i][j+n_qh];
		}
		dets.push_back(new CF_determinant(n_qh+n_qe, n_qh, n_qe, l, m, odd, verb));
		delete [] l;
		delete [] m;
	}
	return dets;
}

VVI create_Lz_combinations(int n_qe, int n_qh, VI qe_levels, VI qh_levels, int twoLz, bool& odd, int twoq) {
	
	cout << "Finding Lz combinations of " << n_qh << " quasiholes and " << n_qe << " quasielectrons\nwith total Lz = " << getLstr(twoLz) << "... ";
	cout.flush();
	
	if(n_qh>0) {
		odd=qh_levels[0]%2;
		for(int i=1;i<n_qh;i++) {
			if(!odd && qh_levels[i]%2) error("Quasiparticle levels must all be odd or all even");
		}
		for(int i=0;i<n_qe;i++) {
			if(!odd && qe_levels[i]%2) error("Quasiparticle levels must all be odd or all even");
		}
	}
	else {
		odd=qe_levels[0]%2;
		for(int i=1;i<n_qe;i++) {
			if(!odd && qe_levels[i]%2) error("Quasiparticle levels must all be odd or all even");
		}
	}
	
	CF_configuration soln(qh_levels, qe_levels);
	VVI combinations=soln.find_Lz_combinations(twoLz);
	
	
	cout << combinations.size() << " determinants in total" << endl;
	
	for(int i=0;i<combinations.size();i++) {
		for(int j=0;j<n_qh;j++) {
			int idx=j+1;
			while(idx<n_qh && qh_levels[j]==qh_levels[idx]) idx++;
			if(idx>j+1) sort(combinations[i].begin()+j, combinations[i].begin()+idx, greater<int>());
		}
		for(int j=n_qh;j<n_qe+n_qh;j++) {
			int idx=j+1;
			while(idx<n_qe+n_qh && qe_levels[j-n_qh]==qe_levels[idx-n_qh]) idx++;
			if(idx>j+1) sort(combinations[i].begin()+j, combinations[i].begin()+idx);
		}
	}
	
	return combinations;
}

bool check_invalid(VI tentative, VVI combinations, VI qh_levels, VI qe_levels, int n_qh, int n_qe, int n_qp) {
	bool invalid=false;
	for(int i=0;i<n_qh-1 && !invalid;i++) {
		for(int j=i+1;j<n_qh;j++) {
			if(qh_levels[i]==qh_levels[j]) {
				if(tentative[i]==tentative[j]) invalid=true;
			}
		}
	}
	for(int i=0;i<n_qe-1 && !invalid;i++) {
		for(int j=i+1;j<n_qe;j++) {
			if(qe_levels[i]==qe_levels[j]) {
				if(tentative[i+n_qh]==tentative[j+n_qh]) invalid=true;
			}
		}
	}
	
	for(int j=0;j<combinations.size() && !invalid;j++) {
		int qh_equal=0;
		for(int i=0;i<n_qh;i++) {
			for(int k=0;k<n_qh;k++) {
				if(qh_levels[i]==qh_levels[k]) {
					if(tentative[i]==combinations[j][k]) qh_equal++;
				}
				else if(k>i) break;
			}
		}
		if(qh_equal==n_qh) {
			int qe_equal=0;
			for(int i=0;i<n_qe;i++) {
				for(int k=0;k<n_qe;k++) {
					if(qe_levels[i]==qe_levels[k]) {
						if(tentative[i+n_qh]==combinations[j][k+n_qh]) qe_equal++;
					}
					else if(k>i) break;
				}
			}
			if(qe_equal==n_qe) invalid=true;
		}
	}
	return invalid;
}

void check_levels(int n_qe, int n_qh, VI qe_levels, VI qh_levels) {
	if(n_qe>qe_levels.size()) error("Number of qe levels must be as many as as number of qe's");
	if(n_qh>qh_levels.size()) error("Number of qh levels must be as many as as number of qh's");
	if(n_qe<qe_levels.size()) error("More qe levels than qe's given");
	if(n_qh<qh_levels.size()) error("More qh levels than qh's given");
	for(int i=0;i<n_qe;i++) {
		for(int j=0;j<n_qh;j++) {
			if(qe_levels[i]<=qh_levels[j]) {
				error("Cannot have qe in level <= that of a qh");
			}
		}
	}
}

VVI sum_determinants(int n_dets, vector<CF_determinant*>& determinants, VI& labels, VI& coeffs) {
	int n=determinants.size();
	if(labels.size()!=n) error("Labels vector different size than determinant vector");
	if(coeffs.size()!=n) error("Coeffs vector different size than determinant vector");
	VVI coeffmatrix;
	for(int i=0;i<n;i++) {
		coeffmatrix.push_back(VI(n_dets));
		coeffmatrix[i][labels[i]]=coeffs[i];
		for(int j=i+1;j<n;j++) {
			if(eq_det(determinants[i], determinants[j])) {
				coeffmatrix[i][labels[j]]=coeffs[j];
				coeffs.erase(coeffs.begin()+j);
				labels.erase(labels.begin()+j);
				determinants.erase(determinants.begin()+j);
				j--;
				n--;
			}
		}
	}
	return coeffmatrix;
}

vector<Eigen::Triplet<double> > sum_determinants_triplets(int n_dets, vector<CF_determinant*>& determinants, VI& labels, VI& coeffs) {
	int n=determinants.size();
	if(labels.size()!=n) error("Labels vector different size than determinant vector");
	if(coeffs.size()!=n) error("Coeffs vector different size than determinant vector");
	
	vector<Eigen::Triplet<double> > coeffmatrix;
	coeffmatrix.reserve(coeffs.size());
	
	for(int i=0;i<n;i++) {
		//coeffmatrix.push_back(VI(n_dets));
		//coeffmatrix[i][labels[i]]=coeffs[i];
		coeffmatrix.push_back(Eigen::Triplet<double>(i, labels[i], sqrt(coeffs[i]/4.0)));
		for(int j=i+1;j<n;j++) {
			if(eq_det(determinants[i], determinants[j])) {
				coeffmatrix.push_back(Eigen::Triplet<double>(i, labels[j], sqrt(coeffs[j]/4.0)));
				//coeffmatrix[i][labels[j]]=coeffs[j];
				coeffs.erase(coeffs.begin()+j);
				labels.erase(labels.begin()+j);
				determinants.erase(determinants.begin()+j);
				j--;
				n--;
			}
		}
	}
	return coeffmatrix;
}

vector<VD> get_nullspace(vector<VI > coeffmatrix, string method, bool verb) {
	int n=coeffmatrix.size(), m=coeffmatrix[0].size();
	Eigen::MatrixXd CFM(n, m);
	for(int i=0;i<n;i++) {
		for(int j=0;j<m;j++) CFM(i, j)=sqrt(coeffmatrix[i][j]/4.0);
	}
	
	vector<VD> ret;
	if(method=="LU") {
		if(verb) cout << "Finding kernel using LU decomposition" << endl;
		Eigen::FullPivLU<Eigen::MatrixXd> lu_decomp(CFM);
		Eigen::MatrixXd kernel=lu_decomp.kernel().transpose();
		ret.resize(kernel.rows(),VD(kernel.cols()));
		if(kernel.rows()<1) error("Kernel empty; no states with target L");
		if(verb) cout << "Kernel dimension: " << kernel.rows() << endl;
		for(int i=0;i<kernel.rows();i++) {
			for(int j=0;j<kernel.cols();j++) ret[i][j]=kernel(i, j);
		}
	}
	else if(method=="SVD") {
		if(verb) cout << "Finding kernel using SVD decomposition" << endl;
		Eigen::JacobiSVD<Eigen::MatrixXd> svd(CFM, Eigen::ComputeFullU | Eigen::ComputeFullV);
		Eigen::MatrixXd V=svd.matrixV();
		int Rank=svd.nonzeroSingularValues();
		int dim=m-Rank;
		if(dim<1) error("Kernel empty; no states with target L");
		if(verb) cout << "Kernel dimension: " << dim << endl;
		ret.resize(dim, VD(m));
		for(int i=0;i<dim;i++) {
			for(int j=0;j<m;j++) ret[i][j]=V(j, m-1-i);
		}
	}
	else if(method=="QR") {
		if(verb) cout << "Finding kernel using QR decomposition" << endl;
		Eigen::ColPivHouseholderQR<Eigen::MatrixXd> qr(CFM.transpose());
		Eigen::MatrixXd Q=qr.matrixQ();
		int dim=m-qr.rank();
		if(dim<1) error("Kernel empty; no states with target L");
		if(verb) cout << "Kernel dimension: " << dim << endl;
		ret.resize(dim, VD(m));
		for(int i=0;i<dim;i++) {
			for(int j=0;j<m;j++) ret[i][j]=Q(j, m-1-i);
		}
	}
	else error("Kernel finding method "+method+" unknown");
	
	return ret;
}

vector<VD> get_nullspace_sparse(vector<VI > coeffmatrix, string method, bool verb) {
	int n=coeffmatrix.size(), m=coeffmatrix[0].size();
	Eigen::SparseMatrix<complex<double> > CFM(n, m);
	/*Eigen::MatrixXd CFM(n, m);
	for(int i=0;i<n;i++) {
		for(int j=0;j<m;j++) CFM(i, j)=sqrt(coeffmatrix[i][j]/4.0);
	}
	
	if(verb) cout << "Finding kernel using sparse QR decomposition" << endl;
	Eigen::ColPivHouseholderQR<Eigen::MatrixXd> qr(CFM);
	Eigen::MatrixXd Q=qr.matrixQR();
	int dim=qr.dimensionOfKernel();
	if(dim<1) error("Kernel empty");
	if(verb) cout << "Kernel dimension: " << dim << endl;
	ret.resize(dim, VD(m));
	for(int i=0;i<dim;i++) {
		for(int j=0;j<m;j++) ret[i][j]=Q(j, n-i);
	}
	
	return ret;*/
}

bool eq_det(CF_determinant* d1, CF_determinant* d2) {
	if(d1->n_qp!=d2->n_qp) error("Comparing incompatible vectors");
	for(int i=0;i<d1->n_qp;i++) {
		if(d1->l[i]!=d2->l[i] || d1->m[i]!=d2->m[i]) return false;
	}
	return true;
}

bool insignificant(VD vec, int k, double threshold) {
	double max; int idx;
	findmax(vec, max, idx);
	if(abs(vec[k])/max<threshold) return true;
	return false;
}

vector<CF_determinant*> read_det_from_file(string filename, int n_qh, int n_qe, bool verb, bool& odd) {
	ifstream in(filename.c_str());
	if(!in) error("Opening file "+filename);
	int n_qp;
	in >> n_qp;
	int *l=new int(n_qp);
	int *m=new int(n_qp);
	vector<CF_determinant*> dets;
	bool determined_odd=false;
	
	while(in >> l[0]) {
		if(!determined_odd) {
			odd=l[0]%2;
			determined_odd=true;
		}
		else if((odd && !l[0]%2) || (!odd && l[0]%2)) error("Input l and m must all be odd or all even");
		if(!in) error("Format of file "+filename);
		in >> m[0];
		if((odd && !m[0]%2) || (!odd && m[0]%2)) error("Input l and m must all be odd or all even");
		for(int i=1;i<n_qp;i++) {
			if(!in) error("Format of file "+filename);
			in >> l[i];
			if((odd && !l[i]%2) || (!odd && l[i]%2)) error("Input l and m must all be odd or all even");
			if(!in) error("Format of file "+filename);
			in >> m[i];
			if((odd && !m[i]%2) || (!odd && m[i]%2)) error("Input l and m must all be odd or all even");
		}
		dets.push_back(new CF_determinant(n_qh+n_qe, n_qh, n_qe, l, m, odd, verb));
	}
	in.close();
	delete [] l; delete [] m;
	return dets;
}

VD concatenate_determinants(vector<CF_determinant*>& determinants, VI& labels, VI& lowering_coeffs, VD coeffs) {
	int n=determinants.size();
	if(labels.size()!=n) error("Labels vector different size than determinant vector");
	if(lowering_coeffs.size()!=n) error("Lowering coeffs vector different size than determinant vector");
	VD result_coeffs;
	for(int i=0;i<n;i++) {
		if(labels[i]>coeffs.size()) error("Label higher than # coeffs");
		result_coeffs.push_back(coeffs[labels[i]]*sqrt(lowering_coeffs[i]/4.0));
		
		for(int j=i+1;j<n;j++) {
			if(eq_det(determinants[i], determinants[j])) {
				if(labels[j]>coeffs.size()) error("Label higher than # coeffs");
				result_coeffs[i]+=(coeffs[labels[j]]*sqrt(lowering_coeffs[j]/4.0));
				lowering_coeffs.erase(lowering_coeffs.begin()+j);
				labels.erase(labels.begin()+j);
				determinants.erase(determinants.begin()+j);
				j--;
				n--;
			}
		}
	}
	return result_coeffs;
}
