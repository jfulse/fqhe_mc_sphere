/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_determinant.h"
#include "input.h"
#include "main_misc.h"
#include "misc.h"
#include "Eigen/Dense"
#include "Eigen/SparseCore"
#include "Eigen/SVD"
#include "CF_configuration.h"

#ifndef CF_STATES_H
#define CF_STATES_H

typedef std::vector<int> VI;
typedef std::vector<double> VD;
typedef std::vector<string> VS;
typedef std::vector<VI> VVI;

vector<CF_determinant*> create_determinants(VVI, int, int, VI, VI, bool, bool);
VVI create_Lz_combinations(int, int, VI, VI, int, bool&, int);
void check_levels(int, int, VI, VI);
string getLstr(int);
string getcoeffdiv4str(int);
string getcoeffdiv2str(int);
VVI sum_determinants(int, vector<CF_determinant*>&, VI&, VI&);
vector<Eigen::Triplet<double> > sum_determinants_triplets(int, vector<CF_determinant*>&, VI&, VI&);
vector<VD> get_nullspace(vector<VI >, string, bool);
vector<VD> get_nullspace_sparse(vector<VI >, string, bool);
bool eq_det(CF_determinant*, CF_determinant*);
bool insignificant(VD, int, double);
vector<CF_determinant*> read_det_from_file(string, int, int, bool, bool&);
VD concatenate_determinants(vector<CF_determinant*>&, VI&, VI&, VD);
void check_linear_independence(vector<vector<CF_determinant*> >&, vector<VD>&, bool);
bool check_invalid(VI, VVI, VI, VI, int, int, int);
void compute_Lplus(vector<CF_determinant*>, vector<VD>);

#endif
