/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "misc.h"

#ifndef CF_DETERMINANT_H
#define CF_DETERMINANT_H

typedef std::vector<int> VI;
typedef std::vector<VI> VVI;

class CF_determinant{

	private:
	int n_qh, n_qe;
	bool verb, GS, odd;
	bool raisedexists(int, int);
	bool loweredexists(int, int);

	public:
	int *l, *m, n_qp, Lz;
	
	vector<CF_determinant*> raise(vector<int>&);
	vector<CF_determinant*> lower(vector<int>&);
	void display();
	void print(ofstream&);
	void print_machine(ofstream&);
	void print_CF_levels(ofstream&, int, int);
	int Lplus_coeff_sq(int, int);
	int Lminus_coeff_sq(int, int);
	static int Lminus_coeff_sq_st(int, int);
	
	CF_determinant(int, int, int, bool verb_=false);
	CF_determinant(int, int, int, int*, int*, bool odd_=false, bool verb_=false);
	CF_determinant(VD, int, bool verb_=false);
	
	~CF_determinant() {
		delete [] l;
		delete [] m;
	}

};

#endif
