/**

 Copyright (C) 2016  Jørgen Fulsebakke
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

 Author: Jørgen Fulsebakke

**/
#include "CF_linear_combination.h"

int main(int argc, const char* argv[]) {
	
	try {
		clock_t starttime=clock();
		input *params=new input("_","");
		
		params->add_input("--particles,-Ne", "Number of electrons", "int");
		params->add_input("--CF-state-files,-f", "Files containing states to be combined", "VS");
		params->add_input("--coefficients,-c", "Coefficients for the linear combination", "VD");
		params->add_input("--coeff-file,-cf", "File containing coefficients for the linear combination", "string");
		params->add_input("--out-file,-o", "Output file", "string","CF_combined.dat");
		params->add_input("--quiet,-q", "Do not print extra information to screen", "bool");
		
		if(params->command_line_input(argc, argv)!=0) return 0;
		if(!params->check_integrity()) return 0;
		
		string outfile=params->getstring("-o");
		int Ne=-1;
		if(params->is_activated("-Ne")) Ne=params->getint("-Ne");
		bool verb=true, use_unit=false;
		if(params->is_flagged("-q")) verb=false;
		VS files=params->getVS("-f");
		int n_files=files.size();
		VD coeffs;
		if(params->is_activated("-c")) {
			coeffs=params->getVD("-c");
			if(params->is_activated("-cf")) warning("Provided both coefficients and file; ignoring the latter");
			if(coeffs.size()!=n_files) error("Provided "+to_string(n_files)+" states and "+to_string(coeffs.size())+" coefficients");
		}
		else if(params->is_activated("-cf")) {
			string filename=params->getstring("-cf");
			ifstream coeffin(filename);
			if(!coeffin) error("Could not open "+filename+" for reading");
			double tmp;
			while(coeffin >> tmp) coeffs.push_back(tmp);
			if(coeffs.size()!=n_files) error("Provided "+to_string(n_files)+" states and "+to_string(coeffs.size())+" coefficients");
			coeffin.close();
		}
		else {
			coeffs.resize(n_files);
			for(int i=0;i<n_files;i++) coeffs[i]=1.0;
			use_unit=true;
		}
		
		if(verb) {
			display_start(params, 60, "_", argv[0]);
			if(verb && use_unit) cout << "Using unit coefficients in combination" << endl << endl;
		}
		
		Ne=get_Ne(files[0], Ne);
		VVD state;
		
		for(int i=0;i<n_files;i++) {
			vector<VD> new_state=read_CF_state(files[i], Ne);
			merge_states(state, new_state, coeffs[i]);
		}
		
		ofstream out(outfile);
		if(!out) error("Could not open "+outfile+" for writing");
		out.precision(17);
		for(int i=0;i<state.size();i++) {
			if(abs(state[i][0])>1e-12) {
				for(int j=0;j<state[i].size();j++) out << state[i][j] << " ";
				out << endl;
			}
		}
		out.close();
		cout << "Result written to " << outfile << endl;
		
		cout.precision(3);
		if(verb) display_end(starttime, 60, "_");
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		cerr << "Run with '-h' for options" << endl; 
		return 1;
	}
	catch(...) {
		cerr << "UNKNOWN ERROR" << endl; 
		cerr << "Run with '-h' for options" << endl;
		return 1;
	}
}

int get_Ne(string filename, int Ne) {
	ifstream in(filename);
	if(!in) error("Could not open file "+filename);
	string line;
	getline(in, line);
	istringstream iss(line);
	VS elements{istream_iterator<string>{iss},istream_iterator<string>{}};
	int n=elements.size();
	if(!(n&1)) error("First line of "+filename+" has an even number of elements ("+to_string(n)+"); need coefficient, l's and m's for each CF determinant");
	if(Ne!=-1) {
		if((n-1)/2!=Ne) error("First line of "+filename+" corresponds to "+to_string((n-1)/2)+" particles; expected "+to_string(Ne));
	}
	else Ne=(n-1)/2;
	
	in.close();
	return Ne;
}

void merge_states(VVD& state, VVD new_state, double coeff) {
	for(int i=0;i<new_state.size();i++) {
		bool exists=false;
		for(int j=0;j<state.size();j++) {
			VD v1=vector<double>(new_state[i].begin()+1, new_state[i].end());
			VD v2=vector<double>(state[j].begin()+1, state[j].end());
			if(v1==v2) {
				exists=true;
				state[j][0]+=coeff*new_state[i][0];
				break;
			}
		}
		if(!exists) {
			state.push_back(new_state[i]);
			state[state.size()-1][0]*=coeff;
		}
	}
}

VVD read_CF_state(string filename, int& Ne) {
	ifstream in(filename);
	if(!in) error("Could not open file "+filename);
	in.precision(17);
	string line;
	int ctr=0;
	VVD state;
	
	while(getline(in, line)) {
		istringstream iss(line);
		VS elements{istream_iterator<string>{iss},istream_iterator<string>{}};
		int n=elements.size();
		if((n-1)/2!=Ne) error("Line "+to_string(ctr+1)+" of "+filename+" corresponds to "+to_string((n-1)/2)+" particles; expected "+to_string(Ne));
		VD configuration;
		configuration.reserve(n);
		transform(elements.begin(), elements.end(), back_inserter(configuration),[](string const& val) {return stod(val);});
		state.push_back(configuration);
	}
	
	in.close();
	return state;
}

