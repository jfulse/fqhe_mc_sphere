.. _CF_states:

***********************
Program: CF_states
***********************

This program generates configuration files for excited composite fermion states, for different configurations of quasiholes and quasielectrons and different
values of the quantum numbers :math:`M` and :math:`L` of the operators :math:`L_z` and :math:`L^2` (z-component and total relative angular momentum respectively).
These files can be used with :ref:`Run_MC_sphere`.

===============
Input options
===============


====================================== ==============================================================================================================================================================================
Input                                     Description
====================================== ==============================================================================================================================================================================
:code:`-n`                              Number of occupied CF Landau levels in the ground state.
:code:`--twice-flux` or :code:`-2q`     Twice the effective CF flux. The lowest CF Landau level has :math:`2q+1` states.
:code:`--twice-L` or :code:`-2L`        Twice the quantum number :math:`L` of total angular momentum.
:code:`--twice-Lz` or :code:`-2Lz`      Twice the quantum number :math:`M` of the z-component of angular momentum.
:code:`--qh-LLs` or :code:`-qh`         The CF Landau levels in which to insert quasiholes (counting from 0).
:code:`--qe-LLs` or :code:`-qe`         The CF Landau levels in which to insert quasielectrons (counting from 0).
:code:`--out-prefix` or :code:`-op`     Prefix for the files containing the result states (compatible with "Run_MC_sphere").
:code:`--Lz-prefix` or :code:`-Lzp`     When activated all the :math:`L_z`-states are written to files compatible with "Run_MC_sphere" using this prefix.
:code:`--det-file-out` or :code:`-do`   When activated all the :math:`L_z`-states are written to this file; compatible with this program ("CF_states").
:code:`--det-file-in` or :code:`-di`    When activated all the :math:`L_z`-states are read from this file.
:code:`--kernel-method`                 Elects what matrix decomposition is used to find the kernel giving the :math:`L`-states, choosing from sparse QR, QR, SVD and LU. *Note*: LU can be numerically unstable.
:code:`--test-kernel`                   When activated a check is performed to test that the kernel states are actually zero.
:code:`--Lz-only`                       Programs stops running after finding the :math:`L_z`-states.            
:code:`--verbose` or :code:`-v`         Print more information than usual to the screen.
:code:`--quiet` or :code:`-q`           Print less information than usual to the screen.
====================================== ==============================================================================================================================================================================



===============
Example usage
===============

""""""""""""""""""""""""""""""""""""""""""""""""""
Example 1: :math:`\nu=2/5` with a single exciton
""""""""""""""""""""""""""""""""""""""""""""""""""

Our starting point is :math:`\nu=2/5`, i.e. two CF Landau levels filled: :math:`n=2`. Note that :math:`n=2` also corresponds to other states; increasing the
number of attached flux on the electrons from :math:`2p=2` to :math:`2p=4,6,\ldots` gives :math:`\nu=3/7,4/9,\ldots`, while reverse flux composite fermions
at :math:`n=2` has a corresponding series :math:`\nu=2/3,3/5,4/7,\ldots`.

If we want e.g. eight electrons there will be three composite fermions in
the lowest CF Landau level and five in the first, which corresponds to effective flux :math:`2q=2`. This can also be seen from the formula
:math:`2q=N_e/n-n=8/2-2=2`.

Then we excite a single composite fermion, which is modelled using a quasihole and a quasielectron. If the quasihole is in the first Landau level (counting 
from zero) and the quasielectron is in the second, we use :code:`--qh-LLs 1 --qe-LLs 2`. An example :math:`L_z`-state from this configuration can be 
diagrammatically represented as: ::

	_ _ _ _ x _ _
	  x x x _ x
	    x x x

Finally lets assume we want to have a total relative angular momentum 
quantum number :math:`L=2` with z-component :math:`L_z=0` and save the results in files starting with "nu_2_5_exciton." We can then run: ::

	CF_states -n 2 -2q 2 -2L 4 -2Lz 0 -qh 1 -qe 2 -op nu_2_5_exciton
	
It turns out there is only one state with these criteria, which is then saved in "nu_2_5_exciton.dat" and looks like this: ::

	0.37796447300922736  0 -1 0 0 0 1 1 -2 1 0 1 1 1 2 2 -1 
	-0.59761430466719667  0 -1 0 0 0 1 1 -1 1 0 1 1 1 2 2 -2 
	-0.37796447300922753  0 -1 0 0 0 1 1 -2 1 -1 1 0 1 2 2 1 
	0.59761430466719678  0 -1 0 0 0 1 1 -2 1 -1 1 0 1 1 2 2

Every line corresponds to an :math:`L_z`-state, where the first number gives the coefficient in the superposition of the :math:`L`-state. After that the CF LL and
momentum for the individual composite fermions are given alternatingly. Let's check that the first state (first line) has the correct total z-component, which
should be zero. We skip the superposition coefficient and then count every other integer: :math:`L_z=-1+0+1-2+0+1+2-1=0`.

