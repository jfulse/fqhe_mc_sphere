*********************
Installation guide
*********************

Following are install instructions on Ubuntu. For other Linux distributions or Mac substitute :code:`apt-get` 
for the appropriate package manager; on Windows the easiest option is to use a Linux-like environment, 
e.g. `Cygwin <https://www.cygwin.com/>`_.

------------------------------

.. _Ubuntu_Instructions:

=======================
Ubuntu instructions
=======================

^^^^^^^^^^^^^^^^^^^^^^^^^^^
Prerequisites:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

git (version control system)

c++ compiler that supports c++11

gsl (c++ library)

hdf5 (Fileformat library)

""""""""""""""""""""""""""""
To install these:
""""""""""""""""""""""""""""
::

    sudo apt-get install git

    sudo apt-get install g++

    sudo apt-get install libgsl0-dev

    sudo apt-get install libhdf5-dev

------------------------------------------------------

^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Setup:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

"""""""""""""""""""""""
Download project: 
"""""""""""""""""""""""
::

    git clone https://jfulse@bitbucket.org/jfulse/fqhe_mc_sphere.git

To download to a different folder "folderpath/": ::

    git clone https://jfulse@bitbucket.org/jfulse/fqhe_mc_sphere.git folderpath/

"""""""""""""""""""""""
Compilation:
"""""""""""""""""""""""

Go to the project folder (usually "fqhe_mc_sphere/" from where project was cloned, or "folderpath/" in example above) ::

    cd fqhe_mc_sphere

Compile: ::

    make

to use several cores use -j #. Example with 4 cores: ::

    make -j 4

---------------------------------------------

^^^^^^^^^^^^^^
Post install:
^^^^^^^^^^^^^^

"""""""""""""""""""""""""""
Updating programs:
"""""""""""""""""""""""""""

To get the newest version of the programs, go into the project directory and enter ::

    git pull origin master

The programs then have to be rebuilt: ::

    make

"""""""""""""""""""""""""""""""""""""
Running programs from anywhere:
"""""""""""""""""""""""""""""""""""""

To add the programs to the system path, use :code:`PATH=$PATH:[path_to_programs]`; e.g. ::

    PATH=$PATH:~/fqhe_mc_sphere/

To make this permanent: ::

    echo "PATH=$PATH:~/fqhe_mc_sphere/" >> ~/.bashrc

