.. FQHE_MC_sphere documentation master file, created by
   sphinx-quickstart on Wed Apr  6 20:55:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FQHE MC sphere
==========================================

This page contains documentation for `FQHE_MC_sphere <https://bitbucket.org/jfulse/fqhe_mc_sphere>`_, a set of programs built for various Monte Carlo 
computations for the `fractional quantum Hall effect <https://en.wikipedia.org/wiki/Fractional_quantum_Hall_effect>`_ on the sphere.

Send comments, requests and bug reports to fqhe.mc.sphere@gmail.com.

Contents:

.. toctree::
   :maxdepth: 2

   Installation
   QuickIntro
   Run_MC_sphere
   CF_states

* :ref:`genindex`
* :ref:`search`
