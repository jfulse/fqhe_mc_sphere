.. _Run_MC_sphere:

***********************
Program: Run_MC_sphere
***********************

The main purpose of this program is to generate Monte Carlo configurations according to a probability distribution and evaluate a FQHE trial wavefunction on these
configurations. The probability distribution may be the absolute squared of the trial wavefunction itself or of the Laughlin trial wavefunction at a given 
filling fraction 1/m with integer m. The program can also evaluate a chose trial wavefunction on preexisting configurations.

===============
Input options
===============


====================================== ===================================================================================================================================
Input                                     Description
====================================== ===================================================================================================================================
\                                          **Wavefunction options**
:code:`-Ne`                             Gives the number of electrons.
:code:`--wavefunction` or :code:`-wf`   Wavefunction to evaluate. The options are: L (Laughlin), CF (composite fermions), BS (Bonderson-Slingerland) and MR (Moore-Read). 
:code:`--mod-factor` or :code:`-d`      Nonzero d gives an overall modification factor :math:`\prod_{i<j}|z_i-z_j|^{2d}`. d can be any floating number but negative values can lead to singular wavefunctions.
:code:`--L:m` or :code:`-m`             Laughlin WF parameter; power of slater determinant (filling factor nu=1/m).
:code:`--CF:n` or :code:`-n`            CF WF ground state parameter; number of filled CF Landau levels.
:code:`--CF:2p` or :code:`-2p`          CF WF parameter; number of flux attached in the form :math:`\prod_{i<j}(z_i-z_j)^{2p}`. Must be positive.
:code:`--CF:twice-q` or :code:`-2q`     CF WF excited state parameter; gives the number of effective CF flux. There are 2q+1 states in the lowest CF Landau level (note that this parameter is positive also when using reverse flux CF).
:code:`--CF:conf-file` or :code:`-c`    CF WF excited state parameter; configuration file giving the occupation of CF orbitals in the determinant. Each line corresponds to a CF determinant in the superposition and has the format *"Coefficient n_1 m_1 n_2 m_2 ... n_N m_N"* where n (integer from 0) is CF Landau level and m (half integer between -q and q) is Lz quantum number.
:code:`--CF:reverse` or :code:`-R`      CF WF parameter; toggles reverse flux composite fermions.
:code:`--CF:unproj` or :code:`-UP`       CF WF parameter; turns of Jain-Kamilla projection.
:code:`--MR:p`                          Moore-Read WF parameter; power of Slater determinant (filling factor nu=1/p).
:code:`--MR:no-phase`                   Moore-Read WF parameter; wavefunction will have a random phase (faster computation).
:code:`--MR:conf-file` or :code:`cPf`   Moore-Read WF excited state parameter; two files with configurations for Ne/2 particles each (same format as :code:`--CF:conf-file`).
:code:`--MR:Nphi`                       Moore-Read WF excited state parameter; total magnetic flux of the state.
:code:`--MR-unsym` or :code:`-U`        Moore-Read WF parameter; unsymmetrised version.
:code:`--MR-d`                          Moore-Read WF parameter; gives modification factor on groups of Ne/2 Laughlin wavefunctions to be symmetrised.
:code:`--BS:p`                          Bonderson-Slingerland WF parameter; power of Slater determinant (in addition to that of the CF sector).
\                                         **Monte Carlo options**
:code:`--Laughlin-P` or :code:`-lp`     Use Laughlin wavefunction absolute squared as probability distribution instead of that of the sampled wavefunction. The parameter :code:`--L:m` then pertains to the probability.
:code:`--existing-pos` or :code:`-X`   Use existing configurations and probability values on which to evaluate wavefunction instead of generating configurations.
:code:`--no-WF-input` or :code:`-nWF`   Used with :code:`--existing-pos`; no probability value inputs (assumes that the probability and wavefunction to evaluate are the same).
:code:`--thermalise` or :code:`-th`     Number of Monte Carlo iterations to use for thermalisation.
:code:`--delay` or :code:`-dl`          Number of Monte Carlo iterations in terms of Ne between sampling.
:code:`--samples` or :code:`-N`         Number of Monte Carlo samples to evaluate.
:code:`--step` or :code:`-st`           Steplength in terms of 1/R. Ideally the steplength should correspond to an acceptance ratio ~50%.
:code:`--wf-outfile` or :code:`-wo`     File in which to write probability and wavefunction values  in log format (hdf5 format).
:code:`--pos-outfile` or :code:`-po`    File in which to write configurations in terms of theta and phi angles (hdf5 format).
:code:`--wf-infile` or :code:`-wi`      File from which to read probability and wavefunction values (hdf5 format); used with :code:`--existing-pos`.
:code:`--pos-infile` or :code:`-pi`     File form which to read configurations in terms of theta and phi angles (hdf5 format); used with :code:`--existing-pos`.
:code:`--seed` or :code:`-sd`           Integer to add to computer time for use as random seed.
:code:`--force-seed`                    Integer to use as random seed.
:code:`--no-stab`                       Do not attempt to stabilise steplength to an acceptance ratio of ~50%.
:code:`--skip-samples`                  Number of initial samples and configurations to skip when reading from file(s); used with :code:`--existing-pos`.
:code:`--init-conf`                     File containing initial electron configuration (ascii format) in the format *"theta_1 phi_1 theta_2 phi_2 ... theta_N phi_N"*.
:code:`--quiet` or :code:`-q`           Print less information to the screen.
====================================== ===================================================================================================================================



===============
Example usage
===============

""""""""""""""""""""""""""""""""
Example 1: Laughlin samples
""""""""""""""""""""""""""""""""

Let's say we want to generate :math:`10^6` samples of the Laughlin wavefunction with power :math:`m=5`, i.e. at filling factor :math:`\nu=1/3`, for :math:`N_e=10` 
particles. We want to save the wavefunction values in "WF_values.hdf5" and the configurations in "configurations.hdf5." The number of MC iterations used 
for thermalisation should be :math:`10^6` and the wavefunction should be sampled every :math:`4N_e`. We then run the following: ::

	Run_MC_sphere --wavefunction L -N 1000000 --L:m 5 -Ne 10 --wf-outfile WF_values.hdf5 --pos-outfile configurations.hdf5 --thermalise 1000000 --delay 4
	
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Example 2: Reverse flux composite fermion ground state samples
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Composite fermion wavefunctions are obtained using :code:`--wavefunction CF`; to get reverse flux states we use :code:`--CF:reverse`. If we for example want 
:math:`10^5` samples with :math:`N_e=9` electrons and :math:`n=3` CF Landau levels filled, and attach :math:`2p=4` flux to each electron, corresponding 
to a filling factor :math:`\nu=3/(4\cdot 3-1)=3/11`, we run ::

	Run_MC_sphere --wavefunction CF --CF:reverse -N 100000 -Ne 9 --CF:n 3 --CF:2p 4

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Example 3: Excited composite fermion states: Evaluated on preexisting configurations using several cores
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

First we need some configurations and probability samples to evaluate the states on. This is done either because the target state takes long to evaluate, in
which case it makes sense to do only evaluation and not thermalisation and relaxation (delay) with the state in question but a faster one; or because we want
to compute some observable in which several states *must* be evaluated on the same configurations (e.g. overlap between states).

For composite fermion states it is not a good idea to use configurations generated with the Laughlin :math:`\nu=1/3` state because this excludes correlations 
where the relative angular momentum between electrons is less than :math:`l=3`, which can happen in the CF states. Therefore we use Laughlin :math:`\nu=1`, which 
includes correlations with as low relative angular momentum as is allowed for fermions.

Since we want to use several cores in parallell we generate several configuration and probability value files. If we want for example five sets of files with
:math:`10^4` samples in each at :math:`N_e=8` electrons we can run, utilising bash commands (see :ref:`Handy_bash_commands`): ::

	for i in $(seq 0 4); do Run_MC_sphere --wavefunction L --L:m 1 --seed $i -Ne 8 -N 10000 --wf-outfile nu1_wf_${i}.hdf5 --pos-outfile nu1_pos_${i}.hdf5 & done
	
**Important**: Note the :code:`--seed $i` in the code above. In general the random seed is taken from the computer time; :code:`--seed` adds a different integer to the 
seed of each of the runs -- otherwise we would end up with five copies of the same configurations.

Also note that the probability values and configurations are saved in different files for the different runs, indexed using the running integer with :code:`${i}`.

Now that we have the samples to evaluate the states on we need configuration files for the composite fermion excitations. These can be obtained using the program
"CF_states" (see ...) or written manually. Each line in the file corresponds to an :math:`L_z` CF determinant; the first number gives the coefficient in the
superposition and the subsequent ones give CF Landau level and angular momentum alternatingly for each of the :math:`N_e` composite fermions.

As an example let's assume we want the :math:`\nu=2/5` state with two excitons (i.e. two quasiholes and two quasielectrons) at total angular momentum quantum 
number :math:`L=7` and total z-component angular momentum quantum number also :math:`M=7`. This gives the following excitation configuration file "State.dat": ::

	-0.61237243569579447  0 -1 0 0 0 1 1 0 1 1 1 2 2 1 2 3 
	0.79056941504209488  0 -1 0 0 0 1 1 -1 1 1 1 2 2 2 2 3

To sum up we have a excitation configuration file "State.dat", position files "nu1_wf_0.hdf5", ... ,"nu1_wf_4.hdf5" and probability files "nu1_wf_0.hdf5",
... ,"nu1_wf_4.hdf5." Now we want to evaluate the excited state on the position and probability values. The effective CF flux for :math:`\nu=2/5` is :math:`2q=2`.
Using bash commands we can run ::

	for i in `seq 0 4`; do Run_MC_sphere --wavefunction CF -Ne 8 --CF:twice-q 2 --CF:conf-file State.dat --existing-pos -N 10000 --wf-infile nu1_wf_${i}.hdf5 --pos-infile nu1_pos_${i}.hdf5 --wf-outfile nu_2_5_L_7_Lz_7_2_excitons_${i}.hdf5 & done

The resulting wavefunction files are then saved in "nu_2_5_L_7_Lz_7_2_excitons_0.hdf5" etc. To compute e.g. the energy of the excited state using all 
:math:`5\times10^4` samples and 1000 samples in each error bin we can use the program "Analyse_MC" (see ...): ::

	Analyse_MC --observable energy -N 50000 --bin-size 1000 -Ne 8 -Nphi 16 --out-file nu_2_5_L_7_Lz_7_2_excitons_energy.dat --pos-input $(for i in `seq 0 4`; do echo nu1_pos_${i}.hdf5" "; done) --wf-input $(for i in `seq 0 4`; do echo nu_2_5_L_7_Lz_7_2_excitons_${i}.hdf5" "; done)


